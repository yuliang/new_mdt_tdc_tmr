/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : trigger_mode_sim.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-05-23 16:28:51
//  Note       : 
//     
module trigger_mode_sim(
	input clk160);
reg [11:0] trigger_position;
reg [11:0] event_id,bunch_id;
task bunch_id_test();
	begin
		$fdisplay(testbench_inst.logfile,"%t Bunch ID test start========================",$time);
		$display("%t Bunch ID test start========================",$time);
		testbench_inst.data_decoder_inst.trigger=1'b1;
		testbench_inst.data_decoder_inst.length=3'd4;
		jtag_test_interface_inst.TRST_1;
  		jtag_test_interface_inst.TRST_0;
  		#200
  		jtag_test_interface_inst.TRST_1;
  		#200
  		testbench_inst.jtag_test_interface_inst.JTAG_config(
			115'b1000010100101111111111111111111111111111111111111111111111111111111111111111111111111111010101010101010010000010000,
			9'd114,
			5'h12
		);
		@(negedge clk160) testbench_inst.reset = 1'b1;
  		#200
  		@(negedge clk160) testbench_inst.reset = 1'b0;
  		#500  
  		testbench_inst.ttc_generator_sim_inst.new_ttc_mode=1'b1;
  		testbench_inst.ttc_generator_sim_inst.new_ttc_code=4'b1011;
  		@(negedge clk160) testbench_inst.ttc_generator_sim_inst.new_ttc_start = 1'b1;
  		@(negedge clk160) testbench_inst.ttc_generator_sim_inst.new_ttc_start = 1'b0;
  		#200;
  		testbench_inst.ttc_generator_sim_inst.new_ttc_code=4'b1000;
  		@(negedge clk160) testbench_inst.ttc_generator_sim_inst.new_ttc_start = 1'b1;
		@(posedge clk160) trigger_position = testbench_inst.triggless_mode_sim_inst.coarse_ref.coarse_time_0[14:3]  ;
		@(negedge clk160) testbench_inst.ttc_generator_sim_inst.new_ttc_start = 1'b0;
		@(posedge testbench_inst.output_enable);
		@(negedge clk160) begin
			event_id = testbench_inst.data_out[23:12];
			bunch_id = testbench_inst.data_out[11:0];
		end
		@(posedge clk160) 
		$fdisplay(testbench_inst.logfile,"%t event id: %0d, bunch id: %0d",$time,event_id,bunch_id);
		$display("%t event id: %0d, bunch id: %0d",$time,event_id,bunch_id);
		if((bunch_id-trigger_position)==12'hf9f) begin end
		else begin
			$fdisplay(testbench_inst.logfile,"%t Error trigger bunch offset bunch_id, %d  trigger_position %d",$time,bunch_id,trigger_position);
			$display("%t Error trigger bunch offset bunch_id, %d  trigger_position %d",$time,bunch_id,trigger_position);
		end
		#200
  		testbench_inst.jtag_test_interface_inst.JTAG_config(
			94'b0000101000000100000000111111111111111111111111000000000000111110011101000000000000000000011111,
			9'd93,
			5'h03
		);
		@(negedge clk160) testbench_inst.reset = 1'b1;
  		#200
  		@(negedge clk160) testbench_inst.reset = 1'b0;
  		#500  
		testbench_inst.ttc_generator_sim_inst.new_ttc_mode=1'b1;
  		testbench_inst.ttc_generator_sim_inst.new_ttc_code=4'b1011;
  		@(negedge clk160) testbench_inst.ttc_generator_sim_inst.new_ttc_start = 1'b1;
  		@(negedge clk160) testbench_inst.ttc_generator_sim_inst.new_ttc_start = 1'b0;
  		#200;
  		testbench_inst.ttc_generator_sim_inst.new_ttc_code=4'b1000;
  		@(negedge clk160) testbench_inst.ttc_generator_sim_inst.new_ttc_start = 1'b1;
		@(posedge clk160) trigger_position = testbench_inst.triggless_mode_sim_inst.coarse_ref.coarse_time_0[14:3]  ;
		@(negedge clk160) testbench_inst.ttc_generator_sim_inst.new_ttc_start = 1'b0;
		@(posedge testbench_inst.output_enable);
		@(negedge clk160) begin
			event_id = testbench_inst.data_out[23:12];
			bunch_id = testbench_inst.data_out[11:0];
		end
		@(posedge clk160) 
		$fdisplay(testbench_inst.logfile,"%t event id: %0d, bunch id: %0d",$time,event_id,bunch_id);
		$display("%t event id: %0d, bunch id: %0d",$time,event_id,bunch_id);
		if((bunch_id-trigger_position)==12'hfa0) begin end
		else begin
			$fdisplay(testbench_inst.logfile,"%t Error trigger bunch offset bunch_id, %d  trigger_position %d",$time,bunch_id,trigger_position);
			$display("%t Error trigger bunch offset bunch_id, %d  trigger_position %d",$time,bunch_id,trigger_position);
		end
		$fdisplay(testbench_inst.logfile,"%t Bunch ID test finish========================",$time);
		$display("%t Bunch ID test finish========================",$time);
	end
endtask
reg [4:0] channel_ID;
reg [16:0] leading_edge;
reg [7:0] width;
reg [9:0] hit_counter;
task trigger_matching_test();
	begin
		$fdisplay(testbench_inst.logfile,"%t Trigger matching test start========================",$time);
		$display("%t Trigger matching test start========================",$time);
		testbench_inst.data_decoder_inst.trigger=1'b1;
		testbench_inst.data_decoder_inst.length=3'd4;
		jtag_test_interface_inst.TRST_1;
  		jtag_test_interface_inst.TRST_0;
  		#200
  		jtag_test_interface_inst.TRST_1;
  		#200
  		testbench_inst.jtag_test_interface_inst.JTAG_config(
			115'b1000010100101111111111111111111111111111111111111111111111111111111111111111111111111111010101010101010010000010000,
			9'd114,
			5'h12
		);
		@(negedge clk160) testbench_inst.reset = 1'b1;
  		#200
  		@(negedge clk160) testbench_inst.reset = 1'b0;
  		#500 
  		testbench_inst.ttc_generator_sim_inst.new_ttc_mode=1'b1;
  		testbench_inst.ttc_generator_sim_inst.new_ttc_code=4'b1011;
  		@(negedge clk160) testbench_inst.ttc_generator_sim_inst.new_ttc_start = 1'b1;
  		@(negedge clk160) testbench_inst.ttc_generator_sim_inst.new_ttc_start = 1'b0;
  		triggless_mode_sim_inst.single_hit_gen(0,0,100);
  		#500
  		triggless_mode_sim_inst.multi_hit_gen(0,24'h800001,100);
  		#500
  		triggless_mode_sim_inst.single_hit_gen(0,0,100);
  		#2000;
  		testbench_inst.ttc_generator_sim_inst.new_ttc_code=4'b1000;
  		@(negedge clk160) testbench_inst.ttc_generator_sim_inst.new_ttc_start = 1'b1;
		@(posedge clk160) trigger_position = testbench_inst.triggless_mode_sim_inst.coarse_ref.coarse_time_0[14:3]  ;
		@(negedge clk160) testbench_inst.ttc_generator_sim_inst.new_ttc_start = 1'b0;
		@(posedge testbench_inst.output_enable);
		@(negedge clk160) begin
			event_id = testbench_inst.data_out[23:12];
			bunch_id = testbench_inst.data_out[11:0];
		end
		@(posedge clk160) ;
		$fdisplay(testbench_inst.logfile,"%t Info: event id: %0d, bunch id: 0x%h",$time,event_id,bunch_id);
		$display("%t Info: event id: %0d, bunch id: 0x%h",$time,event_id,bunch_id);
		@(posedge testbench_inst.output_enable);
		@(negedge clk160) begin
			channel_ID = testbench_inst.data_out[31:27];
			leading_edge = testbench_inst.data_out[24:8];
			width = testbench_inst.data_out[7:0];
			$fdisplay(testbench_inst.logfile,"%t Info: Channel %0d, leading edge 0x%h (bunch id 0x%h,coarse time 0x%h, fine time 0x%h), width %0d, indicate b%b",$time,channel_ID,leading_edge,testbench_inst.data_out[24:13],testbench_inst.data_out[24:10],testbench_inst.data_out[9:8],testbench_inst.data_out[7:0],testbench_inst.data_out[26:25]);
			$display("%t Info: Channel %0d, leading edge 0x%h (bunch id 0x%h,coarse time 0x%h, fine time 0x%h), width %0d, indicate b%b",$time,channel_ID,leading_edge,testbench_inst.data_out[24:13],testbench_inst.data_out[24:10],testbench_inst.data_out[9:8],testbench_inst.data_out[7:0],testbench_inst.data_out[26:25]);
		end
		@(posedge testbench_inst.output_enable);
		@(negedge clk160) begin
			channel_ID = testbench_inst.data_out[31:27];
			leading_edge = testbench_inst.data_out[24:8];
			width = testbench_inst.data_out[7:0];
			$fdisplay(testbench_inst.logfile,"%t Info: Channel %0d, leading edge 0x%h (bunch id 0x%h,coarse time 0x%h, fine time 0x%h), width %0d, indicate b%b",$time,channel_ID,leading_edge,testbench_inst.data_out[24:13],testbench_inst.data_out[24:10],testbench_inst.data_out[9:8],testbench_inst.data_out[7:0],testbench_inst.data_out[26:25]);
			$display("%t Info: Channel %0d, leading edge 0x%h (bunch id 0x%h,coarse time 0x%h, fine time 0x%h), width %0d, indicate b%b",$time,channel_ID,leading_edge,testbench_inst.data_out[24:13],testbench_inst.data_out[24:10],testbench_inst.data_out[9:8],testbench_inst.data_out[7:0],testbench_inst.data_out[26:25]);
		end
		@(posedge clk160) ;
		@(posedge testbench_inst.output_enable);
		@(negedge clk160) begin
			hit_counter = testbench_inst.data_out[9:0];
			$fdisplay(testbench_inst.logfile,"%t Info: HIt counter %0d========================",$time,hit_counter);
			$display("%t Info: HIt counter %0d========================",$time,hit_counter);
		end
		@(posedge clk160) ;
	end
endtask
endmodule
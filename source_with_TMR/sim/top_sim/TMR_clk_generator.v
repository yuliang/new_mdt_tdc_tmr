/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : TMR_clk_generator.v
//  Author     : Yuxiang Guo
//  Revision   : 
//               First created on March 22, 2019
//  Note       : 
//     
`timescale 1 ns / 1 fs


module TMR_clk_generator(
  input enable320,
	output reg clk,
	output reg clk_A,
	output reg clk_B,
	output reg clk_C,
	output check_enable
);

reg check_enable_r = 1'b1;
assign  check_enable = check_enable_r;

reg output_check;

always begin
  if(enable320) begin
      clk <= 1'b1;
      clk_A <= 1'b1;
      clk_B <= 1'b1;
      clk_C <= 1'b1;
      #1.5625;
      clk <= 1'b0;
      clk_A <= 1'b0;
      clk_B <= 1'b0;
      clk_C <= 1'b0;
      #1.5625;
  end else begin
      clk <= 1'b1;
      clk_A <= 1'b1;
      clk_B <= 1'b1;
      clk_C <= 1'b1;
      #3.125;
      clk <= 1'b0;
      clk_A <= 1'b0;
      clk_B <= 1'b0;
      clk_C <= 1'b0;
      #3.125;
  end
end






task force_clk_fre(
    input clk_A_force,
    input clk_B_force,
    input clk_C_force,
    input real half_period,
    input integer force_cycle);
begin
    check_enable_r = 1'b0;
    repeat(force_cycle)begin
        if(clk_A_force) begin if(clk_A)force clk_A = 1'b0;else force clk_A = 1'b1; $display("At%t clk_A force to %d",$realtime,clk_A);end
        if(clk_B_force) begin if(clk_B)force clk_B = 1'b0;else force clk_B = 1'b1; $display("At%t clk_B force to %d",$realtime,clk_B);end
        if(clk_C_force) begin if(clk_C)force clk_C = 1'b0;else force clk_C = 1'b1; $display("At%t clk_C force to %d",$realtime,clk_C);end
        #half_period;
        if(clk_A_force) begin if(clk_A)force clk_A = 1'b0;else force clk_A = 1'b1; $display("At%t clk_A force to %d",$realtime,clk_A);end
        if(clk_B_force) begin if(clk_B)force clk_B = 1'b0;else force clk_B = 1'b1; $display("At%t clk_B force to %d",$realtime,clk_B);end
        if(clk_C_force) begin if(clk_C)force clk_C = 1'b0;else force clk_C = 1'b1; $display("At%t clk_C force to %d",$realtime,clk_C);end
        #half_period;
    end
    if(clk_A_force) begin release clk_A; $display("At%t clk_A released",$realtime); end
    if(clk_B_force) begin release clk_B; $display("At%t clk_B released",$realtime); end
    if(clk_C_force) begin release clk_C; $display("At%t clk_C released",$realtime); end 

end
endtask

task force_clk(
    input clk_A_force,
    input clk_B_force,
    input clk_C_force,
    input real force_time);
begin
    check_enable_r = 1'b0;
    if(clk_A_force) begin if(clk_A)force clk_A = 1'b0;else force clk_A = 1'b1; $display("At%t clk_A force to %d",$realtime,clk_A);end
    if(clk_B_force) begin if(clk_B)force clk_B = 1'b0;else force clk_B = 1'b1; $display("At%t clk_B force to %d",$realtime,clk_B);end
    if(clk_C_force) begin if(clk_C)force clk_C = 1'b0;else force clk_C = 1'b1; $display("At%t clk_C force to %d",$realtime,clk_C);end
    #force_time;
    // @(posedge clk)
    if(clk_A_force) begin release clk_A; $display("At%t clk_A released",$realtime); end
    if(clk_B_force) begin release clk_B; $display("At%t clk_B released",$realtime); end
    if(clk_C_force) begin release clk_C; $display("At%t clk_C released",$realtime); end   
  
end
endtask

task clock_pulse_generator(
  input clk_A_force,
  input clk_B_force,
  input clk_C_force, 
  input real delay_time, //delay_time+pulse_time < clk/2
  input real pulse_width
  );
begin
  @(posedge clk or negedge clk);  
  #delay_time; //pulse followed by a normal edge and has a delay
  force_clk(clk_A_force,clk_B_force,clk_C_force,pulse_width);
  force_clk(clk_A_force,clk_B_force,clk_C_force,0);
end
endtask

task two_clock_pulse_generator(
  input clk_A_force,
  input clk_B_force,
  input clk_C_force, 
  input real delay_time, //delay_time+pulse_time < clk/2
  input real pulse_width
  );
begin
  @(posedge clk or negedge clk);
  #delay_time;
  force_clk(clk_A_force,clk_B_force,clk_C_force,pulse_width);
  force_clk(clk_A_force,clk_B_force,clk_C_force,0);
  #delay_time;
  force_clk(clk_A_force,clk_B_force,clk_C_force,pulse_width);
  force_clk(clk_A_force,clk_B_force,clk_C_force,0);
end
endtask


task single_clock_TMR_test(
  input enable_A,
  input enable_B,
  input enable_C,
  input integer repeat_number);
  real delay_time;
  real pulse_width;
  real random_time;
begin
    
    //for one SEU
    repeat(repeat_number)begin  //half clk cycle
        #($itor($urandom_range(10000,1))/100);//randomly wait for 0.01 to 100 ns
        if(enable320)force_clk(enable_A,enable_B,enable_C,1.5625);
        else force_clk(enable_A,enable_B,enable_C,1.5625*2);
        check_enable_r = 1'b1;
    end
    repeat(repeat_number)begin  //one clk cycle
        #($itor($urandom_range(10000,1))/100);//randomly wait for 0.01 to 100 ns
        if(enable320)force_clk(enable_A,enable_B,enable_C,1.5625*2);
        else force_clk(enable_A,enable_B,enable_C,1.5625*4);
        check_enable_r = 1'b1;
    end
    repeat(repeat_number)begin//long time
        #($itor($urandom_range(10000,1))/100);//randomly wait for 0.01 to 100 ns
        random_time = $itor($urandom_range(10000,1))/100;
        force_clk(enable_A,enable_B,enable_C,random_time);
        check_enable_r = 1'b1;
    end


    repeat(10)begin//for random period
        if(enable320)random_time = $itor($urandom_range(1000,1))/1000*1.5625/2*2+1.5625/2; //0.5~1.5 clk320
        else random_time = $itor($urandom_range(1000,1))/1000*1.5625*2+1.5625; //0.5~1.5 clk160
        force_clk_fre(enable_A,enable_B,enable_C,random_time,repeat_number);
        check_enable_r = 1'b1;
    end

    

    

    repeat(repeat_number)begin //for one glitch
        #($itor($urandom_range(10000,1))/100);//randomly wait for 0.01 to 100 ns
        delay_time = $urandom_range(10000,1);
        if(enable320)delay_time = delay_time/10000*1.5625/2;
        else delay_time = delay_time/10000*1.5625;        
        pulse_width = $urandom_range(10000,1);
        if(enable320)pulse_width = pulse_width/10000*1.5625/2;
        else pulse_width = pulse_width/10000*1.5625;
        clock_pulse_generator(enable_A,enable_B,enable_C,delay_time,pulse_width);
        $display("At%t glitch occurs",$realtime);
        check_enable_r = 1'b1;
    end
    
    repeat(repeat_number)begin //for double glitch
        #($itor($urandom_range(10000,1))/100);//randomly wait for 0.01 to 100 ns
        delay_time = $urandom_range(10000,1);
        if(enable320)delay_time = delay_time/10000*1.5625/4;
        else delay_time = delay_time/10000*1.5625/2;
        pulse_width = $urandom_range(10000,1);
        if(enable320)pulse_width = pulse_width/10000*1.5625/4;
        else pulse_width = pulse_width/10000*1.5625/2;
        two_clock_pulse_generator(enable_A,enable_B,enable_C,delay_time,pulse_width);
        $display("At%t 2 glitches occur",$realtime);
        check_enable_r = 1'b1;
    end
end
endtask

task clock_TMR_test(
  input integer repeat_number);
begin
    single_clock_TMR_test(1,0,0,repeat_number);
    @(posedge clk); //incase clk SEU doesn't occur in the same cycle
    single_clock_TMR_test(0,1,0,repeat_number);
    @(posedge clk); //incase clk SEU doesn't occur in the same cycle
    single_clock_TMR_test(0,0,1,repeat_number);
    #100;
    if(check_enable_r)$display("check_enable_r_clk is on.");
    else $display("warning: check_enable_r_clk is off!!");
    $display("clock TMR test passed.");
end
endtask

endmodule





/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : TMR_clk40_generator.v
//  Author     : Yuxiang Guo,Yu Liang
//  Revision   : 
//               First created on March 22, 2019
//  Note       : 
//     
`timescale 1 ns / 1 fs
module TMR_clk40_generator(
  input enable_40_A,
  input enable_40_B,
  input enable_40_C,
  input fast_40_A,
  input fast_40_B,
  input fast_40_C,
	output clk_40_A,
	output clk_40_B,
	output clk_40_C,
  output clk_40_out

);
reg clk_40=1'b0;

reg clk_40_fast=1'b0;

always begin
  clk_40 <= 1'b1;
  #12.5;
  clk_40 <= 1'b0;
  #12.5;
end

always begin
  clk_40_fast <= 1'b1;
  #9.123;
  clk_40_fast <= 1'b0;
  #9.123;
end


assign clk_40_A =  enable_40_A ? (fast_40_A? clk_40_fast : clk_40) : 1'b0;
assign clk_40_B =  enable_40_B ? (fast_40_B? clk_40_fast : clk_40) : 1'b0;
assign clk_40_C =  enable_40_C ? (fast_40_C? clk_40_fast : clk_40) : 1'b0;


assign  clk_40_out = clk_40;

endmodule
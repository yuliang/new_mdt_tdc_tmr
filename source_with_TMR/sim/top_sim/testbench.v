/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : testbench.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-05-18 17:48:41
//  Note       : 
//     
`timescale 1 ns / 1 fs
module testbench(
    output clk320_A_out,
    output clk320_B_out,
    output clk320_C_out,

    output clk160_A_out,
    output clk160_B_out,
    output clk160_C_out,

    output clk40_A_out,
    output clk40_B_out,
    output clk40_C_out,



	output trst,
	output tck,
	output tms,
	output tdi,
	input  tdo,

	input ASD_TCK,
	input ASD_shift_out,
	input ASD_update_out,
	input ASD_data_out,
	output  ASD_data_in,

	output [23:0] hit,

	output encoded_control,
	output reg reset,
	output bunch_reset_direct,

	input bcr,

	input [1:0] d_line,   
    
    input [4  :0] phase_clk160,
    input [3  :0] phase_clk320_0, phase_clk320_1, phase_clk320_2,
    input         rst_ePLL,    
    input [3  :0] ePllResA, ePllResB, ePllResC,
    input [3  :0] ePllIcpA, ePllIcpB, ePllIcpC,
    input [1  :0] ePllCapA, ePllCapB, ePllCapC,    
    output        ePll_lock
);
// wire clk_320M_4;
// wire clk_hit;
// wire clk40,clk160,clk320;
// sim_clock_gen #(.clk_40_delay(0),.clk_320_delay(0),.clk_160_delay(0))
// sim_clock_gen_inst(
// 	.clk_40(clk40),
// 	.clk_160(clk160),
// 	.clk_320(clk320),
// 	.clk_320M_4(clk_320M_4),
// 	.clk_hit(clk_hit)
// );

// reg enable_40 = 1'b1;
// assign clk40_A_out = enable_40 & clk40;
// assign clk40_B_out = enable_40 & clk40;
// assign clk40_C_out = enable_40 & clk40;

// wire clk160, clk160_A, clk160_B, clk160_C;
// wire clk160, clk160_A, clk160_B, clk160_C;
// TMR_clk_generator 
// TMR_clk160_generator_inst (
//   .enable320(1'b0),
//   .clk(clk160), 
//   .clk_A(clk160_A), 
//   .clk_B(clk160_B), 
//   .clk_C(clk160_C),
//   .check_enable()
//   );
// wire clk320, clk320_A, clk320_B, clk320_C;
// // wire clk320, clk320_A, clk320_B, clk320_C;
// TMR_clk_generator 
// TMR_clk320_generator_inst (
//   .enable320(1'b1), 
//   .clk(clk320), 
//   .clk_A(clk320_A), 
//   .clk_B(clk320_B), 
//   .clk_C(clk320_C),
//   .check_enable()
//   );
reg enable_320= 1'b1;
reg enable_160= 1'b1;
// reg clk160_B;
// always begin
//     clk160_B = 1'b1;
//     #2.7783;
//     clk160_B = 1'b0;
//     #2.7783;
// end
// reg clk320_B;
// always begin
//     clk320_B = 1'b1;
//     #1.2714;
//     clk320_B = 1'b0;
//     #1.2714;
// end
reg enable_160_A,enable_320_A,enable_160_B,enable_320_B,enable_160_C,enable_320_C;
reg fast_160_A,fast_320_A,fast_160_B,fast_320_B,fast_160_C,fast_320_C;

  TMR_clk_generator_configrable inst_TMR_clk_generator_configrable
    (
      .enable_160_A (enable_160_A&enable_160),
      .enable_320_A (enable_320_A&enable_320),
      .enable_160_B (enable_160_B&enable_160),
      .enable_320_B (enable_320_B&enable_320),
      .enable_160_C (enable_160_C&enable_160),
      .enable_320_C (enable_320_C&enable_320),
      .fast_160_A   (fast_160_A),
      .fast_320_A   (fast_320_A),
      .fast_160_B   (fast_160_B),
      .fast_320_B   (fast_320_B),
      .fast_160_C   (fast_160_C),
      .fast_320_C   (fast_320_C),
      .clk_160_A    (clk160_A),
      .clk_320_A    (clk320_A),
      .clk_160_B    (clk160_B),
      .clk_320_B    (clk320_B),
      .clk_160_C    (clk160_C),
      .clk_320_C    (clk320_C),
      .clk_160_out  (clk160),
      .clk_320_out  (clk320)
    );



assign clk320_A_out = clk320_A;
assign clk320_B_out = clk320_B;
assign clk320_C_out = clk320_C;
assign clk160_A_out = clk160_A;
assign clk160_B_out = clk160_B;
assign clk160_C_out = clk160_C;





// reg enable_160= 1'b1;assign clk160_output = enable_160 & clk160;

reg clk40;
always begin
    clk40 = 1'b1;
    #12.5;
    clk40 = 1'b0;
    #12.5;
end
assign #3.1 clk40_A_out = clk40;  //for TMR update
assign #3.1 clk40_B_out = clk40;
assign #3.1 clk40_C_out = clk40;

jtag_test_interface
jtag_test_interface_inst
	(
    .clk(clk160),
    .TCK(tck),
    .TMS(tms),
    .TDI(tdi),
    .TDO(tdo),
    .TRST(trst)
    );


always @(posedge tck) begin
	if((ePllResA != ePllResB)|
	   (ePllResA != ePllResC)|
	   (ePllIcpA != ePllIcpB)|
	   (ePllIcpA != ePllIcpC)|
	   (ePllCapA != ePllCapB)|
	   (ePllCapA != ePllCapC))begin
		$display("%t : ePLL control ABC not equal ",$time);
		$stop;
	end
end
wire  output_enable;
wire [39:0] data_out;
data_decoder 
	data_decoder_inst (
		.clk_320(clk320), 
		.d_line(d_line),
		.output_enable(output_enable),
		.data_out(data_out)
	);

triggless_mode_sim 
	triggless_mode_sim_inst (
		.clk160(clk160), 
		.clk320(clk320), 
		.hit(hit), 
		.d_line(d_line), 
		.bcr(bcr),
		.roll_over(12'hfff)
	);

ttc_generator_sim
	ttc_generator_sim_inst(
		.clk320(clk320),
		.clk160(clk160),
		.clk40(clk40),
		.encoded_control(encoded_control),
		.bunch_reset_direct(bunch_reset_direct),
		.bcr(bcr)
	);

trigger_mode_sim
	trigger_mode_sim_inst(.clk160(clk160));
integer logfile;
initial begin
	//shall print %t with scaled in ns (-9), with 2 precision digits, and would print the " ns" string
  	$timeformat(-9, 2, " ns", 20);
  	logfile = $fopen("clk_normal_clk40tmr.log" , "w" );
    $display("At %t simulation start!", $realtime);

    enable_160_A = 1'b1;enable_160_B = 1'b1;enable_160_C = 1'b1;
    enable_320_A = 1'b1;enable_320_B = 1'b1;enable_320_C = 1'b1;
      fast_160_A = 1'b0;  fast_160_B = 1'b0;  fast_160_C = 1'b0;
      fast_320_A = 1'b0;  fast_320_B = 1'b0;  fast_320_C = 1'b0;
    $fdisplay(testbench_inst.logfile,"%t : clk_normal ",$time);
    $display("%t : clk_normal ",$time);

    // enable_160_A = 1'b1;enable_160_B = 1'b0;enable_160_C = 1'b1;
    // enable_320_A = 1'b1;enable_320_B = 1'b1;enable_320_C = 1'b1;
    //   fast_160_A = 1'b0;  fast_160_B = 1'b0;  fast_160_C = 1'b0;
    //   fast_320_A = 1'b0;  fast_320_B = 1'b0;  fast_320_C = 1'b0;
    // $fdisplay(testbench_inst.logfile,"%t : clk_160_B_disable ",$time);
    // $display("%t : clk_160_B_disable ",$time);

    // enable_160_A = 1'b1;enable_160_B = 1'b1;enable_160_C = 1'b1;
    // enable_320_A = 1'b1;enable_320_B = 1'b1;enable_320_C = 1'b1;
    //   fast_160_A = 1'b0;  fast_160_B = 1'b1;  fast_160_C = 1'b0;
    //   fast_320_A = 1'b0;  fast_320_B = 1'b0;  fast_320_C = 1'b0;
    // $fdisplay(testbench_inst.logfile,"%t : clk_160_B_faster ",$time);
    // $display("%t : clk_160_B_faster ",$time);

    // enable_160_A = 1'b1;enable_160_B = 1'b1;enable_160_C = 1'b0;
    // enable_320_A = 1'b1;enable_320_B = 1'b1;enable_320_C = 1'b1;
    //   fast_160_A = 1'b0;  fast_160_B = 1'b0;  fast_160_C = 1'b0;
    //   fast_320_A = 1'b0;  fast_320_B = 1'b0;  fast_320_C = 1'b0;
    // $fdisplay(testbench_inst.logfile,"%t : clk_160_C_disable ",$time);
    // $display("%t : clk_160_C_disable ",$time);

    // enable_160_A = 1'b1;enable_160_B = 1'b1;enable_160_C = 1'b1;
    // enable_320_A = 1'b1;enable_320_B = 1'b1;enable_320_C = 1'b1;
    //   fast_160_A = 1'b0;  fast_160_B = 1'b0;  fast_160_C = 1'b1;
    //   fast_320_A = 1'b0;  fast_320_B = 1'b0;  fast_320_C = 1'b0;
    // $fdisplay(testbench_inst.logfile,"%t : clk_160_C_faster ",$time);
    // $display("%t : clk_160_C_faster ",$time);

    // enable_160_A = 1'b1;enable_160_B = 1'b1;enable_160_C = 1'b1;
    // enable_320_A = 1'b1;enable_320_B = 1'b0;enable_320_C = 1'b1;
    //   fast_160_A = 1'b0;  fast_160_B = 1'b0;  fast_160_C = 1'b0;
    //   fast_320_A = 1'b0;  fast_320_B = 1'b0;  fast_320_C = 1'b0;
    // $fdisplay(testbench_inst.logfile,"%t : clk_320_B_disable ",$time);
    // $display("%t : clk_320_B_disable ",$time);

    // enable_160_A = 1'b1;enable_160_B = 1'b1;enable_160_C = 1'b1;
    // enable_320_A = 1'b1;enable_320_B = 1'b1;enable_320_C = 1'b1;
    //   fast_160_A = 1'b0;  fast_160_B = 1'b0;  fast_160_C = 1'b0;
    //   fast_320_A = 1'b0;  fast_320_B = 1'b1;  fast_320_C = 1'b0;
    // $fdisplay(testbench_inst.logfile,"%t : clk_320_B_faster ",$time);
    // $display("%t : clk_320_B_faster ",$time);

    // enable_160_A = 1'b1;enable_160_B = 1'b1;enable_160_C = 1'b1;
    // enable_320_A = 1'b1;enable_320_B = 1'b1;enable_320_C = 1'b0;
    //   fast_160_A = 1'b0;  fast_160_B = 1'b0;  fast_160_C = 1'b0;
    //   fast_320_A = 1'b0;  fast_320_B = 1'b0;  fast_320_C = 1'b0;
    // $fdisplay(testbench_inst.logfile,"%t : clk_320_C_disable ",$time);
    // $display("%t :clk_320_C_disable ",$time);

    // enable_160_A = 1'b1;enable_160_B = 1'b1;enable_160_C = 1'b1;
    // enable_320_A = 1'b1;enable_320_B = 1'b1;enable_320_C = 1'b1;
    //   fast_160_A = 1'b0;  fast_160_B = 1'b0;  fast_160_C = 1'b0;
    //   fast_320_A = 1'b0;  fast_320_B = 1'b0;  fast_320_C = 1'b1;
    // $fdisplay(testbench_inst.logfile,"%t : clk_320_C_faster ",$time);
    // $display("%t : clk_320_C_faster ",$time);


  	reset = 1'b0;
    @(negedge clk40)
  	jtag_test_interface_inst.TRST_1;
    @(negedge clk40)
  	jtag_test_interface_inst.TRST_0;
  	#200
  	#200
  	@(negedge clk160)
  	reset = 1'b1;
  	#200
  	@(negedge clk160)
  	reset = 1'b0;
    #200;
    jtag_test_interface_inst.spi_test;
	#200;
    @(negedge clk40)
  	jtag_test_interface_inst.TRST_1;
    @(negedge clk40)
  	jtag_test_interface_inst.TRST_0;
  	#200
    @(negedge clk40)
  	jtag_test_interface_inst.TRST_1;
  	#200
  	@(negedge clk160)
  	reset = 1'b1;
  	#200
  	@(negedge clk160)
  	reset = 1'b0;
    #200;
	jtag_test_interface_inst.JTAG_test;
	#200
	triggless_mode_sim_inst.fine_time_decode_test;
	triggless_mode_sim_inst.single_hit_pair_test;	
	triggless_mode_sim_inst.multi_hit_pair_test;
	triggless_mode_sim_inst.single_hit_leading_test;
	triggless_mode_sim_inst.multi_hit_leading_test;
	ttc_generator_sim_inst.direct_bcr_test;
	ttc_generator_sim_inst.encode_bcr_test;
	trigger_mode_sim_inst.bunch_id_test;
	trigger_mode_sim_inst.trigger_matching_test;
	$fclose(logfile);
	#5000
	$stop;
end
endmodule
// File: asd.v
// Auth: M. Fras, Electronics Division, MPI for Physics, Munich
// Mod.: M. Fras, Electronics Division, MPI for Physics, Munich
// Date: 04 Jun 2013
// Rev.: 10 Sep 2019
//
// Simulation model of the ATLAS MDT ASD serial interface.
// Based on code written by Sergey Abovyan.
//





// Width of ASD configuration register.
`define ASD_CFG_REG_WIDTH 53
//`timescale  1 ns / 100 ps




module ASD_interface (
    i_rst,
    i_asd_sclk,
    i_asd_sload,
    i_asd_sin,
    i_asd_shift,
    i_asd_sdown,
    o_asd_sout
);



// Define IOs.
input i_rst;
input i_asd_sclk;
input i_asd_sload;
input i_asd_sin;
input i_asd_shift;
input i_asd_sdown;
output o_asd_sout;



// Local wires and variables.
wire rst = i_rst;
reg [`ASD_CFG_REG_WIDTH-1:0] asd_reg = 0;
reg [`ASD_CFG_REG_WIDTH-1:0] asd_shadow_reg = 0;



// ASD shift register.
always @(posedge i_asd_sclk or posedge rst) begin
if (rst) begin
    asd_reg <= 0;
end else begin
    if (i_asd_sdown)
        asd_reg <= asd_shadow_reg;
    else if (i_asd_shift) begin
        asd_reg[`ASD_CFG_REG_WIDTH-1] <= i_asd_sin;
        asd_reg[`ASD_CFG_REG_WIDTH-2:0] <= asd_reg[`ASD_CFG_REG_WIDTH-1:1];
    end
end
end // always

// ASD shadow register.
always @(posedge i_asd_sload or posedge rst) begin
if (rst) begin
    asd_shadow_reg <= 0;
end else begin
    asd_shadow_reg <= asd_reg;
end
end // always

// ASD serial output register active on fallig clock edge.
reg asd_sout_reg;
always @(negedge i_asd_sclk) begin
    if (i_asd_sdown)
        asd_sout_reg <= asd_sout_reg;   // HOLD operation, as mentioned in
                                        // section 2.8.2 of the ASD manual.
    else if (i_asd_shift)
        asd_sout_reg <= asd_reg[0];
end

assign o_asd_sout = asd_sout_reg;

endmodule   // asd


/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : TMR_clk_generator_configrable.v
//  Author     : Yuxiang Guo,Yu Liang
//  Revision   : 
//               First created on March 22, 2019
//  Note       : 
//     
`timescale 1 ns / 1 fs
module TMR_clk_generator_configrable(
  input enable_160_A,enable_320_A,
  input enable_160_B,enable_320_B,
  input enable_160_C,enable_320_C,
  input fast_160_A,fast_320_A,
  input fast_160_B,fast_320_B,
  input fast_160_C,fast_320_C,


	output clk_160_A,clk_320_A,
	output clk_160_B,clk_320_B,
	output clk_160_C,clk_320_C,

  output clk_160_out,
  output clk_320_out
);
reg clk_160=1'b0;
reg clk_320=1'b0;
reg clk_160_fast=1'b0;
reg clk_320_fast=1'b0;
always begin
  clk_160 <= 1'b1;
  #3.125;
  clk_160 <= 1'b0;
  #3.125;
end
always begin
  clk_320 <= 1'b1;
  #1.5625;
  clk_320 <= 1'b0;
  #1.5625;
end

always begin
  clk_160_fast <= 1'b1;
  #2.123;
  clk_160_fast <= 1'b0;
  #2.123;
end
always begin
  clk_320_fast <= 1'b1;
  #1.1625;
  clk_320_fast <= 1'b0;
  #1.1625;
end

assign clk_160_A =  enable_160_A ? (fast_160_A? clk_160_fast : clk_160) : 1'b0;
assign clk_160_B =  enable_160_B ? (fast_160_B? clk_160_fast : clk_160) : 1'b0;
assign clk_160_C =  enable_160_C ? (fast_160_C? clk_160_fast : clk_160) : 1'b0;
assign clk_320_A =  enable_320_A ? (fast_320_A? clk_320_fast : clk_320) : 1'b0;
assign clk_320_B =  enable_320_B ? (fast_320_B? clk_320_fast : clk_320) : 1'b0;
assign clk_320_C =  enable_320_C ? (fast_320_C? clk_320_fast : clk_320) : 1'b0;

assign  clk_160_out = clk_160;
assign  clk_320_out = clk_320;
endmodule
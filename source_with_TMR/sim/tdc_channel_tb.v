/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : tdc_channel_tb.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on July 5th, 2017
//  Note       :  test bench for MDT TDC channel logic
`timescale 1 ns / 1 fs
`include "common_definition.v"
module tdc_channel_tb;

reg clk320;
wire clk160;

wire fake_hit;
reg rst;
wire channel_ready;
wire [5:0] q;
wire [14:0] cnt_channel;
wire [14:0] cnt_channel_inv;

fake_hit_generator #(.WIDTH(12))
fake_hit_generator_inst(
    .clk(clk160),
    .rst(rst),
    .fake_hit(fake_hit),
    .time_interval(12'h050)
    );

reg fifo_read;
wire [16:0] channel_data;
wire fifo_empty;

wire trigger;
wire [11:0] limit_up;
wire [11:0] limit_down;
wire trigger_processing;

tdc_channel #(.DATA_WIDTH(18),.DATA_DEEP(16)
	)tdc_channel_inst
(
//    .chnl_id(5'b01010), // channel number
    .fine_sel(4'b0011), //encoding for fine time selection

    .lut0(6'h00), .lut1(6'h05), .lut2(6'h0a), .lut3(6'h0d), .lut4(6'h13), .lut5(6'h14), .lut6(6'h1a), .lut7(6'h1e), .lut8(6'h20), .lut9(6'h24),
    .luta(6'h28), .lutb(6'h2d), .lutc(6'h33), .lutd(6'h34), .lute(6'h3b), .lutf(6'h3c),

    .rst(rst),      // reset, not BC reset
//    .edge_mode(1'b1), // 1: leading; 0: trailing
    .clk160(clk160),
    .Rdy(channel_ready), // ready
    .q(q), // raw fine codes 
    .cnt(cnt_channel), .cnt_inv(cnt_channel_inv), // coarse counter

    .fake_hit(fake_hit),

    .fifo_read(fifo_read),
    .fifo_read_clk(clk160),
    .channel_data_out(channel_data),
    .fifo_empty(fifo_empty),

    .trigger(trigger),
    .limit_up(limit_up),
    .limit_down(limit_down),
    .trigger_processing(trigger_processing)
);
reg clk_fast;
reg [16:0] counter_fast=17'b0;
always@(posedge clk_fast)begin
    if(counter_fast==17'b110111101100_11111)begin
      counter_fast <= 17'b0;
    end else begin
      counter_fast <= counter_fast + 17'b1;
    end     
end

wire hit;
wire trigger_input;

fake_trigger_process fake_trigger_process_inst(
.trigger(trigger_input),
.rst(rst),
.clk160(clk160),
.counter_fast(counter_fast[16:5]),
.roll_over(12'b110111101100),
.trigger_latency(12'h064),
.matching_window(12'h028),
.trigger_out(trigger),
.limit_up(limit_up),.limit_down(limit_down));

reg clk_calibre_reg=1'b0;
wire clk_calibre;
assign #1.5 clk_calibre = clk_calibre_reg;
always begin    
    clk_calibre_reg = 1'b1;
    #2.5;
    clk_calibre_reg = 1'b0;
    #2.5;
end

random_hit_generator random_hit_generator_inst(
.clk(clk_calibre),
.rst(rst),
.hit_out(hit),
.enable_out(1'b1),
.RATE(32'h0083126E),//32'h00417874->200k,32'h0083126E->400k
.seed1(32'hAD1659EF),
.seed2(32'h3D9EF9ED),
.dead_time_input(8'ha0)
    );


fake_trigger_gen fake_trigger_gen_inst(
.clk320(clk320),
.rst(rst),
.hit(hit),
.trigger(trigger_input),
.trigger_th(32'hffff_ffff));

tdcdata_gen tdcdata_gen_inst(
.hit(hit),
.rst(rst),
.clk320(clk320),
.counter_fast(counter_fast),
.Rdy_chnl(channel_ready), .t_Rdy_chnl(),
.q_chnl(q), .t_q_chnl(),
.cnt_chnl(cnt_channel), .t_cnt_chnl(),
.cnt_inv_chnl(cnt_channel_inv), .t_cnt_inv_chnl());

initial begin
	rst=1'b0;
	#3000;
	rst=1'b1;
	#100
	rst=1'b0;
end


always begin
    clk_fast=1'b1;
    #0.390625;
    clk_fast=1'b0;
    #0.390625;
end


always begin
  clk320 =1'b1;
  #1.5625;
  clk320 =1'b0;
  #1.5625;
end
reg clk160_tmp;
always begin
  clk160_tmp =1'b0;
  #3.125;
  clk160_tmp =1'b1;
  #3.125;
end
assign #0.8 clk160 =clk160_tmp;



initial begin
    #150000;
    $stop;
end

initial begin
    fifo_read = 1'b1;    
end

endmodule
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : fake_trigger_gen.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on June 14th, 2017
//  Note       : 
// 

module fake_trigger_gen
#(parameter
	latency=2500
)
(
input clk320,
input rst,
input hit,
output trigger,
input [31:0]  trigger_th);

reg hit_delay;
always @(*) begin
	if (rst) begin
		// reset
		hit_delay <= 1'b0;
	end
	else  begin
		hit_delay <= #(latency) hit;
	end
end

reg [2:0] hit_delay_r;
always @(posedge clk320) begin
	if (rst) begin
		// reset
		hit_delay_r <=  #(`ff_delay) 10'b0;
	end
	else begin
		hit_delay_r <= #(`ff_delay) {hit_delay_r[1:0],hit_delay};
	end
end
reg trigger_fast;

always @(posedge clk320) begin
	if (rst) begin
		// reset
		trigger_fast <= #(`ff_delay) 1'b0;
	end
	else if((~hit_delay_r[2])&hit_delay_r[1])begin
		if($random < trigger_th)begin
			trigger_fast <= #(`ff_delay) 1'b1;
		end		
	end
	else begin
		trigger_fast <= #(`ff_delay) 1'b0;
	end
end

reg [7:0]trigger_fast_r;

always @(posedge clk320) begin
	if (rst) begin
		// reset
		trigger_fast_r <= #(`ff_delay) 8'b0;
	end
	else begin
		trigger_fast_r <= #(`ff_delay) {trigger_fast_r[6:0],trigger_fast};
	end
end

assign #(`ff_delay)  trigger = |trigger_fast_r;
endmodule
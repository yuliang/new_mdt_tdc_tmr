/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : reset_gen.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 12th, 2018
//  Note       :  reset signal generation


module reset_gen  
#(parameter
	start = 3000,
	width = 100
)
(
	output reg rst
);
	initial begin
		rst=1'b0;
		#start;
		rst=1'b1;
		#width
		rst=1'b0;
	end
endmodule
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : jtag_programming.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 22st, 2018
//  Note       : 
// 


module jtag_programming( 
	output jtag_tck, 
	output reg jtag_tms, 
	output reg jtag_tdi, 
	input jtag_tdo, 
	output reg jtag_trst );

reg	jtag_tck_int, clock_enable;

//input
reg	make_instruction_parity_error;

//output
reg	read_instruction_parity;
reg [3:0] read_instruction;
initial	begin
	jtag_tms = 1;
	jtag_tdi = 0;
	jtag_trst = 0;
	jtag_tck_int = 1;
	clock_enable = 0;
end

//programming clock
always
	begin
	#25 jtag_tck_int = ~jtag_tck_int;
	end

assign jtag_tck = clock_enable & jtag_tck_int;

task start_jtag_clock;
	begin
	@(negedge jtag_tck_int)
	#1;
	clock_enable = 1;
	end
endtask

task stop_jtag_clock;
	begin
	@(negedge jtag_tck_int)
	#1;
	clock_enable = 0;
	end
endtask

task reset_tap_controller;
	begin
	start_jtag_clock;
	jtag_tdi = 0;
	@(posedge jtag_tck);
	@(posedge jtag_tck);
	@(negedge jtag_tck);
	jtag_tms = 1;
	jtag_trst = 0;
	@(posedge jtag_tck);
	@(posedge jtag_tck);
	jtag_trst = 1;
	@(posedge jtag_tck);
	@(negedge jtag_tck);
	jtag_tms = 0;
	@(posedge jtag_tck);
	stop_jtag_clock;
	end
endtask

task set_ir;
	input[3:0]	instruction_code;

	integer		i;

	begin
	@(posedge jtag_tck);

	@(negedge jtag_tck) jtag_tms = 0; //idle
	@(posedge jtag_tck);

	@(negedge jtag_tck) jtag_tms = 1; //select_dr_scan
	@(posedge jtag_tck);

	@(negedge jtag_tck) jtag_tms = 1; //select_ir_scan
	@(posedge jtag_tck);

	@(negedge jtag_tck) jtag_tms = 0; //capture_ir
	@(posedge jtag_tck);

	@(negedge jtag_tck) jtag_tms = 0; //shift_ir
	@(posedge jtag_tck);

	//tap controller will stay in shift_ir utill tms=0
	
	for ( i = 0 ; i <= 3 ; i = i + 1)
		begin	
		@(negedge jtag_tck) jtag_tdi = instruction_code[i];
		@(posedge jtag_tck);
		read_instruction[i] = jtag_tdo;
		end


	@(negedge jtag_tck) 
	if (make_instruction_parity_error == 0)
		jtag_tdi = ^ instruction_code[3:0]; //parity
	else
		jtag_tdi = ~(^ instruction_code[3:0]); //parity error
		
	jtag_tms = 1; //exit1_ir
	@(posedge jtag_tck);
	read_instruction_parity = jtag_tdo;


	@(negedge jtag_tck) jtag_tms = 1; //update_ir
	@(posedge jtag_tck);


	@(negedge jtag_tck) jtag_tms = 0; //run_test_idle
	@(posedge jtag_tck);

	end
endtask

reg [31:0] id;
//------------------------------------------------------------------------------
task read_id;
	integer		i;

	begin


	start_jtag_clock;

	set_ir(4'd1); //id scan path

	//select shift of coretest chain	
	@(negedge jtag_tck) jtag_tms = 1; //select_dr_scan
	@(posedge jtag_tck);

	@(negedge jtag_tck) jtag_tms = 0; //capture_dr
	@(posedge jtag_tck);

	@(negedge jtag_tck) jtag_tms = 0; //shift_dr
	@(posedge jtag_tck);

	for ( i = 0 ; i <= 31; i = i +1)
		begin
		@(negedge jtag_tck) jtag_tdi =  0; //read only
		if ( i == 31 )
			jtag_tms = 1; //exit1_dr
		@(posedge jtag_tck);
		id[i] = jtag_tdo;
		end

	//return to idle state


	@(negedge jtag_tck) jtag_tms = 1; //update_dr
	@(posedge jtag_tck);

	@(negedge jtag_tck) jtag_tms = 0; //idle
	@(posedge jtag_tck);

	stop_jtag_clock;
	end
endtask

task check_whole_through;
	input[3:0]	instruction_code;
	input[31:0]	length;

	integer		i;

	reg		check_bit;

	begin

	
	start_jtag_clock;

	set_ir(instruction_code); //setup scan path


	//select shift of chain	

	@(negedge jtag_tck) jtag_tms = 1; //select_dr_scan
	@(posedge jtag_tck);

	@(negedge jtag_tck) jtag_tms = 0; //capture_dr
	@(posedge jtag_tck);

	@(negedge jtag_tck) jtag_tms = 0; //shift_dr
	@(posedge jtag_tck);


	//write zero's in whole chain
	@(negedge jtag_tck) jtag_tdi = 0;
	for ( i= 0; i <= length; i = i + 1)
		begin
		@(posedge jtag_tck);
		end
	
	//write ones's in whole chain and check zero's
	@(negedge jtag_tck) jtag_tdi = 1;
	for ( i= 0; i <= length; i = i + 1)
		begin
		@(posedge jtag_tck);
		if ( jtag_tdo !== (0) ) 
			$display("ERROR in checking zero's in scan path %d at %d", instruction_code, i);
		end


	//write checker board pattern into scan path and check one's
	jtag_tdi = 1;
	for ( i= 0; i <= length; i = i + 1)
		begin
		@(negedge jtag_tck) jtag_tdi = ~jtag_tdi;
		@(posedge jtag_tck);
		if ( jtag_tdo !== (1) ) 
			$display("ERROR in checking ones's in scan path %d at %d", instruction_code, i);
		end

	@(negedge jtag_tck) jtag_tdi = 0;

	//verify checker board pattern comming out
	check_bit = 0;
	for ( i = 0; i <= length; i = i + 1)
		begin
		@(posedge jtag_tck);
		if ( jtag_tdo !== (check_bit) ) 
			$display("ERROR in checking checker board in scan path %d at %d", instruction_code, i);
		check_bit = ~check_bit;
		end
		
	for ( i = 0; i <= 3; i = i + 1)
		begin
		@(posedge jtag_tck);
		if ( jtag_tdo !== (0) ) 
			$display("ERROR in checking scan path end %d", instruction_code);
		end
	//return to idle state

	@(negedge jtag_tck) jtag_tms = 1; //exit1_dr
	@(posedge jtag_tck);

	@(negedge jtag_tck) jtag_tms = 1; //update_dr
	@(posedge jtag_tck);

	@(negedge jtag_tck) jtag_tms = 0; //idle
	@(posedge jtag_tck);

	stop_jtag_clock;

	end
endtask

reg [127:0] content;
reg [127:0] content_output=128'b0;
//--------------------------------------------------------------------
task load_dr;
	input [3:0]	instruction_code;
	input [31:0] length;
	input [127:0] content_input;
	integer		i;
	begin
	content_output ='b0;
	content = content_input;
	start_jtag_clock;

	set_ir(instruction_code); //control scan path

	//select shift of setup chain	
	@(negedge jtag_tck) jtag_tms = 1; //select_dr_scan
	@(posedge jtag_tck);

	@(negedge jtag_tck) jtag_tms = 0; //capture_dr
	@(posedge jtag_tck);

	@(negedge jtag_tck) jtag_tms = 0; //shift_dr
	@(posedge jtag_tck);

	@(negedge jtag_tck) jtag_tdi = content[0]; 
	@(posedge jtag_tck); content_output[0] = jtag_tdo;

	for ( i = 1 ; i <length; i = i +1)
		begin
		@(negedge jtag_tck) jtag_tdi = content[i];
		@(posedge jtag_tck); content_output[i] = jtag_tdo;
		end

	@(negedge jtag_tck) jtag_tdi = content[i];
	jtag_tms = 1;
    @(posedge jtag_tck); content_output[i] = jtag_tdo;

	//return to idle state
	@(negedge jtag_tck) jtag_tms = 1; //update_dr
	@(posedge jtag_tck);

	@(negedge jtag_tck) jtag_tms = 0; //idle
	@(posedge jtag_tck);

	stop_jtag_clock;

	end
endtask

endmodule
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : setup_control_status_tb.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-04-12 15:20:14
//  Note       : 
//   

`include "common_definition.v"
`define REG_LIST

module setup_control_status_TMR_tb();
`include "setup_control_status_TMR_tb_reglist.v"
wire        reset_jtag_in;
wire        event_teset_jtag_in;
wire        enable_new_ttc;
wire        enable_master_reset_code;
wire        enable_trigger;
wire [11:0] bunch_offset;
wire [11:0] event_offset;
wire [11:0] match_window;
wire        enable_direct_trigger;
wire        enable_direct_bunch_reset;
wire        enable_direct_event_reset;
wire [11:0] roll_over;
wire [11:0] coarse_count_offset;
wire        auto_roll_over;
wire        bypass_bcr_distribution;
wire        channel_data_debug;
wire        enbale_fake_hit;
wire [11:0] fake_hit_time_interval;
wire        enable_trigger_timeout;
wire        enable_leading;
wire        enable_pair;
wire 		full_width_res;
wire [2:0]  width_select;
wire        enable_8b10b;
wire        enable_insert;
wire        enable_error_packet;
wire        enable_TDC_ID;
wire [18:0] TDC_ID;
wire        enable_error_notify;
wire        config_error;
wire        enable_high_speed;
wire        enable_legacy;
wire [`MAX_PACKET_NUM_SIZE-1:0] syn_packet_number;
wire [23:0] rising_is_leading;
wire [`COMBINE_TIME_OUT_SIZE-1:0] combine_time_out_config;
wire [23:0] channel_enable_r;
wire [23:0] channel_enable_f;
wire [3:0]  fine_sel;
wire [1:0]  lut0,lut1,lut2,lut3,lut4,lut5,lut6,lut7;
wire [1:0]  lut8,lut9,luta,lutb,lutc,lutd,lute,lutf;
wire [3:0]  debug_port_select;

wire [23:0] chnl_fifo_overflow;
wire  chnl_fifo_overflow_clear;
wire ePll_lock ;
assign ePll_lock= 1'b1;
assign chnl_fifo_overflow = 24'b0;

wire [4  :0] phase_clk160;
wire [3  :0] phase_clk320_0, phase_clk320_1, phase_clk320_2;
wire         rst_ePLL;
wire [3  :0] ePllResA, ePllResB, ePllResC;
wire [3  :0] ePllIcpA, ePllIcpB, ePllIcpC;
wire [1  :0] ePllCapA, ePllCapB, ePllCapC; 

wire tdi_to_dr;
wire setup0_shift,setup0_update,setup0_enable,setup0_in;
wire setup1_shift,setup1_update,setup1_enable,setup1_in;
wire setup2_shift,setup2_update,setup2_enable,setup2_in;
wire control0_enable,control0_update,control0_shift,control0_in;
wire control1_enable,control1_update,control1_shift,control1_in;
wire status0_shift,status0_in;
wire status1_shift,status1_in;

wire tck,tms,tdi,tdo;
reg trst;

wire instruction_error;
wire tdo_jtag;
wire tdo_b;
assign tdo = trst ? tdo_jtag : tdo_b;
wire clk_160;
wire clk160_A, clk160_B, clk160_C;
reg check_enable;
TMR_clk_generator TMR_clk_generator_inst (
  .enable320(1'b0),
  .clk(clk_160), 
  .clk_A(clk160_A), 
  .clk_B(clk160_B), 
  .clk_C(clk160_C),
  .check_enable(check_enable_clk)
  );



wire tdi_to_dr_TMR;
wire setup0_shift_TMR,setup0_update_TMR,setup0_enable_TMR,setup0_in_TMR;
wire setup1_shift_TMR,setup1_update_TMR,setup1_enable_TMR,setup1_in_TMR;
wire setup2_shift_TMR,setup2_update_TMR,setup2_enable_TMR,setup2_in_TMR;
wire control0_enable_TMR,control0_update_TMR,control0_shift_TMR,control0_in_TMR;
wire control1_enable_TMR,control1_update_TMR,control1_shift_TMR,control1_in_TMR;
wire status0_shift_TMR,status0_in_TMR;
wire status1_shift_TMR,status1_in_TMR;
wire instruction_error_TMR;


jtag_controller_TMR
    jtag_controller_inst( 
	   .tck(tck), 
	   .tms(tms), 
	   .tdi(tdi), 
	   .trst(trst), 
	   .tdo(tdo_jtag), 
	   .enable(), 
    
	   .tdi_to_dr(tdi_to_dr_TMR), 
    
        .setup0_shift(setup0_shift_TMR),
        .setup0_update(setup0_update_TMR),
        .setup0_enable(setup0_enable_TMR),
        .setup0_in(setup0_in_TMR), 

        .setup1_shift      (setup1_shift_TMR),
        .setup1_update     (setup1_update_TMR),
        .setup1_enable     (setup1_enable_TMR),
        .setup1_in         (setup1_in_TMR), 
    
        .setup2_shift      (setup2_shift_TMR),
        .setup2_update     (setup2_update_TMR),
        .setup2_enable     (setup2_enable_TMR),
        .setup2_in         (setup2_in_TMR), 
    
        .control0_enable(control0_enable_TMR), 
        .control0_update(control0_update_TMR), 
        .control0_shift(control0_shift_TMR),
        .control0_in(control0_in_TMR), 
    
        .control1_enable(control1_enable_TMR), 
        .control1_update(control1_update_TMR), 
        .control1_shift(control1_shift_TMR),
        .control1_in(control1_in_TMR), 
    
        .status0_shift(status0_shift_TMR),
        .status0_in(status0_in_TMR), 
    
        .status1_shift(status1_shift_TMR),
        .status1_in(status1_in_TMR), 
    
	   .instruction_error(instruction_error_TMR)
    );

jtag_controller
    jtag_controller_normal_inst( 
       .tck(tck), 
       .tms(tms), 
       .tdi(tdi), 
       .trst(trst), 
       .tdo(), 
       .enable(), 
    
       .tdi_to_dr(tdi_to_dr), 
    
        .setup0_shift(setup0_shift),
        .setup0_in(setup0_in), 
    
        .setup1_shift(setup1_shift),
        .setup1_in(setup1_in), 
    
        .setup2_shift(setup2_shift),
        .setup2_in(setup2_in), 
    
        .control0_enable(control0_enable), 
        .control0_update(control0_update), 
        .control0_shift(control0_shift),
        .control0_in(control0_in), 
    
        .control1_enable(control1_enable), 
        .control1_update(control1_update), 
        .control1_shift(control1_shift),
        .control1_in(control1_in), 
    
        .status0_shift(status0_shift),
        .status0_in(status0_in), 
    
        .status1_shift(status1_shift),
        .status1_in(status1_in), 
    
       .instruction_error(instruction_error)
    );


common_setup
    common_setup_inst(
        .tck(tck),
        .trst(trst),
        .tdi(tdi),
        .tdo_b(tdo_b),
        .tms(tms),
    
        .tdi_to_dr(tdi_to_dr),
    
        .setup0_shift(setup0_shift),
        .setup0_data(),
    
        .setup1_shift(setup1_shift),
        .setup1_data(),
    
        .setup2_shift(setup2_shift),
        .setup2_data(),
    
    
        .control0_shift(control0_shift),
        .control0_update(control0_update),
        .control0_enable(control0_enable),
        .control0_data(),
    
        .control1_shift(control1_shift),
        .control1_update(control1_update),
        .control1_enable(control1_enable),
        .control1_data(),
    
        .status0_shift(status0_shift),
        .status0_data(status0_in),
    
        .status1_shift(status1_shift),
        .status1_data(status1_in),
    
    
        .phase_clk160(phase_clk160),
        .phase_clk320_0(phase_clk320_0), .phase_clk320_1(phase_clk320_1), .phase_clk320_2(phase_clk320_2),
        .rst_ePLL(rst_ePLL),
        
        .ePllResA(ePllResA), .ePllResB(ePllResB), .ePllResC(ePllResC),
        .ePllIcpA(ePllIcpA), .ePllIcpB(ePllIcpB), .ePllIcpC(ePllIcpC),
        .ePllCapA(ePllCapA), .ePllCapB(ePllCapB), .ePllCapC(ePllCapC),
        
        .ePll_lock(ePll_lock),
        
        
        .config_error(config_error),
        
        
        .debug_port_select(debug_port_select),
        .chnl_fifo_overflow_clear(chnl_fifo_overflow_clear),
        .chnl_fifo_overflow(chnl_fifo_overflow),
        .instruction_error(instruction_error),
        //for TTC interface
        .reset_jtag_in(reset_jtag_in),
        .event_teset_jtag_in(event_teset_jtag_in),
        
        .enable_new_ttc(enable_new_ttc),
        .enable_master_reset_code(enable_master_reset_code),
        .enable_direct_bunch_reset(enable_direct_bunch_reset),
        .enable_direct_event_reset(enable_direct_event_reset),
        .enable_direct_trigger(enable_direct_trigger),
        
        //for bcr
        .roll_over(roll_over),
        .coarse_count_offset(coarse_count_offset),
        .auto_roll_over(auto_roll_over),
        .bypass_bcr_distribution(bypass_bcr_distribution),
        
        //channel model
        .enable_trigger(enable_trigger),
        .channel_data_debug(channel_data_debug),
        .enable_leading(enable_leading),
        .enable_pair(enable_pair),
        .enbale_fake_hit(enbale_fake_hit),
        .fake_hit_time_interval(fake_hit_time_interval),
        .rising_is_leading(rising_is_leading),
        .combine_time_out_config(combine_time_out_config),
        .channel_enable_r(channel_enable_r),
        .channel_enable_f(channel_enable_f),
        
        //for trigger para
        .bunch_offset(bunch_offset),
        .event_offset(event_offset),
        .match_window(match_window),
        
        
        //for readout interface
        .enable_trigger_timeout(enable_trigger_timeout),
        .enable_high_speed(enable_high_speed),
        .enable_legacy(enable_legacy),
        .full_width_res(full_width_res),
        .width_select(width_select),
        .enable_8b10b(enable_8b10b),
        .enable_insert(enable_insert),
        .syn_packet_number(syn_packet_number),
        .enable_error_packet(enable_error_packet),
        .enable_TDC_ID(enable_TDC_ID),
        .TDC_ID(TDC_ID),
        .enable_error_notify(enable_error_notify),
        
        
        .fine_sel(fine_sel),
        .lut0(lut0),.lut1(lut1),.lut2(lut2),.lut3(lut3),.lut4(lut4),.lut5(lut5),.lut6(lut6),.lut7(lut7),
        .lut8(lut8),.lut9(lut9),.luta(luta),.lutb(lutb),.lutc(lutc),.lutd(lutd),.lute(lute),.lutf(lutf)
    );
wire [299:0] config_output;
wire [114:0] setup0_output;
wire [93:0] setup1_output;
wire [35:0] setup2_output;
wire [7:0] control0_output;
wire [46:0] control1_output;
assign setup0_output = {
enable_new_ttc,
enable_master_reset_code,
enable_direct_bunch_reset,
enable_direct_event_reset,
enable_direct_trigger,
auto_roll_over,
bypass_bcr_distribution,
enable_trigger,
channel_data_debug,
enable_leading,
enable_pair,
enbale_fake_hit,
rising_is_leading,
channel_enable_r,
channel_enable_f,
TDC_ID,
enable_trigger_timeout,
enable_high_speed,
enable_legacy,
full_width_res,
width_select,
enable_8b10b,
enable_insert,
enable_error_packet,
enable_TDC_ID,
enable_error_notify
};
assign setup1_output = {
combine_time_out_config,
fake_hit_time_interval,
syn_packet_number,
roll_over,
coarse_count_offset,
bunch_offset,
event_offset,
match_window
};
assign setup2_output = {
fine_sel,
lut0,lut1,lut2,lut3,lut4,lut5,lut6,lut7,
lut8,lut9,luta,lutb,lutc,lutd,lute,lutf    
};
assign control0_output = {
rst_ePLL,
reset_jtag_in,
event_teset_jtag_in,
chnl_fifo_overflow_clear,
debug_port_select
};
assign control1_output = {
phase_clk160,
phase_clk320_0,phase_clk320_1, phase_clk320_2,
ePllResA,ePllIcpA,ePllCapA,
ePllResB,ePllIcpB,ePllCapB,
ePllResC,ePllIcpC,ePllCapC
};
assign config_output={setup0_output, setup1_output, setup2_output, control0_output, control1_output};






wire        reset_jtag_in_TMR;
wire        event_teset_jtag_in_TMR;
wire        enable_new_ttc_TMR;
wire        enable_master_reset_code_TMR;
wire        enable_trigger_TMR;
wire [11:0] bunch_offset_TMR;
wire [11:0] event_offset_TMR;
wire [11:0] match_window_TMR;
wire        enable_direct_trigger_TMR;
wire        enable_direct_bunch_reset_TMR;
wire        enable_direct_event_reset_TMR;
wire [11:0] roll_over_TMR;
wire [11:0] coarse_count_offset_TMR;
wire        auto_roll_over_TMR;
wire        bypass_bcr_distribution_TMR;
wire        channel_data_debug_TMR;
wire        enbale_fake_hit_TMR;
wire [11:0] fake_hit_time_interval_TMR;
wire        enable_trigger_timeout_TMR;
wire        enable_leading_TMR;
wire        enable_pair_TMR;
wire        full_width_res_TMR;
wire [2:0]  width_select_TMR;
wire        enable_8b10b_TMR;
wire        enable_insert_TMR;
wire        enable_error_packet_TMR;
wire        enable_TDC_ID_TMR;
wire [18:0] TDC_ID_TMR;
wire        enable_error_notify_TMR;
wire        config_error_TMR;
wire        enable_high_speed_TMR;
wire        enable_legacy_TMR;
wire [`MAX_PACKET_NUM_SIZE-1:0] syn_packet_number_TMR;
wire [23:0] rising_is_leading_TMR;
wire [`COMBINE_TIME_OUT_SIZE-1:0] combine_time_out_config_TMR;
wire [23:0] channel_enable_r_TMR;
wire [23:0] channel_enable_f_TMR;
wire [3:0]  fine_sel_TMR;
wire [1:0]  lut0_TMR,lut1_TMR,lut2_TMR,lut3_TMR,lut4_TMR,lut5_TMR,lut6_TMR,lut7_TMR;
wire [1:0]  lut8_TMR,lut9_TMR,luta_TMR,lutb_TMR,lutc_TMR,lutd_TMR,lute_TMR,lutf_TMR;
wire [3:0]  debug_port_select_TMR;

wire [4  :0] phase_clk160_TMR;
wire [3  :0] phase_clk320_0_TMR, phase_clk320_1_TMR, phase_clk320_2_TMR;
wire         rst_ePLL_TMR;
wire [3  :0] ePllResA_TMR, ePllResB_TMR, ePllResC_TMR;
wire [3  :0] ePllIcpA_TMR, ePllIcpB_TMR, ePllIcpC_TMR;
wire [1  :0] ePllCapA_TMR, ePllCapB_TMR, ePllCapC_TMR; 

reg clk_40M;
always begin
    clk_40M = 1'b0;
    #12.5;
    clk_40M = 1'b1;
    #12.5;
end
reg enable_40_A, enable_40_B, enable_40_C;
reg fast_40_A, fast_40_B, fast_40_C;
wire clk_40_A, clk_40_B, clk_40_C, clk_40;
TMR_clk40_generator inst_TMR_clk40_generator
    (
        .enable_40_A (enable_40_A),
        .enable_40_B (enable_40_B),
        .enable_40_C (enable_40_C),
        .fast_40_A   (fast_40_A),
        .fast_40_B   (fast_40_B),
        .fast_40_C   (fast_40_C),
        .clk_40_A    (clk_40_A),
        .clk_40_B    (clk_40_B),
        .clk_40_C    (clk_40_C),
        .clk_40_out  (clk_40)
    );


wire  chnl_fifo_overflow_clear_TMR;
common_setup_with_TMR
    common_setup_with_TMR_inst(
        .tck(tck),
        .trst(trst),
        .tdi(tdi),
        .tdo_b(tdo_b),
        .tms(tms),
        .clk_update_A(clk_40_A), .clk_update_B(clk_40_B), .clk_update_C(clk_40_C),
    
        .tdi_to_dr(tdi_to_dr_TMR),
    
        .setup0_shift(setup0_shift_TMR),
        .setup0_update(setup0_update_TMR),
        .setup0_enable(setup0_enable_TMR),
        .setup0_data(setup0_in_TMR),
    
        .setup1_shift(setup1_shift_TMR),
        .setup1_update     (setup1_update_TMR),
        .setup1_enable     (setup1_enable_TMR),
        .setup1_data(setup1_in_TMR),
    
        .setup2_shift(setup2_shift_TMR),
        .setup2_update     (setup2_update_TMR),
        .setup2_enable     (setup2_enable_TMR),
        .setup2_data(setup2_in_TMR),
    
    
        .control0_shift(control0_shift_TMR),
        .control0_update(control0_update_TMR),
        .control0_enable(control0_enable_TMR),
        .control0_data(control0_in_TMR),
    
        .control1_shift(control1_shift_TMR),
        .control1_update(control1_update_TMR),
        .control1_enable(control1_enable_TMR),
        .control1_data(control1_in_TMR),
    
        .status0_shift(status0_shift_TMR),
        .status0_data(status0_in_TMR),
    
        .status1_shift(status1_shift_TMR),
        .status1_data(status1_in_TMR),
    
    
        .phase_clk160(phase_clk160_TMR),
        .phase_clk320_0(phase_clk320_0_TMR), .phase_clk320_1(phase_clk320_1_TMR), .phase_clk320_2(phase_clk320_2_TMR),
        .rst_ePLL(rst_ePLL_TMR),
        
        .ePllResA(ePllResA_TMR), .ePllResB(ePllResB_TMR), .ePllResC(ePllResC_TMR),
        .ePllIcpA(ePllIcpA_TMR), .ePllIcpB(ePllIcpB_TMR), .ePllIcpC(ePllIcpC_TMR),
        .ePllCapA(ePllCapA_TMR), .ePllCapB(ePllCapB_TMR), .ePllCapC(ePllCapC_TMR),
        
        .ePll_lock(ePll_lock),
        
        
        .config_error(config_error_TMR),
        
        
        .debug_port_select(debug_port_select_TMR),
        .chnl_fifo_overflow_clear(chnl_fifo_overflow_clear_TMR),
        .chnl_fifo_overflow(chnl_fifo_overflow),
        .instruction_error(instruction_error_TMR),
        //for TTC interface
        .reset_jtag_in(reset_jtag_in_TMR),
        .event_teset_jtag_in(event_teset_jtag_in_TMR),
        
        .enable_new_ttc(enable_new_ttc_TMR),
        .enable_master_reset_code(enable_master_reset_code_TMR),
        .enable_direct_bunch_reset(enable_direct_bunch_reset_TMR),
        .enable_direct_event_reset(enable_direct_event_reset_TMR),
        .enable_direct_trigger(enable_direct_trigger_TMR),
        
        //for bcr
        .roll_over(roll_over_TMR),
        .coarse_count_offset(coarse_count_offset_TMR),
        .auto_roll_over(auto_roll_over_TMR),
        .bypass_bcr_distribution(bypass_bcr_distribution_TMR),
        
        //channel model
        .enable_trigger(enable_trigger_TMR),
        .channel_data_debug(channel_data_debug_TMR),
        .enable_leading(enable_leading_TMR),
        .enable_pair(enable_pair_TMR),
        .enbale_fake_hit(enbale_fake_hit_TMR),
        .fake_hit_time_interval(fake_hit_time_interval_TMR),
        .rising_is_leading(rising_is_leading_TMR),
        .combine_time_out_config(combine_time_out_config_TMR),
        .channel_enable_r(channel_enable_r_TMR),
        .channel_enable_f(channel_enable_f_TMR),
        
        //for trigger para
        .bunch_offset(bunch_offset_TMR),
        .event_offset(event_offset_TMR),
        .match_window(match_window_TMR),
        
        
        //for readout interface
        .enable_trigger_timeout(enable_trigger_timeout_TMR),
        .enable_high_speed(enable_high_speed_TMR),
        .enable_legacy(enable_legacy_TMR),
        .full_width_res(full_width_res_TMR),
        .width_select(width_select_TMR),
        .enable_8b10b(enable_8b10b_TMR),
        .enable_insert(enable_insert_TMR),
        .syn_packet_number(syn_packet_number_TMR),
        .enable_error_packet(enable_error_packet_TMR),
        .enable_TDC_ID(enable_TDC_ID_TMR),
        .TDC_ID(TDC_ID_TMR),
        .enable_error_notify(enable_error_notify_TMR),
        
        
        .fine_sel(fine_sel_TMR),
        .lut0(lut0_TMR),.lut1(lut1_TMR),.lut2(lut2_TMR),.lut3(lut3_TMR),.lut4(lut4_TMR),.lut5(lut5_TMR),.lut6(lut6_TMR),.lut7(lut7_TMR),
        .lut8(lut8_TMR),.lut9(lut9_TMR),.luta(luta_TMR),.lutb(lutb_TMR),.lutc(lutc_TMR),.lutd(lutd_TMR),.lute(lute_TMR),.lutf(lutf_TMR)
    );

wire [299:0] config_output_TMR;
wire [114:0] setup0_output_TMR;
wire [93:0] setup1_output_TMR;
wire [35:0] setup2_output_TMR;
wire [7:0] control0_output_TMR;
wire [46:0] control1_output_TMR;

assign setup0_output_TMR = {
enable_new_ttc_TMR,
enable_master_reset_code_TMR,
enable_direct_bunch_reset_TMR,
enable_direct_event_reset_TMR,
enable_direct_trigger_TMR,
auto_roll_over_TMR,
bypass_bcr_distribution_TMR,
enable_trigger_TMR,
channel_data_debug_TMR,
enable_leading_TMR,
enable_pair_TMR,
enbale_fake_hit_TMR,
rising_is_leading_TMR,
channel_enable_r_TMR,
channel_enable_f_TMR,
TDC_ID_TMR,
enable_trigger_timeout_TMR,
enable_high_speed_TMR,
enable_legacy_TMR,
full_width_res_TMR,
width_select_TMR,
enable_8b10b_TMR,
enable_insert_TMR,
enable_error_packet_TMR,
enable_TDC_ID_TMR,
enable_error_notify_TMR};

assign setup1_output_TMR = {
combine_time_out_config_TMR,
fake_hit_time_interval_TMR,
syn_packet_number_TMR,
roll_over_TMR,
coarse_count_offset_TMR,
bunch_offset_TMR,
event_offset_TMR,
match_window_TMR};

assign setup2_output_TMR = {
fine_sel_TMR,
lut0_TMR,lut1_TMR,lut2_TMR,lut3_TMR,lut4_TMR,lut5_TMR,lut6_TMR,lut7_TMR,
lut8_TMR,lut9_TMR,luta_TMR,lutb_TMR,lutc_TMR,lutd_TMR,lute_TMR,lutf_TMR};

assign control0_output_TMR = {
rst_ePLL_TMR,
reset_jtag_in_TMR,
event_teset_jtag_in_TMR,
chnl_fifo_overflow_clear_TMR,
debug_port_select_TMR};

assign control1_output_TMR = {
phase_clk160_TMR,
phase_clk320_0_TMR,phase_clk320_1_TMR, phase_clk320_2_TMR,
ePllResA_TMR,ePllIcpA_TMR,ePllCapA_TMR,
ePllResB_TMR,ePllIcpB_TMR,ePllCapB_TMR,
ePllResC_TMR,ePllIcpC_TMR,ePllCapC_TMR};

assign config_output_TMR={setup0_output_TMR, setup1_output_TMR, setup2_output_TMR, control0_output_TMR, control1_output_TMR};









wire JTAG_busy;
wire [511:0] JTAG_data_out;
reg  start_action;
reg  [11:0] config_period;
reg  [511:0] JTAG_bits;
reg  [8:0] bit_length;
reg  [4:0] JTAG_inst;

wire tck_jtag,tms_jtag,tdi_jtag;
JTAG_master 
    JTAG_master_inst(
        .clk (clk_160),
        .TCK (tck_jtag),
        .TMS (tms_jtag),
        .TDI (tdi_jtag),
        .TDO (tdo),
        .start_action  (start_action),
        .config_period (config_period),
        .JTAG_bits     (JTAG_bits),
        .bit_length    (bit_length),
        .JTAG_inst     (JTAG_inst),
        .JTAG_data_out (JTAG_data_out),
        .JTAG_busy     (JTAG_busy)
    );


wire SPI_busy;
wire [511:0] SPI_data_out;
reg  [511:0] SPI_bits;

wire tck_spi,tms_spi,tdi_spi;
SPI_master 
    SPI_master_inst(
        .clk           (clk_160),
        .TCK           (tck_spi),
        .TMS           (tms_spi),
        .TDI           (tdi_spi),
        .TDO           (tdo),
        .start_action  (start_action),
        .config_period (config_period),
        .SPI_bits      (SPI_bits),
        .bit_length    (bit_length),
        .SPI_data_out  (SPI_data_out),
        .SPI_busy      (SPI_busy)
    );
assign tck = trst ? tck_jtag : tck_spi;
// assign tms =tms_spi ;
assign tms = trst ? tms_jtag : tms_spi;
assign tdi = trst ? tdi_jtag : tdi_spi;





task JTAG_config(
    input [511:0] JTAG_content,
    input [8:0] JTAG_length,
    input [4:0] JTAG_command
);begin
    @(negedge clk_40M)
    JTAG_bits   <= JTAG_content;
    bit_length  <= JTAG_length;
    JTAG_inst   <= JTAG_command;
    check_enable <= 1'b0;
    start_action <=1'b1;
    @(negedge clk_40M)
    start_action <=1'b0;
    @(negedge JTAG_busy);
    check_enable <= 1'b1;

end
endtask


task SPI_config(
    input [511:0] SPI_content,
    input [8:0] SPI_length
);begin
    @(negedge clk_40M)
    SPI_bits   <= SPI_content;
    bit_length  <= SPI_length;
    check_enable <= 1'b0;
    start_action <=1'b1;
    @(negedge clk_40M)
    start_action <=1'b0;
    @(negedge JTAG_busy);
    check_enable <= 1'b1;

end
endtask
parameter   idcode = 32'b10000100011100001101101011001110;
parameter   setup_0_reg = 115'b0000010000101111111111111111111111111111111111111111111111111111111111111111111111111111010101010101010010000010000;                               
parameter   setup_1_reg = 94'b0000101000000100000000111111111111111111111111000000000000111110011100000000000000000000011111;
parameter   setup_2_reg = 36'b001100011001110010100000000111001100;
parameter   control_0_reg = 8'b00000000;
parameter   control_1_reg = 47'b00000010000000010001001001000100100100010010010;
parameter   spi_reg = 300'h042ffffffffffffffffff0aaaa820140807fffff8007ce00000f98ce500e600010089224892;

wire normal_output_input_equal;
assign normal_output_input_equal = ~|(config_output ^ {setup_0_reg, setup_1_reg, setup_2_reg, control_0_reg, control_1_reg}) ;
wire TMR_output_input_equal;
assign TMR_output_input_equal = ~|(config_output_TMR ^ {setup_0_reg, setup_1_reg, setup_2_reg, control_0_reg, control_1_reg}) ;

wire TMR_normal_output_equal;
assign TMR_normal_output_equal = ~|(config_output ^ config_output_TMR) ;

wire SPI_TMR_output_input_equal;
assign SPI_TMR_output_input_equal = ~|(config_output_TMR ^ spi_reg) ;


always @(posedge clk_160) begin
    if(check_enable) begin
        if(~TMR_normal_output_equal) begin
            $display("TMR_output != golden output!");
            $stop;
        end 
    end
end

reg check_enable_reg = 1'b1;

task force_reg(
  input integer reg_index,
  input real delay_time);
begin
    check_enable_reg = 1'b0;
    @(posedge clk_160); //register only have SEU after posedge 
    #delay_time;
    enable_force_reg[reg_index]= 1'b1;
    #0.001;
    enable_force_reg[reg_index]= 1'b0;     
end
endtask


task single_reg_TMR_test(
  input integer reg_index,
  input integer repeat_number);
  real delay_time;
begin    
    //for one SEU
    repeat(repeat_number)begin  //
        #($itor($urandom_range(100,1))/100*2);  //randomly wait for 0.01 to 100 ns
        delay_time =  $itor($urandom_range(9990,10))/10000*1.5625*4;//randomly wait for 0.001 to 0.999 clk160
        force_reg(reg_index,delay_time);
        check_enable_reg = 1'b1;
    end
end
endtask

task reg_TMR_test(
  input integer repeat_number);
  integer i;
begin
    for (i = 0; i < `REG_NUM; i = i + 1)begin
      single_reg_TMR_test(i,repeat_number);
    end
    #100;
    if(check_enable_reg)$display("check_enable is on.");
    else $display("warning: check_enable off!!");
    $display("reg TMR test passed.");

end
endtask

// task force_clk_40(input integer repeat_number);
// real delay_time;
// real pulse_width;
// begin
//     repeat(repeat_number)begin //for one glitch
//         #($itor($urandom_range(10000,1))/100);//randomly wait for 0.01 to 100 ns
//         delay_time = $urandom_range(10000,1);
//         delay_time = delay_time/10000*1.5625;        
//         pulse_width = $urandom_range(10000,1);
//         pulse_width = pulse_width/10000*1.5625;
//         @(posedge clk40 or negedge clk40);
//         #(delay_time);
//         if(clk40)begin 
//             force clk40 = 1'b0;
//             #(pulse_width);
//             force clk40 = 1'b1;
//             release clk40;
//         end else begin
//             force clk40 = 1'b1;
//             #(pulse_width);
//             force clk40 = 1'b0;
//             release clk40;
//         end
//         $display("At%t glitch occurs",$realtime);
//     end
    
// end
// endtask
task perform_test();begin
    $display("At %t JTAG_config start!", $realtime);
    JTAG_config(setup_0_reg,9'd114,5'h12);
    $display("At %t setup_0_reg configured!", $realtime);
    #100;
    JTAG_config(setup_1_reg,9'd93,5'h03);
    $display("At %t setup_1_reg configured!", $realtime);

    #100;
    JTAG_config(setup_2_reg,9'd35,5'h14);
    $display("At %t setup_2_reg configured!", $realtime);
    #100;
    JTAG_config(control_0_reg,9'd7,5'h05);
    $display("At %t control_0_reg configured!", $realtime);
    #100;
    JTAG_config(control_1_reg,9'd46,5'h06);
    $display("At %t control_1_reg configured!", $realtime);
    #1000;

    $display("At %t SPI_config start!", $realtime);
    SPI_config(spi_reg,9'd299);
    $display("At %t SPI_config finished!", $realtime);

    #1000;
end
endtask
    integer reg_force_count = 1;
initial begin
    $timeformat(-9, 5, "ns", 8);
    trst = 1'b1;
	start_action = 1'b0;
	config_period = 'b1;
	JTAG_bits = 'b0;
	bit_length = 'b0;
	JTAG_inst = 5'h06;
    SPI_bits = 512'b0;
    check_enable = 1'b0;
    #5;
    trst = 1'b0;
	#100;
	trst = 1'b1;
	#100;
    check_enable = 1'b1;	
    #100;

    enable_40_A = 1'b1;enable_40_B = 1'b1;enable_40_C = 1'b1;
      fast_40_A = 1'b0;  fast_40_B = 1'b0;  fast_40_C = 1'b0;
      repeat(100)begin
          perform_test();
      end
    
    #300;
    // reg_TMR_test(reg_force_count);
    // #300;
    // enable_40_A = 1'b0;enable_40_B = 1'b1;enable_40_C = 1'b1;
    //   fast_40_A = 1'b0;  fast_40_B = 1'b0;  fast_40_C = 1'b0;
    // perform_test();
    //     #300;
    // reg_TMR_test(reg_force_count);
    // #300;
    // enable_40_A = 1'b1;enable_40_B = 1'b0;enable_40_C = 1'b1;
    //   fast_40_A = 1'b0;  fast_40_B = 1'b0;  fast_40_C = 1'b0;
    // perform_test();
    //     #300;
    // reg_TMR_test(reg_force_count);
    // #300;
    // enable_40_A = 1'b1;enable_40_B = 1'b1;enable_40_C = 1'b0;
    //   fast_40_A = 1'b0;  fast_40_B = 1'b0;  fast_40_C = 1'b0;
    // perform_test();
    //     #300;
    // reg_TMR_test(reg_force_count);
    // #300;
    // enable_40_A = 1'b1;enable_40_B = 1'b1;enable_40_C = 1'b1;
    //   fast_40_A = 1'b1;  fast_40_B = 1'b0;  fast_40_C = 1'b0;
    // perform_test();
    //     #300;
    // reg_TMR_test(reg_force_count);
    // #300;
    // enable_40_A = 1'b1;enable_40_B = 1'b1;enable_40_C = 1'b1;
    //   fast_40_A = 1'b0;  fast_40_B = 1'b1;  fast_40_C = 1'b0;
    // perform_test();
    //     #300;
    // reg_TMR_test(reg_force_count);
    // #300;
    // enable_40_A = 1'b1;enable_40_B = 1'b1;enable_40_C = 1'b1;
    //   fast_40_A = 1'b0;  fast_40_B = 1'b0;  fast_40_C = 1'b1;
    // perform_test();
    //     #300;
    // reg_TMR_test(reg_force_count);
    // #300;

    // force_clk_40(1000);
    // TMR_clk_generator_inst.clock_TMR_test(1000);
    // #100;
    
    // #300;
    // trst = 1'b0;
    // #300;










    $stop;
end


endmodule
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : bcr_distribution_TMR_sim.v
//  Author     : Yuxiang Guo
//  Revision   : 
//               First created on Mar 27th, 2019
//  Note       : 
// 

`define REG_LIST
module bcr_distribution_TMR_sim();    
`include "bcr_distribution_TMR_sim_reglist.v"
reg master_reset;
reg bcr;
reg [11:0] roll_over;
reg [11:0] coarse_count_offset;
reg auto_roll_over;
reg bypass_bcr_distribution;
wire coarse_bcr;
wire trigger_count_bcr;



reg check_enable = 1'b1;
wire clk160, clk160_A, clk160_B, clk160_C;
TMR_clk_generator TMR_clk_generator_inst (
  .enable320(1'b0),
  .clk(clk160), 
  .clk_A(clk160_A), 
  .clk_B(clk160_B), 
  .clk_C(clk160_C),
  .check_enable()
  );


bcr_distribution
	bcr_distribution_inst(
	.clk160(clk160),
//	.clk320(clk320),
	.rst(master_reset),

	.bcr(bcr),
	.roll_over(roll_over),
	.coarse_count_offset(coarse_count_offset),
	//input [11:0] bunch_offset,

	.auto_roll_over(auto_roll_over),
	.bypass_bcr_distribution(bypass_bcr_distribution),

	.coarse_bcr(coarse_bcr),
	.trigger_count_bcr(trigger_count_bcr)
);

wire coarse_bcr_TMR;
wire trigger_count_bcr_TMR;

  bcr_distribution_TMR
	bcr_distribution_TMR_inst(
	.clk160_A(clk160_A),.clk160_B(clk160_B),.clk160_C(clk160_C),
//	.clk320(clk320),
	.rst(master_reset),

	.bcr(bcr),
	.roll_over(roll_over),
	.coarse_count_offset(coarse_count_offset),
	//input [11:0] bunch_offset,

	.auto_roll_over(auto_roll_over),
	.bypass_bcr_distribution(bypass_bcr_distribution),

	.coarse_bcr(coarse_bcr_TMR),
	.trigger_count_bcr(trigger_count_bcr_TMR)
);


wire TMR_normal_equal;
assign TMR_normal_equal = (coarse_bcr == coarse_bcr_TMR) & (trigger_count_bcr == trigger_count_bcr_TMR);
reg TMR_normal_equal_r;
always @(posedge clk160) begin
	TMR_normal_equal_r <= TMR_normal_equal;
end
always @(posedge clk160) begin
	if (check_enable) begin
		if(~TMR_normal_equal_r)begin
			$display("At %t TMR outuput is not equal to normal output!" ,$realtime);
			$stop;
		end	
	end
end


always  begin
	@(negedge clk160);
	bcr = 1'b1;
	@(negedge clk160);
	@(negedge clk160);
	@(negedge clk160);
	@(negedge clk160);
	bcr = 1'b0;
	#1000;
end



reg check_enable_reg;
task force_reg(
  input integer reg_index,
  input real delay_time);
begin
    check_enable_reg = 1'b0;
    @(posedge clk160); //register only have SEU after posedge clk320
    #delay_time;
    enable_force_reg[reg_index]= 1'b1;
    #0.001;
    enable_force_reg[reg_index]= 1'b0;     
end
endtask


task single_reg_TMR_test(
  input integer reg_index,
  input integer repeat_number);
  real delay_time;
begin    
    //for one SEU
    repeat(repeat_number)begin  //
        #($itor($urandom_range(10000,1))/100);  //randomly wait for 0.01 to 100 ns
        delay_time =  $itor($urandom_range(9990,10))/10000*1.5625*2;//randomly wait for 0.001 to 0.999 clk320
        force_reg(reg_index,delay_time);
        check_enable_reg = 1'b1;
    end
end
endtask

task reg_TMR_test(
  input integer repeat_number);
  integer i;
begin
    for (i = 0; i < `REG_NUM; i = i + 1)begin
      single_reg_TMR_test(i,repeat_number);
    end
    #100;
    if(check_enable)$display("check_enable is on.");
    else $display("warning: check_enable off!!");
    $display("reg TMR test passed.");

end
endtask


initial begin
	$timeformat(-9, 5, "ns", 8);
	master_reset = 1'b1;
	bcr = 1'b0;


	roll_over = 12'd20;
	coarse_count_offset = 12'd5;


    // bypass bcr, no internal effect
	auto_roll_over = 1'b0;
	bypass_bcr_distribution = 1'b1;
	#300;
	master_reset = 1'b0;
	#1000;


	// bcr distribution without auto roll over, only bcr resets global counter
	#1000;
	@(negedge clk160);
	auto_roll_over = 1'b0;
	bypass_bcr_distribution = 1'b0;
	#500;
	master_reset = 1'b1;
	#300;
	master_reset = 1'b0;
	#300;
	
	#10000; //global counter keeps growing all time
	TMR_clk_generator_inst.clock_TMR_test(1000);
	#100;
	reg_TMR_test(1000);


	// bcr distribution with auto roll over, bcr has no effect
	#1000;
	@(negedge clk160);
	auto_roll_over = 1'b1;
	bypass_bcr_distribution = 1'b0;
	#300;
	master_reset = 1'b1;
	#300;
	master_reset = 1'b0;

	#300;
	@(negedge clk160);
	master_reset = 1'b1;
	@(posedge clk160);
	@(negedge clk160);
	master_reset = 1'b0;
	#300;
	TMR_clk_generator_inst.clock_TMR_test(1000);
	#100;
	reg_TMR_test(1000);




	#1000;
	$stop;





end
endmodule


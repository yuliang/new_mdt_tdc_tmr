/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : mEnc8b10bMem_tb_sim.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 5st, 2018
//  Note       : verify mEnc8b10bMem_tb module
// 
`define REG_LIST
module mEnc8b10bMem_tb_sim(
);
`include "mEnc8b10bMem_tb_sim_reglist.v"
reg rst;
reg [7:0] i8_Din;
reg i_Kin;
wire [9:0] o10_Dout;
wire o_Rd;
wire o_KErr;
reg i_Disparity;

reg check_enable;
wire clk_160, clk160_A, clk160_B, clk160_C;
TMR_clk_generator TMR_clk_generator_inst (
  .enable320(1'b0),
  .clk(clk_160), 
  .clk_A(clk160_A), 
  .clk_B(clk160_B), 
  .clk_C(clk160_C),
  .check_enable()
  );


mEnc8b10bMem_tb
	mEnc8b10bMem_tb_inst(
		.i8_Din(i8_Din),		//HGFEDCBA
		.i_Kin(i_Kin),
		.i_ForceDisparity(1'b0),
		.i_Disparity(i_Disparity),		//1 Is negative, 0 is positive	
		.o10_Dout(o10_Dout),	//abcdeifghj
		.o_Rd(o_Rd),
		.o_KErr(o_KErr),
		.i_Clk(clk_160),
		.i_ARst_L(rst),

		.i_enable(1'b1),
		.soft_reset_i(1'b0)
    );



wire [9:0] o10_Dout_TMR;
wire o_Rd_TMR;
wire o_KErr_TMR;
reg i_Disparity_TMR;
mEnc8b10bMem_tb_TMR
	mEnc8b10bMem_tb_TMR_inst(
		.i8_Din(i8_Din),		//HGFEDCBA
		.i_Kin(i_Kin),
		.i_ForceDisparity(1'b0),
		.i_Disparity(i_Disparity_TMR),		//1 Is negative, 0 is positive	
		.o10_Dout(o10_Dout_TMR),	//abcdeifghj
		.o_Rd(o_Rd_TMR),
		.o_KErr(o_KErr_TMR),
		.i_Clk_A(clk160_A),
		.i_Clk_B(clk160_B),
		.i_Clk_C(clk160_C),

		.i_enable(1'b1),
		.soft_reset_i(1'b0)
    );


wire TMR_normal_equal;
assign TMR_normal_equal = (o10_Dout == o10_Dout_TMR) & (o_Rd == o_Rd_TMR) &(o_KErr==o_KErr_TMR);
reg TMR_normal_equal_r;
always @(posedge clk_160) begin
	TMR_normal_equal_r <= TMR_normal_equal;
end


always @(negedge clk_160) begin
	i8_Din <= $urandom;
end

always @(posedge clk_160) begin
	if (check_enable) begin
		if(~TMR_normal_equal_r)begin
			$display("At %t TMR outuput is not equal to normal output!" ,$realtime);
			$stop;
		end	
	end
end



reg check_enable_reg;
task force_reg(
  input integer reg_index,
  input real delay_time);
begin
    check_enable_reg = 1'b0;
    @(posedge clk_160); //register only have SEU after posedge clk320
    #delay_time;
    enable_force_reg[reg_index]= 1'b1;
    #0.001;
    enable_force_reg[reg_index]= 1'b0;     
end
endtask


task single_reg_TMR_test(
  input integer reg_index,
  input integer repeat_number);
  real delay_time;
begin    
    //for one SEU
    repeat(repeat_number)begin  //
        #($itor($urandom_range(10000,1))/100);  //randomly wait for 0.01 to 100 ns
        delay_time =  $itor($urandom_range(9990,10))/10000*1.5625*2;//randomly wait for 0.001 to 0.999 clk320
        force_reg(reg_index,delay_time);
        check_enable_reg = 1'b1;
    end
end
endtask

task reg_TMR_test(
  input integer repeat_number);
  integer i;
begin
    for (i = 0; i < `REG_NUM; i = i + 1)begin
      single_reg_TMR_test(i,repeat_number);
    end
    #100;
    if(check_enable)$display("check_enable is on.");
    else $display("warning: check_enable off!!");
    $display("reg TMR test passed.");

end
endtask



initial begin
	$timeformat(-9, 5, "ns", 8);
	rst = 1;

	#300;
	@(negedge clk_160);	
	rst = 0;
	#100;
	check_enable = 1;
	i_Kin = 0;
	i_Disparity = 0;
	i_Disparity_TMR = 0;
	#10000000;
	$display("At %t clk TMR test start" ,$realtime);
	TMR_clk_generator_inst.clock_TMR_test(1000);
	#100;
	reg_TMR_test(1000);
	@(negedge clk_160);
	i_Kin = 1;
	#10000000;
	$display("At %t clk TMR test start" ,$realtime);
	TMR_clk_generator_inst.clock_TMR_test(1000);
	#100;
	reg_TMR_test(1000);
	$stop;
	
end
// wire [7:0] deserial_data;
// wire o_Kout,o_DErr,o_KErr,o_DpErr;
// mDec8b10bMem_tb
//   inst_mDec8b10bMem_tb (
//     .o8_Dout          (deserial_data),
//     .o_Kout           (o_Kout),
//     .o_DErr           (o_DErr),
//     .o_KErr           (o_KErr),
//     .o_DpErr          (o_DpErr),
//     .i_ForceDisparity (1'b0),
//     .i_Disparity      (1'b0),
//     .i10_Din          (deserial_raw_data),
//     .o_Rd             (),
//     .i_Clk            (clk_160),
//     .i_ARst_L         (~reset),
//     .soft_reset_i     (1'b0),
//     .i_enable         (i_enable)
//   );

// wire [7:0] deserial_data_TMR;
// wire o_Kout_TMR,o_DErr_TMR,o_KErr_TMR,o_DpErr_TMR;
//  mDec8b10bMem_tb_TMR
//   inst_mDec8b10bMem_tb_TMR (
//     .o8_Dout          (deserial_data_TMR),
//     .o_Kout           (o_Kout_TMR),
//     .o_DErr           (o_DErr_TMR),
//     .o_KErr           (o_KErr_TMR),
//     .o_DpErr          (o_DpErr_TMR),
//     .i_ForceDisparity (1'b0),
//     .i_Disparity      (1'b0),
//     .i10_Din          (o10_Dout_TMR),
//     .o_Rd             (),
//     .i_Clk            (clk_160),
//     .i_ARst_L         (~reset),
//     .soft_reset_i     (1'b0),
//     .i_enable         (i_enable)
//   );












endmodule
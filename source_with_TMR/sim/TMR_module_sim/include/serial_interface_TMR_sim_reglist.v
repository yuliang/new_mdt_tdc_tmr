`ifdef REG_LIST
`define REG_NUM 14
reg [`REG_NUM-1:0] enable_force_reg = 1'b0;
reg [3:0] send_number_A_tmp;
`define force_r serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_A
always @(posedge enable_force_reg[0]) begin
  send_number_A_tmp = `force_r;
  force `force_r = 4'b1000 ^ send_number_A_tmp;
  $display("At%t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_A[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[0]) begin
   release `force_r; $display("At %t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_A[3] released",$realtime);
end
always @(posedge enable_force_reg[1]) begin
  send_number_A_tmp = `force_r;
  force `force_r = 4'b0100 ^ send_number_A_tmp;
  $display("At%t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_A[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[1]) begin
   release `force_r; $display("At %t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_A[2] released",$realtime);
end
always @(posedge enable_force_reg[2]) begin
  send_number_A_tmp = `force_r;
  force `force_r = 4'b0010 ^ send_number_A_tmp;
  $display("At%t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_A[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[2]) begin
   release `force_r; $display("At %t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_A[1] released",$realtime);
end
always @(posedge enable_force_reg[3]) begin
  send_number_A_tmp = `force_r;
  force `force_r = 4'b0001 ^ send_number_A_tmp;
  $display("At%t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_A[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[3]) begin
   release `force_r; $display("At %t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_A[0] released",$realtime);
end
reg [3:0] send_number_B_tmp;
`define force_r serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_B
always @(posedge enable_force_reg[4]) begin
  send_number_B_tmp = `force_r;
  force `force_r = 4'b1000 ^ send_number_B_tmp;
  $display("At%t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_B[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[4]) begin
   release `force_r; $display("At %t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_B[3] released",$realtime);
end
always @(posedge enable_force_reg[5]) begin
  send_number_B_tmp = `force_r;
  force `force_r = 4'b0100 ^ send_number_B_tmp;
  $display("At%t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_B[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[5]) begin
   release `force_r; $display("At %t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_B[2] released",$realtime);
end
always @(posedge enable_force_reg[6]) begin
  send_number_B_tmp = `force_r;
  force `force_r = 4'b0010 ^ send_number_B_tmp;
  $display("At%t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_B[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[6]) begin
   release `force_r; $display("At %t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_B[1] released",$realtime);
end
always @(posedge enable_force_reg[7]) begin
  send_number_B_tmp = `force_r;
  force `force_r = 4'b0001 ^ send_number_B_tmp;
  $display("At%t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_B[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[7]) begin
   release `force_r; $display("At %t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_B[0] released",$realtime);
end
reg [3:0] send_number_C_tmp;
`define force_r serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_C
always @(posedge enable_force_reg[8]) begin
  send_number_C_tmp = `force_r;
  force `force_r = 4'b1000 ^ send_number_C_tmp;
  $display("At%t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_C[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[8]) begin
   release `force_r; $display("At %t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_C[3] released",$realtime);
end
always @(posedge enable_force_reg[9]) begin
  send_number_C_tmp = `force_r;
  force `force_r = 4'b0100 ^ send_number_C_tmp;
  $display("At%t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_C[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[9]) begin
   release `force_r; $display("At %t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_C[2] released",$realtime);
end
always @(posedge enable_force_reg[10]) begin
  send_number_C_tmp = `force_r;
  force `force_r = 4'b0010 ^ send_number_C_tmp;
  $display("At%t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_C[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[10]) begin
   release `force_r; $display("At %t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_C[1] released",$realtime);
end
always @(posedge enable_force_reg[11]) begin
  send_number_C_tmp = `force_r;
  force `force_r = 4'b0001 ^ send_number_C_tmp;
  $display("At%t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_C[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[11]) begin
   release `force_r; $display("At %t serial_interface_TMR_inst.serial_interface_tick_generator_inst.send_number_C[0] released",$realtime);
end
reg tick_vote_tmp;
`define force_r serial_interface_TMR_inst.serial_interface_tick_generator_inst.tick_vote
always @(posedge enable_force_reg[12]) begin
  tick_vote_tmp = `force_r;
  force `force_r = 1'b1 ^ tick_vote_tmp;
  $display("At%t serial_interface_TMR_inst.serial_interface_tick_generator_inst.tick_vote force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[12]) begin
  release `force_r;   $display("At %t serial_interface_TMR_inst.serial_interface_tick_generator_inst.tick_vote released",$realtime);
end
reg fifo_read_tmp;
`define force_r serial_interface_TMR_inst.fifo_read
always @(posedge enable_force_reg[13]) begin
  fifo_read_tmp = `force_r;
  force `force_r = 1'b1 ^ fifo_read_tmp;
  $display("At%t serial_interface_TMR_inst.fifo_read force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[13]) begin
  release `force_r;   $display("At %t serial_interface_TMR_inst.fifo_read released",$realtime);
end
`endif
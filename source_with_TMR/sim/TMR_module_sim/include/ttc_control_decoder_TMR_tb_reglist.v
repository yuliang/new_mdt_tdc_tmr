`ifdef REG_LIST
`define REG_NUM 96
reg [`REG_NUM-1:0] enable_force_reg = 1'b0;
reg [1:0] ttc_control_decoder_TMR_inst_reset_jtag_syn_A_tmp;
`define force_r ttc_control_decoder_TMR_inst.reset_jtag_syn_A
always @(posedge enable_force_reg[0]) begin
  ttc_control_decoder_TMR_inst_reset_jtag_syn_A_tmp = `force_r;
  force `force_r = 2'b10 ^ ttc_control_decoder_TMR_inst_reset_jtag_syn_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.reset_jtag_syn_A[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[0]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.reset_jtag_syn_A[1] released",$realtime);
end
always @(posedge enable_force_reg[1]) begin
  ttc_control_decoder_TMR_inst_reset_jtag_syn_A_tmp = `force_r;
  force `force_r = 2'b01 ^ ttc_control_decoder_TMR_inst_reset_jtag_syn_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.reset_jtag_syn_A[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[1]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.reset_jtag_syn_A[0] released",$realtime);
end
reg [1:0] ttc_control_decoder_TMR_inst_reset_jtag_syn_B_tmp;
`define force_r ttc_control_decoder_TMR_inst.reset_jtag_syn_B
always @(posedge enable_force_reg[2]) begin
  ttc_control_decoder_TMR_inst_reset_jtag_syn_B_tmp = `force_r;
  force `force_r = 2'b10 ^ ttc_control_decoder_TMR_inst_reset_jtag_syn_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.reset_jtag_syn_B[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[2]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.reset_jtag_syn_B[1] released",$realtime);
end
always @(posedge enable_force_reg[3]) begin
  ttc_control_decoder_TMR_inst_reset_jtag_syn_B_tmp = `force_r;
  force `force_r = 2'b01 ^ ttc_control_decoder_TMR_inst_reset_jtag_syn_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.reset_jtag_syn_B[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[3]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.reset_jtag_syn_B[0] released",$realtime);
end
reg [1:0] ttc_control_decoder_TMR_inst_reset_jtag_syn_C_tmp;
`define force_r ttc_control_decoder_TMR_inst.reset_jtag_syn_C
always @(posedge enable_force_reg[4]) begin
  ttc_control_decoder_TMR_inst_reset_jtag_syn_C_tmp = `force_r;
  force `force_r = 2'b10 ^ ttc_control_decoder_TMR_inst_reset_jtag_syn_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.reset_jtag_syn_C[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[4]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.reset_jtag_syn_C[1] released",$realtime);
end
always @(posedge enable_force_reg[5]) begin
  ttc_control_decoder_TMR_inst_reset_jtag_syn_C_tmp = `force_r;
  force `force_r = 2'b01 ^ ttc_control_decoder_TMR_inst_reset_jtag_syn_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.reset_jtag_syn_C[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[5]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.reset_jtag_syn_C[0] released",$realtime);
end
reg ttc_control_decoder_TMR_inst_master_reset_r_A_tmp;
`define force_r ttc_control_decoder_TMR_inst.master_reset_r_A
always @(posedge enable_force_reg[6]) begin
  ttc_control_decoder_TMR_inst_master_reset_r_A_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_master_reset_r_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.master_reset_r_A force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[6]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.master_reset_r_A released",$realtime);
end
reg ttc_control_decoder_TMR_inst_master_reset_r_B_tmp;
`define force_r ttc_control_decoder_TMR_inst.master_reset_r_B
always @(posedge enable_force_reg[7]) begin
  ttc_control_decoder_TMR_inst_master_reset_r_B_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_master_reset_r_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.master_reset_r_B force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[7]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.master_reset_r_B released",$realtime);
end
reg ttc_control_decoder_TMR_inst_master_reset_r_C_tmp;
`define force_r ttc_control_decoder_TMR_inst.master_reset_r_C
always @(posedge enable_force_reg[8]) begin
  ttc_control_decoder_TMR_inst_master_reset_r_C_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_master_reset_r_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.master_reset_r_C force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[8]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.master_reset_r_C released",$realtime);
end
reg [2:0] ttc_control_decoder_TMR_inst_reset_r_A_tmp;
`define force_r ttc_control_decoder_TMR_inst.reset_r_A
always @(posedge enable_force_reg[9]) begin
  ttc_control_decoder_TMR_inst_reset_r_A_tmp = `force_r;
  force `force_r = 3'b100 ^ ttc_control_decoder_TMR_inst_reset_r_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.reset_r_A[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[9]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.reset_r_A[2] released",$realtime);
end
always @(posedge enable_force_reg[10]) begin
  ttc_control_decoder_TMR_inst_reset_r_A_tmp = `force_r;
  force `force_r = 3'b010 ^ ttc_control_decoder_TMR_inst_reset_r_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.reset_r_A[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[10]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.reset_r_A[1] released",$realtime);
end
always @(posedge enable_force_reg[11]) begin
  ttc_control_decoder_TMR_inst_reset_r_A_tmp = `force_r;
  force `force_r = 3'b001 ^ ttc_control_decoder_TMR_inst_reset_r_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.reset_r_A[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[11]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.reset_r_A[0] released",$realtime);
end
reg [2:0] ttc_control_decoder_TMR_inst_reset_r_B_tmp;
`define force_r ttc_control_decoder_TMR_inst.reset_r_B
always @(posedge enable_force_reg[12]) begin
  ttc_control_decoder_TMR_inst_reset_r_B_tmp = `force_r;
  force `force_r = 3'b100 ^ ttc_control_decoder_TMR_inst_reset_r_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.reset_r_B[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[12]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.reset_r_B[2] released",$realtime);
end
always @(posedge enable_force_reg[13]) begin
  ttc_control_decoder_TMR_inst_reset_r_B_tmp = `force_r;
  force `force_r = 3'b010 ^ ttc_control_decoder_TMR_inst_reset_r_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.reset_r_B[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[13]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.reset_r_B[1] released",$realtime);
end
always @(posedge enable_force_reg[14]) begin
  ttc_control_decoder_TMR_inst_reset_r_B_tmp = `force_r;
  force `force_r = 3'b001 ^ ttc_control_decoder_TMR_inst_reset_r_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.reset_r_B[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[14]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.reset_r_B[0] released",$realtime);
end
reg [2:0] ttc_control_decoder_TMR_inst_reset_r_C_tmp;
`define force_r ttc_control_decoder_TMR_inst.reset_r_C
always @(posedge enable_force_reg[15]) begin
  ttc_control_decoder_TMR_inst_reset_r_C_tmp = `force_r;
  force `force_r = 3'b100 ^ ttc_control_decoder_TMR_inst_reset_r_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.reset_r_C[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[15]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.reset_r_C[2] released",$realtime);
end
always @(posedge enable_force_reg[16]) begin
  ttc_control_decoder_TMR_inst_reset_r_C_tmp = `force_r;
  force `force_r = 3'b010 ^ ttc_control_decoder_TMR_inst_reset_r_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.reset_r_C[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[16]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.reset_r_C[1] released",$realtime);
end
always @(posedge enable_force_reg[17]) begin
  ttc_control_decoder_TMR_inst_reset_r_C_tmp = `force_r;
  force `force_r = 3'b001 ^ ttc_control_decoder_TMR_inst_reset_r_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.reset_r_C[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[17]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.reset_r_C[0] released",$realtime);
end
reg ttc_control_decoder_TMR_inst_bunch_reset_direct_in_r_A_tmp;
`define force_r ttc_control_decoder_TMR_inst.bunch_reset_direct_in_r_A
always @(posedge enable_force_reg[18]) begin
  ttc_control_decoder_TMR_inst_bunch_reset_direct_in_r_A_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_bunch_reset_direct_in_r_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bunch_reset_direct_in_r_A force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[18]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.bunch_reset_direct_in_r_A released",$realtime);
end
reg ttc_control_decoder_TMR_inst_bunch_reset_direct_in_r_B_tmp;
`define force_r ttc_control_decoder_TMR_inst.bunch_reset_direct_in_r_B
always @(posedge enable_force_reg[19]) begin
  ttc_control_decoder_TMR_inst_bunch_reset_direct_in_r_B_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_bunch_reset_direct_in_r_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bunch_reset_direct_in_r_B force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[19]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.bunch_reset_direct_in_r_B released",$realtime);
end
reg ttc_control_decoder_TMR_inst_bunch_reset_direct_in_r_C_tmp;
`define force_r ttc_control_decoder_TMR_inst.bunch_reset_direct_in_r_C
always @(posedge enable_force_reg[20]) begin
  ttc_control_decoder_TMR_inst_bunch_reset_direct_in_r_C_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_bunch_reset_direct_in_r_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bunch_reset_direct_in_r_C force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[20]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.bunch_reset_direct_in_r_C released",$realtime);
end
reg ttc_control_decoder_TMR_inst_bcr_A_tmp;
`define force_r ttc_control_decoder_TMR_inst.bcr_A
always @(posedge enable_force_reg[21]) begin
  ttc_control_decoder_TMR_inst_bcr_A_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_bcr_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_A force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[21]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.bcr_A released",$realtime);
end
reg ttc_control_decoder_TMR_inst_bcr_B_tmp;
`define force_r ttc_control_decoder_TMR_inst.bcr_B
always @(posedge enable_force_reg[22]) begin
  ttc_control_decoder_TMR_inst_bcr_B_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_bcr_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_B force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[22]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.bcr_B released",$realtime);
end
reg ttc_control_decoder_TMR_inst_bcr_C_tmp;
`define force_r ttc_control_decoder_TMR_inst.bcr_C
always @(posedge enable_force_reg[23]) begin
  ttc_control_decoder_TMR_inst_bcr_C_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_bcr_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_C force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[23]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.bcr_C released",$realtime);
end
reg [2:0] ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_A_tmp;
`define force_r ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_A
always @(posedge enable_force_reg[24]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_A_tmp = `force_r;
  force `force_r = 3'b100 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_A[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[24]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_A[2] released",$realtime);
end
always @(posedge enable_force_reg[25]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_A_tmp = `force_r;
  force `force_r = 3'b010 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_A[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[25]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_A[1] released",$realtime);
end
always @(posedge enable_force_reg[26]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_A_tmp = `force_r;
  force `force_r = 3'b001 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_A[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[26]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_A[0] released",$realtime);
end
reg [2:0] ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_B_tmp;
`define force_r ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_B
always @(posedge enable_force_reg[27]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_B_tmp = `force_r;
  force `force_r = 3'b100 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_B[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[27]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_B[2] released",$realtime);
end
always @(posedge enable_force_reg[28]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_B_tmp = `force_r;
  force `force_r = 3'b010 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_B[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[28]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_B[1] released",$realtime);
end
always @(posedge enable_force_reg[29]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_B_tmp = `force_r;
  force `force_r = 3'b001 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_B[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[29]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_B[0] released",$realtime);
end
reg [2:0] ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_C_tmp;
`define force_r ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_C
always @(posedge enable_force_reg[30]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_C_tmp = `force_r;
  force `force_r = 3'b100 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_C[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[30]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_C[2] released",$realtime);
end
always @(posedge enable_force_reg[31]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_C_tmp = `force_r;
  force `force_r = 3'b010 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_C[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[31]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_C[1] released",$realtime);
end
always @(posedge enable_force_reg[32]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_C_tmp = `force_r;
  force `force_r = 3'b001 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_bcr_r_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_C[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[32]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.bcr_r_C[0] released",$realtime);
end
reg [13:0] ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp;
`define force_r ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A
always @(posedge enable_force_reg[33]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp = `force_r;
  force `force_r = 14'b10000000000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[13] force to %d",$realtime,`force_r[13]);
end
always @(negedge enable_force_reg[33]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[13] released",$realtime);
end
always @(posedge enable_force_reg[34]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp = `force_r;
  force `force_r = 14'b01000000000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[12] force to %d",$realtime,`force_r[12]);
end
always @(negedge enable_force_reg[34]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[12] released",$realtime);
end
always @(posedge enable_force_reg[35]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp = `force_r;
  force `force_r = 14'b00100000000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[11] force to %d",$realtime,`force_r[11]);
end
always @(negedge enable_force_reg[35]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[11] released",$realtime);
end
always @(posedge enable_force_reg[36]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp = `force_r;
  force `force_r = 14'b00010000000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[10] force to %d",$realtime,`force_r[10]);
end
always @(negedge enable_force_reg[36]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[10] released",$realtime);
end
always @(posedge enable_force_reg[37]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp = `force_r;
  force `force_r = 14'b00001000000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[9] force to %d",$realtime,`force_r[9]);
end
always @(negedge enable_force_reg[37]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[9] released",$realtime);
end
always @(posedge enable_force_reg[38]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp = `force_r;
  force `force_r = 14'b00000100000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[8] force to %d",$realtime,`force_r[8]);
end
always @(negedge enable_force_reg[38]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[8] released",$realtime);
end
always @(posedge enable_force_reg[39]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp = `force_r;
  force `force_r = 14'b00000010000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[7] force to %d",$realtime,`force_r[7]);
end
always @(negedge enable_force_reg[39]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[7] released",$realtime);
end
always @(posedge enable_force_reg[40]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp = `force_r;
  force `force_r = 14'b00000001000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[6] force to %d",$realtime,`force_r[6]);
end
always @(negedge enable_force_reg[40]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[6] released",$realtime);
end
always @(posedge enable_force_reg[41]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp = `force_r;
  force `force_r = 14'b00000000100000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[5] force to %d",$realtime,`force_r[5]);
end
always @(negedge enable_force_reg[41]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[5] released",$realtime);
end
always @(posedge enable_force_reg[42]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp = `force_r;
  force `force_r = 14'b00000000010000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[4] force to %d",$realtime,`force_r[4]);
end
always @(negedge enable_force_reg[42]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[4] released",$realtime);
end
always @(posedge enable_force_reg[43]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp = `force_r;
  force `force_r = 14'b00000000001000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[43]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[3] released",$realtime);
end
always @(posedge enable_force_reg[44]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp = `force_r;
  force `force_r = 14'b00000000000100 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[44]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[2] released",$realtime);
end
always @(posedge enable_force_reg[45]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp = `force_r;
  force `force_r = 14'b00000000000010 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[45]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[1] released",$realtime);
end
always @(posedge enable_force_reg[46]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp = `force_r;
  force `force_r = 14'b00000000000001 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[46]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_A[0] released",$realtime);
end
reg [13:0] ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp;
`define force_r ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B
always @(posedge enable_force_reg[47]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp = `force_r;
  force `force_r = 14'b10000000000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[13] force to %d",$realtime,`force_r[13]);
end
always @(negedge enable_force_reg[47]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[13] released",$realtime);
end
always @(posedge enable_force_reg[48]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp = `force_r;
  force `force_r = 14'b01000000000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[12] force to %d",$realtime,`force_r[12]);
end
always @(negedge enable_force_reg[48]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[12] released",$realtime);
end
always @(posedge enable_force_reg[49]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp = `force_r;
  force `force_r = 14'b00100000000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[11] force to %d",$realtime,`force_r[11]);
end
always @(negedge enable_force_reg[49]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[11] released",$realtime);
end
always @(posedge enable_force_reg[50]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp = `force_r;
  force `force_r = 14'b00010000000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[10] force to %d",$realtime,`force_r[10]);
end
always @(negedge enable_force_reg[50]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[10] released",$realtime);
end
always @(posedge enable_force_reg[51]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp = `force_r;
  force `force_r = 14'b00001000000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[9] force to %d",$realtime,`force_r[9]);
end
always @(negedge enable_force_reg[51]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[9] released",$realtime);
end
always @(posedge enable_force_reg[52]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp = `force_r;
  force `force_r = 14'b00000100000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[8] force to %d",$realtime,`force_r[8]);
end
always @(negedge enable_force_reg[52]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[8] released",$realtime);
end
always @(posedge enable_force_reg[53]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp = `force_r;
  force `force_r = 14'b00000010000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[7] force to %d",$realtime,`force_r[7]);
end
always @(negedge enable_force_reg[53]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[7] released",$realtime);
end
always @(posedge enable_force_reg[54]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp = `force_r;
  force `force_r = 14'b00000001000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[6] force to %d",$realtime,`force_r[6]);
end
always @(negedge enable_force_reg[54]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[6] released",$realtime);
end
always @(posedge enable_force_reg[55]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp = `force_r;
  force `force_r = 14'b00000000100000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[5] force to %d",$realtime,`force_r[5]);
end
always @(negedge enable_force_reg[55]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[5] released",$realtime);
end
always @(posedge enable_force_reg[56]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp = `force_r;
  force `force_r = 14'b00000000010000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[4] force to %d",$realtime,`force_r[4]);
end
always @(negedge enable_force_reg[56]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[4] released",$realtime);
end
always @(posedge enable_force_reg[57]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp = `force_r;
  force `force_r = 14'b00000000001000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[57]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[3] released",$realtime);
end
always @(posedge enable_force_reg[58]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp = `force_r;
  force `force_r = 14'b00000000000100 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[58]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[2] released",$realtime);
end
always @(posedge enable_force_reg[59]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp = `force_r;
  force `force_r = 14'b00000000000010 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[59]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[1] released",$realtime);
end
always @(posedge enable_force_reg[60]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp = `force_r;
  force `force_r = 14'b00000000000001 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[60]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_B[0] released",$realtime);
end
reg [13:0] ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp;
`define force_r ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C
always @(posedge enable_force_reg[61]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp = `force_r;
  force `force_r = 14'b10000000000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[13] force to %d",$realtime,`force_r[13]);
end
always @(negedge enable_force_reg[61]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[13] released",$realtime);
end
always @(posedge enable_force_reg[62]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp = `force_r;
  force `force_r = 14'b01000000000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[12] force to %d",$realtime,`force_r[12]);
end
always @(negedge enable_force_reg[62]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[12] released",$realtime);
end
always @(posedge enable_force_reg[63]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp = `force_r;
  force `force_r = 14'b00100000000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[11] force to %d",$realtime,`force_r[11]);
end
always @(negedge enable_force_reg[63]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[11] released",$realtime);
end
always @(posedge enable_force_reg[64]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp = `force_r;
  force `force_r = 14'b00010000000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[10] force to %d",$realtime,`force_r[10]);
end
always @(negedge enable_force_reg[64]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[10] released",$realtime);
end
always @(posedge enable_force_reg[65]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp = `force_r;
  force `force_r = 14'b00001000000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[9] force to %d",$realtime,`force_r[9]);
end
always @(negedge enable_force_reg[65]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[9] released",$realtime);
end
always @(posedge enable_force_reg[66]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp = `force_r;
  force `force_r = 14'b00000100000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[8] force to %d",$realtime,`force_r[8]);
end
always @(negedge enable_force_reg[66]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[8] released",$realtime);
end
always @(posedge enable_force_reg[67]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp = `force_r;
  force `force_r = 14'b00000010000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[7] force to %d",$realtime,`force_r[7]);
end
always @(negedge enable_force_reg[67]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[7] released",$realtime);
end
always @(posedge enable_force_reg[68]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp = `force_r;
  force `force_r = 14'b00000001000000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[6] force to %d",$realtime,`force_r[6]);
end
always @(negedge enable_force_reg[68]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[6] released",$realtime);
end
always @(posedge enable_force_reg[69]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp = `force_r;
  force `force_r = 14'b00000000100000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[5] force to %d",$realtime,`force_r[5]);
end
always @(negedge enable_force_reg[69]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[5] released",$realtime);
end
always @(posedge enable_force_reg[70]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp = `force_r;
  force `force_r = 14'b00000000010000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[4] force to %d",$realtime,`force_r[4]);
end
always @(negedge enable_force_reg[70]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[4] released",$realtime);
end
always @(posedge enable_force_reg[71]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp = `force_r;
  force `force_r = 14'b00000000001000 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[71]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[3] released",$realtime);
end
always @(posedge enable_force_reg[72]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp = `force_r;
  force `force_r = 14'b00000000000100 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[72]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[2] released",$realtime);
end
always @(posedge enable_force_reg[73]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp = `force_r;
  force `force_r = 14'b00000000000010 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[73]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[1] released",$realtime);
end
always @(posedge enable_force_reg[74]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp = `force_r;
  force `force_r = 14'b00000000000001 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_global_counter_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[74]) begin
   release `force_r; $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.global_counter_C[0] released",$realtime);
end
reg ttc_control_decoder_TMR_inst_bcr_distribution_inst_coarse_bcr_160M_A_tmp;
`define force_r ttc_control_decoder_TMR_inst.bcr_distribution_inst.coarse_bcr_160M_A
always @(posedge enable_force_reg[75]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_coarse_bcr_160M_A_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_coarse_bcr_160M_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.coarse_bcr_160M_A force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[75]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.coarse_bcr_160M_A released",$realtime);
end
reg ttc_control_decoder_TMR_inst_bcr_distribution_inst_coarse_bcr_160M_B_tmp;
`define force_r ttc_control_decoder_TMR_inst.bcr_distribution_inst.coarse_bcr_160M_B
always @(posedge enable_force_reg[76]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_coarse_bcr_160M_B_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_coarse_bcr_160M_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.coarse_bcr_160M_B force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[76]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.coarse_bcr_160M_B released",$realtime);
end
reg ttc_control_decoder_TMR_inst_bcr_distribution_inst_coarse_bcr_160M_C_tmp;
`define force_r ttc_control_decoder_TMR_inst.bcr_distribution_inst.coarse_bcr_160M_C
always @(posedge enable_force_reg[77]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_coarse_bcr_160M_C_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_coarse_bcr_160M_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.coarse_bcr_160M_C force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[77]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.coarse_bcr_160M_C released",$realtime);
end
reg ttc_control_decoder_TMR_inst_bcr_distribution_inst_coarse_bcr_A_tmp;
`define force_r ttc_control_decoder_TMR_inst.bcr_distribution_inst.coarse_bcr_A
always @(posedge enable_force_reg[78]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_coarse_bcr_A_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_coarse_bcr_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.coarse_bcr_A force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[78]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.coarse_bcr_A released",$realtime);
end
reg ttc_control_decoder_TMR_inst_bcr_distribution_inst_coarse_bcr_B_tmp;
`define force_r ttc_control_decoder_TMR_inst.bcr_distribution_inst.coarse_bcr_B
always @(posedge enable_force_reg[79]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_coarse_bcr_B_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_coarse_bcr_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.coarse_bcr_B force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[79]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.coarse_bcr_B released",$realtime);
end
reg ttc_control_decoder_TMR_inst_bcr_distribution_inst_coarse_bcr_C_tmp;
`define force_r ttc_control_decoder_TMR_inst.bcr_distribution_inst.coarse_bcr_C
always @(posedge enable_force_reg[80]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_coarse_bcr_C_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_coarse_bcr_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.coarse_bcr_C force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[80]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.coarse_bcr_C released",$realtime);
end
reg ttc_control_decoder_TMR_inst_bcr_distribution_inst_trigger_count_bcr_A_tmp;
`define force_r ttc_control_decoder_TMR_inst.bcr_distribution_inst.trigger_count_bcr_A
always @(posedge enable_force_reg[81]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_trigger_count_bcr_A_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_trigger_count_bcr_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.trigger_count_bcr_A force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[81]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.trigger_count_bcr_A released",$realtime);
end
reg ttc_control_decoder_TMR_inst_bcr_distribution_inst_trigger_count_bcr_B_tmp;
`define force_r ttc_control_decoder_TMR_inst.bcr_distribution_inst.trigger_count_bcr_B
always @(posedge enable_force_reg[82]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_trigger_count_bcr_B_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_trigger_count_bcr_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.trigger_count_bcr_B force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[82]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.trigger_count_bcr_B released",$realtime);
end
reg ttc_control_decoder_TMR_inst_bcr_distribution_inst_trigger_count_bcr_C_tmp;
`define force_r ttc_control_decoder_TMR_inst.bcr_distribution_inst.trigger_count_bcr_C
always @(posedge enable_force_reg[83]) begin
  ttc_control_decoder_TMR_inst_bcr_distribution_inst_trigger_count_bcr_C_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_bcr_distribution_inst_trigger_count_bcr_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.bcr_distribution_inst.trigger_count_bcr_C force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[83]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.bcr_distribution_inst.trigger_count_bcr_C released",$realtime);
end
reg ttc_control_decoder_TMR_inst_control_decoder_inst_bunch_reset_A_tmp;
`define force_r ttc_control_decoder_TMR_inst.control_decoder_inst.bunch_reset_A
always @(posedge enable_force_reg[84]) begin
  ttc_control_decoder_TMR_inst_control_decoder_inst_bunch_reset_A_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_control_decoder_inst_bunch_reset_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.control_decoder_inst.bunch_reset_A force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[84]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.control_decoder_inst.bunch_reset_A released",$realtime);
end
reg ttc_control_decoder_TMR_inst_control_decoder_inst_bunch_reset_B_tmp;
`define force_r ttc_control_decoder_TMR_inst.control_decoder_inst.bunch_reset_B
always @(posedge enable_force_reg[85]) begin
  ttc_control_decoder_TMR_inst_control_decoder_inst_bunch_reset_B_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_control_decoder_inst_bunch_reset_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.control_decoder_inst.bunch_reset_B force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[85]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.control_decoder_inst.bunch_reset_B released",$realtime);
end
reg ttc_control_decoder_TMR_inst_control_decoder_inst_bunch_reset_C_tmp;
`define force_r ttc_control_decoder_TMR_inst.control_decoder_inst.bunch_reset_C
always @(posedge enable_force_reg[86]) begin
  ttc_control_decoder_TMR_inst_control_decoder_inst_bunch_reset_C_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_control_decoder_inst_bunch_reset_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.control_decoder_inst.bunch_reset_C force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[86]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.control_decoder_inst.bunch_reset_C released",$realtime);
end
reg ttc_control_decoder_TMR_inst_control_decoder_inst_master_reset_A_tmp;
`define force_r ttc_control_decoder_TMR_inst.control_decoder_inst.master_reset_A
always @(posedge enable_force_reg[87]) begin
  ttc_control_decoder_TMR_inst_control_decoder_inst_master_reset_A_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_control_decoder_inst_master_reset_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.control_decoder_inst.master_reset_A force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[87]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.control_decoder_inst.master_reset_A released",$realtime);
end
reg ttc_control_decoder_TMR_inst_control_decoder_inst_master_reset_B_tmp;
`define force_r ttc_control_decoder_TMR_inst.control_decoder_inst.master_reset_B
always @(posedge enable_force_reg[88]) begin
  ttc_control_decoder_TMR_inst_control_decoder_inst_master_reset_B_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_control_decoder_inst_master_reset_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.control_decoder_inst.master_reset_B force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[88]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.control_decoder_inst.master_reset_B released",$realtime);
end
reg ttc_control_decoder_TMR_inst_control_decoder_inst_master_reset_C_tmp;
`define force_r ttc_control_decoder_TMR_inst.control_decoder_inst.master_reset_C
always @(posedge enable_force_reg[89]) begin
  ttc_control_decoder_TMR_inst_control_decoder_inst_master_reset_C_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_control_decoder_inst_master_reset_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.control_decoder_inst.master_reset_C force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[89]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.control_decoder_inst.master_reset_C released",$realtime);
end
reg ttc_control_decoder_TMR_inst_control_decoder_with_newTTC_inst_bunch_reset_A_tmp;
`define force_r ttc_control_decoder_TMR_inst.control_decoder_with_newTTC_inst.bunch_reset_A
always @(posedge enable_force_reg[90]) begin
  ttc_control_decoder_TMR_inst_control_decoder_with_newTTC_inst_bunch_reset_A_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_control_decoder_with_newTTC_inst_bunch_reset_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.control_decoder_with_newTTC_inst.bunch_reset_A force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[90]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.control_decoder_with_newTTC_inst.bunch_reset_A released",$realtime);
end
reg ttc_control_decoder_TMR_inst_control_decoder_with_newTTC_inst_bunch_reset_B_tmp;
`define force_r ttc_control_decoder_TMR_inst.control_decoder_with_newTTC_inst.bunch_reset_B
always @(posedge enable_force_reg[91]) begin
  ttc_control_decoder_TMR_inst_control_decoder_with_newTTC_inst_bunch_reset_B_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_control_decoder_with_newTTC_inst_bunch_reset_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.control_decoder_with_newTTC_inst.bunch_reset_B force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[91]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.control_decoder_with_newTTC_inst.bunch_reset_B released",$realtime);
end
reg ttc_control_decoder_TMR_inst_control_decoder_with_newTTC_inst_bunch_reset_C_tmp;
`define force_r ttc_control_decoder_TMR_inst.control_decoder_with_newTTC_inst.bunch_reset_C
always @(posedge enable_force_reg[92]) begin
  ttc_control_decoder_TMR_inst_control_decoder_with_newTTC_inst_bunch_reset_C_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_control_decoder_with_newTTC_inst_bunch_reset_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.control_decoder_with_newTTC_inst.bunch_reset_C force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[92]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.control_decoder_with_newTTC_inst.bunch_reset_C released",$realtime);
end
reg ttc_control_decoder_TMR_inst_control_decoder_with_newTTC_inst_master_reset_A_tmp;
`define force_r ttc_control_decoder_TMR_inst.control_decoder_with_newTTC_inst.master_reset_A
always @(posedge enable_force_reg[93]) begin
  ttc_control_decoder_TMR_inst_control_decoder_with_newTTC_inst_master_reset_A_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_control_decoder_with_newTTC_inst_master_reset_A_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.control_decoder_with_newTTC_inst.master_reset_A force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[93]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.control_decoder_with_newTTC_inst.master_reset_A released",$realtime);
end
reg ttc_control_decoder_TMR_inst_control_decoder_with_newTTC_inst_master_reset_B_tmp;
`define force_r ttc_control_decoder_TMR_inst.control_decoder_with_newTTC_inst.master_reset_B
always @(posedge enable_force_reg[94]) begin
  ttc_control_decoder_TMR_inst_control_decoder_with_newTTC_inst_master_reset_B_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_control_decoder_with_newTTC_inst_master_reset_B_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.control_decoder_with_newTTC_inst.master_reset_B force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[94]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.control_decoder_with_newTTC_inst.master_reset_B released",$realtime);
end
reg ttc_control_decoder_TMR_inst_control_decoder_with_newTTC_inst_master_reset_C_tmp;
`define force_r ttc_control_decoder_TMR_inst.control_decoder_with_newTTC_inst.master_reset_C
always @(posedge enable_force_reg[95]) begin
  ttc_control_decoder_TMR_inst_control_decoder_with_newTTC_inst_master_reset_C_tmp = `force_r;
  force `force_r = 1'b1 ^ ttc_control_decoder_TMR_inst_control_decoder_with_newTTC_inst_master_reset_C_tmp;
  $display("At%t ttc_control_decoder_TMR_inst.control_decoder_with_newTTC_inst.master_reset_C force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[95]) begin
  release `force_r;   $display("At %t ttc_control_decoder_TMR_inst.control_decoder_with_newTTC_inst.master_reset_C released",$realtime);
end
`endif
`ifdef REG_LIST
`define REG_NUM 156
reg [`REG_NUM-1:0] enable_force_reg = 1'b0;
reg mEnc8b10bMem_tb_TMR_inst_r_jDisp_A_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.r_jDisp_A
always @(posedge enable_force_reg[0]) begin
  mEnc8b10bMem_tb_TMR_inst_r_jDisp_A_tmp = `force_r;
  force `force_r = 1'b1 ^ mEnc8b10bMem_tb_TMR_inst_r_jDisp_A_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.r_jDisp_A force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[0]) begin
  release `force_r;   $display("At %t mEnc8b10bMem_tb_TMR_inst.r_jDisp_A released",$realtime);
end
reg [9:0] mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.o10_Dout_A
always @(posedge enable_force_reg[1]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp = `force_r;
  force `force_r = 10'b1000000000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_A[9] force to %d",$realtime,`force_r[9]);
end
always @(negedge enable_force_reg[1]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_A[9] released",$realtime);
end
always @(posedge enable_force_reg[2]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp = `force_r;
  force `force_r = 10'b0100000000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_A[8] force to %d",$realtime,`force_r[8]);
end
always @(negedge enable_force_reg[2]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_A[8] released",$realtime);
end
always @(posedge enable_force_reg[3]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp = `force_r;
  force `force_r = 10'b0010000000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_A[7] force to %d",$realtime,`force_r[7]);
end
always @(negedge enable_force_reg[3]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_A[7] released",$realtime);
end
always @(posedge enable_force_reg[4]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp = `force_r;
  force `force_r = 10'b0001000000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_A[6] force to %d",$realtime,`force_r[6]);
end
always @(negedge enable_force_reg[4]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_A[6] released",$realtime);
end
always @(posedge enable_force_reg[5]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp = `force_r;
  force `force_r = 10'b0000100000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_A[5] force to %d",$realtime,`force_r[5]);
end
always @(negedge enable_force_reg[5]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_A[5] released",$realtime);
end
always @(posedge enable_force_reg[6]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp = `force_r;
  force `force_r = 10'b0000010000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_A[4] force to %d",$realtime,`force_r[4]);
end
always @(negedge enable_force_reg[6]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_A[4] released",$realtime);
end
always @(posedge enable_force_reg[7]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp = `force_r;
  force `force_r = 10'b0000001000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_A[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[7]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_A[3] released",$realtime);
end
always @(posedge enable_force_reg[8]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp = `force_r;
  force `force_r = 10'b0000000100 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_A[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[8]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_A[2] released",$realtime);
end
always @(posedge enable_force_reg[9]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp = `force_r;
  force `force_r = 10'b0000000010 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_A[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[9]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_A[1] released",$realtime);
end
always @(posedge enable_force_reg[10]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp = `force_r;
  force `force_r = 10'b0000000001 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_A_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_A[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[10]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_A[0] released",$realtime);
end
reg mEnc8b10bMem_tb_TMR_inst_r_KErr_A_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.r_KErr_A
always @(posedge enable_force_reg[11]) begin
  mEnc8b10bMem_tb_TMR_inst_r_KErr_A_tmp = `force_r;
  force `force_r = 1'b1 ^ mEnc8b10bMem_tb_TMR_inst_r_KErr_A_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.r_KErr_A force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[11]) begin
  release `force_r;   $display("At %t mEnc8b10bMem_tb_TMR_inst.r_KErr_A released",$realtime);
end
reg mEnc8b10bMem_tb_TMR_inst_r_jDisp_B_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.r_jDisp_B
always @(posedge enable_force_reg[12]) begin
  mEnc8b10bMem_tb_TMR_inst_r_jDisp_B_tmp = `force_r;
  force `force_r = 1'b1 ^ mEnc8b10bMem_tb_TMR_inst_r_jDisp_B_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.r_jDisp_B force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[12]) begin
  release `force_r;   $display("At %t mEnc8b10bMem_tb_TMR_inst.r_jDisp_B released",$realtime);
end
reg [9:0] mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.o10_Dout_B
always @(posedge enable_force_reg[13]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp = `force_r;
  force `force_r = 10'b1000000000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_B[9] force to %d",$realtime,`force_r[9]);
end
always @(negedge enable_force_reg[13]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_B[9] released",$realtime);
end
always @(posedge enable_force_reg[14]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp = `force_r;
  force `force_r = 10'b0100000000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_B[8] force to %d",$realtime,`force_r[8]);
end
always @(negedge enable_force_reg[14]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_B[8] released",$realtime);
end
always @(posedge enable_force_reg[15]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp = `force_r;
  force `force_r = 10'b0010000000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_B[7] force to %d",$realtime,`force_r[7]);
end
always @(negedge enable_force_reg[15]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_B[7] released",$realtime);
end
always @(posedge enable_force_reg[16]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp = `force_r;
  force `force_r = 10'b0001000000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_B[6] force to %d",$realtime,`force_r[6]);
end
always @(negedge enable_force_reg[16]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_B[6] released",$realtime);
end
always @(posedge enable_force_reg[17]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp = `force_r;
  force `force_r = 10'b0000100000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_B[5] force to %d",$realtime,`force_r[5]);
end
always @(negedge enable_force_reg[17]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_B[5] released",$realtime);
end
always @(posedge enable_force_reg[18]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp = `force_r;
  force `force_r = 10'b0000010000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_B[4] force to %d",$realtime,`force_r[4]);
end
always @(negedge enable_force_reg[18]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_B[4] released",$realtime);
end
always @(posedge enable_force_reg[19]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp = `force_r;
  force `force_r = 10'b0000001000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_B[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[19]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_B[3] released",$realtime);
end
always @(posedge enable_force_reg[20]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp = `force_r;
  force `force_r = 10'b0000000100 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_B[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[20]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_B[2] released",$realtime);
end
always @(posedge enable_force_reg[21]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp = `force_r;
  force `force_r = 10'b0000000010 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_B[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[21]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_B[1] released",$realtime);
end
always @(posedge enable_force_reg[22]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp = `force_r;
  force `force_r = 10'b0000000001 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_B_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_B[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[22]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_B[0] released",$realtime);
end
reg mEnc8b10bMem_tb_TMR_inst_r_KErr_B_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.r_KErr_B
always @(posedge enable_force_reg[23]) begin
  mEnc8b10bMem_tb_TMR_inst_r_KErr_B_tmp = `force_r;
  force `force_r = 1'b1 ^ mEnc8b10bMem_tb_TMR_inst_r_KErr_B_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.r_KErr_B force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[23]) begin
  release `force_r;   $display("At %t mEnc8b10bMem_tb_TMR_inst.r_KErr_B released",$realtime);
end
reg mEnc8b10bMem_tb_TMR_inst_r_jDisp_C_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.r_jDisp_C
always @(posedge enable_force_reg[24]) begin
  mEnc8b10bMem_tb_TMR_inst_r_jDisp_C_tmp = `force_r;
  force `force_r = 1'b1 ^ mEnc8b10bMem_tb_TMR_inst_r_jDisp_C_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.r_jDisp_C force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[24]) begin
  release `force_r;   $display("At %t mEnc8b10bMem_tb_TMR_inst.r_jDisp_C released",$realtime);
end
reg [9:0] mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.o10_Dout_C
always @(posedge enable_force_reg[25]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp = `force_r;
  force `force_r = 10'b1000000000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_C[9] force to %d",$realtime,`force_r[9]);
end
always @(negedge enable_force_reg[25]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_C[9] released",$realtime);
end
always @(posedge enable_force_reg[26]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp = `force_r;
  force `force_r = 10'b0100000000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_C[8] force to %d",$realtime,`force_r[8]);
end
always @(negedge enable_force_reg[26]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_C[8] released",$realtime);
end
always @(posedge enable_force_reg[27]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp = `force_r;
  force `force_r = 10'b0010000000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_C[7] force to %d",$realtime,`force_r[7]);
end
always @(negedge enable_force_reg[27]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_C[7] released",$realtime);
end
always @(posedge enable_force_reg[28]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp = `force_r;
  force `force_r = 10'b0001000000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_C[6] force to %d",$realtime,`force_r[6]);
end
always @(negedge enable_force_reg[28]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_C[6] released",$realtime);
end
always @(posedge enable_force_reg[29]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp = `force_r;
  force `force_r = 10'b0000100000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_C[5] force to %d",$realtime,`force_r[5]);
end
always @(negedge enable_force_reg[29]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_C[5] released",$realtime);
end
always @(posedge enable_force_reg[30]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp = `force_r;
  force `force_r = 10'b0000010000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_C[4] force to %d",$realtime,`force_r[4]);
end
always @(negedge enable_force_reg[30]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_C[4] released",$realtime);
end
always @(posedge enable_force_reg[31]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp = `force_r;
  force `force_r = 10'b0000001000 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_C[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[31]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_C[3] released",$realtime);
end
always @(posedge enable_force_reg[32]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp = `force_r;
  force `force_r = 10'b0000000100 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_C[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[32]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_C[2] released",$realtime);
end
always @(posedge enable_force_reg[33]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp = `force_r;
  force `force_r = 10'b0000000010 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_C[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[33]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_C[1] released",$realtime);
end
always @(posedge enable_force_reg[34]) begin
  mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp = `force_r;
  force `force_r = 10'b0000000001 ^ mEnc8b10bMem_tb_TMR_inst_o10_Dout_C_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.o10_Dout_C[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[34]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.o10_Dout_C[0] released",$realtime);
end
reg mEnc8b10bMem_tb_TMR_inst_r_KErr_C_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.r_KErr_C
always @(posedge enable_force_reg[35]) begin
  mEnc8b10bMem_tb_TMR_inst_r_KErr_C_tmp = `force_r;
  force `force_r = 1'b1 ^ mEnc8b10bMem_tb_TMR_inst_r_KErr_C_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.r_KErr_C force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[35]) begin
  release `force_r;   $display("At %t mEnc8b10bMem_tb_TMR_inst.r_KErr_C released",$realtime);
end
reg mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_w_jDisp_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.w_jDisp
always @(posedge enable_force_reg[36]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_w_jDisp_tmp = `force_r;
  force `force_r = 1'b1 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_w_jDisp_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.w_jDisp force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[36]) begin
  release `force_r;   $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.w_jDisp released",$realtime);
end
reg [9:0] mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode
always @(posedge enable_force_reg[37]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b1000000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode[9] force to %d",$realtime,`force_r[9]);
end
always @(negedge enable_force_reg[37]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode[9] released",$realtime);
end
always @(posedge enable_force_reg[38]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0100000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode[8] force to %d",$realtime,`force_r[8]);
end
always @(negedge enable_force_reg[38]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode[8] released",$realtime);
end
always @(posedge enable_force_reg[39]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0010000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode[7] force to %d",$realtime,`force_r[7]);
end
always @(negedge enable_force_reg[39]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode[7] released",$realtime);
end
always @(posedge enable_force_reg[40]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0001000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode[6] force to %d",$realtime,`force_r[6]);
end
always @(negedge enable_force_reg[40]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode[6] released",$realtime);
end
always @(posedge enable_force_reg[41]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0000100000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode[5] force to %d",$realtime,`force_r[5]);
end
always @(negedge enable_force_reg[41]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode[5] released",$realtime);
end
always @(posedge enable_force_reg[42]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0000010000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode[4] force to %d",$realtime,`force_r[4]);
end
always @(negedge enable_force_reg[42]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode[4] released",$realtime);
end
always @(posedge enable_force_reg[43]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0000001000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[43]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode[3] released",$realtime);
end
always @(posedge enable_force_reg[44]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0000000100 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[44]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode[2] released",$realtime);
end
always @(posedge enable_force_reg[45]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0000000010 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[45]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode[1] released",$realtime);
end
always @(posedge enable_force_reg[46]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0000000001 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[46]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bKCode[0] released",$realtime);
end
reg [7:0] mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bKCode_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bKCode
always @(posedge enable_force_reg[47]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b10000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bKCode[7] force to %d",$realtime,`force_r[7]);
end
always @(negedge enable_force_reg[47]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bKCode[7] released",$realtime);
end
always @(posedge enable_force_reg[48]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b01000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bKCode[6] force to %d",$realtime,`force_r[6]);
end
always @(negedge enable_force_reg[48]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bKCode[6] released",$realtime);
end
always @(posedge enable_force_reg[49]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b00100000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bKCode[5] force to %d",$realtime,`force_r[5]);
end
always @(negedge enable_force_reg[49]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bKCode[5] released",$realtime);
end
always @(posedge enable_force_reg[50]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b00010000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bKCode[4] force to %d",$realtime,`force_r[4]);
end
always @(negedge enable_force_reg[50]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bKCode[4] released",$realtime);
end
always @(posedge enable_force_reg[51]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b00001000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bKCode[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[51]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bKCode[3] released",$realtime);
end
always @(posedge enable_force_reg[52]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b00000100 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bKCode[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[52]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bKCode[2] released",$realtime);
end
always @(posedge enable_force_reg[53]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b00000010 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bKCode[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[53]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bKCode[1] released",$realtime);
end
always @(posedge enable_force_reg[54]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b00000001 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bKCode[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[54]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bKCode[0] released",$realtime);
end
reg [9:0] mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode
always @(posedge enable_force_reg[55]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b1000000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode[9] force to %d",$realtime,`force_r[9]);
end
always @(negedge enable_force_reg[55]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode[9] released",$realtime);
end
always @(posedge enable_force_reg[56]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0100000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode[8] force to %d",$realtime,`force_r[8]);
end
always @(negedge enable_force_reg[56]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode[8] released",$realtime);
end
always @(posedge enable_force_reg[57]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0010000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode[7] force to %d",$realtime,`force_r[7]);
end
always @(negedge enable_force_reg[57]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode[7] released",$realtime);
end
always @(posedge enable_force_reg[58]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0001000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode[6] force to %d",$realtime,`force_r[6]);
end
always @(negedge enable_force_reg[58]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode[6] released",$realtime);
end
always @(posedge enable_force_reg[59]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0000100000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode[5] force to %d",$realtime,`force_r[5]);
end
always @(negedge enable_force_reg[59]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode[5] released",$realtime);
end
always @(posedge enable_force_reg[60]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0000010000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode[4] force to %d",$realtime,`force_r[4]);
end
always @(negedge enable_force_reg[60]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode[4] released",$realtime);
end
always @(posedge enable_force_reg[61]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0000001000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[61]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode[3] released",$realtime);
end
always @(posedge enable_force_reg[62]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0000000100 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[62]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode[2] released",$realtime);
end
always @(posedge enable_force_reg[63]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0000000010 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[63]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode[1] released",$realtime);
end
always @(posedge enable_force_reg[64]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0000000001 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[64]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r10_6bDCode[0] released",$realtime);
end
reg [7:0] mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bDCode_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bDCode
always @(posedge enable_force_reg[65]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b10000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bDCode[7] force to %d",$realtime,`force_r[7]);
end
always @(negedge enable_force_reg[65]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bDCode[7] released",$realtime);
end
always @(posedge enable_force_reg[66]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b01000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bDCode[6] force to %d",$realtime,`force_r[6]);
end
always @(negedge enable_force_reg[66]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bDCode[6] released",$realtime);
end
always @(posedge enable_force_reg[67]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b00100000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bDCode[5] force to %d",$realtime,`force_r[5]);
end
always @(negedge enable_force_reg[67]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bDCode[5] released",$realtime);
end
always @(posedge enable_force_reg[68]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b00010000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bDCode[4] force to %d",$realtime,`force_r[4]);
end
always @(negedge enable_force_reg[68]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bDCode[4] released",$realtime);
end
always @(posedge enable_force_reg[69]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b00001000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bDCode[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[69]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bDCode[3] released",$realtime);
end
always @(posedge enable_force_reg[70]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b00000100 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bDCode[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[70]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bDCode[2] released",$realtime);
end
always @(posedge enable_force_reg[71]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b00000010 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bDCode[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[71]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bDCode[1] released",$realtime);
end
always @(posedge enable_force_reg[72]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b00000001 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bDCode[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[72]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.r8_4bDCode[0] released",$realtime);
end
reg mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_w_Compl6b_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.w_Compl6b
always @(posedge enable_force_reg[73]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_w_Compl6b_tmp = `force_r;
  force `force_r = 1'b1 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_w_Compl6b_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.w_Compl6b force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[73]) begin
  release `force_r;   $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.w_Compl6b released",$realtime);
end
reg mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_w_Compl4b_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.w_Compl4b
always @(posedge enable_force_reg[74]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_w_Compl4b_tmp = `force_r;
  force `force_r = 1'b1 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_w_Compl4b_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.w_Compl4b force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[74]) begin
  release `force_r;   $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.w_Compl4b released",$realtime);
end
reg mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_w_iDisp_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.w_iDisp
always @(posedge enable_force_reg[75]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_w_iDisp_tmp = `force_r;
  force `force_r = 1'b1 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_A_w_iDisp_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.w_iDisp force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[75]) begin
  release `force_r;   $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_A.w_iDisp released",$realtime);
end
reg mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_w_jDisp_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.w_jDisp
always @(posedge enable_force_reg[76]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_w_jDisp_tmp = `force_r;
  force `force_r = 1'b1 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_w_jDisp_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.w_jDisp force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[76]) begin
  release `force_r;   $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.w_jDisp released",$realtime);
end
reg [9:0] mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode
always @(posedge enable_force_reg[77]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b1000000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode[9] force to %d",$realtime,`force_r[9]);
end
always @(negedge enable_force_reg[77]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode[9] released",$realtime);
end
always @(posedge enable_force_reg[78]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0100000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode[8] force to %d",$realtime,`force_r[8]);
end
always @(negedge enable_force_reg[78]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode[8] released",$realtime);
end
always @(posedge enable_force_reg[79]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0010000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode[7] force to %d",$realtime,`force_r[7]);
end
always @(negedge enable_force_reg[79]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode[7] released",$realtime);
end
always @(posedge enable_force_reg[80]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0001000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode[6] force to %d",$realtime,`force_r[6]);
end
always @(negedge enable_force_reg[80]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode[6] released",$realtime);
end
always @(posedge enable_force_reg[81]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0000100000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode[5] force to %d",$realtime,`force_r[5]);
end
always @(negedge enable_force_reg[81]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode[5] released",$realtime);
end
always @(posedge enable_force_reg[82]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0000010000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode[4] force to %d",$realtime,`force_r[4]);
end
always @(negedge enable_force_reg[82]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode[4] released",$realtime);
end
always @(posedge enable_force_reg[83]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0000001000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[83]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode[3] released",$realtime);
end
always @(posedge enable_force_reg[84]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0000000100 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[84]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode[2] released",$realtime);
end
always @(posedge enable_force_reg[85]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0000000010 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[85]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode[1] released",$realtime);
end
always @(posedge enable_force_reg[86]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0000000001 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[86]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bKCode[0] released",$realtime);
end
reg [7:0] mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bKCode_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bKCode
always @(posedge enable_force_reg[87]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b10000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bKCode[7] force to %d",$realtime,`force_r[7]);
end
always @(negedge enable_force_reg[87]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bKCode[7] released",$realtime);
end
always @(posedge enable_force_reg[88]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b01000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bKCode[6] force to %d",$realtime,`force_r[6]);
end
always @(negedge enable_force_reg[88]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bKCode[6] released",$realtime);
end
always @(posedge enable_force_reg[89]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b00100000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bKCode[5] force to %d",$realtime,`force_r[5]);
end
always @(negedge enable_force_reg[89]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bKCode[5] released",$realtime);
end
always @(posedge enable_force_reg[90]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b00010000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bKCode[4] force to %d",$realtime,`force_r[4]);
end
always @(negedge enable_force_reg[90]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bKCode[4] released",$realtime);
end
always @(posedge enable_force_reg[91]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b00001000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bKCode[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[91]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bKCode[3] released",$realtime);
end
always @(posedge enable_force_reg[92]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b00000100 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bKCode[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[92]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bKCode[2] released",$realtime);
end
always @(posedge enable_force_reg[93]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b00000010 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bKCode[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[93]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bKCode[1] released",$realtime);
end
always @(posedge enable_force_reg[94]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b00000001 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bKCode[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[94]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bKCode[0] released",$realtime);
end
reg [9:0] mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode
always @(posedge enable_force_reg[95]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b1000000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode[9] force to %d",$realtime,`force_r[9]);
end
always @(negedge enable_force_reg[95]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode[9] released",$realtime);
end
always @(posedge enable_force_reg[96]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0100000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode[8] force to %d",$realtime,`force_r[8]);
end
always @(negedge enable_force_reg[96]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode[8] released",$realtime);
end
always @(posedge enable_force_reg[97]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0010000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode[7] force to %d",$realtime,`force_r[7]);
end
always @(negedge enable_force_reg[97]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode[7] released",$realtime);
end
always @(posedge enable_force_reg[98]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0001000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode[6] force to %d",$realtime,`force_r[6]);
end
always @(negedge enable_force_reg[98]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode[6] released",$realtime);
end
always @(posedge enable_force_reg[99]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0000100000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode[5] force to %d",$realtime,`force_r[5]);
end
always @(negedge enable_force_reg[99]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode[5] released",$realtime);
end
always @(posedge enable_force_reg[100]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0000010000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode[4] force to %d",$realtime,`force_r[4]);
end
always @(negedge enable_force_reg[100]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode[4] released",$realtime);
end
always @(posedge enable_force_reg[101]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0000001000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[101]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode[3] released",$realtime);
end
always @(posedge enable_force_reg[102]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0000000100 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[102]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode[2] released",$realtime);
end
always @(posedge enable_force_reg[103]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0000000010 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[103]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode[1] released",$realtime);
end
always @(posedge enable_force_reg[104]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0000000001 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[104]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r10_6bDCode[0] released",$realtime);
end
reg [7:0] mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bDCode_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bDCode
always @(posedge enable_force_reg[105]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b10000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bDCode[7] force to %d",$realtime,`force_r[7]);
end
always @(negedge enable_force_reg[105]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bDCode[7] released",$realtime);
end
always @(posedge enable_force_reg[106]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b01000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bDCode[6] force to %d",$realtime,`force_r[6]);
end
always @(negedge enable_force_reg[106]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bDCode[6] released",$realtime);
end
always @(posedge enable_force_reg[107]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b00100000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bDCode[5] force to %d",$realtime,`force_r[5]);
end
always @(negedge enable_force_reg[107]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bDCode[5] released",$realtime);
end
always @(posedge enable_force_reg[108]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b00010000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bDCode[4] force to %d",$realtime,`force_r[4]);
end
always @(negedge enable_force_reg[108]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bDCode[4] released",$realtime);
end
always @(posedge enable_force_reg[109]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b00001000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bDCode[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[109]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bDCode[3] released",$realtime);
end
always @(posedge enable_force_reg[110]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b00000100 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bDCode[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[110]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bDCode[2] released",$realtime);
end
always @(posedge enable_force_reg[111]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b00000010 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bDCode[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[111]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bDCode[1] released",$realtime);
end
always @(posedge enable_force_reg[112]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b00000001 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bDCode[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[112]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.r8_4bDCode[0] released",$realtime);
end
reg mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_w_Compl6b_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.w_Compl6b
always @(posedge enable_force_reg[113]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_w_Compl6b_tmp = `force_r;
  force `force_r = 1'b1 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_w_Compl6b_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.w_Compl6b force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[113]) begin
  release `force_r;   $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.w_Compl6b released",$realtime);
end
reg mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_w_Compl4b_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.w_Compl4b
always @(posedge enable_force_reg[114]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_w_Compl4b_tmp = `force_r;
  force `force_r = 1'b1 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_w_Compl4b_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.w_Compl4b force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[114]) begin
  release `force_r;   $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.w_Compl4b released",$realtime);
end
reg mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_w_iDisp_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.w_iDisp
always @(posedge enable_force_reg[115]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_w_iDisp_tmp = `force_r;
  force `force_r = 1'b1 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_B_w_iDisp_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.w_iDisp force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[115]) begin
  release `force_r;   $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_B.w_iDisp released",$realtime);
end
reg mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_w_jDisp_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.w_jDisp
always @(posedge enable_force_reg[116]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_w_jDisp_tmp = `force_r;
  force `force_r = 1'b1 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_w_jDisp_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.w_jDisp force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[116]) begin
  release `force_r;   $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.w_jDisp released",$realtime);
end
reg [9:0] mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode
always @(posedge enable_force_reg[117]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b1000000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode[9] force to %d",$realtime,`force_r[9]);
end
always @(negedge enable_force_reg[117]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode[9] released",$realtime);
end
always @(posedge enable_force_reg[118]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0100000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode[8] force to %d",$realtime,`force_r[8]);
end
always @(negedge enable_force_reg[118]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode[8] released",$realtime);
end
always @(posedge enable_force_reg[119]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0010000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode[7] force to %d",$realtime,`force_r[7]);
end
always @(negedge enable_force_reg[119]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode[7] released",$realtime);
end
always @(posedge enable_force_reg[120]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0001000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode[6] force to %d",$realtime,`force_r[6]);
end
always @(negedge enable_force_reg[120]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode[6] released",$realtime);
end
always @(posedge enable_force_reg[121]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0000100000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode[5] force to %d",$realtime,`force_r[5]);
end
always @(negedge enable_force_reg[121]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode[5] released",$realtime);
end
always @(posedge enable_force_reg[122]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0000010000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode[4] force to %d",$realtime,`force_r[4]);
end
always @(negedge enable_force_reg[122]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode[4] released",$realtime);
end
always @(posedge enable_force_reg[123]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0000001000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[123]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode[3] released",$realtime);
end
always @(posedge enable_force_reg[124]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0000000100 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[124]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode[2] released",$realtime);
end
always @(posedge enable_force_reg[125]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0000000010 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[125]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode[1] released",$realtime);
end
always @(posedge enable_force_reg[126]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp = `force_r;
  force `force_r = 10'b0000000001 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[126]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bKCode[0] released",$realtime);
end
reg [7:0] mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bKCode_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bKCode
always @(posedge enable_force_reg[127]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b10000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bKCode[7] force to %d",$realtime,`force_r[7]);
end
always @(negedge enable_force_reg[127]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bKCode[7] released",$realtime);
end
always @(posedge enable_force_reg[128]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b01000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bKCode[6] force to %d",$realtime,`force_r[6]);
end
always @(negedge enable_force_reg[128]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bKCode[6] released",$realtime);
end
always @(posedge enable_force_reg[129]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b00100000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bKCode[5] force to %d",$realtime,`force_r[5]);
end
always @(negedge enable_force_reg[129]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bKCode[5] released",$realtime);
end
always @(posedge enable_force_reg[130]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b00010000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bKCode[4] force to %d",$realtime,`force_r[4]);
end
always @(negedge enable_force_reg[130]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bKCode[4] released",$realtime);
end
always @(posedge enable_force_reg[131]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b00001000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bKCode[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[131]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bKCode[3] released",$realtime);
end
always @(posedge enable_force_reg[132]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b00000100 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bKCode[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[132]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bKCode[2] released",$realtime);
end
always @(posedge enable_force_reg[133]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b00000010 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bKCode[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[133]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bKCode[1] released",$realtime);
end
always @(posedge enable_force_reg[134]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bKCode_tmp = `force_r;
  force `force_r = 8'b00000001 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bKCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bKCode[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[134]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bKCode[0] released",$realtime);
end
reg [9:0] mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode
always @(posedge enable_force_reg[135]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b1000000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode[9] force to %d",$realtime,`force_r[9]);
end
always @(negedge enable_force_reg[135]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode[9] released",$realtime);
end
always @(posedge enable_force_reg[136]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0100000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode[8] force to %d",$realtime,`force_r[8]);
end
always @(negedge enable_force_reg[136]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode[8] released",$realtime);
end
always @(posedge enable_force_reg[137]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0010000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode[7] force to %d",$realtime,`force_r[7]);
end
always @(negedge enable_force_reg[137]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode[7] released",$realtime);
end
always @(posedge enable_force_reg[138]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0001000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode[6] force to %d",$realtime,`force_r[6]);
end
always @(negedge enable_force_reg[138]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode[6] released",$realtime);
end
always @(posedge enable_force_reg[139]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0000100000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode[5] force to %d",$realtime,`force_r[5]);
end
always @(negedge enable_force_reg[139]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode[5] released",$realtime);
end
always @(posedge enable_force_reg[140]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0000010000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode[4] force to %d",$realtime,`force_r[4]);
end
always @(negedge enable_force_reg[140]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode[4] released",$realtime);
end
always @(posedge enable_force_reg[141]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0000001000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[141]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode[3] released",$realtime);
end
always @(posedge enable_force_reg[142]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0000000100 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[142]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode[2] released",$realtime);
end
always @(posedge enable_force_reg[143]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0000000010 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[143]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode[1] released",$realtime);
end
always @(posedge enable_force_reg[144]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp = `force_r;
  force `force_r = 10'b0000000001 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r10_6bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[144]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r10_6bDCode[0] released",$realtime);
end
reg [7:0] mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bDCode_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bDCode
always @(posedge enable_force_reg[145]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b10000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bDCode[7] force to %d",$realtime,`force_r[7]);
end
always @(negedge enable_force_reg[145]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bDCode[7] released",$realtime);
end
always @(posedge enable_force_reg[146]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b01000000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bDCode[6] force to %d",$realtime,`force_r[6]);
end
always @(negedge enable_force_reg[146]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bDCode[6] released",$realtime);
end
always @(posedge enable_force_reg[147]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b00100000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bDCode[5] force to %d",$realtime,`force_r[5]);
end
always @(negedge enable_force_reg[147]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bDCode[5] released",$realtime);
end
always @(posedge enable_force_reg[148]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b00010000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bDCode[4] force to %d",$realtime,`force_r[4]);
end
always @(negedge enable_force_reg[148]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bDCode[4] released",$realtime);
end
always @(posedge enable_force_reg[149]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b00001000 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bDCode[3] force to %d",$realtime,`force_r[3]);
end
always @(negedge enable_force_reg[149]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bDCode[3] released",$realtime);
end
always @(posedge enable_force_reg[150]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b00000100 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bDCode[2] force to %d",$realtime,`force_r[2]);
end
always @(negedge enable_force_reg[150]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bDCode[2] released",$realtime);
end
always @(posedge enable_force_reg[151]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b00000010 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bDCode[1] force to %d",$realtime,`force_r[1]);
end
always @(negedge enable_force_reg[151]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bDCode[1] released",$realtime);
end
always @(posedge enable_force_reg[152]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bDCode_tmp = `force_r;
  force `force_r = 8'b00000001 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_r8_4bDCode_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bDCode[0] force to %d",$realtime,`force_r[0]);
end
always @(negedge enable_force_reg[152]) begin
   release `force_r; $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.r8_4bDCode[0] released",$realtime);
end
reg mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_w_Compl6b_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.w_Compl6b
always @(posedge enable_force_reg[153]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_w_Compl6b_tmp = `force_r;
  force `force_r = 1'b1 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_w_Compl6b_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.w_Compl6b force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[153]) begin
  release `force_r;   $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.w_Compl6b released",$realtime);
end
reg mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_w_Compl4b_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.w_Compl4b
always @(posedge enable_force_reg[154]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_w_Compl4b_tmp = `force_r;
  force `force_r = 1'b1 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_w_Compl4b_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.w_Compl4b force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[154]) begin
  release `force_r;   $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.w_Compl4b released",$realtime);
end
reg mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_w_iDisp_tmp;
`define force_r mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.w_iDisp
always @(posedge enable_force_reg[155]) begin
  mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_w_iDisp_tmp = `force_r;
  force `force_r = 1'b1 ^ mEnc8b10bMem_tb_TMR_inst_inst_Encode8b10b_combine_C_w_iDisp_tmp;
  $display("At%t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.w_iDisp force to %d",$realtime,`force_r);
end
always @(negedge enable_force_reg[155]) begin
  release `force_r;   $display("At %t mEnc8b10bMem_tb_TMR_inst.inst_Encode8b10b_combine_C.w_iDisp released",$realtime);
end
`endif
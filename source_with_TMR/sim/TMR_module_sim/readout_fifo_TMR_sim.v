/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : readout_fifo_TMR_sim.v
//  Author     : Yuxiang Guo
//  Revision   : 
//               First created on March 30th, 2019
//  Note       : 
//     

`define REG_LIST
`define DATASIZE 43
module readout_fifo_TMR_sim();
`include "readout_fifo_TMR_sim_reglist.v"


wire clk160, clk160_A, clk160_B, clk160_C;
TMR_clk_generator TMR_clk160_generator_inst (
  .enable320(1'b0),
  .clk(clk160), 
  .clk_A(clk160_A), 
  .clk_B(clk160_B), 
  .clk_C(clk160_C),
  .check_enable(check_enable)
  );
wire clk160_read, clk160_read_A, clk160_read_B, clk160_read_C;
TMR_clk_generator TMR_clk160_read_generator_inst (
  .enable320(1'b0), //both 160 and 320 read clk TMR test passed
  .clk(clk160_read), 
  .clk_A(clk160_read_A), 
  .clk_B(clk160_read_B), 
  .clk_C(clk160_read_C),
  .check_enable()
  );




reg fifo_write;
reg fifo_read;
wire fifo_full, fifo_empty;
reg [`DATASIZE-1:0] fifo_data_in;
wire [`DATASIZE-1:0] fifo_data_out;
reg rst;
  //interface FIFO
  fifo16 #(.DSIZE(`DATASIZE),.ASIZE(4),.SYN_DEPTH(1)) 
    readout_fifo_inst (
      .rdata(fifo_data_out),
      .wfull(fifo_full),
      .rempty(fifo_empty),
      .wdata(fifo_data_in),
      .winc(fifo_write), .wclk(clk160), .wrst(rst),
      .rinc(fifo_read), .rclk(clk160_read), .rrst(rst)
    );
reg fifo_read_TMR;
wire fifo_full_TMR, fifo_empty_TMR;
reg [`DATASIZE-1:0] fifo_data_in_TMR;
wire [`DATASIZE-1:0] fifo_data_out_TMR;
fifo16_TMR #(.DSIZE(`DATASIZE),.ASIZE(4),.SYN_DEPTH(1)) 
    readout_fifo_TMR_inst (
      .rdata(fifo_data_out_TMR),
      .wfull(fifo_full_TMR),
      .rempty(fifo_empty_TMR),
      .wdata(fifo_data_in_TMR),
      .winc(fifo_write), .wrst(rst),
      .wclk_A (clk160_A), .wclk_B (clk160_B), .wclk_C (clk160_C), 
      .rinc(fifo_read_TMR), .rrst(rst),
      .rclk_A (clk160_read_A), .rclk_B (clk160_read_B), .rclk_C (clk160_read_C)
    );


wire TMR_normal_equal;
reg [16:0] enable_delay;
assign TMR_normal_equal =(fifo_full == fifo_full_TMR) &(fifo_empty==fifo_empty_TMR)&
						((check_enable&enable_delay[16]&(~fifo_empty))?(fifo_data_out == fifo_data_out_TMR):1);
reg TMR_normal_equal_r;
always @(posedge clk160_read) begin
	TMR_normal_equal_r <= TMR_normal_equal;
end


always @(posedge clk160_read or negedge check_enable) begin
	if(~check_enable)
		enable_delay <= 5'b0;
    else if(check_enable&fifo_read_TMR)
        enable_delay <= {enable_delay[15:0],check_enable};
        
end

// reg check_enable = 1'b1;
always @(posedge clk160_read) begin
	// if (check_enable&enable_delay[4]) begin
		if(~TMR_normal_equal_r)begin
			$display("At %t TMR outuput is not equal to normal output!" ,$realtime);
			$stop;
		end	
	// end
end



reg[`DATASIZE-1:0] random_data_in;
always @(posedge clk160  ) begin
	random_data_in[`DATASIZE-1:32] <= $urandom;
	random_data_in[31:0] <= $urandom;
end


// assign #0.5 fifo_read_clk =clk160_read ;
reg fifo_write_random;

always @(posedge clk160 ) begin
	fifo_write_random <= $urandom;
	fifo_write_random <= $urandom;
	fifo_write_random <= $urandom; //different from fifo_read_random
	fifo_write <= fifo_write_random;
end

always @(posedge clk160  ) begin
	if (rst) begin
		// reset
		fifo_data_in <= `DATASIZE'b0;
	end
	else if (~fifo_full&fifo_write_random) begin
		fifo_data_in <= random_data_in;
		// fifo_data_in <= fifo_data_in + 10'b1;
	end
end
always @(posedge clk160  ) begin
	if (rst) begin
		// reset
		fifo_data_in_TMR <= 10'b0;
	end
	else if (~fifo_full_TMR&fifo_write_random) begin
		fifo_data_in_TMR <= random_data_in;
		// fifo_data_in_TMR <= fifo_data_in_TMR + 10'b1;
	end
end


reg fifo_read_random;
always @(posedge clk160_read ) begin
	fifo_read_random <= $urandom;
end
always @(posedge clk160_read  ) begin
	if (rst) begin
		// reset
		fifo_read <= 1'b0;
	end
	else if (~fifo_empty&fifo_read_random) begin
		fifo_read <= 1'b1;
	end else begin
		fifo_read <= 1'b0;
	end
end



always @(posedge clk160_read  ) begin
	if (rst) begin
		// reset
		fifo_read_TMR <= 1'b0;
	end
	else if (~fifo_empty_TMR&fifo_read_random) begin
		fifo_read_TMR <= 1'b1;
	end else begin
		fifo_read_TMR <= 1'b0;
	end
end



reg [11:0] write_number;
reg [11:0] read_number;
always @(posedge clk160  ) begin
	if (rst) begin
		// reset
		write_number <= 12'b0;
	end
	else if (~fifo_full) begin
		write_number <= write_number + 12'b1;
	end
end

always @(posedge clk160_read  ) begin
	if (rst) begin
		// reset
		read_number <= 12'b0;
	end
	else if (fifo_read&(~fifo_empty)) begin
		read_number <= read_number + 12'b1;
	end
end

reg [11:0] write_number_TMR;
reg [11:0] read_number_TMR;
always @(posedge clk160  ) begin
	if (rst) begin
		// reset
		write_number_TMR <= 12'b0;
	end
	else if (~fifo_full_TMR) begin
		write_number_TMR <= write_number_TMR + 12'b1;
	end
end

always @(posedge clk160_read  ) begin
	if (rst) begin
		// reset
		read_number_TMR <= 12'b0;
	end
	else if (fifo_read_TMR&(~fifo_empty_TMR)) begin
		read_number_TMR <= read_number_TMR + 12'b1;
	end
end



reg check_enable_reg;
task force_reg(
  input integer reg_index,
  input real delay_time);
begin
    check_enable_reg = 1'b0;
    @(posedge clk160_read); //register only have SEU after posedge clk160_read
    #delay_time;
    enable_force_reg[reg_index]= 1'b1;
    #0.001;
    enable_force_reg[reg_index]= 1'b0;     
end
endtask


task single_reg_TMR_test(
  input integer reg_index,
  input integer repeat_number);
  real delay_time;
begin    
    //for one SEU
    repeat(repeat_number)begin  //
        #($itor($urandom_range(10000,1))/100);  //randomly wait for 0.01 to 100 ns
        delay_time =  $itor($urandom_range(9990,10))/10000*1.5625*2;//randomly wait for 0.001 to 0.999 clk160_read
        force_reg(reg_index,delay_time);
        check_enable_reg = 1'b1;
    end
end
endtask

task reg_TMR_test(
  input integer repeat_number);
  integer i;
begin
    for (i = 0; i < `REG_NUM; i = i + 1)begin
      single_reg_TMR_test(i,repeat_number);
    end
    #100;
    if(check_enable)$display("check_enable is on.");
    else $display("warning: check_enable off!!");
    $display("reg TMR test passed.");

end
endtask



initial begin
	$timeformat(-9, 5, "ns", 8);	
	rst = 1'b0;
	#300;
	rst = 1'b1;
	#300;
	rst = 1'b0;
	#1000;//normal compare
	//read clk TMR test
	TMR_clk160_read_generator_inst.clock_TMR_test(1000);
	#1000;//
	//write clk TMR test
	TMR_clk160_generator_inst.clock_TMR_test(1000);
	#1000;//
	reg_TMR_test(1000);
	$stop;

end

endmodule
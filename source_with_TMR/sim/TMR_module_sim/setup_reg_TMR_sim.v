/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : setup_reg_TMR_sim.v
//  Author     : Yuxiang Guo
//  Revision   : 
//               First created on March 21st, 2019
//  Note       : 
//     
`timescale 1 ns / 1 fs
`include "common_definition.v"
`define REG_LIST
module setup_reg_TMR_sim();
`include "setup_reg_TMR_sim_reglist.v"
parameter WIDTH = 30;
reg tck, trst, shift, enable, update, check_enable;
wire serial_data_in, serial_data_out, serial_data_out_TMR;
wire [WIDTH-1:0] parallel_data_out, parallel_data_out_TMR;
wire clk, clk160_A, clk160_B, clk160_C;
reg [WIDTH-1:0] tdo_reg, tdo_TMR_reg;
reg [WIDTH-1:0] data_in, data_in_last;

TMR_clk_generator TMR_clk_generator_inst (
  .enable320(1'b0),
  .clk(clk), 
  .clk_A(clk160_A), 
  .clk_B(clk160_B), 
  .clk_C(clk160_C),
  .check_enable(check_enable_clk)
  );


setup_reg #(
		.WIDTH(WIDTH),
		.initial_value(0)
	) inst_setup_reg (
		.clk               (tck),
		.trst              (trst),
		.shift             (shift),
		.serial_data_in    (serial_data_in),
		.serial_data_out   (serial_data_out),
		.parallel_data_out (parallel_data_out)
	);

setup_reg_TMR #(
		.WIDTH(WIDTH),
		.initial_value(0)
	) inst_setup_reg_TMR (
		.clk               (tck),
		.trst              (trst),
		.clk160_A          (clk160_A),
		.clk160_B          (clk160_B),
		.clk160_C          (clk160_C),
		.shift             (shift),
		.enable            (enable),
		.update            (update),
		.serial_data_in    (serial_data_in),
		.serial_data_out   (serial_data_out_TMR),
		.parallel_data_out (parallel_data_out_TMR)
	);
assign serial_data_in = data_in[0];
initial begin
   data_in = 1'b0;
   data_in_last = 1'b0;
   enable = 1'b1;
   tdo_reg = 1'b0;
   tdo_TMR_reg = 1'b0;
end

always begin
	tck <= 1'b0;
	#12.5;
	tck <= 1'b1;
	#12.5;	
end

always @(posedge tck) begin
    if(enable)begin
    	if(shift)begin
    		data_in <= {data_in[0],data_in[WIDTH-1:1]};
    		tdo_reg <= {serial_data_out, tdo_reg[WIDTH-1:1]};
    		tdo_TMR_reg <= {serial_data_out_TMR, tdo_TMR_reg[WIDTH-1:1]};
    	end
    end
end



always @(posedge clk) begin
	if(check_enable) begin
		if(tdo_reg!=data_in_last) begin
			$display("golden serial output != input");
			$stop;
		end else if(tdo_TMR_reg!=data_in_last) begin
			$display("TMR serial output != input");
			$stop;
		end else if(parallel_data_out!=data_in) begin
			$display("golden parallel_data_out != input");
			$stop;
		end else if(parallel_data_out_TMR!=data_in) begin
			$display("TMR serial parallel_data_out_TMR != input");
			$stop;
		end	
	end
end

reg check_enable_reg = 1'b1;

task force_reg(
  input integer reg_index,
  input real delay_time);
begin
    check_enable_reg = 1'b0;
    @(posedge clk); //register only have SEU after posedge 
    #delay_time;
    enable_force_reg[reg_index]= 1'b1;
    #0.001;
    enable_force_reg[reg_index]= 1'b0;     
end
endtask


task single_reg_TMR_test(
  input integer reg_index,
  input integer repeat_number);
  real delay_time;
begin    
    //for one SEU
    repeat(repeat_number)begin  //
        #($itor($urandom_range(10000,1))/100);  //randomly wait for 0.01 to 100 ns
        delay_time =  $itor($urandom_range(9990,10))/10000*1.5625*4;//randomly wait for 0.001 to 0.999 clk160
        force_reg(reg_index,delay_time);
        check_enable_reg = 1'b1;
    end
end
endtask

task reg_TMR_test(
  input integer repeat_number);
  integer i;
begin
    for (i = 0; i < `REG_NUM; i = i + 1)begin
      single_reg_TMR_test(i,repeat_number);
    end
    #100;
    if(check_enable_reg)$display("check_enable is on.");
    else $display("warning: check_enable off!!");
    $display("reg TMR test passed.");

end
endtask



task configuration_check();
begin
	#300;
	@(posedge tck);
	check_enable <= 1'b0;
	data_in_last <= data_in;
	data_in <= $random;
	enable <= 1'b1;
	shift <= 1'b1;
	repeat(30) @ (posedge tck);
	update <= 1'b1;
	enable <= 1'b0;
	shift <= 1'b0;
	#25;
	update <= 1'b0;
	check_enable <= 1'b1;
	
end
endtask



initial begin
	enable = 1'b0;
	update = 1'b0;
	shift  = 1'b0;
	check_enable = 1'b0;
	trst = 1'b1;
	#300;
	trst = 1'b0;
	#300;
	trst = 1'b1;
	#300;

	repeat(100)begin
		configuration_check();
	end
	TMR_clk_generator_inst.clock_TMR_test(100);
	#300;
	reg_TMR_test(100);
	$stop;

end
endmodule
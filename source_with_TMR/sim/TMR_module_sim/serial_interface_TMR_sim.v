/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : serial_interface_TMR_sim.v
//  Author     : Yuxiang Guo
//  Revision   : 
//               First created on March 21st, 2019
//  Note       : 
//     
`timescale 1 ns / 1 fs
`define REG_LIST


module serial_interface_TMR_sim();
`include "serial_interface_TMR_sim_reglist.v"

wire clk320, clk_A, clk_B, clk_C;
wire check_enable;
wire check_enable_clk;
reg check_enable_reg = 1'b1;
assign check_enable = check_enable_clk & check_enable_reg;
TMR_clk_generator TMR_clk_generator_inst (
  .enable320(1'b1),
  .clk(clk320), 
  .clk_A(clk_A), 
  .clk_B(clk_B), 
  .clk_C(clk_C),
  .check_enable(check_enable_clk)
  );

reg rst;
wire interface_fifo_read;
reg [9:0] interface_fifo_data;
wire [1:0] d_line;


serial_interface
serial_interface_inst(
  .clk(clk320),
  .rst(rst),

  .enable_320(1'b1),
  .enable(1'b1),

  .interface_fifo_empty(1'b0),
  .interface_fifo_read(interface_fifo_read),
  .interface_fifo_data(interface_fifo_data),

  .d_line(d_line)
);




wire interface_fifo_read_TMR;
wire [1:0] d_line_TMR;
serial_interface_TMR 
serial_interface_TMR_inst
    (
        .clk_A                (clk_A),
        .clk_B                (clk_B),
        .clk_C                (clk_C),
        .rst                  (rst),
        .enable_320           (1'b1),
        .enable               (1'b1),
        .interface_fifo_empty (1'b0),
        .interface_fifo_read  (interface_fifo_read_TMR),
        .interface_fifo_data  (interface_fifo_data),
        .d_line               (d_line_TMR)
    );


initial begin
   interface_fifo_data = $random;
end

always @(posedge clk320) begin
  if (interface_fifo_read) begin
    interface_fifo_data <= $random;
  end
end



reg [10:0] enable_delay;
always @(posedge clk320 or negedge check_enable) begin
    if(check_enable)
        enable_delay <= {enable_delay[9:0],check_enable};
    else
        enable_delay <= 11'b0;
end


wire TMR_normal_equal;
assign TMR_normal_equal = (d_line == d_line_TMR) & (interface_fifo_read_TMR == interface_fifo_read) ;
reg TMR_normal_equal_r;
always @(posedge clk320) begin
  TMR_normal_equal_r <= TMR_normal_equal;
end
always @(posedge clk320) begin
  if (check_enable&&enable_delay[10]) begin
    if(~TMR_normal_equal_r)begin
      $display("At %t TMR outuput is not equal to normal output!" ,$realtime);
      $stop;
    end 
  end
end
// always @(posedge clk320) begin
//     if(check_enable&&enable_delay[10])begin
//         if(d_line != d_line_TMR)begin
//             $display("Error dline");
//             $stop;              
//         end
//         if(interface_fifo_read_TMR != interface_fifo_read)begin            
//             $display("Error interface_fifo_read");
//             $stop;
//         end
//     end
// end

task module_reset();
begin
    rst = 1'b1;
    #300;
    rst = 1'b0;
end
endtask






task force_reg(
  input integer reg_index,
  input real delay_time);
begin
    check_enable_reg = 1'b0;
    @(posedge clk320); //register only have SEU after posedge clk320
    #delay_time;
    enable_force_reg[reg_index]= 1'b1;
    #0.001;
    enable_force_reg[reg_index]= 1'b0;     
end
endtask


task single_reg_TMR_test(
  input integer reg_index,
  input integer repeat_number);
  real delay_time;
begin    
    //for one SEU
    repeat(repeat_number)begin  //
        #($itor($urandom_range(10000,1))/100);  //randomly wait for 0.01 to 100 ns
        delay_time =  $itor($urandom_range(9990,10))/10000*1.5625*2;//randomly wait for 0.001 to 0.999 clk320
        force_reg(reg_index,delay_time);
        check_enable_reg = 1'b1;
    end
end
endtask

task reg_TMR_test(
  input integer repeat_number);
  integer i;
begin
    for (i = 0; i < `REG_NUM; i = i + 1)begin
      single_reg_TMR_test(i,repeat_number);
    end
    #100;
    if(check_enable)$display("check_enable is on.");
    else $display("warning: check_enable off!!");
    $display("reg TMR test passed.");

end
endtask




initial begin
    $srandom(10);
    $timeformat(-9, 5, "ns", 8);
    //$timeformat params:
    //1) Scaling factor (?9 for nanoseconds, ?12 for picoseconds)
    //2) Number of digits to the right of the decimal point
    //3) A string to print after the time value
    //4) Minimum field width
    rst = 1'b0;
    module_reset();
    TMR_clk_generator_inst.clock_TMR_test(1000);
    #137.12;
    reg_TMR_test(1000);
    $stop;
    


    

end

endmodule
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : ttc_control_decoder_TMR_tb.v
//  Author     : Yuxiang Guo
//  Revision   : 
//               First created on March 27th, 2019
//  Note       : 
//    

`define REG_LIST
module ttc_control_decoder_TMR_tb();
`include "ttc_control_decoder_TMR_tb_reglist.v"

reg rst;

reg trigger_direct_in,event_reset_direct_in;
reg reset_jtag_in;
reg event_reset_jtag_in;
reg encoded_control_in;
reg enable_new_ttc;
wire enable_master_reset_code;  assign  enable_master_reset_code = 1'b1;
wire enable_trigger;			assign  enable_trigger = 1'b1;
reg enable_direct_trigger;		
reg enable_direct_bunch_reset;	
reg enable_direct_event_reset;	
wire trigger;
wire event_reset;
wire master_reset;
wire [11:0] roll_over;			assign roll_over = 12'd20;
wire [11:0] coarse_count_offset;assign coarse_count_offset = 12'd05;
reg auto_roll_over;			
reg bypass_bcr_distribution;   
wire coarse_bcr;
wire trigger_count_bcr;

reg check_enable = 1'b1;
wire clk160, clk160_A, clk160_B, clk160_C;
TMR_clk_generator TMR_clk_generator_inst (
  .enable320(1'b0),
  .clk(clk160), 
  .clk_A(clk160_A), 
  .clk_B(clk160_B), 
  .clk_C(clk160_C),
  .check_enable()
  );



ttc_control_decoder
	ttc_control_decoder_inst( 
	.clk160(clk160), 

	.reset_in(rst), 
	.trigger_direct_in(trigger_direct_in),
	.bunch_reset_direct_in(bcr),
	.event_reset_direct_in(event_reset_direct_in),

	.reset_jtag_in(reset_jtag_in),
	.event_reset_jtag_in(event_reset_jtag_in),

	.encoded_control_in(encoded_control_in),

    .enable_new_ttc(enable_new_ttc),
	.enable_master_reset_code(enable_master_reset_code),
	.enable_trigger(enable_trigger),
	.enable_direct_trigger(enable_direct_trigger),
	.enable_direct_bunch_reset(enable_direct_bunch_reset),
	.enable_direct_event_reset(enable_direct_event_reset),

	. trigger(trigger),

	. event_reset(event_reset),
	. master_reset(master_reset),

	.roll_over(roll_over),
	.coarse_count_offset(coarse_count_offset),

	.auto_roll_over(auto_roll_over),
	.bypass_bcr_distribution(bypass_bcr_distribution),

	.coarse_bcr(coarse_bcr),
	.trigger_count_bcr(trigger_count_bcr)
);


wire trigger_TMR, event_reset_TMR, master_reset_TMR, coarse_bcr_TMR, trigger_count_bcr_TMR;




ttc_control_decoder_TMR
	ttc_control_decoder_TMR_inst( 
	.clk160_A(clk160_A), 
	.clk160_B(clk160_B), 
	.clk160_C(clk160_C), 

	.reset_in(rst), 
	.trigger_direct_in(trigger_direct_in),
	.bunch_reset_direct_in(bcr),
	.event_reset_direct_in(event_reset_direct_in),

	.reset_jtag_in(reset_jtag_in),
	.event_reset_jtag_in(event_reset_jtag_in),

	.encoded_control_in(encoded_control_in),

    .enable_new_ttc(enable_new_ttc),
	.enable_master_reset_code(enable_master_reset_code),
	.enable_trigger(enable_trigger),
	.enable_direct_trigger(enable_direct_trigger),
	.enable_direct_bunch_reset(enable_direct_bunch_reset),
	.enable_direct_event_reset(enable_direct_event_reset),

	. trigger(trigger_TMR),

	. event_reset(event_reset_TMR),
	. master_reset(master_reset_TMR),

	.roll_over(roll_over),
	.coarse_count_offset(coarse_count_offset),

	.auto_roll_over(auto_roll_over),
	.bypass_bcr_distribution(bypass_bcr_distribution),

	.coarse_bcr(coarse_bcr_TMR),
	.trigger_count_bcr(trigger_count_bcr_TMR)
);

wire TMR_normal_equal;
assign TMR_normal_equal = (trigger == trigger_TMR) & (event_reset == event_reset_TMR) &(master_reset==master_reset_TMR) &(coarse_bcr==coarse_bcr_TMR)&(trigger_count_bcr==trigger_count_bcr_TMR);
reg TMR_normal_equal_r;
always @(posedge clk160) begin
	TMR_normal_equal_r <= TMR_normal_equal;
end
always @(posedge clk160) begin
	if (check_enable) begin
		if(~TMR_normal_equal_r)begin
			$display("At %t TMR outuput is not equal to normal output!" ,$realtime);
			$stop;
		end	
	end
end








reg clk40_r;
always begin
  clk40_r =1'b0;
  #12.5;
  clk40_r =1'b1;
  #12.5;
end
wire clk40;
assign #3 clk40 = clk40_r;

reg [11:0] bunch_counter;

always @(posedge clk40  ) begin
	if (rst) begin
		// reset
		bunch_counter <= 12'b0;
	end	else begin
		bunch_counter <= bunch_counter ==roll_over ? 12'b0 : bunch_counter + 12'b1;
	end
end
assign  bcr = auto_roll_over ? 1'b0 : bunch_counter == roll_over ;

task legacy_trigger();
begin
	//trigger
	@(posedge clk40) encoded_control_in <= 1'b1;
	@(posedge clk40) encoded_control_in <= 1'b0;
	@(posedge clk40) encoded_control_in <= 1'b0;
	@(posedge clk40) encoded_control_in <= 1'b0;
end
endtask

task legacy_master_reset();
begin
	//master reset
	@(posedge clk40) encoded_control_in <= 1'b1;
	@(posedge clk40) encoded_control_in <= 1'b0;
	@(posedge clk40) encoded_control_in <= 1'b1;
	@(posedge clk40) encoded_control_in <= 1'b0;	
end
endtask

task legacy_bcr_reset();
begin
	//bcr reset
	@(posedge clk40) encoded_control_in <= 1'b1;
	@(posedge clk40) encoded_control_in <= 1'b1;
	@(posedge clk40) encoded_control_in <= 1'b0;
	@(posedge clk40) encoded_control_in <= 1'b0;	
end
endtask


task legacy_event_reset();
begin
	//event reset
	@(posedge clk40) encoded_control_in <= 1'b1;
	@(posedge clk40) encoded_control_in <= 1'b1;
	@(posedge clk40) encoded_control_in <= 1'b1;
	@(posedge clk40) encoded_control_in <= 1'b0;	
end
endtask

task legacy_random_in();
begin
	//event reset
	repeat(100)begin
	@(posedge clk40) encoded_control_in <= 1'b1;
	@(posedge clk40) encoded_control_in <= $urandom;
	@(posedge clk40) encoded_control_in <= $urandom;
	@(posedge clk40) encoded_control_in <= 1'b0;
	end	
end
endtask


task legacy_TTC_mode_test();
begin
	enable_new_ttc = 1'b0;
	$display("At %t legacy_TTC_mode_test start!", $realtime);
	legacy_trigger();
	#200;
	legacy_master_reset();
	#200;
	legacy_bcr_reset();
	#200;
	legacy_event_reset();
	#200;
	legacy_random_in();
	#200;
	$display("At %t legacy_TTC_mode_test finished!", $realtime);
	
end
endtask

task new_TTC_trigger();
begin
	@(negedge clk160) encoded_control_in <= 1'b1;
	@(negedge clk160) encoded_control_in <= 1'b0;
	@(negedge clk160) encoded_control_in <= 1'b0;
	@(negedge clk160) encoded_control_in <= 1'b0;
	@(negedge clk160) encoded_control_in <= 1'b0;
end
endtask


task new_TTC_master_reset();
begin
	@(negedge clk160) encoded_control_in <= 1'b1;
	@(negedge clk160) encoded_control_in <= 1'b0;
	@(negedge clk160) encoded_control_in <= 1'b0;
	@(negedge clk160) encoded_control_in <= 1'b1;
	@(negedge clk160) encoded_control_in <= 1'b0;	
end
endtask

task new_TTC_bcr_reset();
begin
	@(negedge clk160) encoded_control_in <= 1'b1;
	@(negedge clk160) encoded_control_in <= 1'b1;
	@(negedge clk160) encoded_control_in <= 1'b1;
	@(negedge clk160) encoded_control_in <= 1'b0;
	@(negedge clk160) encoded_control_in <= 1'b0;
end
endtask

task new_TTC_event_reset();
begin
	@(negedge clk160) encoded_control_in <= 1'b1;
	@(negedge clk160) encoded_control_in <= 1'b0;
	@(negedge clk160) encoded_control_in <= 1'b1;
	@(negedge clk160) encoded_control_in <= 1'b1;
	@(negedge clk160) encoded_control_in <= 1'b0;
end
endtask

task new_TTC_random_encoded();
begin
repeat(100) begin
	@(negedge clk160) encoded_control_in <= 1'b1;
	@(negedge clk160) encoded_control_in <= $urandom;
	@(negedge clk160) encoded_control_in <= $urandom;
	@(negedge clk160) encoded_control_in <= $urandom;
	@(negedge clk160) encoded_control_in <= 1'b0;
	end
end
endtask


task new_TTC_mode_test();
begin
	enable_new_ttc = 1'b1;
	$display("At %t new_TTC_mode_test start!", $realtime);
	#200;
	new_TTC_master_reset();
	#200;
	new_TTC_bcr_reset();
	#200;
	new_TTC_event_reset();
	#200;
	new_TTC_random_encoded();
	#200;
	$display("At %t new_TTC_mode_test finished!", $realtime);

end
endtask


reg check_enable_reg;
task force_reg(
  input integer reg_index,
  input real delay_time);
begin
    check_enable_reg = 1'b0;
    @(posedge clk160); //register only have SEU after posedge clk320
    #delay_time;
    enable_force_reg[reg_index]= 1'b1;
    #0.001;
    enable_force_reg[reg_index]= 1'b0;     
end
endtask


task single_reg_TMR_test(
  input integer reg_index,
  input integer repeat_number);
  real delay_time;
begin    
    //for one SEU
    repeat(repeat_number)begin  //
        #($itor($urandom_range(10000,1))/100);  //randomly wait for 0.01 to 100 ns
        delay_time =  $itor($urandom_range(9990,10))/10000*1.5625*2;//randomly wait for 0.001 to 0.999 clk320
        force_reg(reg_index,delay_time);
        check_enable_reg = 1'b1;
    end
end
endtask

task reg_TMR_test(
  input integer repeat_number);
  integer i;
begin
    for (i = 0; i < `REG_NUM; i = i + 1)begin
      single_reg_TMR_test(i,repeat_number);
    end
    #100;
    if(check_enable)$display("check_enable is on.");
    else $display("warning: check_enable off!!");
    $display("reg TMR test passed.");

end
endtask


initial begin
	$timeformat(-9, 5, "ns", 8);
	enable_new_ttc = 1'b0;
	enable_direct_trigger = 1'b1;		
	enable_direct_bunch_reset= 1'b1;	
	enable_direct_event_reset= 1'b1;
	bypass_bcr_distribution= 1'b1;
	auto_roll_over = 1'b1;


	reset_jtag_in = 1'b0;
	trigger_direct_in = 1'b0;
	event_reset_direct_in = 1'b0;
	event_reset_jtag_in = 1'b0;
	encoded_control_in = 1'b0;
	
	rst = 1'b1;
	#300;
	rst = 1'b0;
	//reset direct in mode, auto roll over, no bcr in
	# 700;
	trigger_direct_in = 1'b1;
	#25;
	trigger_direct_in = 1'b0;
	#300;
	event_reset_direct_in = 1'b1;
	#25;
	event_reset_direct_in = 1'b0;
	#300;
	event_reset_jtag_in = 1'b1;
	#25;
	event_reset_jtag_in = 1'b0;
	#300;
	reset_jtag_in = 1'b1;
	#25;
	reset_jtag_in = 1'b0;
	#300;

	//direct bcr in, bypass bcr distribution
	auto_roll_over = 1'b0;
	bypass_bcr_distribution= 1'b1;
	#1000;

	//encode in mode, direct bcr in, bcr distribution
	enable_new_ttc = 1'b0;
	enable_direct_trigger = 1'b0;		
	enable_direct_bunch_reset= 1'b0;	
	enable_direct_event_reset= 1'b0;
	bypass_bcr_distribution= 1'b0;
	auto_roll_over = 1'b0;
	#1000;
	legacy_TTC_mode_test();
	# 300;
	new_TTC_mode_test();
	#1000;
	//no input, TMR test
	#300;
	TMR_clk_generator_inst.clock_TMR_test(1000);
	#300;
	reg_TMR_test(1000);

	//encode in mode, auto rollover, bcr distribution
	auto_roll_over = 1'b1;
	bypass_bcr_distribution= 1'b0;
	#1000;
	legacy_TTC_mode_test();
	# 300;
	new_TTC_mode_test();
	#1000;


	//no input, TMR test
	#300;
	TMR_clk_generator_inst.clock_TMR_test(1000);
	#300;
	reg_TMR_test(1000);
	#300;
	TMR_clk_generator_inst.clock_TMR_test(1000);
	#300;
	reg_TMR_test(1000);
	
	$stop;			

end

// initial begin
// 	#12876000;
// 	//sending code while TMR test, auto rollover, bcr distribution
// 	repeat(1000)begin
// 	legacy_TTC_mode_test();
// 	# 300;
// 	new_TTC_mode_test();
// 	end
// end

endmodule
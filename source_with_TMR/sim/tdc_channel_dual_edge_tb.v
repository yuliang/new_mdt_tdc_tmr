/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : tdc_channel_dual_edge_tb.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 12th, 2018
//  Note       :  test bench for MDT TDC channel dual edge logic


module tdc_channel_dual_edge_tb;

wire rst;
wire clk160,clk320,clk320M_4,clk_hit;

wire [2:0] width_select;
wire [11:0] roll_over; assign  roll_over = 12'b110111101100;
wire enable_pair,enable_leading;
wire [`COMBINE_TIME_OUT_SIZE-1:0] combine_time_out_config;
wire rising_is_leading,enable_trigger,channel_data_debug;

//
assign  enable_pair = 1'b0,
		enable_leading =  1'b0,
		enable_trigger = 1'b1,
		channel_data_debug = 1'b0,
		rising_is_leading = 1'b1,
		combine_time_out_config = 'h96,
		width_select = 'b0;
//


wire Rdy_r,Rdy_f;
wire [3:0] q_r,q_f;
wire [14 :0]    cnt_r, cnt_inv_r,cnt_f, cnt_inv_f;

wire fake_hit;

wire fifo_read;
wire fifo_read_clk; assign fifo_read_clk = clk160;
wire [`CHANNEL_FIFO_WIDTH-1+5:0] channel_data;
wire fifo_empty;

wire trigger;
wire [11:0] limit_up;
wire [11:0] limit_down;
wire trigger_processing;
//in chip
//in chip logic
tdc_channel_dual_edge  #(.DATA_DEEP(16), .FIFO_ASIZE(2))//FIFO_DEEP= 2^FIFO_ASIZE
	DUT(
  		.rst(rst),      // reset, not BC reset
  		.clk160(clk160),
		
		.channel_enable_r(1'b1),.channel_enable_f(1'b1),
  		.fine_sel(4'b0011), //encoding for fine time selection
  		.lut0(6'h00), .lut1(6'h05), .lut2(6'h0a), .lut3(6'h0d), .lut4(6'h13), .lut5(6'h14), .lut6(6'h1a), .lut7(6'h1e), .lut8(6'h20), .lut9(6'h24),
  		.luta(6'h28), .lutb(6'h2d), .lutc(6'h33), .lutd(6'h34), .lute(6'h3b), .lutf(6'h3c),
  		//.width_select(width_select),
  		//.roll_over(roll_over),
  		//.enable_pair(enable_pair),
  		.enable_leading(enable_leading),
  		.combine_time_out_config(combine_time_out_config), 
  		.rising_is_leading(rising_is_leading),
  		.enable_trigger(enable_trigger),
  		.channel_data_debug(channel_data_debug),
  		
  		.chnl_ID(5'b01010),
  		
  		.Rdy_r(Rdy_r),.Rdy_f(Rdy_f), // ready
  		.q_r(q_r),.q_f(q_f), // raw fine codes 
  		.cnt_r(cnt_r), .cnt_inv_r(cnt_inv_r),.cnt_f(cnt_f), .cnt_inv_f(cnt_inv_f), // coarse counter
  		
  		.fake_hit(fake_hit),
  		
  		.fifo_read(fifo_read),
  		.fifo_read_clk(fifo_read_clk),
  		.channel_data_out(channel_data),
  		.fifo_empty(fifo_empty),
  		.fifo_full(),
  		
  		.trigger(trigger),
  		.limit_up(limit_up),
  		.limit_down(limit_down),
  		.trigger_processing(trigger_processing)
	);

fake_hit_generator #(.WIDTH(12),.OUTPUT_WIDTH(1))
	fake_hit_generator_inst(
    	.clk(clk160),
    	.rst(rst),
    	.enable(enable_trigger),
    	.fake_hit(fake_hit),
    	.time_interval(12'h0e0)
    );

fake_trigger_process 
	fake_trigger_process_inst(
		.trigger(trigger_input),
		.rst(rst),
		.clk160(clk160),
		.counter_fast(counter_fast[16:5]),
		.roll_over(roll_over),
		.trigger_latency(12'h0C8),//12'h064
		.matching_window(12'h0C0),//12'h028
		.large_matching(1'b0),
		.trigger_out(trigger),
		.limit_up(limit_up),.limit_down(limit_down)
	);

//in chip
//im chip analog
reg [16:0] counter_fast=17'b0;
always@(posedge clk320M_4)begin
    if(counter_fast=={roll_over,5'b11111})begin
      counter_fast <= 17'b0;
    end else begin
      counter_fast <= counter_fast + 17'b1;
    end     
end
tdcdata_gen 
	tdcdata_gen_inst(
		.hit(hit),
		.rst(rst),
		.clk320(clk320),
		.counter_fast(counter_fast),
		.Rdy_chnl(Rdy_r), .t_Rdy_chnl(Rdy_f),
		.q_chnl(q_r), .t_q_chnl(q_f),
		.cnt_chnl(cnt_r), .t_cnt_chnl(cnt_f),
		.cnt_inv_chnl(cnt_inv_r), .t_cnt_inv_chnl(cnt_inv_f)
	);

//for sim 

random_hit_generator 
	random_hit_generator_inst(
		.clk(clk_hit),
		.rst(rst),
		.hit_out(hit),
		.enable_out(1'b1),
		.RATE(32'h0083126E),//32'h00417874->200k,32'h0083126E->400k,32'hFFFF_FFFF-> high ednsity
		.seed1(32'hAD1659EF),
		.seed2(32'h3D9EF9ED),
		.dead_time_input(8'ha0)
	);

fake_trigger_gen #(.latency(50000))
	fake_trigger_gen_inst(
		.clk320(clk320),
		.rst(rst),
		.hit(hit),
		.trigger(trigger_input),
		.trigger_th(32'hffff_ffff)
	);

sim_clock_gen #(.clk_320_delay(0),.clk_160_delay(0.8))
	sim_clock_gen_inst(
		.clk_160(clk160),
		.clk_320(clk320),
		.clk_320M_4(clk320M_4),
		.clk_hit(clk_hit)
	);

reset_gen  #(.start(3000),.width(100))
	reset_gen_inst(
		.rst(rst)
	);
reg fifo_read_r;
always @(posedge clk160) begin
	fifo_read_r <= 1'b0;
	if (!fifo_empty) begin
		fifo_read_r <= ($random < 32'h0a_00_00_00);
	end
end
assign fifo_read=fifo_read_r&(!fifo_empty);

always @(*) begin
	if (chnl_fifo_overflow) begin
		$stop;
	end
	if(limit_up<limit_down)begin
		$stop;
	end
end

//statistic
integer i;
reg [3:0] martching_number;
always @* begin
	martching_number = 4'b0;
	for(i=0;i<16;i=i+1)begin
		martching_number = martching_number + DUT.chnl_trigger_matching_inst.data_interest_r[i];
	end
end

reg [2:0] chnl_fifo_number;
wire fifo_write;
assign fifo_write = (DUT.chnl_fifo_write&(~DUT.chnl_fifo_full));
always @(posedge clk160  ) begin
	if (rst) begin
		// reset
		chnl_fifo_number <= 2'b0;
	end
	else begin
		chnl_fifo_number <= chnl_fifo_number + fifo_write - DUT.fifo_read;
	end
end
wire chnl_fifo_overflow;
assign  chnl_fifo_overflow = (DUT.chnl_fifo_full) && (DUT.chnl_fifo_write);
endmodule
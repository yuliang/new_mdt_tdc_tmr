
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : gray_to_binary.v
//  Author     :Yu Liang
//  Revision   : 
//               First created on March 13th, 2016
//  Note       : verilog behavior for gray code to binary code
`include "common_definition.v"
module gray_to_binary
#(parameter
	WIDTH=2
)
(
  input  [WIDTH-1:0] gray_code,
  output [WIDTH-1:0] binary_code
);
wire [WIDTH:0] binary_code_inner;
assign binary_code_inner[WIDTH] = 1'b0;
generate
	genvar i;
	for (i = 0; i < WIDTH; i = i + 1)
	begin:graytobinary
		assign	binary_code_inner[WIDTH-1-i] = gray_code[WIDTH-1-i]^binary_code_inner[WIDTH-i];
	end
endgenerate
endmodule
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : tdcdata_gen.v
//  Author     : Jinghong Wang
//  Revision   : 
//               First created on June 8th, 2016
//  Note       : 
// 


module tdcdata_gen(
input hit,
input rst,
input clk320,
input [16:0] counter_fast,
output  Rdy_chnl, t_Rdy_chnl,
output reg [5:0] q_chnl, t_q_chnl,
output reg [14:0] cnt_chnl, t_cnt_chnl,
output reg [14:0] cnt_inv_chnl, t_cnt_inv_chnl);

reg [1:0] hit_r;
wire      pos_hit, trailing_hit;
reg [4:0] pos_hit_r, trailing_hit_r;

reg [16:0] counter_fast_r=17'b0;
reg [16:0] counter_fast_r_con=17'b0;
reg clk_fast=1'b1;


always@(posedge hit)begin
     counter_fast_r <= counter_fast;
end
always@(negedge hit)begin
     counter_fast_r_con <= counter_fast;
end

always @(posedge clk320) begin
  if(rst)
   hit_r <= 'b0;
  else
   hit_r <= {hit_r[0], hit};
end

assign pos_hit      = hit_r[0] & (~hit_r[1]);
assign trailing_hit = (~hit_r[0]) & hit_r[1];

//assign leading edge tdc data
always @(posedge clk320) begin
  if(rst ) begin
    q_chnl <= 6'b0;
    cnt_chnl <= 15'b0;
    cnt_inv_chnl <= 15'b0; end
  else if(pos_hit) begin
    case(counter_fast_r[1:0])
    2'b00:begin
            q_chnl<=6'b00_1001;
            cnt_chnl<=counter_fast_r[16:2];
            cnt_inv_chnl<=counter_fast_r[16:2];
            end
    2'b01:begin
           q_chnl<=6'b00_0011;
           cnt_chnl<=counter_fast_r[16:2];
           cnt_inv_chnl<=counter_fast_r[16:2];
           end
    2'b10:begin
          q_chnl<=6'b00_0110;
          cnt_chnl<=counter_fast_r[16:2];
          cnt_inv_chnl<=counter_fast_r[16:2]+15'b1;
          end
    2'b11:begin
         q_chnl<=6'b00_1100;
         cnt_chnl <= counter_fast_r[16:2];
         cnt_inv_chnl <= counter_fast_r[16:2]+15'b1; end
   endcase
end
end
//assign trailing edge tdc data
always @(posedge clk320) begin
  if(rst ) begin
    t_q_chnl <= 6'b0;
    t_cnt_chnl <= 15'b0;
    t_cnt_inv_chnl <= 15'b0; end
  else if(trailing_hit) begin
      case(counter_fast_r_con[1:0])
  2'b00:begin
          t_q_chnl<=6'b00_1001;
          t_cnt_chnl<=counter_fast_r_con[16:2];
          t_cnt_inv_chnl<=counter_fast_r_con[16:2];
          end
  2'b01:begin
         t_q_chnl<=6'b00_0011;
         t_cnt_chnl<=counter_fast_r_con[16:2];
         t_cnt_inv_chnl<=counter_fast_r_con[16:2];
         end
  2'b10:begin
        t_q_chnl<=6'b00_0110;
        t_cnt_chnl<=counter_fast_r_con[16:2];
        t_cnt_inv_chnl<=counter_fast_r_con[16:2]+15'b1;
        end
  2'b11:begin
       t_q_chnl<=6'b00_1100;
       t_cnt_chnl <= counter_fast_r_con[16:2];
       t_cnt_inv_chnl <= counter_fast_r_con[16:2]+15'b1; end
 endcase
end
end

always @(posedge clk320) begin
   pos_hit_r <= {pos_hit_r[3:0], pos_hit};
   trailing_hit_r <={trailing_hit_r[3:0], trailing_hit};
end

assign Rdy_chnl = |pos_hit_r[4:2],
       t_Rdy_chnl = |trailing_hit_r[4:2];

endmodule
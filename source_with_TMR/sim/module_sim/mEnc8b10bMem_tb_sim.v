/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : mEnc8b10bMem_tb_sim.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 5st, 2018
//  Note       : verify mEnc8b10bMem_tb module
// 

module mEnc8b10bMem_tb_sim(
);
reg clk;
reg rst;
reg [7:0] i8_Din;
reg i_Kin;
wire [9:0] o10_Dout;
wire o_Rd;
wire o_KErr;
reg i_Disparity;
mEnc8b10bMem_tb
	mEnc8b10bMem_tb_inst(
		.i8_Din(i8_Din),		//HGFEDCBA
		.i_Kin(i_Kin),
		.i_ForceDisparity(1'b1),
		.i_Disparity(i_Disparity),		//1 Is negative, 0 is positive	
		.o10_Dout(o10_Dout),	//abcdeifghj
		.o_Rd(o_Rd),
		.o_KErr(o_KErr),
		.i_Clk(clk),
		.i_ARst_L(rst),

		.i_enable(1'b1),
		.soft_reset_i(1'b0)
    );

	initial begin
		clk = 1'b0;
		rst = 1'b0;
		#300;
		rst = 1'b1;
		#300;
		rst = 1'b0;
	end
	
	initial begin
        i8_Din = 8'h67;
        i_Kin = 1'b0;
        i_Disparity = 1'b0;
        #800;
        i_Disparity = 1'b1;
    end
    
	always begin
		# 3.125;
		clk = ~clk;
	end
endmodule
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : serial_interface_sim.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 8st, 2018
//  Note       : 
//     

module serial_interface_sim();
reg clk320;
reg rst;
wire interface_fifo_read;
reg [9:0] interface_fifo_data;
wire [1:0] d_line;
serial_interface
serial_interface_inst(
  .clk_A(clk320),
  .clk_B(clk320),
  .clk_C(clk320),
  
  .rst(rst),

	.enable_320(1'b1),
	.enable(1'b1),
    .enable_TMR(1'b1),
    
	.interface_fifo_empty(1'b0),
	.interface_fifo_read(interface_fifo_read),
	.interface_fifo_data(interface_fifo_data),

	.d_line(d_line)
);


always begin
  clk320 <= 1'b0;
  #1.5625;
  clk320 <= 1'b1;
  #1.5625;
end

initial begin
  rst = 1'b0;
  #300;
  rst = 1'b1;
  #300;
  rst = 1'b0;
end

initial begin
  interface_fifo_data <= $random;
end

always @(posedge clk320) begin
  if (interface_fifo_read) begin
    interface_fifo_data <= $random;
  end
end

endmodule
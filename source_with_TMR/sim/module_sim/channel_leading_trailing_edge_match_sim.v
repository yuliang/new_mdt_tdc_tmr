/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : channel_leading_trailing_edge_match_sim.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 8st, 2018
//  Note       : verify channel_leading_trailing_edge_combine module
// 
`include "common_definition.v"
module channel_leading_trailing_edge_match_sim(
);
	reg clk;
	reg rst;
	reg data_ready_l,data_ready_t;
	reg [`FULL_EDGE_TIME_SIZE-1:0] effictive_data_l,effictive_data_t;
	reg [`COMBINE_TIME_OUT_SIZE-1:0] combine_time_out_config;
	wire combine_data_ready;
	wire [`FULL_EDGE_TIME_SIZE*2-1:0] effictive_combine_data;
	reg [`FULL_EDGE_TIME_SIZE*2-1:0] effictive_combine_data_r;
	wire miss_trailing_edge;
	wire miss_trailing_edge_time_out;
	wire miss_trailing_edge_new_edge;

	channel_leading_trailing_edge_match #(`FULL_EDGE_TIME_SIZE)
		DUT	(
	    	.clk(clk),
	    	.rst(rst),
	    	.data_ready_l(data_ready_l),.data_ready_t(data_ready_t),
	    	.effictive_data_l(effictive_data_l),.effictive_data_t(effictive_data_t),
	    	
	    	.combine_time_out_config(combine_time_out_config),			
		
	    	.combine_data_ready(combine_data_ready),
	    	.effictive_combine_data(effictive_combine_data),
	    	.miss_trailing_edge(miss_trailing_edge),
	    	.miss_trailing_edge_time_out(miss_trailing_edge_time_out),
	    	.miss_trailing_edge_new_edge(miss_trailing_edge_new_edge)
		);
	always @(posedge clk) begin
		effictive_combine_data_r <= effictive_combine_data;
	end

	initial begin
		clk = 1'b0;
		rst = 1'b0;
		combine_time_out_config = 'ha;
		#300;
		rst = 1'b1;
		#300;
		rst = 1'b0;
	end

	always begin
		# 3.125;
		clk = ~clk;
	end

	always @(posedge clk ) begin
		effictive_data_l <= $random;
		effictive_data_t <= $random;
		data_ready_l <= ($random % 10)>6 & ~data_ready_l;
		data_ready_t <= ($random % 10)>6 & ~data_ready_t;
	end
	wire find;
	assign  find = miss_trailing_edge_time_out & miss_trailing_edge_new_edge;
endmodule
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : setup_control_status_tb.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-04-12 15:20:14
//  Note       : 
//    
module setup_control_status_tb();
reg clk_40M;
always begin
    clk_40M = 1'b0;
    #25;
    clk_40M = 1'b1;
    #25;
end

wire        config_reset_jtag_in;
wire        config_event_teset_jtag_in;
wire        config_enable_new_ttc;
wire        config_enable_master_reset_code;
wire        config_enable_trigger;
wire [11:0] config_bunch_offset;
wire [11:0] config_event_offset;
wire [11:0] config_match_window;
wire        config_enable_direct_trigger;
wire        config_enable_direct_bunch_reset;
wire        config_enable_direct_event_reset;
wire [11:0] config_roll_over;
wire [11:0] config_coarse_count_offset;
wire        config_auto_roll_over;
wire        config_bypass_bcr_distribution;
wire        config_channel_data_debug;
wire        config_enbale_fake_hit;
wire [11:0] config_fake_hit_time_interval;
wire        config_enable_trigger_timeout;
wire        config_enable_leading;
wire        config_enable_pair;
wire 		config_full_width_res;
wire [2:0]  config_width_select;
wire        config_enable_8b10b;
wire        config_enable_insert;
wire        config_enable_error_packet;
wire        config_enable_TDC_ID;
wire [18:0] config_TDC_ID;
wire        config_enable_config_error_notify;
wire        config_config_error;
wire        config_enable_high_speed;
wire        config_enable_legacy;
wire [`MAX_PACKET_NUM_SIZE-1:0] config_syn_packet_number;
wire [23:0] config_rising_is_leading;
wire [`COMBINE_TIME_OUT_SIZE-1:0] config_combine_time_out_config;
wire [23:0] config_channel_enable_r;
wire [23:0] config_channel_enable_f;
wire [3:0]  config_fine_sel;
wire [1:0]  config_lut0,config_lut1,config_lut2,config_lut3,config_lut4,config_lut5,config_lut6,config_lut7;
wire [1:0]  config_lut8,config_lut9,config_luta,config_lutb,config_lutc,config_lutd,config_lute,config_lutf;
wire [3:0]  config_debug_port_select;

wire [23:0] chnl_fifo_overflow;
wire  config_chnl_fifo_overflow_clear;
wire ePll_lock ;
assign ePll_lock= 1'b1;
assign chnl_fifo_overflow = 24'b0;

wire [4  :0] phase_clk160;
wire [3  :0] phase_clk320_0, phase_clk320_1, phase_clk320_2;
wire         rst_ePLL;
wire [3  :0] ePllResA, ePllResB, ePllResC;
wire [3  :0] ePllIcpA, ePllIcpB, ePllIcpC;
wire [1  :0] ePllCapA, ePllCapB, ePllCapC; 

wire tdi_to_dr;
wire setup0_enable,setup0_update,setup0_shift,setup0_in;
wire setup1_enable,setup1_update,setup1_shift,setup1_in;
wire setup2_enable,setup2_update,setup2_shift,setup2_in;
wire control0_enable,control0_update,control0_shift,control0_in;
wire control1_enable,control1_update,control1_shift,control1_in;
wire status0_shift,status0_in;
wire status1_shift,status1_in;

wire tck,tms,tdi,tdo;
reg trst;

wire instruction_error;
wire tdo_jtag;
wire tdo_b;
assign tdo = trst ? tdo_jtag : tdo_b;

jtag_controller
    jtag_controller_inst( 
	   .tck(tck), 
	   .tms(tms), 
	   .tdi(tdi), 
	   .trst(trst), 
	   .tdo(tdo_jtag), 
	   .enable(), 
    
	   .tdi_to_dr(tdi_to_dr), 
    
        .setup0_shift(setup0_shift),
        .setup0_update(setup0_update),
        .setup0_enable(setup0_enable),
        .setup0_in(setup0_in),
    
        .setup1_shift(setup1_shift),
        .setup1_update(setup1_update),
        .setup1_enable(setup1_enable),
        .setup1_in(setup1_in),
    
        .setup2_shift(setup2_shift),
        .setup2_update(setup2_update),
        .setup2_enable(setup2_enable),
        .setup2_in(setup2_in),
    
        .control0_enable(control0_enable), 
        .control0_update(control0_update), 
        .control0_shift(control0_shift),
        .control0_in(control0_in), 
    
        .control1_enable(control1_enable), 
        .control1_update(control1_update), 
        .control1_shift(control1_shift),
        .control1_in(control1_in), 
    
        .status0_shift(status0_shift),
        .status0_in(status0_in), 
    
        .status1_shift(status1_shift),
        .status1_in(status1_in), 
    
	   .instruction_error(instruction_error)
    );


common_setup
    common_setup_inst(
        .tck(tck),
        .trst(trst),
        .tdi(tdi),
        .tdo_b(tdo_b),
        .tms(tms),
        
        .clk40_A(clk_40M),
        .clk40_B(clk_40M),
        .clk40_C(clk_40M),

        .tdi_to_dr(tdi_to_dr),
    
        .setup0_shift(setup0_shift),
        .setup0_update(setup0_update),
        .setup0_enable(setup0_enable),
        .setup0_data(setup0_in),
    
        .setup1_shift(setup1_shift),
        .setup1_update(setup1_update),
        .setup1_enable(setup1_enable),
        .setup1_data(setup1_in),
    
        .setup2_shift(setup2_shift),
        .setup2_update(setup2_update),
        .setup2_enable(setup2_enable),
        .setup2_data(setup2_in),
    
    
        .control0_shift(control0_shift),
        .control0_update(control0_update),
        .control0_enable(control0_enable),
        .control0_data(control0_in),
    
        .control1_shift(control1_shift),
        .control1_update(control1_update),
        .control1_enable(control1_enable),
        .control1_data(control1_in),
    
        .status0_shift(status0_shift),
        .status0_data(status0_in),
    
        .status1_shift(status1_shift),
        .status1_data(status1_in),
    
    
        .phase_clk160(phase_clk160),
        .phase_clk320_0(phase_clk320_0), .phase_clk320_1(phase_clk320_1), .phase_clk320_2(phase_clk320_2),
        .rst_ePLL(rst_ePLL),
        
        .ePllResA(ePllResA), .ePllResB(ePllResB), .ePllResC(ePllResC),
        .ePllIcpA(ePllIcpA), .ePllIcpB(ePllIcpB), .ePllIcpC(ePllIcpC),
        .ePllCapA(ePllCapA), .ePllCapB(ePllCapB), .ePllCapC(ePllCapC),
        
        .ePll_lock(ePll_lock),
        
        
        .config_error(config_config_error),
        
        
        .debug_port_select(config_debug_port_select),
        .chnl_fifo_overflow_clear(config_chnl_fifo_overflow_clear),
        .chnl_fifo_overflow(chnl_fifo_overflow),
        .instruction_error(instruction_error),
        //for TTC interface
        .reset_jtag_in(config_reset_jtag_in),
        .event_teset_jtag_in(config_event_teset_jtag_in),
        
        .enable_new_ttc(config_enable_new_ttc),
        .enable_master_reset_code(config_enable_master_reset_code),
        .enable_direct_bunch_reset(config_enable_direct_bunch_reset),
        .enable_direct_event_reset(config_enable_direct_event_reset),
        .enable_direct_trigger(config_enable_direct_trigger),
        
        //for bcr
        .roll_over(config_roll_over),
        .coarse_count_offset(config_coarse_count_offset),
        .auto_roll_over(config_auto_roll_over),
        .bypass_bcr_distribution(config_bypass_bcr_distribution),
        
        //channel model
        .enable_trigger(config_enable_trigger),
        .channel_data_debug(config_channel_data_debug),
        .enable_leading(config_enable_leading),
        .enable_pair(config_enable_pair),
        .enbale_fake_hit(config_enbale_fake_hit),
        .fake_hit_time_interval(config_fake_hit_time_interval),
        .rising_is_leading(config_rising_is_leading),
        .combine_time_out_config(config_combine_time_out_config),
        .channel_enable_r(config_channel_enable_r),
        .channel_enable_f(config_channel_enable_f),
        
        //for trigger para
        .bunch_offset(config_bunch_offset),
        .event_offset(config_event_offset),
        .match_window(config_match_window),
        
        
        //for readout interface
        .enable_trigger_timeout(config_enable_trigger_timeout),
        .enable_high_speed(config_enable_high_speed),
        .enable_legacy(config_enable_legacy),
        .full_width_res(config_full_width_res),
        .width_select(config_width_select),
        .enable_8b10b(config_enable_8b10b),
        .enable_insert(config_enable_insert),
        .syn_packet_number(config_syn_packet_number),
        .enable_error_packet(config_enable_error_packet),
        .enable_TDC_ID(config_enable_TDC_ID),
        .TDC_ID(config_TDC_ID),
        .enable_error_notify(config_enable_config_error_notify),
        
        
        .fine_sel(config_fine_sel),
        .lut0(config_lut0),.lut1(config_lut1),.lut2(config_lut2),.lut3(config_lut3),.lut4(config_lut4),.lut5(config_lut5),.lut6(config_lut6),.lut7(config_lut7),
        .lut8(config_lut8),.lut9(config_lut9),.luta(config_luta),.lutb(config_lutb),.lutc(config_lutc),.lutd(config_lutd),.lute(config_lute),.lutf(config_lutf)
    );



wire JTAG_busy;
wire [511:0] JTAG_data_out;
reg  start_action;
reg  [11:0] config_period;
reg  [511:0] JTAG_bits;
reg  [8:0] bit_length;
reg  [4:0] JTAG_inst;

wire tck_jtag,tms_jtag,tdi_jtag;
JTAG_master_sim 
    JTAG_master_inst(
        .clk (clk_40M),
        .TCK (tck_jtag),
        .TMS (tms_jtag),
        .TDI (tdi_jtag),
        .TDO (tdo),
        .start_action  (start_action),
        .config_period (config_period),
        .JTAG_bits     (JTAG_bits),
        .bit_length    (bit_length),
        .JTAG_inst     (JTAG_inst),
        .JTAG_data_out (JTAG_data_out),
        .JTAG_busy     (JTAG_busy)
    );


wire SPI_busy;
wire [511:0] SPI_data_out;
reg  [511:0] SPI_bits;

wire tck_spi,tms_spi,tdi_spi;
SPI_master 
    SPI_master_inst(
        .clk           (clk_40M),
        .TCK           (tck_spi),
        .TMS           (tms_spi),
        .TDI           (tdi_spi),
        .TDO           (tdo),
        .start_action  (start_action),
        .config_period (config_period),
        .SPI_bits      (SPI_bits),
        .bit_length    (bit_length),
        .SPI_data_out  (SPI_data_out),
        .SPI_busy      (SPI_busy)
    );
assign tck = trst ? tck_jtag : tck_spi;
assign tms =trst ? tms_jtag : tms_spi;
assign tdi = trst ? tdi_jtag : tdi_spi;




initial begin
    trst = 1'b1;
	start_action = 1'b0;
	config_period = 'b0;
	JTAG_bits = 'b0;
	bit_length = 'b0;
	JTAG_inst = 5'h12;
    SPI_bits = {512{1'b1}};
    #5;
    trst = 1'b0;
	#100;
	trst = 1'b1;
	#1000;
	bit_length = 9'd114;
	start_action = 1'b1;
	#10000;
	start_action = 1'b0;
    #100000;
    JTAG_bits = {512{1'b1}};
    start_action = 1'b1;
    #10000;
    start_action = 1'b0;
    #100000;
end


endmodule
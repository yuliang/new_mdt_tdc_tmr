/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : channel_leading_trailing_edge_combine_sim.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 8st, 2018
//  Note       : verify channel_leading_trailing_edge_combine module
// 

module channel_leading_trailing_edge_combine_sim(
);
	reg clk;
	reg rst;
	reg data_ready_l,data_ready_t;
	reg [`FULL_EDGE_TIME_SIZE-1:0] effictive_data_l,effictive_data_t;
	reg [`COMBINE_TIME_OUT_SIZE-1:0] combine_time_out_config;
	wire combine_data_ready,combine_data_ready_ref;
	wire [`FULL_EDGE_TIME_SIZE*2-1:0] effictive_combine_data,effictive_combine_data_ref;

	reg enable_pair,enable_leading;
	//reg [11:0] roll_over;
	//reg [2:0] width_select;
	reg channel_data_debug,rising_is_leading;

	wire chnl_fifo_write_triggerless,chnl_fifo_write_triggerless_ref;
	wire [`CHANNEL_FIFO_WIDTH-1:0] fifo_data_in_triggerless,fifo_data_in_triggerless_ref;

	reg [`FULL_DATA_SIZE-`FULL_EDGE_TIME_SIZE-1:0] debug_data;
	wire [1:0] combine_flag,combine_flag_ref;

	channel_leading_trailing_edge_combine2
		DUT	(
	    	.clk(clk),
	    	.rst(rst),
	    	.data_ready_l(data_ready_l),.data_ready_t(data_ready_t),
	    	.effictive_data_l(effictive_data_l),.effictive_data_t(effictive_data_t),
	    	.debug_data(debug_data),

	    	//.enable_pair(enable_pair),
	    	.enable_leading(enable_leading),
	    	//.roll_over(roll_over),
	    	//.width_select(width_select),
	    	.combine_time_out_config(combine_time_out_config),	
	    	.channel_data_debug(channel_data_debug)	,
	    	.rising_is_leading(rising_is_leading),	
		
	    	.combine_data_ready(combine_data_ready),
	    	.effictive_combine_data(effictive_combine_data),
	    	.combine_flag(combine_flag),

	    	.chnl_fifo_write_triggerless(chnl_fifo_write_triggerless),
	    	.fifo_data_in_triggerless(fifo_data_in_triggerless)
		);
		
		channel_leading_trailing_edge_combine
            DUT_ref    (
                .clk(clk),
                .rst(rst),
                .data_ready_l(data_ready_l),.data_ready_t(data_ready_t),
                .effictive_data_l(effictive_data_l),.effictive_data_t(effictive_data_t),
                .debug_data(debug_data),
    
                //.enable_pair(enable_pair),
                .enable_leading(enable_leading),
                //.roll_over(roll_over),
                //.width_select(width_select),
                .combine_time_out_config(combine_time_out_config),    
                .channel_data_debug(channel_data_debug),
                .rising_is_leading(rising_is_leading),    
            
                .combine_data_ready(combine_data_ready_ref),
                .effictive_combine_data(effictive_combine_data_ref),
                .combine_flag(combine_flag_ref),
    
                .chnl_fifo_write_triggerless(chnl_fifo_write_triggerless_ref),
                .fifo_data_in_triggerless(fifo_data_in_triggerless_ref)
            );
    reg no_correct;
    always @(posedge clk) begin
          no_correct <= (combine_data_ready != combine_data_ready_ref)|
                     (effictive_combine_data != effictive_combine_data_ref) |
                     (combine_flag != combine_flag_ref) |
                     (chnl_fifo_write_triggerless != chnl_fifo_write_triggerless_ref) |
                     (fifo_data_in_triggerless != fifo_data_in_triggerless_ref) ;
    end
    
//    always @(posedge clk) begin
//        if(~|combine_flag_ref)begin
//            if((effictive_combine_data != effictive_combine_data_ref) |
//            (combine_flag != combine_flag_ref) |
//            (chnl_fifo_write_triggerless != chnl_fifo_write_triggerless_ref) |
//            (fifo_data_in_triggerless != fifo_data_in_triggerless_ref))begin
//                $stop;
//            end
//        end
//    end    
//        always @(posedge clk) begin
//            if(DUT.channel_leading_trailing_edge_match_inst.data_ready_l&DUT.channel_leading_trailing_edge_match_inst.data_ready_t&DUT.channel_leading_trailing_edge_match_inst.combine_time_out)begin
//               // $stop;
//            end
//    end  
    
    always @(*) begin
       if(no_correct) $stop;
    end
    
    
     //always @(*) begin
     //    if(data_ready_l&data_ready_t) $stop;
     //end
	initial begin
		clk = 1'b0;
		rst = 1'b0;
		combine_time_out_config = 'h0;
		enable_pair = 1'b1;
		enable_leading = 1'b0;
		rising_is_leading = 1'b0;
		channel_data_debug = 1'b0;
		
		//roll_over =12'b110111101100; 
		#300;
		rst = 1'b1;
		#300;
		rst = 1'b0;
	end

	always begin
		# 3.125;
		clk = ~clk;
	end

	always @(posedge clk ) begin
	    //width_select =3'b010;
	    //combine_time_out_config = $random;
	    //enable_pair =  $random;
	    //enable_leading = $random;
	    //rising_is_leading = $random;
	    //channel_data_debug = $random;
		effictive_data_l <= $random;
		effictive_data_t <= $random;
		data_ready_l <= ($random % 100)>0 & ~data_ready_l;
		data_ready_t <= ($random % 100)>0 & ~data_ready_t;
		debug_data <= $random;
	end
endmodule
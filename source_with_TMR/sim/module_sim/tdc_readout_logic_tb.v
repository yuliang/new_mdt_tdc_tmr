/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : tdc_readout_logic_tb.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 12nd, 2018
//  Note       : 
// 

module tdc_readout_logic_tb();
wire  rst;
wire clk160,clk320,clk_hit;
reg [16:0] counter_fast=17'b0;




wire channel_data_debug; 		     
wire enable_trigger; 			       
wire enable_trigger_timeout; 	   
wire enable_leading; 			       
wire enable_pair; 				       
wire [11:0] roll_over; 			     
wire full_width_res; 			       
wire [2:0] width_select; 		     
wire enable_8b10b;				       
wire enable_insert;              
wire enable_error_packet; 		   
wire enable_TDC_ID; 			       
wire [18:0] TDC_ID; 			       
wire enable_config_error_notify; 
wire config_error;               
wire enable_high_speed; 		     
wire enable_legacy; 			       
wire rising_is_leading;          
wire [`COMBINE_TIME_OUT_SIZE-1:0] combine_time_out_config; 

assign channel_data_debug         = 1'b0;
assign enable_trigger             = 1'b0;
assign enable_trigger_timeout     = 1'b0;
assign enable_leading             = 1'b0;
assign enable_pair                = 1'b1;
assign roll_over                  = 12'hfff;
assign full_width_res             = 1'b0;
assign width_select               = 3'b000;
assign enable_8b10b               = 1'b0;
assign enable_insert              = 1'b1;
assign enable_error_packet        = 1'b1;
assign enable_TDC_ID              = 1'b0;
assign TDC_ID                     = 19'h0aaaa;
assign enable_config_error_notify = 1'b0;
assign config_error               = 1'b0;
assign enable_high_speed          = 1'b1;
assign enable_legacy              = 1'b1;
assign rising_is_leading          = 1'b1;
assign combine_time_out_config    ='h96;

wire [23:0] chnl_fifo_empty;
wire [23:0] chnl_fifo_full;
wire [23:0] chnl_fifo_read;
wire [1079:0] chnl_data;
wire [23:0] trigger_processing;
wire channel_fifo_clear;
wire get_trigger;
wire [1:0] d_line;

tdc_readout_logic #( .CHANNEL_NUMBER(24), .READ_OUT_FIFO_ASIZE(4))
    tdc_readout_logic_inst(
      .rst(rst),      // reset, not BC reset
      .clk160(clk160),
      .clk320(clk320),

      .d_line(d_line),

  //config
      .channel_data_debug(channel_data_debug),
      .enable_trigger(enable_trigger),
      .enable_trigger_timeout(enable_trigger_timeout),
      .enable_leading(enable_leading),
      .enable_pair(enable_pair),
      .roll_over(roll_over),
      .full_width_res(full_width_res),
      .width_select(width_select),  
      .enable_error_packet(enable_error_packet),
      .enable_8b10b(enable_8b10b),
      .enable_insert(enable_insert),
      .enable_TDC_ID(enable_TDC_ID),
      .TDC_ID(TDC_ID),
      .enable_config_error_notify(enable_config_error_notify),
      .config_error(config_error),
      .enable_high_speed(enable_high_speed),
      .enable_legacy(enable_legacy),
      .syn_packet_number(10'h3ff),
      
      //interface to channel logic
      .chnl_fifo_empty(chnl_fifo_empty),
      .chnl_fifo_full(chnl_fifo_full),
      .chnl_fifo_read(chnl_fifo_read),
      .chnl_data(chnl_data),
      
      //interface to channel logic for trigger
      .trigger_processing(trigger_processing),
      .channel_fifo_clear(channel_fifo_clear),
      .get_trigger(get_trigger),
    
      //interface to trigger interface 
      .trigger_fifo_empty(1'b0),
      .trigger_event_id($random),
      .trigger_bunch_id($random),
      .error_flag($random)

);

wire [23:0] hit;
wire [23:0] Rdy_r,Rdy_f;
wire [3:0] q_r [23:0];
wire [3:0] q_f [23:0];
wire [14:0] cnt_r [23:0];
wire [14:0] cnt_inv_r [23:0];
wire [14:0] cnt_f [23:0];
wire [14:0] cnt_inv_f [23:0];
wire fake_hit;
wire [2:0] chnl_fifo_depth [23:0];
reg [7:0] chnl_fifo_depth_total;
reg [23:0] hit_last;
reg [31:0] hit_counter[23:0];
reg hit_enable_out;
generate
	genvar i;
	for (i = 0; i < 24; i = i + 1)
	begin:tdc_channel

		tdc_channel_dual_edge  #(.DATA_DEEP(16), .FIFO_ASIZE(2))//FIFO_DEEP= 2^FIFO_ASIZE
			tdc_channel_dual_edge_inst(
  				.rst(rst),      // reset, not BC reset
  				.clk160(clk160),
				
				.channel_enable_r(1'b1),.channel_enable_f(1'b1),
  				.fine_sel(4'b0011), //encoding for fine time selection
  				.lut0(6'h00), .lut1(6'h05), .lut2(6'h0a), .lut3(6'h0d), .lut4(6'h13), .lut5(6'h14), .lut6(6'h1a), .lut7(6'h1e), .lut8(6'h20), .lut9(6'h24),
  				.luta(6'h28), .lutb(6'h2d), .lutc(6'h33), .lutd(6'h34), .lute(6'h3b), .lutf(6'h3c),
  				//.width_select(width_select),
  				//.roll_over(roll_over),
  				//.enable_pair(enable_pair),
  				.enable_leading(enable_leading),
  				.combine_time_out_config(combine_time_out_config), 
  				.rising_is_leading(rising_is_leading),
  				.enable_trigger(enable_trigger),
  				.channel_data_debug(channel_data_debug),
  				
  				.chnl_ID(i),
  				
  				.Rdy_r(Rdy_r[i]),.Rdy_f(Rdy_f[i]), // ready
  				.q_r(q_r[i]),.q_f(q_f[i]), // raw fine codes 
  				.cnt_r(cnt_r[i]), .cnt_inv_r(cnt_inv_r[i]),.cnt_f(cnt_f[i]), .cnt_inv_f(cnt_inv_f[i]), // coarse counter
  				
  				.fake_hit(fake_hit),
  				
  				.fifo_read(chnl_fifo_read[i]),
  				.fifo_read_clk(clk160),
  				.channel_data_out(chnl_data[(i*45+44) : i*45]),
  				.fifo_empty(chnl_fifo_empty[i]),
  				.fifo_full(chnl_fifo_full[i]),


  				.trigger(get_trigger),
  				.limit_up(12'hfff),
  				.limit_down(12'h000),
  				.trigger_processing(trigger_processing[i]),

          .chnl_fifo_overflow(),//work in triggerless mode when fifo is full, but still need to write
          .chnl_fifo_overflow_clear(1'b0)
			);
    assign chnl_fifo_depth[i] = {tdc_channel[i].tdc_channel_dual_edge_inst.chnl_fifo_inst.wptr[2],tdc_channel[i].tdc_channel_dual_edge_inst.chnl_fifo_inst.waddr} - {tdc_channel[i].tdc_channel_dual_edge_inst.chnl_fifo_inst.rptr[2],tdc_channel[i].tdc_channel_dual_edge_inst.chnl_fifo_inst.raddr} ;

		tdcdata_gen 
			tdcdata_gen_inst(
				.hit(hit[i]),
				.rst(rst),
				.clk320(clk320),
				.counter_fast(counter_fast),
				.Rdy_chnl(Rdy_r[i]), .t_Rdy_chnl(Rdy_f[i]),
				.q_chnl(q_r[i]), .t_q_chnl(q_f[i]),
				.cnt_chnl(cnt_r[i]), .t_cnt_chnl(cnt_f[i]),
				.cnt_inv_chnl(cnt_inv_r[i]), .t_cnt_inv_chnl(cnt_inv_f[i])
			);

		random_hit_generator 
			random_hit_generator_inst(
				.clk(clk_hit),
				.rst(rst),
				.hit_out(hit[i]),
				.enable_out(hit_enable_out),
				.RATE(32'h00417874),//32'h00417874->200k,32'h0083126E->400k,32'hFFFF_FFFF-> high ednsity
				.seed1(32'hAD1659EF),
				.seed2(32'h3D9EF9ED),
				.dead_time_input(8'h00)//8'ha0
			);

    always @(posedge clk_hit  ) begin
      if (rst) begin
        // reset
        hit_last[i] <= 1'b0;
        hit_counter[i] <= 32'b0; 
      end
      else begin
        hit_last[i] <= hit[i];
        hit_counter[i] <= hit_counter[i] +((~hit_last[i])&(hit[i]));
      end
    end

	end
endgenerate

integer j;
reg [63:0] total_hit_counter;
always @(*) begin
  total_hit_counter = 64'b0;
    for (j = 0; j < 24; j = j + 1)
    begin:sum_counter
      total_hit_counter = total_hit_counter+ hit_counter[j];
    end
end


always @(*) begin
  chnl_fifo_depth_total = 'b0;
  for(j=0;j<24;j=j+1)begin
    chnl_fifo_depth_total = chnl_fifo_depth_total + chnl_fifo_depth[j];
  end 
end

reg [63:0] tdc_static[95:0];
always @(posedge clk160) begin
  if (rst) begin
    // reset
    for(j=0;j<96;j=j+1)begin
      tdc_static[j] <= 64'b0;
    end
  end else begin
     tdc_static [chnl_fifo_depth_total] <= tdc_static [chnl_fifo_depth_total]+64'b1;
  end
end




sim_clock_gen #(.clk_320_delay(0),.clk_160_delay(0.8))
	sim_clock_gen_inst(
		.clk_160(clk160),
		.clk_320(clk320),
		.clk_320M_4(clk320M_4),
		.clk_hit(clk_hit)
	);

reset_gen  #(.start(3000),.width(100))
	reset_gen_inst(
		.rst(rst)
	);

always@(posedge clk320M_4)begin
    if(counter_fast=={roll_over,5'b11111})begin
      counter_fast <= 17'b0;
    end else begin
      counter_fast <= counter_fast + 17'b1;
    end     
end

fake_hit_generator #(.WIDTH(12))
  fake_hit_generator_inst(
    .clk(clk160),
    .rst(rst),
    .enable(enable_trigger),
    .fake_hit(fake_hit),
    .time_interval(12'h050)
  );

//wire [7:0] o8_Dout;
//wire o_Kout;
//wire o_DErr;
//wire o_KErr;
//wire o_DpErr;
// mDec8b10bMem_tb
// mDec8b10bMem_tb_inst(
//  .o8_Dout(o8_Dout),    //HGFEDCBA
//  .o_Kout(o_Kout),
//  .o_DErr(o_DErr),
//  .o_KErr(o_KErr),
//  .o_DpErr(o_DpErr),
//  .i_ForceDisparity(1'b0),
//  .i_Disparity(1'b0),    //1 Is negative, 0 is positive  
//  .i10_Din(tdc_readout_logic_inst.interface_fifo_data),  //abcdeifghj
//  .o_Rd(), 
//  .i_Clk(clk320),
//  .i_ARst_L(~rst),
//  .soft_reset_i(1'b0),
//  
//  .i_enable(tdc_readout_logic_inst.interface_fifo_read)
//    );

reg [63:0] data_counter;
reg [64:0] k_count;
always @(posedge clk320  ) begin
  if (rst) begin
    // reset
    data_counter <= 64'b0;
    k_count      <= 64'b0;
  end
  else if (tdc_readout_logic_inst.serial_interface_inst.interface_fifo_read) begin
    if ((tdc_readout_logic_inst.serial_interface_inst.interface_fifo_data[9:8]==2'b00)) begin
      data_counter <= data_counter + 64'b1;
    end else begin
      k_count <= k_count + 64'b1;
    end
  end
end
//wire [4:0] fifo_depth;
//assign fifo_depth = {tdc_readout_logic_inst.readout_fifo_inst.wptr[4],tdc_readout_logic_inst.readout_fifo_inst.waddr}-{tdc_readout_logic_inst.readout_fifo_inst.rptr[4],tdc_readout_logic_inst.readout_fifo_inst.raddr};
//reg [63:0] tdc_static_readout[15:0];
//always @(posedge clk160) begin
//  if (rst) begin
//    // reset
//    for(j=0;j<16;j=j+1)begin
//      tdc_static_readout[j] <= 64'b0;
//    end
//  end else begin
//     tdc_static_readout [fifo_depth] <= tdc_static_readout [fifo_depth]+64'b1;
//  end
//end
//
//
//
//reg [31:0] eff_data;
//reg [2:0] eff_count;
//always @(posedge clk320  ) begin
//  if (rst) begin
//    // reset
//    eff_data <= 32'b0;
//  end
//  else if (tdc_readout_logic_inst.serial_interface_inst.interface_fifo_read&(tdc_readout_logic_inst.serial_interface_inst.interface_fifo_data[9:8]==2'b00)) begin
//    eff_data <= {eff_data[23:0],tdc_readout_logic_inst.serial_interface_inst.interface_fifo_data[7:0]};
//  end
//end
//
//always @(posedge clk320  ) begin
//  if (rst) begin
//    // reset
//    eff_count <= 3'b000;
//  end  else if (tdc_readout_logic_inst.serial_interface_inst.interface_fifo_read&(tdc_readout_logic_inst.serial_interface_inst.interface_fifo_data[9:8]==2'b01)) begin
//    eff_count <=  3'b000;
//  end else if(tdc_readout_logic_inst.serial_interface_inst.interface_fifo_read&(tdc_readout_logic_inst.serial_interface_inst.interface_fifo_data[9:8]==2'b00))begin
//    eff_count <= (eff_count==3'b100) ? 3'b001: eff_count + 3'b1;
//  end
//end
//
//wire data_ready;
//assign data_ready = eff_count==3'b100;
//reg [16:0] latency_reg;
//reg latency_ready_1,latency_ready;
//always @(posedge clk320) begin
//  latency_ready_1 <= data_ready;
//end
//always @(posedge clk320) begin
//  latency_ready <= data_ready&(~latency_ready_1);
//end
//always @(posedge clk320  ) begin
//  if (rst) begin
//    // reset
//    latency_reg <= 17'b0;
//  end
//  else if (latency_ready) begin
//    latency_reg <= counter_fast-eff_data[24:8]-eff_data[7:0]-17'b1000000;
//  end
//end
//
//
//integer k;
//reg [31:0] latency_statasitc[131071:0];
//always @(posedge clk320)begin
//    if(rst)begin
//        for(k=0;k<131072;k=k+1)begin
//            latency_statasitc[k]=32'b0;
//        end
//    end
//    if(latency_ready) begin
//        latency_statasitc[latency_reg]<=latency_statasitc[latency_reg]+32'b1;
//    end
//end
//
//always @(posedge clk320) begin
//  if(latency_ready&&(&eff_data[7:0]))begin
//    $stop;
//  end
//end
//integer percent;
//integer latecny_file;
//integer total_occupy_file;
//integer readout_fifo_occupy_file;
//initial begin
//    percent =0;
//    repeat(100)begin
//    $display("%d",percent);
//    #10000;
//    percent=percent+1;  
//    end
//    
//    latecny_file=$fopen("latency_200k.dat","w");
//    total_occupy_file=$fopen("total_occupy_200k.dat","w");
//    readout_fifo_occupy_file= $fopen("readout_fifo_occupy_200k.dat","w");
//    for(k=0;k<131072;k=k+1)begin
//        $fdisplay(latecny_file,"%d,%d",k,latency_statasitc[k]);
//    end
//    $fclose(latecny_file);
//    for(k=0;k<96;k=k+1)begin
//        $fdisplay(total_occupy_file,"%d,%d",k,tdc_static[k]);
//    end
//    $fclose(total_occupy_file);
//    for(k=0;k<16;k=k+1)begin
//        $fdisplay(readout_fifo_occupy_file,"%d,%d",k,tdc_static_readout[k]);
//    end
//    $fclose(readout_fifo_occupy_file);
//    $stop;
//
//end
integer percent;
initial begin
    hit_enable_out=1'b1;
    percent =0;
    repeat(100)begin
    $display("%d",percent);
    #10000;
    percent=percent+1;  
    end
    hit_enable_out=1'b0;
    #10000;
    $stop;
end

//initial begin
//   force tdc_channel[22].tdc_channel_dual_edge_inst.chnl_fifo_overflow_1=1'b1;
//end


endmodule




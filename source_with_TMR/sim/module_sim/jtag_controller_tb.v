/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : jtag_controller_tb.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 22st, 2018
//  Note       : 
// 

module jtag_controller_tb(
);
wire tck,tms,tdi,trst,tdo,enable;
wire tdi_to_dr;
wire setup_shift,setup_in;
wire control_enable,control_update,control_shift,control_in;
wire status1_shift,status1_in;
wire status2_shift,status2_in;
wire instruction_error;
jtag_controller
	jtag_controller_inst( 
		.tck(tck), 
		.tms(tms), 
		.tdi(tdi), 
		.trst(trst), 
		.tdo(tdo), 
		.enable(enable), 
	
		.tdi_to_dr(tdi_to_dr), 
	
	
		.setup_shift(setup_shift),
		.setup_in(setup_in), 
	
		.control_enable(control_enable), 
		.control_update(control_update), 
		.control_shift(control_shift),
		.control_in(control_in), 
	
		.status1_shift(status1_shift),
		.status1_in(status1_in), 
	
		.status2_shift(status2_shift),
		.status2_in(status2_in), 
	
		.instruction_error(instruction_error));

wire  [16:0] control_parallel_data_out;
control_reg #(.WIDTH(17))
control_reg_inst(
	.clk(tck),
	.trst(trst),
	.shift(control_shift),
	.enable(control_enable),
	.update(control_update),
	.serial_data_in(tdi_to_dr),
	.initial_value($random),
	.serial_data_out(control_in),
	.parallel_data_out(control_parallel_data_out)
);

wire  [22:0] setup_parallel_data_out;
setup_reg #(.WIDTH(23))
setup_reg_inst(
	.clk(tck),
	.trst(trst),
	.shift(setup_shift),
	.serial_data_in(tdi_to_dr),
	.initial_value($random),
	.serial_data_out(setup_in),
	.parallel_data_out(setup_parallel_data_out)
);
reg [6:0] parallel_data1;
status_reg #(.WIDTH(7))
status_reg1_inst(
	.clk(tck),
	.shift(status1_shift),
	.serial_data_in(tdi_to_dr),
	.serial_data_out(status1_in),
	.parallel_data_in(parallel_data1)
);
reg [4:0] parallel_data2;
status_reg #(.WIDTH(5))
status_reg2_inst(
	.clk(tck),
	.shift(status2_shift),
	.serial_data_in(tdi_to_dr),
	.serial_data_out(status2_in),
	.parallel_data_in(parallel_data2)
);

integer i;
reg [21:0] set_up_content;
reg [21:0] last_set_up_content;
reg [16:0] control_content;
reg [16:0] last_control_content;

jtag_programming
jtag_programming_inst( 
	.jtag_tck(tck), .jtag_tms(tms), .jtag_tdi(tdi), 
	.jtag_tdo(tdo), .jtag_trst(trst) );
initial begin
	jtag_programming_inst.make_instruction_parity_error=1'b0;
	jtag_programming_inst.reset_tap_controller;
	jtag_programming_inst.read_id;
	jtag_programming_inst.load_dr(4'hf,32'd7,$random);
	for ( i = 0 ; i <100; i = i +1)begin
		last_set_up_content = setup_parallel_data_out;
		set_up_content = $random;
		jtag_programming_inst.load_dr(4'h8,32'd22,set_up_content);
		if ((last_set_up_content != jtag_programming_inst.content_output[21:0])|(set_up_content!=setup_parallel_data_out)) begin
			$stop;
		end
	end
	
	for ( i = 0 ; i <100; i = i +1)begin
		last_control_content = control_parallel_data_out;
		control_content = $random;
		jtag_programming_inst.load_dr(4'h9,32'd16,control_content);
		if ((last_control_content != jtag_programming_inst.content_output[16:0])|(control_content!=control_parallel_data_out)) begin
			$stop;
		end
	end

	for ( i = 0 ; i <100; i = i +1)begin
		parallel_data1 =  $random;;
		jtag_programming_inst.load_dr(4'ha,32'd6,$random);
		if ((parallel_data1 != jtag_programming_inst.content_output[6:0])) begin
			$stop;
		end
	end

	for ( i = 0 ; i <100; i = i +1)begin
		parallel_data2 =  $random;;
		jtag_programming_inst.load_dr(4'hb,32'd4,$random);
		if ((parallel_data2 != jtag_programming_inst.content_output[4:0])) begin
			$stop;
		end
	end

	$display("pass the test");

	$stop;
end
endmodule
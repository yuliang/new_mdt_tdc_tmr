/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : roll_over_difference_sim.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 6st, 2018
//  Note       : verify roll_over_differce module
// 
`include "common_definition.v"
module roll_over_difference_sim(
);
reg [16 :0] a,b;
wire [16:0] a_gated,b_gated;
reg [11:0] roll_over;
wire [16:0] a_b_reference;
wire [16:0] a_b;
wire [16:0] a_b_inst_1;
reg enable;


roll_over_difference 
	roll_over_difference_inst
(
    .enable(enable),
    .roll_over(roll_over),
    .a(a),
    .b(b),
    .a_b(a_b) 
);

roll_over_difference_back 
	roll_over_difference_back_inst
(
    .enable(enable),
    .roll_over(roll_over),
    .a(a),
    .b(b),
    .a_b(a_b_inst_1) 
);


assign a_gated = a & ({17{enable}});
assign b_gated = b & ({17{enable}});
assign a_b_reference = (a_gated < b_gated) ? (a_gated - b_gated + {roll_over+1'b1,5'b0}) : (a_gated - b_gated) ;

reg clk;
always begin
 #5;
 clk = ~clk;
end
always @(posedge clk ) begin
	a = $random;
	b = $random;
	roll_over = $random;
	enable = 1'b1;
	if (a_b_reference != a_b) begin
	   $stop;
		$display (" Error time:%d %a=%d b=%d enable=%d roll_over=%d a_b_reference=%d a_b=%d",$time, a ,b ,enable ,roll_over ,a_b_reference ,a_b);
	end
end

initial begin
	enable = 1'b1;
	clk = 1'b0;
end



endmodule
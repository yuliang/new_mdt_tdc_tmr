/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : interface_fifo_sim.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 7st, 2018
//  Note       : 
//     


module interface_fifo_sim();

reg clk160;
wire fifo_read_clk;
reg fifo_read;
reg [9:0] fifo_data_in;
wire [9:0] fifo_data_out;
reg rst;
  //interface FIFO
  fifo1 #(.DSIZE(10),.ASIZE(2),.SYN_DEPTH(2)) 
    chnl_fifo1_r_inst (
      .rdata(fifo_data_out),
      .wfull(fifo_full),
      .rempty(fifo_empty),
      .wdata(fifo_data_in),
      .winc(1'b1), .wclk(clk160), .wrst(rst),
      .rinc(fifo_read), .rclk(fifo_read_clk), .rrst(rst)
    );


always begin
	clk160 <= 1'b0;
	#3.125;
	clk160 <= 1'b1;
	#3.125;
end

reg clk320;
always begin
	clk320 <= 1'b0;
	#1.5625;
	clk320 <= 1'b1;
	#1.5625;
end
assign #0.5 fifo_read_clk =clk320 ;

always @(posedge clk160  ) begin
	if (rst) begin
		// reset
		fifo_data_in <= 10'b0;
	end
	else if (~fifo_full) begin
		fifo_data_in <= fifo_data_in + 10'b1;
	end
end


initial begin
	rst = 1'b0;
	#300;
	rst = 1'b1;
	#300;
	rst = 1'b0;
end

always @(posedge fifo_read_clk  ) begin
	if (rst) begin
		// reset
		fifo_read <= 1'b0;
	end
	else if (~fifo_empty&(~|send_number)) begin
		fifo_read <= 1'b1;
	end else begin
		fifo_read <= 1'b0;
	end
end

reg [3:0] send_number;
always @(posedge fifo_read_clk  ) begin
	if (rst) begin
		// reset
		send_number <= 4'b0;
	end	else if (~fifo_empty&(~|send_number)) begin
		send_number <= 4'h4;
	end else if((|send_number))begin
		send_number <= send_number-4'h1;
	end
end

reg [11:0] write_number;
reg [11:0] read_number;
always @(posedge clk160  ) begin
	if (rst) begin
		// reset
		write_number <= 12'b0;
	end
	else if (~fifo_full) begin
		write_number <= write_number + 12'b1;
	end
end

always @(posedge fifo_read_clk  ) begin
	if (rst) begin
		// reset
		read_number <= 12'b0;
	end
	else if (fifo_read&(~fifo_empty)) begin
		read_number <= read_number + 12'b1;
	end
end

endmodule
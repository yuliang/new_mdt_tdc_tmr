/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : trigger_interface_tb.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 20st, 2018
//  Note       : 
// 
//TODO


module trigger_interface_tb();
reg  rst;
reg clk160,clk320;
wire trigger_count_reset;
wire coarse_bcr;

wire trigger_lost;
wire [11:0] event_id,bunch_id,limit_up,limit_down;
wire trigger_fifo_empty;

wire bcr;
reg get_trigger;
reg trigger;

wire [11:0] roll_over;
assign roll_over = 12'd99;
wire [11:0] coarse_count_offset;
assign coarse_count_offset = 12'd05;

trigger_interface 
	trigger_interface_inst(
		.clk(clk160),
		.rst(rst),
	
		.enable(1'b1),

	    .reset_event_id(rst),
		.trigger_count_reset(trigger_count_reset),
		.bunch_offset(12'd10),
	    .event_offset($random),
		.roll_over(roll_over),
		.match_window(12'd40),

		.trigger_in(trigger),

		.get_trigger(get_trigger),
		.trigger_fifo_rd_clk(clk160),
		.trigger_fifo_empty(trigger_fifo_empty),
		.event_id(event_id),
		.bunch_id(bunch_id),
		.limit_up(limit_up),
		.limit_down(limit_down),

		.trigger_lost(trigger_lost)
);
wire auto_roll_over;
assign  auto_roll_over = 1'b1;
bcr_distribution
	bcr_distribution_inst(
	.clk160(clk160),
//	.clk320(clk320),
	.rst(rst),

	.bcr(bcr),
	.roll_over(roll_over),
	.coarse_count_offset(coarse_count_offset),
	//input [11:0] bunch_offset,

	.auto_roll_over(auto_roll_over),
    .bypass_bcr_distribution(1'b0),
	.coarse_bcr(coarse_bcr),
	.trigger_count_bcr(trigger_count_reset)
);

always begin
  clk160 =1'b0;
  #3.125;
  clk160 =1'b1;
  #3.125;
end
always begin
  clk320 =1'b0;
  #1.5625;
  clk320 =1'b1;
  #1.5625;
end
initial begin
	rst = 1'b1;
	#300;
	rst = 1'b1;
	#300;
	rst = 1'b0;
end

reg clk40;
always begin
  clk40 =1'b0;
  #12.5;
  clk40 =1'b1;
  #12.5;
end
reg [11:0] bunch_counter;

always @(posedge clk40  ) begin
	if (rst) begin
		// reset
		bunch_counter <= 12'b0;
	end	else begin
		bunch_counter <= bunch_counter ==roll_over ? 12'b0 : bunch_counter + 12'b1;
	end
end

assign  bcr = auto_roll_over ? 1'b0 : bunch_counter == roll_over ;

always @(posedge clk160  ) begin
	if (rst) begin
		// reset
		trigger <= 1'b0;
		get_trigger <= 1'b0;
	end	else begin
		trigger <= ($random %100) > 70;
		get_trigger <= ($random %100) > 90 &(~trigger_fifo_empty);
	end
end

endmodule
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : ttc_control_decoder_tb.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 26th, 2018
//  Note       : 
//    


module ttc_control_decoder_tb();


reg rst;
reg clk160,clk320;
reg trigger_direct_in,event_reset_direct_in;
reg reset_jtag_in;
reg event_reset_jtag_in;
reg encoded_control_in;
wire enable_master_reset_code;  assign  enable_master_reset_code = 1'b1;
wire enable_trigger;			assign  enable_trigger = 1'b1;
wire enable_direct_trigger;		assign  enable_direct_trigger = 1'b0;
wire enable_direct_bunch_reset;	assign  enable_direct_bunch_reset = 1'b0;
wire enable_direct_event_reset;	assign  enable_direct_event_reset = 1'b0;
wire trigger;
wire event_reset;
wire master_reset;
wire [11:0] roll_over;			assign roll_over = 12'd20;
wire [11:0] coarse_count_offset;assign coarse_count_offset = 12'd05;
wire auto_roll_over;			assign auto_roll_over = 1'b0;
wire bypass_bcr_distribution;   assign bypass_bcr_distribution = 1'b0;
wire coarse_bcr;
wire trigger_count_bcr;

ttc_control_decoder
	ttc_control_decoder_inst( 
	.clk160(clk160), 

	.reset_in(rst), 
	.trigger_direct_in(trigger_direct_in),
	.bunch_reset_direct_in(bcr),
	.event_reset_direct_in(event_reset_direct_in),

	.reset_jtag_in(reset_jtag_in),
	.event_reset_jtag_in(event_reset_jtag_in),

	.encoded_control_in(encoded_control_in),

    .enable_new_ttc(1'b0),
	.enable_master_reset_code(enable_master_reset_code),
	.enable_trigger(enable_trigger),
	.enable_direct_trigger(enable_direct_trigger),
	.enable_direct_bunch_reset(enable_direct_bunch_reset),
	.enable_direct_event_reset(enable_direct_event_reset),

	. trigger(trigger),

	. event_reset(event_reset),
	. master_reset(master_reset),

//	.clk320(clk320),

	.roll_over(roll_over),
	.coarse_count_offset(coarse_count_offset),

	.auto_roll_over(auto_roll_over),
	.bypass_bcr_distribution(bypass_bcr_distribution),

	.coarse_bcr(coarse_bcr),
	.trigger_count_bcr(trigger_count_bcr)
);

always begin
  clk160 =1'b0;
  #3.125;
  clk160 =1'b1;
  #3.125;
end
always begin
  clk320 =1'b0;
  #1.5625;
  clk320 =1'b1;
  #1.5625;
end
initial begin
	rst = 1'b1;
	#300;
	rst = 1'b1;
	#300;
	rst = 1'b0;
end


initial begin
	reset_jtag_in = 1'b0;
	trigger_direct_in = 1'b0;
	event_reset_direct_in = 1'b0;
	encoded_control_in = 1'b0;
	event_reset_jtag_in = 1'b0;
end

reg clk40_r;
always begin
  clk40_r =1'b0;
  #12.5;
  clk40_r =1'b1;
  #12.5;
end
wire clk40;
assign #3 clk40 = clk40_r;

reg [11:0] bunch_counter;

always @(posedge clk40  ) begin
	if (rst) begin
		// reset
		bunch_counter <= 12'b0;
	end	else begin
		bunch_counter <= bunch_counter ==roll_over ? 12'b0 : bunch_counter + 12'b1;
	end
end
assign  bcr = auto_roll_over ? 1'b0 : bunch_counter == roll_over ;

initial begin
	encoded_control_in = 1'b0;
	# 3150;
	@(posedge clk40) encoded_control_in = 1'b1;
	@(posedge clk40) encoded_control_in = 1'b0;
	@(posedge clk40) encoded_control_in = 1'b0;
	@(posedge clk40) encoded_control_in = 1'b0;
//	# 200;
	@(posedge clk40) encoded_control_in = 1'b1;
	@(posedge clk40) encoded_control_in = 1'b0;
	@(posedge clk40) encoded_control_in = 1'b1;
	@(posedge clk40) encoded_control_in = 1'b0;	
//	# 200;
	@(posedge clk40) encoded_control_in = 1'b1;
	@(posedge clk40) encoded_control_in = 1'b1;
	@(posedge clk40) encoded_control_in = 1'b0;
	@(posedge clk40) encoded_control_in = 1'b0;

//	# 200;
	@(posedge clk40) encoded_control_in = 1'b1;
	@(posedge clk40) encoded_control_in = 1'b1;
	@(posedge clk40) encoded_control_in = 1'b1;
	@(posedge clk40) encoded_control_in = 1'b0;	

	// # 3150;
	// @(posedge clk160) encoded_control_in = 1'b1;
	// @(posedge clk160) encoded_control_in = 1'b0;
	// @(posedge clk160) encoded_control_in = 1'b0;
	// @(posedge clk160) encoded_control_in = 1'b0;
	// @(posedge clk160) encoded_control_in = 1'b0;
	// //# 200;
	// @(posedge clk160) encoded_control_in = 1'b1;
	// @(posedge clk160) encoded_control_in = 1'b0;
	// @(posedge clk160) encoded_control_in = 1'b0;
	// @(posedge clk160) encoded_control_in = 1'b1;
	// @(posedge clk160) encoded_control_in = 1'b0;	
	// //# 200;
	// @(posedge clk160) encoded_control_in = 1'b1;
	// @(posedge clk160) encoded_control_in = 1'b0;
	// @(posedge clk160) encoded_control_in = 1'b1;
	// @(posedge clk160) encoded_control_in = 1'b0;
	// @(posedge clk160) encoded_control_in = 1'b0;
	// //# 200;
	// @(posedge clk160) encoded_control_in = 1'b1;
	// @(posedge clk160) encoded_control_in = 1'b0;
	// @(posedge clk160) encoded_control_in = 1'b1;
	// @(posedge clk160) encoded_control_in = 1'b1;
	// @(posedge clk160) encoded_control_in = 1'b0;


	// //# 3150;
	// @(posedge clk160) encoded_control_in = 1'b1;
	// @(posedge clk160) encoded_control_in = 1'b1;
	// @(posedge clk160) encoded_control_in = 1'b0;
	// @(posedge clk160) encoded_control_in = 1'b0;
	// @(posedge clk160) encoded_control_in = 1'b0;
	// //# 200;
	// @(posedge clk160) encoded_control_in = 1'b1;
	// @(posedge clk160) encoded_control_in = 1'b1;
	// @(posedge clk160) encoded_control_in = 1'b0;
	// @(posedge clk160) encoded_control_in = 1'b1;
	// @(posedge clk160) encoded_control_in = 1'b0;	
	// //# 200;
	// @(posedge clk160) encoded_control_in = 1'b1;
	// @(posedge clk160) encoded_control_in = 1'b1;
	// @(posedge clk160) encoded_control_in = 1'b1;
	// @(posedge clk160) encoded_control_in = 1'b0;
	// @(posedge clk160) encoded_control_in = 1'b0;
	// //# 200;
	// @(posedge clk160) encoded_control_in = 1'b1;
	// @(posedge clk160) encoded_control_in = 1'b1;
	// @(posedge clk160) encoded_control_in = 1'b1;
	// @(posedge clk160) encoded_control_in = 1'b1;
	// @(posedge clk160) encoded_control_in = 1'b0;				

end

endmodule
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/25/2018 04:08:36 PM
// Design Name: 
// Module Name: add_speed_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module add_speed_sim(

    );
    reg[11:0] a_r;
    reg [11:0] b_r;
    reg carry_in;
    wire [11:0] s;
    wire  carry_out;
    wire [11:0] s_ref;
    wire carry_out_ref;
    adder_speed     adder_speed_inst(
                .a(a_r),.b(b_r),.carry_in(carry_in),
                .s(s),.carry_out(carry_out));
    assign {carry_out_ref,s_ref} =a_r+b_r+carry_in;
    reg clk40=1'b0;
    always begin
        #12.5 clk40= 1'b1;
        #12.5 clk40= 1'b0;
   end
   always @(posedge clk40) begin
      a_r <= $random;
      b_r <= $random;
      carry_in <= $random;
  end
   
endmodule

/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : trigger_event_builder_tb.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 16st, 2018
//  Note       :  Test triggerless_channel_mux and 
// 

module trigger_event_builder_tb;
reg clk,rst;
reg [45*24-1:0] channel_data;
reg [23:0] channel_fifo_empty;
reg [23:0] trigger_processing;
wire [23:0] channel_fifo_read;
wire channel_fifo_clear;

reg read_out_fifo_full;
wire read_out_fifo_write;
wire data_ready;
wire [44:0] data;
wire [40:0] read_out_fifo_data;

reg trigger_fifo_empty;
reg [11:0] trigger_event_id;
reg [11:0] trigger_bunch_id;
reg [(24-`TRIGGER_MATCHING_HIT_COUNT_SIZE)-1:0] error_flag;

wire get_trigger;
wire trigger_head;
wire trigger_trail;

trigger_event_builder2 #( .CHANNEL_NUMBER(24), .CHANNEL_DATA_WIDTH(45))
	trigger_event_builder_DUT(
		.clk(clk),
		.rst(rst),
	
		.enable(1'b1),
        .enable_timeout(1'b1),
		.trigger_fifo_empty(trigger_fifo_empty),
		.trigger_event_id(trigger_event_id),
		.trigger_bunch_id(trigger_bunch_id),
		.error_flag(error_flag),

	 	.trigger_processing(trigger_processing),
	 	.chnl_fifo_empty(channel_fifo_empty),
	 	.chnl_fifo_read(channel_fifo_read),
		.chnl_data(channel_data),
		.channel_fifo_clear(channel_fifo_clear),

		.get_trigger(get_trigger),

		.read_out_fifo_full(read_out_fifo_full),

    	.data_ready(data_ready),
    	.data_out(data),
    	.trigger_head(trigger_head),
    	.trigger_trail(trigger_trail)
	);

data_recombine_process
	data_recombine_process_DUT(
	.clk(clk),
	.rst(rst),
	.channel_data_debug(1'b0),

	.data_ready(data_ready),
	.data_in(data),
	.read_out_fifo_full(read_out_fifo_full),
	.read_out_fifo_write(read_out_fifo_write),
	.read_out_fifo_data(read_out_fifo_data),

	.enable_trigger(1'b1),
	.enable_leading(1'b0),
	.enable_pair(1'b1),
	.roll_over(12'b110111101100),
	.full_width_res(1'b0),
	.width_select(3'b000), 

	.trigger_head(trigger_head),
	.trigger_trail(trigger_trail)
	);

always begin
	clk = 1'b0;
	# 3.125;
	clk = 1'b1;
	# 3.125;
end

initial begin
	rst = 1'b1;
	#300;
	rst = 1'b1;
	#300;
	rst = 1'b0;
end

reg [44:0] channel_data_0; reg [44:0] channel_data_1; reg [44:0] channel_data_2; reg [44:0] channel_data_3;
reg [44:0] channel_data_4; reg [44:0] channel_data_5; reg [44:0] channel_data_6; reg [44:0] channel_data_7;
reg [44:0] channel_data_8; reg [44:0] channel_data_9; reg [44:0] channel_data_10;reg [44:0] channel_data_11;
reg [44:0] channel_data_12;reg [44:0] channel_data_13;reg [44:0] channel_data_14;reg [44:0] channel_data_15;
reg [44:0] channel_data_16;reg [44:0] channel_data_17;reg [44:0] channel_data_18;reg [44:0] channel_data_19;
reg [44:0] channel_data_20;reg [44:0] channel_data_21;reg [44:0] channel_data_22;reg [44:0] channel_data_23;

initial begin
	channel_data_0  <=  $random ; channel_data_1  <= $random ; channel_data_2  <=  $random ;  channel_data_3  <= $random ; 
	channel_data_4  <=  $random ; channel_data_5  <= $random ; channel_data_6  <=  $random ;  channel_data_7  <= $random ; 
	channel_data_8  <=  $random ; channel_data_9  <= $random ; channel_data_10 <=  $random ;  channel_data_11 <= $random ; 
	channel_data_12 <=  $random ; channel_data_13 <= $random ; channel_data_14 <=  $random ;  channel_data_15 <= $random ; 
	channel_data_16 <=  $random ; channel_data_17 <= $random ; channel_data_18 <=  $random ;  channel_data_19 <= $random ; 
	channel_data_20 <=  $random ; channel_data_21 <= $random ; channel_data_22 <=  $random ;  channel_data_23 <= $random ; 
end
always @(posedge clk) begin
	channel_data_0  <= channel_fifo_read[0]  ? $random : channel_data_0 ; channel_data_1  <=channel_fifo_read[1  ] ? $random : channel_data_1  ; channel_data_2  <= channel_fifo_read[2  ] ? $random : channel_data_2  ;  channel_data_3  <= channel_fifo_read[3 ] ? $random : channel_data_3 ; 
	channel_data_4  <= channel_fifo_read[4]  ? $random : channel_data_4 ; channel_data_5  <=channel_fifo_read[5  ] ? $random : channel_data_6  ; channel_data_6  <= channel_fifo_read[6  ] ? $random : channel_data_6  ;  channel_data_7  <= channel_fifo_read[7 ] ? $random : channel_data_7 ; 
	channel_data_8  <= channel_fifo_read[8]  ? $random : channel_data_8 ; channel_data_9  <=channel_fifo_read[9  ] ? $random : channel_data_9  ; channel_data_10 <= channel_fifo_read[10 ] ? $random : channel_data_10 ;  channel_data_11 <= channel_fifo_read[11] ? $random : channel_data_11; 
	channel_data_12 <= channel_fifo_read[12] ? $random : channel_data_12; channel_data_13 <=channel_fifo_read[13 ] ? $random : channel_data_13 ; channel_data_14 <= channel_fifo_read[14 ] ? $random : channel_data_14 ;  channel_data_15 <= channel_fifo_read[15] ? $random : channel_data_15; 
	channel_data_16 <= channel_fifo_read[16] ? $random : channel_data_16; channel_data_17 <=channel_fifo_read[17 ] ? $random : channel_data_17 ; channel_data_18 <= channel_fifo_read[18 ] ? $random : channel_data_18 ;  channel_data_19 <= channel_fifo_read[19] ? $random : channel_data_19; 
	channel_data_20 <= channel_fifo_read[20] ? $random : channel_data_20; channel_data_21 <=channel_fifo_read[21 ] ? $random : channel_data_21 ; channel_data_22 <= channel_fifo_read[22 ] ? $random : channel_data_22 ;  channel_data_23 <= channel_fifo_read[23] ? $random : channel_data_23; 
	channel_data       <= {	channel_data_23 , channel_data_22 , channel_data_21 , channel_data_20 ,
	                        channel_data_19 , channel_data_18 , channel_data_17 , channel_data_16 ,
	                        channel_data_15 , channel_data_14 , channel_data_13, channel_data_12,
	                        channel_data_11, channel_data_10, channel_data_9, channel_data_8,
	                        channel_data_7, channel_data_6, channel_data_5, channel_data_4,
	                        channel_data_3, channel_data_2, channel_data_1, channel_data_0 };
end



initial begin
	trigger_event_id = $random;
	trigger_bunch_id = $random;
	error_flag =  $random;
end
always @(posedge clk ) begin
	if (get_trigger) begin
		trigger_event_id <=  $random;
		trigger_bunch_id <=  $random;
		error_flag <= $random;
	end
end
always @(posedge clk) begin
	trigger_fifo_empty <=  $random ;
end

always @(posedge clk) begin
	read_out_fifo_full <= ($random<32'ha000_0000);
end

initial begin
	channel_fifo_empty <= 24'hffffff;
end
generate
	genvar i;
	for (i = 0; i < 24; i = i + 1)
	begin:channel_fifo_empty_inst
		always @(posedge clk  ) begin
			if (rst) begin
				// reset
				channel_fifo_empty[i] <= 1'b1;
			end
			else if(channel_fifo_empty[i]) begin
				channel_fifo_empty[i] <= ($random<32'hFF00_0000);
			end else begin
				channel_fifo_empty[i] <= ($random<32'hFF00_0000)&(channel_fifo_read[i]);
			end
		end
	end
endgenerate

reg [1:0] trigger_r;
wire trigger_inner;
//syn trriger
always @(posedge clk  ) begin
	if (rst) begin
		trigger_r <= 2'b0;
	end
	else begin
		trigger_r <= {trigger_r[0],get_trigger};	
	end
end
assign  trigger_inner = trigger_r[0]&(~trigger_r[1]);

generate
	genvar j;
	for (j = 0; j < 24; j = j + 1)
	begin:trigger_processing_inst
		always @(posedge clk  ) begin
			if (rst) begin
				// reset
				trigger_processing[j] <= 1'b0;
			end	else if(~trigger_processing) begin
				trigger_processing[j] <= trigger_inner; 
			end	else begin
				trigger_processing[j] <= ($random<32'hF000_0000);
			end
		end
	end
endgenerate


endmodule
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : triggerless_channel_mux_tb.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 16st, 2018
//  Note       :  Test triggerless_channel_mux and 
// 

module triggerless_channel_mux_tb;
reg clk,rst;
reg [45*24-1:0] channel_data;
reg [23:0] channel_fifo_full;
reg [23:0] channel_fifo_empty;
wire [23:0] channel_fifo_read;
reg read_out_fifo_full;
wire read_out_fifo_write;
wire data_ready;
wire [44:0] data;
wire [40:0] read_out_fifo_data;
wire channel_data_debug;
assign channel_data_debug = 1'b1;
triggerless_channel_mux #(.CHANNEL_NUMBER(24),.CHANNEL_DATA_WIDTH(45))
	triggerless_channel_mux_DUT(
    	.clk(clk),
    	.rst(rst),
		.enable(1'b1),
		.channel_data_debug(channel_data_debug),
		
    	.channel_data(channel_data),
    	.channel_fifo_full(channel_fifo_full),
    	.channel_fifo_empty(channel_fifo_empty),
    	.channel_fifo_read(channel_fifo_read),
		
    	.read_out_fifo_full(read_out_fifo_full),
		
    	.data_ready(data_ready),
    	.data_out(data)
	);

//wire data_ready_ref;
//wire [44:0] data_ref;
//wire [23:0] channel_fifo_read_ref;
//triggerless_channel_mux_without_debug #(.CHANNEL_NUMBER(24),.CHANNEL_DATA_WIDTH(45))
//	triggerless_channel_mux_without_debug_DUT(
//    	.clk(clk),
//    	.rst(rst),
//		.enable(1'b1),


//    	.channel_data(channel_data),
//    	.channel_fifo_full(channel_fifo_full),
//    	.channel_fifo_empty(channel_fifo_empty),
//    	.channel_fifo_read(channel_fifo_read_ref),
		
//    	.read_out_fifo_full(read_out_fifo_full),
		
//    	.data_ready(data_ready_ref),
//    	.data_out(data_ref)
//	);



data_recombine_process
	data_recombine_process_DUT(
	.clk(clk),
	.rst(rst),
	.channel_data_debug(channel_data_debug),

	.data_ready(data_ready),
	.data_in(data),
	.read_out_fifo_full(read_out_fifo_full),
	.read_out_fifo_write(read_out_fifo_write),
	.read_out_fifo_data(read_out_fifo_data),

	.enable_trigger(1'b0),
	.enable_leading(1'b0),
	.enable_pair(1'b1),
	.roll_over(12'b110111101100),
	.trigger_head(1'b0),
	.trigger_trail(1'b0)
	);

always begin
	clk = 1'b0;
	# 3.125;
	clk = 1'b1;
	# 3.125;
end

initial begin
	rst = 1'b1;
	#300;
	rst = 1'b1;
	#300;
	rst = 1'b0;
end

reg [44:0] channel_data_0; reg [44:0] channel_data_1; reg [44:0] channel_data_2; reg [44:0] channel_data_3;
reg [44:0] channel_data_4; reg [44:0] channel_data_5; reg [44:0] channel_data_6; reg [44:0] channel_data_7;
reg [44:0] channel_data_8; reg [44:0] channel_data_9; reg [44:0] channel_data_10;reg [44:0] channel_data_11;
reg [44:0] channel_data_12;reg [44:0] channel_data_13;reg [44:0] channel_data_14;reg [44:0] channel_data_15;
reg [44:0] channel_data_16;reg [44:0] channel_data_17;reg [44:0] channel_data_18;reg [44:0] channel_data_19;
reg [44:0] channel_data_20;reg [44:0] channel_data_21;reg [44:0] channel_data_22;reg [44:0] channel_data_23;

initial begin
	channel_data_0  <=  $random ; channel_data_1  <= $random ; channel_data_2  <=  $random ;  channel_data_3  <= $random ; 
	channel_data_4  <=  $random ; channel_data_5  <= $random ; channel_data_6  <=  $random ;  channel_data_7  <= $random ; 
	channel_data_8  <=  $random ; channel_data_9  <= $random ; channel_data_10 <=  $random ;  channel_data_11 <= $random ; 
	channel_data_12 <=  $random ; channel_data_13 <= $random ; channel_data_14 <=  $random ;  channel_data_15 <= $random ; 
	channel_data_16 <=  $random ; channel_data_17 <= $random ; channel_data_18 <=  $random ;  channel_data_19 <= $random ; 
	channel_data_20 <=  $random ; channel_data_21 <= $random ; channel_data_22 <=  $random ;  channel_data_23 <= $random ; 
end
always @(posedge clk) begin
	channel_fifo_empty <= ($random);
	channel_fifo_full  <= ($random)&($random)&($random)&($random);

	channel_data_0  <= channel_fifo_read[0]  ? $random : channel_data_0 ; channel_data_1  <=channel_fifo_read[1  ] ? $random : channel_data_1  ; channel_data_2  <= channel_fifo_read[2  ] ? $random : channel_data_2  ;  channel_data_3  <= channel_fifo_read[3 ] ? $random : channel_data_3 ; 
	channel_data_4  <= channel_fifo_read[4]  ? $random : channel_data_4 ; channel_data_5  <=channel_fifo_read[5  ] ? $random : channel_data_6  ; channel_data_6  <= channel_fifo_read[6  ] ? $random : channel_data_6  ;  channel_data_7  <= channel_fifo_read[7 ] ? $random : channel_data_7 ; 
	channel_data_8  <= channel_fifo_read[8]  ? $random : channel_data_8 ; channel_data_9  <=channel_fifo_read[9  ] ? $random : channel_data_9  ; channel_data_10 <= channel_fifo_read[10 ] ? $random : channel_data_10 ;  channel_data_11 <= channel_fifo_read[11] ? $random : channel_data_11; 
	channel_data_12 <= channel_fifo_read[12] ? $random : channel_data_12; channel_data_13 <=channel_fifo_read[13 ] ? $random : channel_data_13 ; channel_data_14 <= channel_fifo_read[14 ] ? $random : channel_data_14 ;  channel_data_15 <= channel_fifo_read[15] ? $random : channel_data_15; 
	channel_data_16 <= channel_fifo_read[16] ? $random : channel_data_16; channel_data_17 <=channel_fifo_read[17 ] ? $random : channel_data_17 ; channel_data_18 <= channel_fifo_read[18 ] ? $random : channel_data_18 ;  channel_data_19 <= channel_fifo_read[19] ? $random : channel_data_19; 
	channel_data_20 <= channel_fifo_read[20] ? $random : channel_data_20; channel_data_21 <=channel_fifo_read[21 ] ? $random : channel_data_21 ; channel_data_22 <= channel_fifo_read[22 ] ? $random : channel_data_22 ;  channel_data_23 <= channel_fifo_read[23] ? $random : channel_data_23; 
	channel_data       <= {	channel_data_23 , channel_data_22 , channel_data_21 , channel_data_20 ,
	                        channel_data_19 , channel_data_18 , channel_data_17 , channel_data_16 ,
	                        channel_data_15 , channel_data_14 , channel_data_13, channel_data_12,
	                        channel_data_11, channel_data_10, channel_data_9, channel_data_8,
	                        channel_data_7, channel_data_6, channel_data_5, channel_data_4,
	                        channel_data_3, channel_data_2, channel_data_1, channel_data_0 };
	read_out_fifo_full <= ($random<32'ha000_0000);
	//if((data !== data_ref) | (data_ready !== data_ready_ref) |(channel_fifo_read != channel_fifo_read_ref))begin
	//	$stop;
	//end
end

endmodule
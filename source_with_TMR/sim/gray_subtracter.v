
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : gray_to_binary.v
//  Author     :Yu Liang
//  Revision   : 
//               First created on March 13th, 2016
//  Note       : verilog behavior for gray code subtract
`include "common_definition.v"
module gray_subtracter
#(parameter
	WIDTH=2
)
(
  input  [WIDTH-1:0] minuend,
  input  [WIDTH-1:0] subtrahend,
  output [WIDTH-1:0] result
);
wire [WIDTH-1:0] minuend_binary;
wire [WIDTH-1:0] subtrahend_binary;
gray_to_binary#(WIDTH)
  gray_to_binary_minuend(
    .gray_code(minuend),
    .binary_code(minuend_binary)
  ),
  gray_to_binary_subtrahend(
    .gray_code(subtrahend),
    .binary_code(subtrahend_binary)
  );

assign  result = minuend_binary-subtrahend_binary;

endmodule
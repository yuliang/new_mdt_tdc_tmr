/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : fake_trigger_process.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on June 14th, 2017
//  Note       : 
// 


module fake_trigger_process(
input trigger,
input rst,
input clk160,
input [11:0] counter_fast,
input [11:0] roll_over,
input [11:0] trigger_latency,
input [11:0] matching_window,
input large_matching,
output reg trigger_out,
output reg [11:0] limit_up,limit_down);

reg [1:0] trigger_r;

always @(posedge clk160) begin
  if (rst) begin
    // reset
    trigger_r <= #(`ff_delay) 2'b0;
  end
  else begin
    trigger_r <= #(`ff_delay) {trigger_r[0],trigger};
  end
end
wire trigger_out_inner;
assign #(`logic_delay) trigger_out_inner = trigger_r[0]&(~trigger_r[1]);

always @(posedge clk160) begin
  if (rst) begin
    // reset
    trigger_out <= #(`ff_delay) 1'b0;
  end
  else begin
    trigger_out <= #(`ff_delay) trigger_out_inner;
  end
end
wire [11:0] trigger_latency_inner;
assign trigger_latency_inner = trigger_latency - (matching_window >> 1);
always @(posedge clk160) begin
  if (rst|large_matching) begin
    // reset
    limit_up <= #(`ff_delay) 12'hfff;
    limit_down <= #(`ff_delay) 12'h000;
  end
  else if (trigger_out_inner) begin
    if(counter_fast>=trigger_latency_inner+matching_window) begin
      limit_up <= #(`ff_delay) counter_fast-trigger_latency_inner;
      limit_down <= #(`ff_delay) counter_fast-trigger_latency_inner-matching_window;
    end
    else if(counter_fast>=trigger_latency_inner) begin
      limit_up <= #(`ff_delay) counter_fast-trigger_latency_inner;
      limit_down <= #(`ff_delay)  roll_over+counter_fast-matching_window-trigger_latency_inner+'b1;
    end
    else begin
      limit_up  <= #(`ff_delay) roll_over+counter_fast-trigger_latency_inner+'b1;
      limit_down <= #(`ff_delay) roll_over+counter_fast-trigger_latency_inner-matching_window+'b1;
    end
  end
end
endmodule
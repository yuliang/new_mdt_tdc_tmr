/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : data_sampling.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-04-20 09:21:12
//  Note       : 
//   

module data_sampling(
   input clk,
   input rst,
   input d_line_0,
   output d_line_0_out,
   input d_line_1,
   output d_line_1_out,
   input reselect,
   input [9:0] correct_value_0,correct_value_1,
   input [9:0] correct_counter_th,
   output locked_0,locked_1,
   output edge_select_0,edge_select_1,
   output data_ready_even,data_ready_odd
);
wire d_line_0_1,d_line_0_2;
IDDR #(
      .DDR_CLK_EDGE("SAME_EDGE"), // "OPPOSITE_EDGE", "SAME_EDGE" 
                                      //    or "SAME_EDGE_PIPELINED" 
      .INIT_Q1(1'b0), // Initial value of Q1: 1'b0 or 1'b1
      .INIT_Q2(1'b0), // Initial value of Q2: 1'b0 or 1'b1
      .SRTYPE("SYNC") // Set/Reset type: "SYNC" or "ASYNC" 
   ) IDDR_d_line_0_inst (
      .Q1(d_line_0_1), // 1-bit output for positive edge of clock 
      .Q2(d_line_0_2), // 1-bit output for negative edge of clock
      .C(clk),   // 1-bit clock input
      .CE(1'b1), // 1-bit clock enable input
      .D(d_line_0),   // 1-bit DDR data input
      .R(1'b0),   // 1-bit reset
      .S(1'b0)    // 1-bit set
   );
assign d_line_0_out = edge_select_0 ? d_line_0_2 : d_line_0_1;
wire [9:0] data_even;
data_selector
data_selector_0(
   .clk(clk),
   .rst(rst),
   .reselect(reselect),
   .correct_value(correct_value_0),
   .correct_counter_th(correct_counter_th),
   .data({d_line_0_2,d_line_0_1}),
   .locked(locked_0),
   .edge_select(edge_select_0),
   .data_out(data_even),
   .data_ready(data_ready_even)
);

wire d_line_1_1,d_line_1_2;
IDDR #(
      .DDR_CLK_EDGE("SAME_EDGE"), // "OPPOSITE_EDGE", "SAME_EDGE" 
                                      //    or "SAME_EDGE_PIPELINED" 
      .INIT_Q1(1'b0), // Initial value of Q1: 1'b0 or 1'b1
      .INIT_Q2(1'b0), // Initial value of Q2: 1'b0 or 1'b1
      .SRTYPE("SYNC") // Set/Reset type: "SYNC" or "ASYNC" 
   ) IDDR_d_line_1_inst (
      .Q1(d_line_1_1), // 1-bit output for positive edge of clock 
      .Q2(d_line_1_2), // 1-bit output for negative edge of clock
      .C(clk),   // 1-bit clock input
      .CE(1'b1), // 1-bit clock enable input
      .D(d_line_1),   // 1-bit DDR data input
      .R(1'b0),   // 1-bit reset
      .S(1'b0)    // 1-bit set
   );
assign d_line_1_out = edge_select_1 ? d_line_1_2 : d_line_1_1;
wire [9:0] data_odd;
data_selector
data_selector_1(
   .clk(clk),
   .rst(rst),
   .reselect(reselect),
   .correct_value(correct_value_1),
   .correct_counter_th(correct_counter_th),
   .data({d_line_1_2,d_line_1_1}),
   .locked(locked_1),
   .edge_select(edge_select_1),
   .data_out(data_odd),
   .data_ready(data_ready_odd)  
);

d_line_ila d_line_ila_inst (
	.clk(clk), // input wire clk
	.probe0(d_line_0_1), // input wire [0:0]  probe0  
	.probe1(d_line_0_2), // input wire [0:0]  probe1 
	.probe2(d_line_1_1), // input wire [0:0]  probe2 
	.probe3(d_line_1_2) // input wire [0:0]  probe3
);
endmodule
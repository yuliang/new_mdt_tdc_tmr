#for KC705
set_property CFGBVS VCCO [current_design]
set_property CONFIG_VOLTAGE 2.5 [current_design]

#clock to test_env
set_property IOSTANDARD LVDS_25 [get_ports clk_input_p]
set_property PACKAGE_PIN C25 [get_ports clk_input_p]
set_property PACKAGE_PIN B25 [get_ports clk_input_n]
set_property IOSTANDARD LVDS_25 [get_ports clk_input_n]


#============================================== JTAG ==============================================
#tck FMC K16
set_property PACKAGE_PIN G13 [get_ports tck]
set_property IOSTANDARD LVCMOS25 [get_ports tck]


#tms FMC K13
set_property PACKAGE_PIN A11 [get_ports tms]
set_property IOSTANDARD LVCMOS25 [get_ports tms]

#tdi FMC K10
set_property PACKAGE_PIN D14 [get_ports tdi]
set_property IOSTANDARD LVCMOS25 [get_ports tdi]

#trst FMC K7
set_property PACKAGE_PIN D11 [get_ports trst]
set_property IOSTANDARD LVCMOS25 [get_ports trst]

#tdo FMC K19
set_property PACKAGE_PIN J11 [get_ports tdo]
set_property IOSTANDARD LVCMOS25 [get_ports tdo]

#============================================== control ==============================================
#encoded_control_in
set_property IOSTANDARD LVDS_25 [get_ports encoded_control_out_p]
set_property PACKAGE_PIN H24 [get_ports encoded_control_out_p]
set_property PACKAGE_PIN H25 [get_ports encoded_control_out_n]
set_property IOSTANDARD LVDS_25 [get_ports encoded_control_out_n]

#reset_in
set_property PACKAGE_PIN G28 [get_ports reset_out]
set_property IOSTANDARD LVCMOS25 [get_ports reset_out]

#trigger_direct_in
set_property PACKAGE_PIN E28 [get_ports trigger_direct_out]
set_property IOSTANDARD LVCMOS25 [get_ports trigger_direct_out]

#bunch_reset_direct_in
set_property PACKAGE_PIN G27 [get_ports bunch_reset_direct_out]
set_property IOSTANDARD LVCMOS25 [get_ports bunch_reset_direct_out]

#event_reset_direct_in
set_property PACKAGE_PIN C24 [get_ports event_reset_direct_out]
set_property IOSTANDARD LVCMOS25 [get_ports event_reset_direct_out]

#=========================================== output  d_line ======================================

#d_line[0]
set_property IOSTANDARD LVDS_25 [get_ports {d_line_p[0]}]
set_property PACKAGE_PIN D22 [get_ports {d_line_p[0]}]
set_property PACKAGE_PIN C22 [get_ports {d_line_n[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {d_line_n[0]}]

#d_line[1]
set_property IOSTANDARD LVDS_25 [get_ports {d_line_p[1]}]
set_property PACKAGE_PIN D21 [get_ports {d_line_p[1]}]
set_property PACKAGE_PIN C21 [get_ports {d_line_n[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {d_line_n[1]}]

#=========================================== debug ======================================

#debug
set_property IOSTANDARD LVDS_25 [get_ports debug_input_p]
set_property PACKAGE_PIN Y23 [get_ports debug_input_p]
set_property PACKAGE_PIN Y24 [get_ports debug_input_n]
set_property IOSTANDARD LVDS_25 [get_ports debug_input_n]


#============================================ Hit ================================================
#hit0
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[0]}]
set_property PACKAGE_PIN H30 [get_ports {hit_p[0]}]
set_property PACKAGE_PIN G30 [get_ports {hit_n[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[0]}]

#hit1
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[1]}]
set_property PACKAGE_PIN D29 [get_ports {hit_p[1]}]
set_property PACKAGE_PIN C30 [get_ports {hit_n[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[1]}]

#hit2
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[2]}]
set_property PACKAGE_PIN B28 [get_ports {hit_p[2]}]
set_property PACKAGE_PIN A28 [get_ports {hit_n[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[2]}]

#hit3
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[3]}]
set_property PACKAGE_PIN F21 [get_ports {hit_p[3]}]
set_property PACKAGE_PIN E21 [get_ports {hit_n[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[3]}]

#hit4
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[4]}]
set_property PACKAGE_PIN C19 [get_ports {hit_p[4]}]
set_property PACKAGE_PIN B19 [get_ports {hit_n[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[4]}]

#hit5
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[5]}]
set_property PACKAGE_PIN D26 [get_ports {hit_p[5]}]
set_property PACKAGE_PIN C26 [get_ports {hit_n[5]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[5]}]

#hit6
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[6]}]
set_property PACKAGE_PIN G29 [get_ports {hit_p[6]}]
set_property PACKAGE_PIN F30 [get_ports {hit_n[6]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[6]}]

#hit7
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[7]}]
set_property PACKAGE_PIN B30 [get_ports {hit_p[7]}]
set_property PACKAGE_PIN A30 [get_ports {hit_n[7]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[7]}]

#hit8
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[8]}]
set_property PACKAGE_PIN A25 [get_ports {hit_p[8]}]
set_property PACKAGE_PIN A26 [get_ports {hit_n[8]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[8]}]

#hit9
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[9]}]
set_property PACKAGE_PIN F20 [get_ports {hit_p[9]}]
set_property PACKAGE_PIN E20 [get_ports {hit_n[9]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[9]}]

#hit10
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[10]}]
set_property PACKAGE_PIN B22 [get_ports {hit_p[10]}]
set_property PACKAGE_PIN A22 [get_ports {hit_n[10]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[10]}]

#hit11
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[11]}]
set_property PACKAGE_PIN B18 [get_ports {hit_p[11]}]
set_property PACKAGE_PIN A18 [get_ports {hit_n[11]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[11]}]

#hit12
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[12]}]
set_property PACKAGE_PIN H14 [get_ports {hit_p[12]}]
set_property PACKAGE_PIN G14 [get_ports {hit_n[12]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[12]}]

#hit13
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[13]}]
set_property PACKAGE_PIN F15 [get_ports {hit_p[13]}]
set_property PACKAGE_PIN E16 [get_ports {hit_n[13]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[13]}]

#hit14
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[14]}]
set_property PACKAGE_PIN F12 [get_ports {hit_p[14]}]
set_property PACKAGE_PIN E13 [get_ports {hit_n[14]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[14]}]

#hit15
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[15]}]
set_property PACKAGE_PIN L16 [get_ports {hit_p[15]}]
set_property PACKAGE_PIN K16 [get_ports {hit_n[15]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[15]}]

#hit16
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[16]}]
set_property PACKAGE_PIN L15 [get_ports {hit_p[16]}]
set_property PACKAGE_PIN K15 [get_ports {hit_n[16]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[16]}]

#hit17
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[17]}]
set_property PACKAGE_PIN K13 [get_ports {hit_p[17]}]
set_property PACKAGE_PIN J13 [get_ports {hit_n[17]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[17]}]

#hit18
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[18]}]
set_property PACKAGE_PIN D12 [get_ports {hit_p[18]}]
set_property PACKAGE_PIN D13 [get_ports {hit_n[18]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[18]}]

#hit19
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[19]}]
set_property PACKAGE_PIN F11 [get_ports {hit_p[19]}]
set_property PACKAGE_PIN E11 [get_ports {hit_n[19]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[19]}]

#hit20
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[20]}]
set_property PACKAGE_PIN E14 [get_ports {hit_p[20]}]
set_property PACKAGE_PIN E15 [get_ports {hit_n[20]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[20]}]

#hit21
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[21]}]
set_property PACKAGE_PIN C15 [get_ports {hit_p[21]}]
set_property PACKAGE_PIN B15 [get_ports {hit_n[21]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[21]}]

#hit22
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[22]}]
set_property PACKAGE_PIN H15 [get_ports {hit_p[22]}]
set_property PACKAGE_PIN G15 [get_ports {hit_n[22]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[22]}]

#hit23
set_property IOSTANDARD LVDS_25 [get_ports {hit_p[23]}]
set_property PACKAGE_PIN H11 [get_ports {hit_p[23]}]
set_property PACKAGE_PIN H12 [get_ports {hit_n[23]}]
set_property IOSTANDARD LVDS_25 [get_ports {hit_n[23]}]

#============================================ ePLL config ================================================
set_property PACKAGE_PIN H26 [get_ports {phase_clk160[0]}]
set_property PACKAGE_PIN H27 [get_ports {phase_clk160[1]}]
set_property PACKAGE_PIN E29 [get_ports {phase_clk160[2]}]
set_property PACKAGE_PIN E30 [get_ports {phase_clk160[3]}]
set_property PACKAGE_PIN C29 [get_ports {phase_clk160[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports phase_clk160*]


set_property PACKAGE_PIN B29 [get_ports {phase_clk320_0[0]}]
set_property PACKAGE_PIN B27 [get_ports {phase_clk320_0[1]}]
set_property PACKAGE_PIN A27 [get_ports {phase_clk320_0[2]}]
set_property PACKAGE_PIN E19 [get_ports {phase_clk320_0[3]}]

set_property PACKAGE_PIN D19 [get_ports {phase_clk320_1[0]}]
set_property PACKAGE_PIN C20 [get_ports {phase_clk320_1[1]}]
set_property PACKAGE_PIN B20 [get_ports {phase_clk320_1[2]}]
set_property PACKAGE_PIN G17 [get_ports {phase_clk320_1[3]}]

set_property PACKAGE_PIN F17 [get_ports {phase_clk320_2[0]}]
set_property PACKAGE_PIN C17 [get_ports {phase_clk320_2[1]}]
set_property PACKAGE_PIN B17 [get_ports {phase_clk320_2[2]}]
set_property PACKAGE_PIN G22 [get_ports {phase_clk320_2[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports phase_clk320*]



set_property PACKAGE_PIN F22 [get_ports {ePllResA[0]}]
set_property PACKAGE_PIN H21 [get_ports {ePllResA[1]}]
set_property PACKAGE_PIN H22 [get_ports {ePllResA[2]}]
set_property PACKAGE_PIN C12 [get_ports {ePllResA[3]}]

set_property PACKAGE_PIN B12 [get_ports {ePllIcpA[0]}]
set_property PACKAGE_PIN B14 [get_ports {ePllIcpA[1]}]
set_property PACKAGE_PIN A15 [get_ports {ePllIcpA[2]}]
set_property PACKAGE_PIN B13 [get_ports {ePllIcpA[3]}]

set_property PACKAGE_PIN A13 [get_ports {ePllCapA[0]}]
set_property PACKAGE_PIN J16 [get_ports {ePllCapA[1]}]
#
#
#
#set_property PACKAGE_PIN AG20  [get_ports {ePllResB[0]}]
#set_property PACKAGE_PIN AH20  [get_ports {ePllResB[1]}]
#set_property PACKAGE_PIN AJ22  [get_ports {ePllResB[2]}]
#set_property PACKAGE_PIN AJ23  [get_ports {ePllResB[3]}]
#
#set_property PACKAGE_PIN AA20  [get_ports {ePllIcpB[0]}]
#set_property PACKAGE_PIN AB20  [get_ports {ePllIcpB[1]}]
#set_property PACKAGE_PIN AC22  [get_ports {ePllIcpB[2]}]
#set_property PACKAGE_PIN AD22  [get_ports {ePllIcpB[3]}]
#
#set_property PACKAGE_PIN AF26  [get_ports {ePllCapB[0]}]
#set_property PACKAGE_PIN AF27  [get_ports {ePllCapB[1]}]
#
#
#set_property PACKAGE_PIN AJ27  [get_ports {ePllResC[0]}]
#set_property PACKAGE_PIN AK28  [get_ports {ePllResC[1]}]
#set_property PACKAGE_PIN AC26  [get_ports {ePllResC[2]}]
#set_property PACKAGE_PIN AD26  [get_ports {ePllResC[3]}]
#
#set_property PACKAGE_PIN AE28  [get_ports {ePllIcpC[0]}]
#set_property PACKAGE_PIN AF28  [get_ports {ePllIcpC[1]}]
#set_property PACKAGE_PIN AD29  [get_ports {ePllIcpC[2]}]
#set_property PACKAGE_PIN AE29  [get_ports {ePllIcpC[3]}]
#
#set_property PACKAGE_PIN AC29  [get_ports {ePllCapC[0]}]
#set_property PACKAGE_PIN AC30  [get_ports {ePllCapC[1]}]
#
set_property IOSTANDARD LVCMOS25 [get_ports ePllRes*]
set_property IOSTANDARD LVCMOS25 [get_ports ePllIcp*]
set_property IOSTANDARD LVCMOS25 [get_ports ePllCap*]

set_property PACKAGE_PIN H16 [get_ports ASD_TCK]
set_property IOSTANDARD LVCMOS25 [get_ports ASD_TCK]
set_property PACKAGE_PIN K14 [get_ports ASD_shift_out]
set_property IOSTANDARD LVCMOS25 [get_ports ASD_shift_out]
set_property PACKAGE_PIN J14 [get_ports ASD_update_out]
set_property IOSTANDARD LVCMOS25 [get_ports ASD_update_out]
set_property PACKAGE_PIN L11 [get_ports ASD_data_out]
set_property IOSTANDARD LVCMOS25 [get_ports ASD_data_out]
set_property PACKAGE_PIN K11 [get_ports ASD_data_in]
set_property IOSTANDARD LVCMOS25 [get_ports ASD_data_in]


set_property PACKAGE_PIN L12 [get_ports debug_output[0]]
set_property IOSTANDARD LVCMOS25 [get_ports debug_output[0]]
set_property PACKAGE_PIN L13 [get_ports debug_output[1]]
set_property IOSTANDARD LVCMOS25 [get_ports debug_output[1]]
set_property PACKAGE_PIN AE30 [get_ports debug_output[2]]
set_property IOSTANDARD LVCMOS25 [get_ports debug_output[2]]
set_property PACKAGE_PIN AF30 [get_ports debug_output[3]]
set_property IOSTANDARD LVCMOS25 [get_ports debug_output[3]]
set_property PACKAGE_PIN AB29 [get_ports debug_output[4]]
set_property IOSTANDARD LVCMOS25 [get_ports debug_output[4]]
set_property PACKAGE_PIN AB30 [get_ports debug_output[5]]
set_property IOSTANDARD LVCMOS25 [get_ports debug_output[5]]

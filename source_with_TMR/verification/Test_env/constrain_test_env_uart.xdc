#UART
set_property PACKAGE_PIN M19 [get_ports rxd]
set_property IOSTANDARD LVCMOS25 [get_ports rxd]
set_property PACKAGE_PIN K24 [get_ports txd]
set_property IOSTANDARD LVCMOS25 [get_ports txd]

#syn rx
set_false_path -to [get_pins -hierarchical -filter {NAME =~ *rxd_syn_reg[1]*/D}]

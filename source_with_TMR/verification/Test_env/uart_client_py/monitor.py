import serial

from TDC_config_low_level_function import *
ser = serial.Serial( port='COM6', baudrate = 115200, bytesize = serial.EIGHTBITS,parity =serial.PARITY_EVEN, stopbits = serial.STOPBITS_ONE, timeout=0.01)
update_config_period(0,ser)
update_JTAG_inst('\x18',ser)
update_bit_length(25,ser)
start_action(ser)
ser.close()
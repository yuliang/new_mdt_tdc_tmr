import serial
from TDC_config_low_level_function import *
from serial_config_tdc import *
from TDCreg import *
ser = serial.Serial( port='COM6', baudrate = 115200, bytesize = serial.EIGHTBITS,parity =serial.PARITY_EVEN, stopbits = serial.STOPBITS_ONE, timeout=0.01)
TDC0 = TDCreg(ser)

verificate_pass_TD_CODE = verificate_ID_CODE(ser)
verificate_pass_setup0 = verificate_setup0(ser)
verificate_pass_setup1 = verificate_setup1(ser)
verificate_pass_setup2 = verificate_setup2(ser)
verificate_pass_control0 = verificate_control0(ser)
verificate_pass_control1 = verificate_control1(ser)
verificate_pass_spi = verificate_spi(ser)

ASD_config(ser,5,'\x15')


trst_0(ser)
trst_1(ser)
get_ePLLconfig_reg(ser)
get_ePLLconfig_reg(ser)
#for ePLL_control
TDC0.phase_clk160[0] = '11111'
TDC0.phase_clk320_0[0] = '1111'
TDC0.phase_clk320_1[0] = '1111'
TDC0.phase_clk320_2[0] = '1111'
TDC0.ePllRes[0] = '0000'
TDC0.ePllIcp[0] = '0000'
TDC0.ePllCap[0] = '00'
ePLLconfig_reg_ref = {}
ePLLconfig_reg_ref['ePllCapA'] = TDC0.ePllCap[0]
ePLLconfig_reg_ref['ePllIcpA'] = TDC0.ePllIcp[0]
ePLLconfig_reg_ref['ePllResA'] = TDC0.ePllRes[0]
ePLLconfig_reg_ref['phase_clk320_2'] = TDC0.phase_clk320_2[0]
ePLLconfig_reg_ref['phase_clk320_1'] = TDC0.phase_clk320_1[0]
ePLLconfig_reg_ref['phase_clk320_0'] = TDC0.phase_clk320_0[0]
ePLLconfig_reg_ref['phase_clk160'] = TDC0.phase_clk160[0]
TDC0.update_control_1()
ePLL_config=get_ePLLconfig_reg(ser)
print(ePLL_config)
print(ePLLconfig_reg_ref)
trst_0(ser)
trst_1(ser)


tdc_high_speed(ser)
update_data_length(ser, 4)
tdc_triggerless_mode(ser)
#tdc_trigger_mode(ser)
update_input_deserial_para(ser)
reselect_input_deserial(ser)

update_hit_width(ser,1)
update_inv_hit(ser,'notinv')
update_hit_delay(ser,0)
update_hit_mask_hit(ser,'\xff\xff\xff')
start_single_hit(ser)

update_hit_interval(ser,19)
hit_start(ser)
hit_stop(ser)


update_ttc_delay(ser, 1)
ttc_mode_select(ser, 1)
update_new_ttc_code(ser, '\x08')
single_TTC(ser)

update_ttc_delay(ser, 1)
ttc_mode_select(ser, 0)
enable_ttc_trigger(ser)
single_TTC(ser)


trst_1(ser)
trst_0(ser)
get_ePLLconfig_reg(ser)
get_ePLLconfig_reg(ser)
#spi_test='\x04\x2f\xff\xff\xff\xff\xff\xff\xff\xff\xf0\xaa\xaa\x82\x01\x40\x80\x7f\xff\xff\x80\x07\xce\x00\x00\x0f\x98\xce\x50\x0e\x60\x02\x10\x80\x92\x24\x89\x20'
update_reg(0, '\xce\x00\x00\x0f\x98\xce\x50\x0e\x60\x02\x10\x80\x92\x24\x89\x20', ser)
update_reg(1, '\xff\xff\xff\xff\xf0\xaa\xaa\x82\x01\x40\x80\x7f\xff\xff\x80\x07', ser)
update_reg(2, '\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x04\x2f\xff\xff\xff\xff', ser)
update_reg(3, '\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00', ser)
update_config_period(0, ser)
update_bit_length(300,ser)
start_action(ser)
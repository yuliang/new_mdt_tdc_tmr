
set_param general.maxThreads 8
set_param messaging.defaultLimit 10000


add_files -fileset constrs_1 -norecurse E:/work/from_C/work/MDT_TDC_V1/code/channel/parallel_comp_ring_buffer/source/verification/Test_env/constrain_test_env_timing.xdc
add_files -fileset constrs_1 -norecurse E:/work/from_C/work/MDT_TDC_V1/code/channel/parallel_comp_ring_buffer/source/verification/Test_env/constrain_test_env_position.xdc

create_ip -name ila -vendor xilinx.com -library ip -version 6.1 -module_name ila_Jtag_master
set_property -dict [list CONFIG.C_PROBE13_WIDTH {512} CONFIG.C_PROBE12_WIDTH {512} CONFIG.C_PROBE10_WIDTH {9} CONFIG.C_PROBE9_WIDTH {5} CONFIG.C_PROBE8_WIDTH {9} CONFIG.C_PROBE7_WIDTH {5} CONFIG.C_PROBE6_WIDTH {12} CONFIG.C_PROBE5_WIDTH {1} CONFIG.C_PROBE4_WIDTH {12} CONFIG.C_DATA_DEPTH {4096} CONFIG.C_NUM_OF_PROBES {14} CONFIG.C_PROBE15_WIDTH {1}] [get_ips ila_Jtag_master]
generate_target {instantiation_template} [get_files e:/work/from_C/work/MDT_TDC_V1/FPGA_VARIFICATION/Test_env/Test_env.srcs/sources_1/ip/ila_Jtag_master/ila_Jtag_master.xci]

create_ip -name fifo_generator -vendor xilinx.com -library ip -version 13.1 -module_name fifo_160bit
set_property -dict [list CONFIG.Fifo_Implementation {Independent_Clocks_Builtin_FIFO} CONFIG.Performance_Options {First_Word_Fall_Through} CONFIG.Input_Data_Width {160} CONFIG.Read_Clock_Frequency {40} CONFIG.Write_Clock_Frequency {40} CONFIG.Output_Data_Width {160} CONFIG.Reset_Type {Asynchronous_Reset} CONFIG.Use_Dout_Reset {false} CONFIG.Full_Threshold_Assert_Value {1017} CONFIG.Full_Threshold_Negate_Value {1016} CONFIG.Empty_Threshold_Assert_Value {6} CONFIG.Empty_Threshold_Negate_Value {7} CONFIG.write_clk.FREQ_HZ {40000000} CONFIG.read_clk.FREQ_HZ {40000000}] [get_ips fifo_160bit]

create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_uart
set_property -dict [list CONFIG.C_NUM_PROBE_IN {0} CONFIG.C_NUM_PROBE_OUT {2} CONFIG.C_PROBE_OUT0_WIDTH {2} CONFIG.C_PROBE_OUT1_WIDTH {2} CONFIG.C_PROBE_OUT1_INIT_VAL {0x3} CONFIG.C_EN_PROBE_IN_ACTIVITY {0}] [get_ips vio_uart]

create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name vio_Jtag_monitor
set_property -dict [list CONFIG.C_NUM_PROBE_IN {7} CONFIG.C_NUM_PROBE_OUT {0} CONFIG.C_PROBE_IN1_WIDTH {12} CONFIG.C_PROBE_IN2_WIDTH {256} CONFIG.C_PROBE_IN3_WIDTH {256} CONFIG.C_PROBE_IN4_WIDTH {9} CONFIG.C_PROBE_IN5_WIDTH {5}] [get_ips vio_Jtag_monitor]

create_ip -name vio -vendor xilinx.com -library ip -version 3.0 -module_name ePLL_configration
set_property -dict [list CONFIG.C_NUM_PROBE_IN {7} CONFIG.C_NUM_PROBE_OUT {0} CONFIG.C_PROBE_IN0_WIDTH {5} CONFIG.C_PROBE_IN1_WIDTH {4} CONFIG.C_PROBE_IN2_WIDTH {4} CONFIG.C_PROBE_IN3_WIDTH {4} CONFIG.C_PROBE_IN4_WIDTH {4} CONFIG.C_PROBE_IN5_WIDTH {4} CONFIG.C_PROBE_IN6_WIDTH {2}] [get_ips ePLL_configration]

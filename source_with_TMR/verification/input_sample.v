`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/01/2018 10:27:42 AM
// Design Name: 
// Module Name: input_sample
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module input_sample(
//    input hit_p,hit_n,
    input hit,
    input clk,
    input [14:0] coarse_data,
    output hit_out,
    output ready_r_out,
    output [3:0] q_r_out,
    output [14:0] coarse_counter_r_out,
    output [14:0] coarse_counter_r_inv_out,
    output ready_f_out,
    output [3:0] q_f_out,
    output [14:0] coarse_counter_f_out,
    output [14:0] coarse_counter_f_inv_out
    );

    wire hit_i;
    assign hit_i=hit;
    assign hit_out=hit_i;
//    IBUFDS #(
//            .DIFF_TERM("FALSE"),       // Differential Termination
//            .IBUF_LOW_PWR("TRUE"),     // Low power="TRUE", Highest performance="FALSE" 
//            .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
//    ) IBUFDS_inst (
//        .O(hit_i),  // Buffer output
//        .I(hit_p),  // Diff_p buffer input (connect directly to top-level port)
//        .IB(hit_n) // Diff_n buffer input (connect directly to top-level port)
//        );
    wire[1:0] Q_data_ddr;
    IDDR #(
      .DDR_CLK_EDGE("SAME_EDGE"), // "OPPOSITE_EDGE", "SAME_EDGE" 
                                      //    or "SAME_EDGE_PIPELINED" 
      .INIT_Q1(1'b0), // Initial value of Q1: 1'b0 or 1'b1
      .INIT_Q2(1'b0), // Initial value of Q2: 1'b0 or 1'b1
      .SRTYPE("SYNC") // Set/Reset type: "SYNC" or "ASYNC" 
    ) IDDR_input_sample_0degree (
      .Q1(Q_data_ddr[1]), // 1-bit output for positive edge of clock 
      .Q2(Q_data_ddr[0]), // 1-bit output for negative edge of clock
      .C(clk),   // 1-bit clock input
      .CE(1'b1), // 1-bit clock enable input
      .D(hit_i),   // 1-bit DDR data input
      .R(1'b0),   // 1-bit reset
      .S(1'b0)    // 1-bit set
    );
    reg [1:0] Q_data=2'b0;
    reg [1:0] Q_data_last=2'b0;
    always @(posedge clk) begin
        Q_data <= Q_data_ddr;
    end
    always @(posedge clk) begin
        Q_data_last <= Q_data;
    end
    
    reg ready_r_0=1'b0;
    reg [3:0] ready_r_1=1'b0;
    reg ready_r=1'b0;
    assign ready_r_out=ready_r;
    reg [3:0] q_r=4'b0;
    assign q_r_out = q_r;
    reg [14:0] coarse_counter_r=14'b0;
    assign coarse_counter_r_out =coarse_counter_r;
    reg [14:0] coarse_counter_r_inv=14'b0;
    assign coarse_counter_r_inv_out =coarse_counter_r_inv;
    wire  rising_detect;
    assign rising_detect = ((~Q_data[0])&(Q_data[1]))|(((~Q_data_last[1])&(Q_data[0])));
    always @(posedge clk) begin
        if(rising_detect) begin
            ready_r_0 <= 1'b1;
        end else begin
            ready_r_0 <= 1'b0;
        end
    end
    always @(posedge clk) begin
        ready_r_1 <= {ready_r_1[2:0],ready_r_0};
    end
    always @(posedge clk) begin
        ready_r <= ((|ready_r_1)|ready_r_0);
    end
    reg first_half_r=1'b1;
    reg second_half_r=1'b1;    
    always @(posedge clk) begin
        if(rising_detect&(Q_data[0]))begin
            q_r<=first_half_r ?  4'b1001: 4'b0011;
            first_half_r <=~ first_half_r;
        end else if(rising_detect&(~Q_data[0]))begin
            q_r<=second_half_r ?  4'b0110: 4'b1100;
            second_half_r <=~ second_half_r;        
        end
    end
    always @(posedge clk) begin
        if(rising_detect&(Q_data[0]))begin
            coarse_counter_r<=coarse_data;
            coarse_counter_r_inv <=coarse_data;
        end else if(rising_detect&(~Q_data[0]))begin
            coarse_counter_r<=coarse_data;  
            coarse_counter_r_inv <=   coarse_data+15'b1;   
        end
    end

    reg ready_f_0=1'b0;
    reg [3:0] ready_f_1=1'b0;
    reg ready_f=1'b0;
    assign ready_f_out= ready_f;
    reg [3:0] q_f=4'b0;
    assign q_f_out=q_f;
    reg [14:0] coarse_counter_f=14'b0;
    assign coarse_counter_f_out=coarse_counter_f; 
    reg [14:0] coarse_counter_f_inv=14'b0;
    assign coarse_counter_f_inv_out=coarse_counter_f_inv;
    wire  falling_detect;
    assign falling_detect = ((Q_data[0])&(~Q_data[1]))|(((Q_data_last[1])&(~Q_data[0])));
    always @(posedge clk) begin
        if(falling_detect) begin
            ready_f_0 <= 1'b1;
        end else begin
            ready_f_0 <= 1'b0;
        end
    end
    always @(posedge clk) begin
        ready_f_1 <= {ready_f_1[2:0],ready_f_0};
    end
    always @(posedge clk) begin
        ready_f <= ((|ready_f_1)|ready_f_0);
    end
    reg first_half_f=1'b1;
    reg second_half_f=1'b1;    
    always @(posedge clk) begin
        if(falling_detect&(~Q_data[0]))begin
            q_f<=first_half_f ?  4'b1001: 4'b0011;
            first_half_f <=~ first_half_f;
        end else if(falling_detect&(Q_data[0]))begin
            q_f<=second_half_f ?  4'b0110: 4'b1100;
            second_half_f <=~ second_half_f;        
        end
    end
    always @(posedge clk) begin
        if(falling_detect&(~Q_data[0]))begin
            coarse_counter_f<=coarse_data;
            coarse_counter_f_inv <=coarse_data;
        end else if(falling_detect&(Q_data[0]))begin
            coarse_counter_f<=coarse_data;  
            coarse_counter_f_inv <=   coarse_data+15'b1;   
        end
    end

endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/05/2018 04:44:30 PM
// Design Name: 
// Module Name: tdc_logic_DUT
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////



module tdc_logic_DUT(
input clk_p,
input clk_n,

output clk_output_p,
output clk_output_n,

input tck,
input tms,
input tdi,
input trst,
output tdo,

output ASD_TCK,
output ASD_shift_out,
output ASD_update_out,
output ASD_data_out,
input  ASD_data_in,

//input [23:0] hit_p,hit_n,
input [23:0] hit,

input encoded_control_in_p,
input encoded_control_in_n,
input reset_in,
//input trigger_direct_in,
input bunch_reset_direct_in,
//input event_reset_direct_in,

//output debug_output_p,
//output debug_output_n,

output [1:0] d_line_p,
output [1:0] d_line_n,

input [5:0] debug_input,

output [4  :0] phase_clk160,
output [3  :0] phase_clk320_0, phase_clk320_1, phase_clk320_2,
output [3  :0] ePllResA, ePllResB, ePllResC,
output [3  :0] ePllIcpA, ePllIcpB, ePllIcpC,
output [1  :0] ePllCapA, ePllCapB, ePllCapC  

    );

//wire   [4  :0] phase_clk160;
//wire   [3  :0] phase_clk320_0, phase_clk320_1, phase_clk320_2;
//wire   [3  :0] ePllResA, ePllResB, ePllResC;
//wire   [3  :0] ePllIcpA, ePllIcpB, ePllIcpC;
//wire   [1  :0] ePllCapA, ePllCapB, ePllCapC;     
    
wire encoded_control_in;
IBUFDS #(
   .DIFF_TERM("FALSE"),       // Differential Termination
   .IBUF_LOW_PWR("TRUE"),     // Low power="TRUE", Highest performance="FALSE" 
   .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
) IBUFDS_encoded_control_in (
   .O(encoded_control_in),  // Buffer output
   .I(encoded_control_in_p),  // Diff_p buffer input (connect directly to top-level port)
   .IB(encoded_control_in_n) // Diff_n buffer input (connect directly to top-level port)
);

wire [1:0] d_line;
 OBUFDS #(
      .IOSTANDARD("DEFAULT"), // Specify the output I/O standard
      .SLEW("SLOW")           // Specify the output slew rate
   ) OBUFDS_d_line_0 (
      .O(d_line_p[0]),     // Diff_p output (connect directly to top-level port)
      .OB(d_line_n[0]),   // Diff_n output (connect directly to top-level port)
      .I(d_line[0])      // Buffer input 
   );

 OBUFDS #(
      .IOSTANDARD("DEFAULT"), // Specify the output I/O standard
      .SLEW("SLOW")           // Specify the output slew rate
   ) OBUFDS_d_line_1 (
      .O(d_line_p[1]),     // Diff_p output (connect directly to top-level port)
      .OB(d_line_n[1]),   // Diff_n output (connect directly to top-level port)
      .I(d_line[1])      // Buffer input 
   );
//wire debug_output;
// OBUFDS #(
//      .IOSTANDARD("DEFAULT"), // Specify the output I/O standard
//      .SLEW("SLOW")           // Specify the output slew rate
//   ) OBUFDS_debug_output (
//      .O(debug_output_p),     // Diff_p output (connect directly to top-level port)
//      .OB(debug_output_n),   // Diff_n output (connect directly to top-level port)
//      .I(debug_output)      // Buffer input 
//   );

wire clk40_A;
wire clk40_B;
wire clk40_C;
wire clk160_A;
wire clk160_B;
wire clk160_C;
wire clk320_A, clk320_B, clk320_C;
//  input_clock 
//  input_clock_inst(
//   // Clock in ports
//    .clk_in1_p(clk_p),    // input clk_in1_p
//    .clk_in1_n(clk_n),    // input clk_in1_n
//    // Clock out ports
//    .clk_out1(clk40_pre),     // output clk_out1
//    .clk_out2(clk160_pre),     // output clk_out2
//    .clk_out3(clk320_pre));    // output clk_out3

  input_clock_TMR input_clock_inst
   (
   // Clock in ports
    .clk_in1_p(clk_p),    // input clk_in1_p
    .clk_in1_n(clk_n),    // input clk_in1_n
    // Clock out ports
    .clk_out1(clk40_A),     // output clk_out1
    .clk_out2(clk40_B),     // output clk_out2
    .clk_out3(clk40_C)     // output clk_out3



    );   
    
     


 OBUFDS #(
      .IOSTANDARD("DEFAULT"), // Specify the output I/O standard
      .SLEW("SLOW")           // Specify the output slew rate
   ) OBUFDS_clk40_pre (
      .O(clk_output_p),     // Diff_p output (connect directly to top-level port)
      .OB(clk_output_n),   // Diff_n output (connect directly to top-level port)
      .I(clk40_A)      // Buffer input 
   );

 master_clock_TMR master_clk_generator_inst
       (
       // Clock in ports
        .clk_in1(clk40_A),      // input clk_in1
        // Clock out ports
        .clk_out1(clk320_A),     // output clk_out1
        .clk_out2(clk320_B),     // output clk_out2
        .clk_out3(clk320_C),     // output clk_out3
        .clk_out4(clk160_A),     // output clk_out4
        .clk_out5(clk160_B),     // output clk_out5
        .clk_out6(clk160_C),     // output clk_out6
        // Status and control signals
        .reset(reset), // input reset
        .locked(ePll_lock));      // output locked
wire coarse_bcr;
reg coarse_bcr_distribution;

wire [23:0] Rdy_r,Rdy_f;
wire [3:0] q_r [23:0];
wire [3:0] q_f [23:0];
wire [14:0] cnt_r [23:0];
wire [14:0] cnt_inv_r [23:0];
wire [14:0] cnt_f [23:0];
wire [14:0] cnt_inv_f [23:0];
wire [23:0] hit_i;
wire [14:0] coarse_counter [23:0];
 generate
	genvar i;
	for (i = 0; i < 24; i = i + 1)
	begin:tdc_front
		(* dont_touch = "true" *) input_sample
    		input_sample_inst(
    		    .hit(hit[i]),
//      			.hit_p(hit_p[i]),.hit_n(hit_n[i]),
      			.clk(clk320_A),
      			.coarse_data(coarse_counter[i]),
      			.hit_out(hit_i[i]),
      			.ready_r_out(Rdy_r[i]),
      			.q_r_out(q_r[i]),
      			.coarse_counter_r_out(cnt_r[i]),
      			.coarse_counter_r_inv_out(cnt_inv_r[i]),
      			.ready_f_out(Rdy_f[i]),
      			.q_f_out(q_f[i]),
      			.coarse_counter_f_out(cnt_f[i]),
      			.coarse_counter_f_inv_out(cnt_inv_f[i])
    		);
		(* dont_touch = "true" *) tdc_coarse
        tdc_coarse_inst
     ( .coarse_time(coarse_counter[i]),// coarse_time_inv,
       .clk320(clk320_A), 
       .LHC_BC_rst(coarse_bcr_distribution)
    );
    
//    coarse_bcr_ila your_instance_name (
//        .clk(clk320_A), // input wire clk
    
    
//        .probe0(coarse_bcr_distribution), // input wire [0:0]  probe0  
//        .probe1(coarse_counter[0]) // input wire [14:0]  probe1
//    );
    

	end
endgenerate

always @(posedge clk320_A) begin
  coarse_bcr_distribution <= coarse_bcr;
end

wire tck_clk;
   BUFG BUFG_tck (
      .O(tck_clk), // 1-bit output: Clock output
      .I(tck)  // 1-bit input: Clock input
   );



(* keep_hierarchy = "yes" *) 


tdc_logic_TMR
tdc_logic_inst(
    .clk320_A(clk320_A),
    .clk320_B(clk320_B),
    .clk320_C(clk320_C),

    .clk160_A(clk160_A),
    .clk160_B(clk160_B),
    .clk160_C(clk160_C),

    .clk40_A(clk40_A),
    .clk40_B(clk40_B),
    .clk40_C(clk40_C),

    
    .tck(tck_clk),
    .tms(tms),
    .tdi(tdi),
    .trst(trst),
    .tdo(tdo), 
    
    .ASD_TCK(ASD_TCK),
    .ASD_shift_out(ASD_shift_out),
    .ASD_update_out(ASD_update_out),
    .ASD_data_out_debug(ASD_data_out),
    .ASD_data_in(ASD_data_in),

    .encoded_control_in(encoded_control_in),
    .reset_in(reset_in),
    //.trigger_direct_in(trigger_direct_in),
    .bunch_reset_direct_in(bunch_reset_direct_in),
    //.event_reset_direct_in(event_reset_direct_in),
    //.debug_output(debug_output),
    
    .Rdy_r_0(Rdy_r[0]), .Rdy_r_1(Rdy_r[1]), .Rdy_r_2(Rdy_r[2]), .Rdy_r_3(Rdy_r[3]), .Rdy_r_4(Rdy_r[4]), .Rdy_r_5(Rdy_r[5]), .Rdy_r_6(Rdy_r[6]), .Rdy_r_7(Rdy_r[7]), .Rdy_r_8(Rdy_r[8]), .Rdy_r_9(Rdy_r[9]), .Rdy_r_10(Rdy_r[10]), .Rdy_r_11(Rdy_r[11]), 
    .Rdy_r_12(Rdy_r[12]), .Rdy_r_13(Rdy_r[13]), .Rdy_r_14(Rdy_r[14]), .Rdy_r_15(Rdy_r[15]), .Rdy_r_16(Rdy_r[16]), .Rdy_r_17(Rdy_r[17]), .Rdy_r_18(Rdy_r[18]), .Rdy_r_19(Rdy_r[19]), .Rdy_r_20(Rdy_r[20]), .Rdy_r_21(Rdy_r[21]), .Rdy_r_22(Rdy_r[22]), .Rdy_r_23(Rdy_r[23]), 
    
    .Rdy_f_0(Rdy_f[0]), .Rdy_f_1(Rdy_f[1]), .Rdy_f_2(Rdy_f[2]), .Rdy_f_3(Rdy_f[3]), .Rdy_f_4(Rdy_f[4]), .Rdy_f_5(Rdy_f[5]), .Rdy_f_6(Rdy_f[6]), .Rdy_f_7(Rdy_f[7]), .Rdy_f_8(Rdy_f[8]), .Rdy_f_9(Rdy_f[9]), .Rdy_f_10(Rdy_f[10]), .Rdy_f_11(Rdy_f[11]), 
    .Rdy_f_12(Rdy_f[12]), .Rdy_f_13(Rdy_f[13]), .Rdy_f_14(Rdy_f[14]), .Rdy_f_15(Rdy_f[15]), .Rdy_f_16(Rdy_f[16]), .Rdy_f_17(Rdy_f[17]), .Rdy_f_18(Rdy_f[18]), .Rdy_f_19(Rdy_f[19]), .Rdy_f_20(Rdy_f[20]), .Rdy_f_21(Rdy_f[21]), .Rdy_f_22(Rdy_f[22]), .Rdy_f_23(Rdy_f[23]),
    
    .q_r_0(q_r[0]), .q_r_1(q_r[1]), .q_r_2(q_r[2]), .q_r_3(q_r[3]), .q_r_4(q_r[4]), .q_r_5(q_r[5]), .q_r_6(q_r[6]), .q_r_7(q_r[7]), .q_r_8(q_r[8]), .q_r_9(q_r[9]), .q_r_10(q_r[10]), .q_r_11(q_r[11]), 
    .q_r_12(q_r[12]), .q_r_13(q_r[13]), .q_r_14(q_r[14]), .q_r_15(q_r[15]), .q_r_16(q_r[16]), .q_r_17(q_r[17]), .q_r_18(q_r[18]), .q_r_19(q_r[19]), .q_r_20(q_r[20]), .q_r_21(q_r[21]), .q_r_22(q_r[22]), .q_r_23(q_r[23]),
    
    .q_f_0(q_f[0]), .q_f_1(q_f[1]), .q_f_2(q_f[2]), .q_f_3(q_f[3]), .q_f_4(q_f[4]), .q_f_5(q_f[5]), .q_f_6(q_f[6]), .q_f_7(q_f[7]), .q_f_8(q_f[8]), .q_f_9(q_f[9]), .q_f_10(q_f[10]), .q_f_11(q_f[11]), 
    .q_f_12(q_f[12]), .q_f_13(q_f[13]), .q_f_14(q_f[14]), .q_f_15(q_f[15]), .q_f_16(q_f[16]), .q_f_17(q_f[17]), .q_f_18(q_f[18]), .q_f_19(q_f[19]), .q_f_20(q_f[20]), .q_f_21(q_f[21]), .q_f_22(q_f[22]), .q_f_23(q_f[23]),
    
    .cnt_r_0(cnt_r[0]), .cnt_r_1(cnt_r[1]), .cnt_r_2(cnt_r[2]), .cnt_r_3(cnt_r[3]), .cnt_r_4(cnt_r[4]), .cnt_r_5(cnt_r[5]), .cnt_r_6(cnt_r[6]), .cnt_r_7(cnt_r[7]), .cnt_r_8(cnt_r[8]), .cnt_r_9(cnt_r[9]), .cnt_r_10(cnt_r[10]), .cnt_r_11(cnt_r[11]), 
    .cnt_r_12(cnt_r[12]), .cnt_r_13(cnt_r[13]), .cnt_r_14(cnt_r[14]), .cnt_r_15(cnt_r[15]), .cnt_r_16(cnt_r[16]), .cnt_r_17(cnt_r[17]), .cnt_r_18(cnt_r[18]), .cnt_r_19(cnt_r[19]), .cnt_r_20(cnt_r[20]), .cnt_r_21(cnt_r[21]), .cnt_r_22(cnt_r[22]), .cnt_r_23(cnt_r[23]),
    
    .cnt_inv_r_0(cnt_inv_r[0]), .cnt_inv_r_1(cnt_inv_r[1]), .cnt_inv_r_2(cnt_inv_r[2]), .cnt_inv_r_3(cnt_inv_r[3]), .cnt_inv_r_4(cnt_inv_r[4]), .cnt_inv_r_5(cnt_inv_r[5]), .cnt_inv_r_6(cnt_inv_r[6]), .cnt_inv_r_7(cnt_inv_r[7]), .cnt_inv_r_8(cnt_inv_r[8]), .cnt_inv_r_9(cnt_inv_r[9]), .cnt_inv_r_10(cnt_inv_r[10]), .cnt_inv_r_11(cnt_inv_r[11]), 
    .cnt_inv_r_12(cnt_inv_r[12]), .cnt_inv_r_13(cnt_inv_r[13]), .cnt_inv_r_14(cnt_inv_r[14]), .cnt_inv_r_15(cnt_inv_r[15]), .cnt_inv_r_16(cnt_inv_r[16]), .cnt_inv_r_17(cnt_inv_r[17]), .cnt_inv_r_18(cnt_inv_r[18]), .cnt_inv_r_19(cnt_inv_r[19]), .cnt_inv_r_20(cnt_inv_r[20]), .cnt_inv_r_21(cnt_inv_r[21]), .cnt_inv_r_22(cnt_inv_r[22]), .cnt_inv_r_23(cnt_inv_r[23]),

    .cnt_f_0(cnt_f[0]), .cnt_f_1(cnt_f[1]), .cnt_f_2(cnt_f[2]), .cnt_f_3(cnt_f[3]), .cnt_f_4(cnt_f[4]), .cnt_f_5(cnt_f[5]), .cnt_f_6(cnt_f[6]), .cnt_f_7(cnt_f[7]), .cnt_f_8(cnt_f[8]), .cnt_f_9(cnt_f[9]), .cnt_f_10(cnt_f[10]), .cnt_f_11(cnt_f[11]), 
    .cnt_f_12(cnt_f[12]), .cnt_f_13(cnt_f[13]), .cnt_f_14(cnt_f[14]), .cnt_f_15(cnt_f[15]), .cnt_f_16(cnt_f[16]), .cnt_f_17(cnt_f[17]), .cnt_f_18(cnt_f[18]), .cnt_f_19(cnt_f[19]), .cnt_f_20(cnt_f[20]), .cnt_f_21(cnt_f[21]), .cnt_f_22(cnt_f[22]), .cnt_f_23(cnt_f[23]),

    .cnt_inv_f_0(cnt_inv_f[0]), .cnt_inv_f_1(cnt_inv_f[1]), .cnt_inv_f_2(cnt_inv_f[2]), .cnt_inv_f_3(cnt_inv_f[3]), .cnt_inv_f_4(cnt_inv_f[4]), .cnt_inv_f_5(cnt_inv_f[5]), .cnt_inv_f_6(cnt_inv_f[6]), .cnt_inv_f_7(cnt_inv_f[7]), .cnt_inv_f_8(cnt_inv_f[8]), .cnt_inv_f_9(cnt_inv_f[9]), .cnt_inv_f_10(cnt_inv_f[10]), .cnt_inv_f_11(cnt_inv_f[11]), 
    .cnt_inv_f_12(cnt_inv_f[12]), .cnt_inv_f_13(cnt_inv_f[13]), .cnt_inv_f_14(cnt_inv_f[14]), .cnt_inv_f_15(cnt_inv_f[15]), .cnt_inv_f_16(cnt_inv_f[16]), .cnt_inv_f_17(cnt_inv_f[17]), .cnt_inv_f_18(cnt_inv_f[18]), .cnt_inv_f_19(cnt_inv_f[19]), .cnt_inv_f_20(cnt_inv_f[20]), .cnt_inv_f_21(cnt_inv_f[21]), .cnt_inv_f_22(cnt_inv_f[22]), .cnt_inv_f_23(cnt_inv_f[23]),
    

    .coarse_bcr(coarse_bcr),

    .d_line(d_line),    

    .debug_input(debug_input),
    
    .phase_clk160(phase_clk160),
    .phase_clk320_0(phase_clk320_0), 
    .phase_clk320_1(phase_clk320_1), 
    .phase_clk320_2(phase_clk320_2),
    .rst_ePLL(reset),    
    .ePllResA(ePllResA), .ePllResB(ePllResB), .ePllResC(ePllResC),
    .ePllIcpA(ePllIcpA), .ePllIcpB(ePllIcpB), .ePllIcpC(ePllIcpC),
    .ePllCapA(ePllCapA), .ePllCapB(ePllCapB), .ePllCapC(ePllCapC),    
    .ePll_lock(ePll_lock)
);




//io_0 Jtag_instruction (
// .clk(clk40_pre),                // input wire clk
// .probe_in0(tdc_logic_DUT.tdc_logic_inst.jtag_controller_inst.ir_update_reg)   // input wire [4 : 0] probe_in0
//;


endmodule


#for VC707
set_property CFGBVS GND [current_design]
set_property CONFIG_VOLTAGE 1.8 [current_design]

#system clock
set_property IOSTANDARD LVDS [get_ports clk_p]
set_property PACKAGE_PIN E19 [get_ports clk_p]
set_property PACKAGE_PIN E18 [get_ports clk_n]
set_property IOSTANDARD LVDS [get_ports clk_n]

#clock to KC705
set_property IOSTANDARD LVDS [get_ports clk_output_p]
set_property PACKAGE_PIN K39 [get_ports clk_output_p]
set_property PACKAGE_PIN K40 [get_ports clk_output_n]
set_property IOSTANDARD LVDS [get_ports clk_output_n]


#============================================== JTAG ==============================================
#tck FMC K16
set_property PACKAGE_PIN C35 [get_ports tck]
set_property IOSTANDARD LVCMOS18 [get_ports tck]


#tms FMC K13
set_property PACKAGE_PIN H38 [get_ports tms]
set_property IOSTANDARD LVCMOS18 [get_ports tms]

#tdi FMC K10
set_property PACKAGE_PIN G36 [get_ports tdi]
set_property IOSTANDARD LVCMOS18 [get_ports tdi]

#trst FMC K7
set_property PACKAGE_PIN E33 [get_ports trst]
set_property IOSTANDARD LVCMOS18 [get_ports trst]

#tdo FMC K19
set_property PACKAGE_PIN D37 [get_ports tdo]
set_property IOSTANDARD LVCMOS18 [get_ports tdo]

#============================================== control ==============================================
#encoded_control_in
set_property IOSTANDARD LVDS [get_ports encoded_control_in_p]
set_property PACKAGE_PIN P41 [get_ports encoded_control_in_p]
set_property PACKAGE_PIN N41 [get_ports encoded_control_in_n]
set_property IOSTANDARD LVDS [get_ports encoded_control_in_n]

#reset_in
set_property PACKAGE_PIN H40 [get_ports reset_in]
set_property IOSTANDARD LVCMOS18 [get_ports reset_in]

#trigger_direct_in
set_property PACKAGE_PIN G41 [get_ports trigger_direct_in]
set_property IOSTANDARD LVCMOS18 [get_ports trigger_direct_in]

#bunch_reset_direct_in
set_property PACKAGE_PIN F40 [get_ports bunch_reset_direct_in]
set_property IOSTANDARD LVCMOS18 [get_ports bunch_reset_direct_in]

#event_reset_direct_in
set_property PACKAGE_PIN M36 [get_ports event_reset_direct_in]
set_property IOSTANDARD LVCMOS18 [get_ports event_reset_direct_in]

#=========================================== output  d_line ======================================

#d_line[0]
set_property IOSTANDARD LVDS [get_ports {d_line_p[0]}]
set_property PACKAGE_PIN V30 [get_ports {d_line_p[0]}]
set_property PACKAGE_PIN V31 [get_ports {d_line_n[0]}]
set_property IOSTANDARD LVDS [get_ports {d_line_n[0]}]

#d_line[1]
set_property IOSTANDARD LVDS [get_ports {d_line_p[1]}]
set_property PACKAGE_PIN V29 [get_ports {d_line_p[1]}]
set_property PACKAGE_PIN U29 [get_ports {d_line_n[1]}]
set_property IOSTANDARD LVDS [get_ports {d_line_n[1]}]

#=========================================== debug ======================================

#d_line[0]
set_property IOSTANDARD LVDS [get_ports debug_output_p]
set_property PACKAGE_PIN AN31 [get_ports debug_output_p]
set_property PACKAGE_PIN AP31 [get_ports debug_output_n]
set_property IOSTANDARD LVDS [get_ports debug_output_n]


#============================================ Hit ================================================
#hit0
set_property IOSTANDARD LVDS [get_ports {hit_p[0]}]
set_property PACKAGE_PIN K42 [get_ports {hit_p[0]}]
set_property PACKAGE_PIN J42 [get_ports {hit_n[0]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[0]}]

#hit1
set_property IOSTANDARD LVDS [get_ports {hit_p[1]}]
set_property PACKAGE_PIN N38 [get_ports {hit_p[1]}]
set_property PACKAGE_PIN M39 [get_ports {hit_n[1]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[1]}]

#hit2
set_property IOSTANDARD LVDS [get_ports {hit_p[2]}]
set_property PACKAGE_PIN N39 [get_ports {hit_p[2]}]
set_property PACKAGE_PIN N40 [get_ports {hit_n[2]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[2]}]

#hit3
set_property IOSTANDARD LVDS [get_ports {hit_p[3]}]
set_property PACKAGE_PIN M32 [get_ports {hit_p[3]}]
set_property PACKAGE_PIN L32 [get_ports {hit_n[3]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[3]}]

#hit4
set_property IOSTANDARD LVDS [get_ports {hit_p[4]}]
set_property PACKAGE_PIN J31 [get_ports {hit_p[4]}]
set_property PACKAGE_PIN H31 [get_ports {hit_n[4]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[4]}]

#hit5
set_property IOSTANDARD LVDS [get_ports {hit_p[5]}]
set_property PACKAGE_PIN J40 [get_ports {hit_p[5]}]
set_property PACKAGE_PIN J41 [get_ports {hit_n[5]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[5]}]

#hit6
set_property IOSTANDARD LVDS [get_ports {hit_p[6]}]
set_property PACKAGE_PIN M41 [get_ports {hit_p[6]}]
set_property PACKAGE_PIN L41 [get_ports {hit_n[6]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[6]}]

#hit7
set_property IOSTANDARD LVDS [get_ports {hit_p[7]}]
set_property PACKAGE_PIN R42 [get_ports {hit_p[7]}]
set_property PACKAGE_PIN P42 [get_ports {hit_n[7]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[7]}]

#hit8
set_property IOSTANDARD LVDS [get_ports {hit_p[8]}]
set_property PACKAGE_PIN H39 [get_ports {hit_p[8]}]
set_property PACKAGE_PIN G39 [get_ports {hit_n[8]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[8]}]

#hit9
set_property IOSTANDARD LVDS [get_ports {hit_p[9]}]
set_property PACKAGE_PIN L31 [get_ports {hit_p[9]}]
set_property PACKAGE_PIN K32 [get_ports {hit_n[9]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[9]}]

#hit10
set_property IOSTANDARD LVDS [get_ports {hit_p[10]}]
set_property PACKAGE_PIN P30 [get_ports {hit_p[10]}]
set_property PACKAGE_PIN N31 [get_ports {hit_n[10]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[10]}]

#hit11
set_property IOSTANDARD LVDS [get_ports {hit_p[11]}]
set_property PACKAGE_PIN J30 [get_ports {hit_p[11]}]
set_property PACKAGE_PIN H30 [get_ports {hit_n[11]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[11]}]

#hit12
set_property IOSTANDARD LVDS [get_ports {hit_p[12]}]
set_property PACKAGE_PIN D35 [get_ports {hit_p[12]}]
set_property PACKAGE_PIN D36 [get_ports {hit_n[12]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[12]}]

#hit13
set_property IOSTANDARD LVDS [get_ports {hit_p[13]}]
set_property PACKAGE_PIN G32 [get_ports {hit_p[13]}]
set_property PACKAGE_PIN F32 [get_ports {hit_n[13]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[13]}]

#hit14
set_property IOSTANDARD LVDS [get_ports {hit_p[14]}]
set_property PACKAGE_PIN E32 [get_ports {hit_p[14]}]
set_property PACKAGE_PIN D32 [get_ports {hit_n[14]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[14]}]

#hit15
set_property IOSTANDARD LVDS [get_ports {hit_p[15]}]
set_property PACKAGE_PIN B36 [get_ports {hit_p[15]}]
set_property PACKAGE_PIN A37 [get_ports {hit_n[15]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[15]}]

#hit16
set_property IOSTANDARD LVDS [get_ports {hit_p[16]}]
set_property PACKAGE_PIN B39 [get_ports {hit_p[16]}]
set_property PACKAGE_PIN A39 [get_ports {hit_n[16]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[16]}]

#hit17
set_property IOSTANDARD LVDS [get_ports {hit_p[17]}]
set_property PACKAGE_PIN B34 [get_ports {hit_p[17]}]
set_property PACKAGE_PIN A34 [get_ports {hit_n[17]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[17]}]

#hit18
set_property IOSTANDARD LVDS [get_ports {hit_p[18]}]
set_property PACKAGE_PIN E34 [get_ports {hit_p[18]}]
set_property PACKAGE_PIN E35 [get_ports {hit_n[18]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[18]}]

#hit19
set_property IOSTANDARD LVDS [get_ports {hit_p[19]}]
set_property PACKAGE_PIN F34 [get_ports {hit_p[19]}]
set_property PACKAGE_PIN F35 [get_ports {hit_n[19]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[19]}]

#hit20
set_property IOSTANDARD LVDS [get_ports {hit_p[20]}]
set_property PACKAGE_PIN J36 [get_ports {hit_p[20]}]
set_property PACKAGE_PIN H36 [get_ports {hit_n[20]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[20]}]

#hit21
set_property IOSTANDARD LVDS [get_ports {hit_p[21]}]
set_property PACKAGE_PIN B37 [get_ports {hit_p[21]}]
set_property PACKAGE_PIN B38 [get_ports {hit_n[21]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[21]}]

#hit22
set_property IOSTANDARD LVDS [get_ports {hit_p[22]}]
set_property PACKAGE_PIN C33 [get_ports {hit_p[22]}]
set_property PACKAGE_PIN C34 [get_ports {hit_n[22]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[22]}]

#hit23
set_property IOSTANDARD LVDS [get_ports {hit_p[23]}]
set_property PACKAGE_PIN B32 [get_ports {hit_p[23]}]
set_property PACKAGE_PIN B33 [get_ports {hit_n[23]}]
set_property IOSTANDARD LVDS [get_ports {hit_n[23]}]

#============================================ ePLL config ================================================
set_property PACKAGE_PIN M42 [get_ports {phase_clk160[0]}]
set_property PACKAGE_PIN L42 [get_ports {phase_clk160[1]}]
set_property PACKAGE_PIN M37 [get_ports {phase_clk160[2]}]
set_property PACKAGE_PIN M38 [get_ports {phase_clk160[3]}]
set_property PACKAGE_PIN R40 [get_ports {phase_clk160[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports phase_clk160*]


set_property PACKAGE_PIN P40 [get_ports {phase_clk320_0[0]}]
set_property PACKAGE_PIN K37 [get_ports {phase_clk320_0[1]}]
set_property PACKAGE_PIN K38 [get_ports {phase_clk320_0[2]}]
set_property PACKAGE_PIN Y29 [get_ports {phase_clk320_0[3]}]

set_property PACKAGE_PIN Y30 [get_ports {phase_clk320_1[0]}]
set_property PACKAGE_PIN R28 [get_ports {phase_clk320_1[1]}]
set_property PACKAGE_PIN P28 [get_ports {phase_clk320_1[2]}]
set_property PACKAGE_PIN K29 [get_ports {phase_clk320_1[3]}]

set_property PACKAGE_PIN K30 [get_ports {phase_clk320_2[0]}]
set_property PACKAGE_PIN T29 [get_ports {phase_clk320_2[1]}]
set_property PACKAGE_PIN T30 [get_ports {phase_clk320_2[2]}]
set_property PACKAGE_PIN M28 [get_ports {phase_clk320_2[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports phase_clk320*]




set_property PACKAGE_PIN M29 [get_ports {ePllResA[0]}]
set_property PACKAGE_PIN U31 [get_ports {ePllResA[1]}]
set_property PACKAGE_PIN T31 [get_ports {ePllResA[2]}]
set_property PACKAGE_PIN H33 [get_ports {ePllResA[3]}]

set_property PACKAGE_PIN G33 [get_ports {ePllIcpA[0]}]
set_property PACKAGE_PIN C38 [get_ports {ePllIcpA[1]}]
set_property PACKAGE_PIN C39 [get_ports {ePllIcpA[2]}]
set_property PACKAGE_PIN J37 [get_ports {ePllIcpA[3]}]

set_property PACKAGE_PIN J38 [get_ports {ePllCapA[0]}]
set_property PACKAGE_PIN E37 [get_ports {ePllCapA[1]}]



set_property PACKAGE_PIN AA29 [get_ports {ePllResB[0]}]
set_property PACKAGE_PIN AA30 [get_ports {ePllResB[1]}]
set_property PACKAGE_PIN AC31 [get_ports {ePllResB[2]}]
set_property PACKAGE_PIN AD31 [get_ports {ePllResB[3]}]

set_property PACKAGE_PIN AE34 [get_ports {ePllIcpB[0]}]
set_property PACKAGE_PIN AE35 [get_ports {ePllIcpB[1]}]
set_property PACKAGE_PIN AF35 [get_ports {ePllIcpB[2]}]
set_property PACKAGE_PIN AF36 [get_ports {ePllIcpB[3]}]

set_property PACKAGE_PIN AB36 [get_ports {ePllCapB[0]}]
set_property PACKAGE_PIN AB37 [get_ports {ePllCapB[1]}]


set_property PACKAGE_PIN AD32 [get_ports {ePllResC[0]}]
set_property PACKAGE_PIN AD33 [get_ports {ePllResC[1]}]
set_property PACKAGE_PIN Y32 [get_ports {ePllResC[2]}]
set_property PACKAGE_PIN Y33 [get_ports {ePllResC[3]}]

set_property PACKAGE_PIN AE29 [get_ports {ePllIcpC[0]}]
set_property PACKAGE_PIN AE30 [get_ports {ePllIcpC[1]}]
set_property PACKAGE_PIN AE32 [get_ports {ePllIcpC[2]}]
set_property PACKAGE_PIN AE33 [get_ports {ePllIcpC[3]}]

set_property PACKAGE_PIN AG36 [get_ports {ePllCapC[0]}]
set_property PACKAGE_PIN AH36 [get_ports {ePllCapC[1]}]

set_property IOSTANDARD LVCMOS18 [get_ports ePllRes*]
set_property IOSTANDARD LVCMOS18 [get_ports ePllIcp*]
set_property IOSTANDARD LVCMOS18 [get_ports ePllCap*]


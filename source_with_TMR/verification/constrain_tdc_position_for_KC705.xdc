#for KC705
set_property CFGBVS VCCO [current_design]
set_property CONFIG_VOLTAGE 2.5 [current_design]

#system clock
set_property IOSTANDARD LVDS [get_ports clk_p]
set_property PACKAGE_PIN AD12 [get_ports clk_p]
set_property PACKAGE_PIN AD11 [get_ports clk_n]
set_property IOSTANDARD LVDS [get_ports clk_n]

#clock to test_env
set_property IOSTANDARD LVDS_25 [get_ports clk_output_p]
set_property PACKAGE_PIN D17 [get_ports clk_output_p]
set_property PACKAGE_PIN D18 [get_ports clk_output_n]
set_property IOSTANDARD LVDS_25 [get_ports clk_output_n]

#============================================== JTAG ==============================================
#tck FMC J13
set_property PACKAGE_PIN A13 [get_ports tck]
set_property IOSTANDARD LVCMOS25 [get_ports tck]


#tms FMC K14
set_property PACKAGE_PIN A12 [get_ports tms]
set_property IOSTANDARD LVCMOS25 [get_ports tms]

#tdi FMC J10
set_property PACKAGE_PIN A15 [get_ports tdi]
set_property IOSTANDARD LVCMOS25 [get_ports tdi]

#trst FMC K8
set_property PACKAGE_PIN C11 [get_ports trst]
set_property IOSTANDARD LVCMOS25 [get_ports trst]

#tdo FMC K11
set_property PACKAGE_PIN C14 [get_ports tdo]
set_property IOSTANDARD LVCMOS25 [get_ports tdo]

#============================================== control ==============================================
#encoded_control_in
set_property IOSTANDARD LVDS_25 [get_ports encoded_control_in_p]
set_property PACKAGE_PIN G13 [get_ports encoded_control_in_p]
set_property PACKAGE_PIN F13 [get_ports encoded_control_in_n]
set_property IOSTANDARD LVDS_25 [get_ports encoded_control_in_n]
set_property DIFF_TERM TRUE [get_ports encoded_control_in_p]
set_property DIFF_TERM TRUE [get_ports encoded_control_in_n]

#reset_in
set_property PACKAGE_PIN B12 [get_ports reset_in]
set_property IOSTANDARD LVCMOS25 [get_ports reset_in]

##trigger_direct_in
#set_property PACKAGE_PIN E28 [get_ports trigger_direct_in]
#set_property IOSTANDARD LVCMOS25 [get_ports trigger_direct_in]

#bunch_reset_direct_in
set_property PACKAGE_PIN G27 [get_ports bunch_reset_direct_in]
set_property IOSTANDARD LVCMOS25 [get_ports bunch_reset_direct_in]

##event_reset_direct_in
#set_property PACKAGE_PIN C24 [get_ports event_reset_direct_in]
#set_property IOSTANDARD LVCMOS25 [get_ports event_reset_direct_in]

#=========================================== output  d_line ======================================

#d_line[0]
set_property IOSTANDARD LVDS_25 [get_ports {d_line_p[0]}]
set_property PACKAGE_PIN F21 [get_ports {d_line_p[0]}]
set_property PACKAGE_PIN E21 [get_ports {d_line_n[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {d_line_n[0]}]
set_property DIFF_TERM TRUE [get_ports {d_line_p[0]}]
set_property DIFF_TERM TRUE [get_ports {d_line_n[0]}]

#d_line[1]
set_property IOSTANDARD LVDS_25 [get_ports {d_line_p[1]}]
set_property PACKAGE_PIN F20 [get_ports {d_line_p[1]}]
set_property PACKAGE_PIN E20 [get_ports {d_line_n[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {d_line_n[1]}]
set_property DIFF_TERM TRUE [get_ports {d_line_p[1]}]
set_property DIFF_TERM TRUE [get_ports {d_line_n[1]}]


#=========================================== debug ======================================

##debug
#set_property IOSTANDARD LVDS_25 [get_ports debug_output_p]
#set_property PACKAGE_PIN Y23 [get_ports debug_output_p]
#set_property PACKAGE_PIN Y24 [get_ports debug_output_n]
#set_property IOSTANDARD LVDS_25 [get_ports debug_output_n]


#============================================ Hit ================================================
set_property PACKAGE_PIN C27 [get_ports {hit[0]}]
set_property PACKAGE_PIN B25 [get_ports {hit[1]}]
set_property PACKAGE_PIN H27 [get_ports {hit[2]}]
set_property PACKAGE_PIN E30 [get_ports {hit[3]}]
set_property PACKAGE_PIN B29 [get_ports {hit[4]}]
set_property PACKAGE_PIN A27 [get_ports {hit[5]}]
set_property PACKAGE_PIN H12 [get_ports {hit[6]}]
set_property PACKAGE_PIN H25 [get_ports {hit[7]}]
set_property PACKAGE_PIN F28 [get_ports {hit[8]}]
set_property PACKAGE_PIN D28 [get_ports {hit[9]}]
set_property PACKAGE_PIN F27 [get_ports {hit[10]}]
set_property PACKAGE_PIN B24 [get_ports {hit[11]}]
set_property PACKAGE_PIN F18 [get_ports {hit[12]}]
set_property PACKAGE_PIN A21 [get_ports {hit[13]}]
set_property PACKAGE_PIN A17 [get_ports {hit[14]}]
set_property PACKAGE_PIN C16 [get_ports {hit[15]}]
set_property PACKAGE_PIN C22 [get_ports {hit[16]}]
set_property PACKAGE_PIN L13 [get_ports {hit[17]}]
set_property PACKAGE_PIN B20 [get_ports {hit[18]}]
set_property PACKAGE_PIN F17 [get_ports {hit[19]}]
set_property PACKAGE_PIN B17 [get_ports {hit[20]}]
set_property PACKAGE_PIN F22 [get_ports {hit[21]}]
set_property PACKAGE_PIN H22 [get_ports {hit[22]}]
set_property PACKAGE_PIN C21 [get_ports {hit[23]}]
set_property IOSTANDARD LVCMOS25 [get_ports hit*]

#============================================ ePLL config ================================================
set_property PACKAGE_PIN H26 [get_ports {phase_clk160[0]}]
set_property PACKAGE_PIN B18 [get_ports {phase_clk160[1]}]
set_property PACKAGE_PIN E29 [get_ports {phase_clk160[2]}]
set_property PACKAGE_PIN A18 [get_ports {phase_clk160[3]}]
set_property PACKAGE_PIN C29 [get_ports {phase_clk160[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports phase_clk160*]


set_property PACKAGE_PIN H14 [get_ports {phase_clk320_0[0]}]
set_property PACKAGE_PIN B27 [get_ports {phase_clk320_0[1]}]
set_property PACKAGE_PIN G14 [get_ports {phase_clk320_0[2]}]
set_property PACKAGE_PIN E19 [get_ports {phase_clk320_0[3]}]

set_property PACKAGE_PIN D19 [get_ports {phase_clk320_1[0]}]
set_property PACKAGE_PIN C20 [get_ports {phase_clk320_1[1]}]
set_property PACKAGE_PIN A25 [get_ports {phase_clk320_1[2]}]
set_property PACKAGE_PIN G17 [get_ports {phase_clk320_1[3]}]

set_property PACKAGE_PIN A26 [get_ports {phase_clk320_2[0]}]
set_property PACKAGE_PIN C17 [get_ports {phase_clk320_2[1]}]
set_property PACKAGE_PIN B22 [get_ports {phase_clk320_2[2]}]
set_property PACKAGE_PIN G22 [get_ports {phase_clk320_2[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports phase_clk320*]



set_property PACKAGE_PIN F15 [get_ports {ePllResA[0]}]
set_property PACKAGE_PIN H21 [get_ports {ePllResA[1]}]
set_property PACKAGE_PIN E16 [get_ports {ePllResA[2]}]
set_property PACKAGE_PIN C12 [get_ports {ePllResA[3]}]

set_property PACKAGE_PIN A28 [get_ports {ePllIcpA[0]}]
set_property PACKAGE_PIN B14 [get_ports {ePllIcpA[1]}]
set_property PACKAGE_PIN B28 [get_ports {ePllIcpA[2]}]
set_property PACKAGE_PIN B13 [get_ports {ePllIcpA[3]}]

set_property PACKAGE_PIN C19 [get_ports {ePllCapA[0]}]
set_property PACKAGE_PIN J16 [get_ports {ePllCapA[1]}]
#
#

set_property PACKAGE_PIN AG20  [get_ports {ePllResB[0]}]
set_property PACKAGE_PIN AH20  [get_ports {ePllResB[1]}]
set_property PACKAGE_PIN AJ22  [get_ports {ePllResB[2]}]
set_property PACKAGE_PIN AJ23  [get_ports {ePllResB[3]}]

set_property PACKAGE_PIN AA20  [get_ports {ePllIcpB[0]}]
set_property PACKAGE_PIN AB20  [get_ports {ePllIcpB[1]}]
set_property PACKAGE_PIN AC22  [get_ports {ePllIcpB[2]}]
set_property PACKAGE_PIN AD22  [get_ports {ePllIcpB[3]}]

set_property PACKAGE_PIN AF26  [get_ports {ePllCapB[0]}]
set_property PACKAGE_PIN AF27  [get_ports {ePllCapB[1]}]


set_property PACKAGE_PIN AJ27  [get_ports {ePllResC[0]}]
set_property PACKAGE_PIN AK28  [get_ports {ePllResC[1]}]
set_property PACKAGE_PIN AC26  [get_ports {ePllResC[2]}]
set_property PACKAGE_PIN AD26  [get_ports {ePllResC[3]}]

set_property PACKAGE_PIN AE28  [get_ports {ePllIcpC[0]}]
set_property PACKAGE_PIN AF28  [get_ports {ePllIcpC[1]}]
set_property PACKAGE_PIN AD29  [get_ports {ePllIcpC[2]}]
set_property PACKAGE_PIN AE29  [get_ports {ePllIcpC[3]}]

set_property PACKAGE_PIN AC29  [get_ports {ePllCapC[0]}]
set_property PACKAGE_PIN AC30  [get_ports {ePllCapC[1]}]
#
set_property IOSTANDARD LVCMOS25 [get_ports ePllRes*]
set_property IOSTANDARD LVCMOS25 [get_ports ePllIcp*]
set_property IOSTANDARD LVCMOS25 [get_ports ePllCap*]

set_property PACKAGE_PIN C26 [get_ports ASD_TCK]
set_property IOSTANDARD LVCMOS25 [get_ports ASD_TCK]
set_property PACKAGE_PIN F30 [get_ports ASD_shift_out]
set_property IOSTANDARD LVCMOS25 [get_ports ASD_shift_out]
set_property PACKAGE_PIN G30 [get_ports ASD_update_out]
set_property IOSTANDARD LVCMOS25 [get_ports ASD_update_out]
set_property PACKAGE_PIN A30 [get_ports ASD_data_out]
set_property IOSTANDARD LVCMOS25 [get_ports ASD_data_out]
set_property PACKAGE_PIN C30 [get_ports ASD_data_in]
set_property IOSTANDARD LVCMOS25 [get_ports ASD_data_in]


set_property PACKAGE_PIN L12 [get_ports {debug_input[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {debug_input[0]}]
set_property PACKAGE_PIN B19 [get_ports {debug_input[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {debug_input[1]}]
set_property PACKAGE_PIN AE30 [get_ports {debug_input[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {debug_input[2]}]
set_property PACKAGE_PIN AF30 [get_ports {debug_input[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {debug_input[3]}]
set_property PACKAGE_PIN AB29 [get_ports {debug_input[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {debug_input[4]}]
set_property PACKAGE_PIN AB30 [get_ports {debug_input[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {debug_input[5]}]
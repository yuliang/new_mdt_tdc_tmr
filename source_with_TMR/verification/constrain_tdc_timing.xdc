create_clock -period 25.000 -name tck -waveform {0.000 12.500} [get_ports tck]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets tck]

set_false_path -to [get_pins -hier -filter {NAME =~ */*interface_fifo*/*rptr_empty*/rempty*_reg/D}]

# set false path tck to/from all clk
set_false_path -from [get_clocks tck] -to [get_clocks clk_out1_input_clock_TMR]
set_false_path -from [get_clocks tck] -to [get_clocks clk_out2_input_clock_TMR]
set_false_path -from [get_clocks tck] -to [get_clocks clk_out3_input_clock_TMR]

set_false_path -from [get_clocks tck] -to [get_clocks clk_out1_master_clock_TMR]
set_false_path -from [get_clocks tck] -to [get_clocks clk_out2_master_clock_TMR]
set_false_path -from [get_clocks tck] -to [get_clocks clk_out3_master_clock_TMR]
set_false_path -from [get_clocks tck] -to [get_clocks clk_out4_master_clock_TMR]
set_false_path -from [get_clocks tck] -to [get_clocks clk_out5_master_clock_TMR]
set_false_path -from [get_clocks tck] -to [get_clocks clk_out6_master_clock_TMR]

set_false_path -from [get_clocks clk_out1_input_clock_TMR] -to [get_clocks tck]
set_false_path -from [get_clocks clk_out2_input_clock_TMR] -to [get_clocks tck]
set_false_path -from [get_clocks clk_out3_input_clock_TMR] -to [get_clocks tck]

set_false_path -from [get_clocks clk_out1_master_clock_TMR] -to [get_clocks tck]
set_false_path -from [get_clocks clk_out2_master_clock_TMR] -to [get_clocks tck]
set_false_path -from [get_clocks clk_out3_master_clock_TMR] -to [get_clocks tck]
set_false_path -from [get_clocks clk_out4_master_clock_TMR] -to [get_clocks tck]
set_false_path -from [get_clocks clk_out5_master_clock_TMR] -to [get_clocks tck]
set_false_path -from [get_clocks clk_out6_master_clock_TMR] -to [get_clocks tck]

# set false path clk40 to/from clk160
set_false_path -from [get_clocks clk_out1_input_clock_TMR] -to [get_clocks clk_out1_master_clock_TMR]
set_false_path -from [get_clocks clk_out1_input_clock_TMR] -to [get_clocks clk_out2_master_clock_TMR]
set_false_path -from [get_clocks clk_out1_input_clock_TMR] -to [get_clocks clk_out3_master_clock_TMR]
set_false_path -from [get_clocks clk_out2_input_clock_TMR] -to [get_clocks clk_out1_master_clock_TMR]
set_false_path -from [get_clocks clk_out2_input_clock_TMR] -to [get_clocks clk_out2_master_clock_TMR]
set_false_path -from [get_clocks clk_out2_input_clock_TMR] -to [get_clocks clk_out3_master_clock_TMR]
set_false_path -from [get_clocks clk_out3_input_clock_TMR] -to [get_clocks clk_out1_master_clock_TMR]
set_false_path -from [get_clocks clk_out3_input_clock_TMR] -to [get_clocks clk_out2_master_clock_TMR]
set_false_path -from [get_clocks clk_out3_input_clock_TMR] -to [get_clocks clk_out3_master_clock_TMR]

set_false_path -from [get_clocks clk_out1_master_clock_TMR] -to [get_clocks clk_out1_input_clock_TMR]
set_false_path -from [get_clocks clk_out1_master_clock_TMR] -to [get_clocks clk_out2_input_clock_TMR]
set_false_path -from [get_clocks clk_out1_master_clock_TMR] -to [get_clocks clk_out3_input_clock_TMR]
set_false_path -from [get_clocks clk_out2_master_clock_TMR] -to [get_clocks clk_out1_input_clock_TMR]
set_false_path -from [get_clocks clk_out2_master_clock_TMR] -to [get_clocks clk_out2_input_clock_TMR]
set_false_path -from [get_clocks clk_out2_master_clock_TMR] -to [get_clocks clk_out3_input_clock_TMR]
set_false_path -from [get_clocks clk_out3_master_clock_TMR] -to [get_clocks clk_out1_input_clock_TMR]
set_false_path -from [get_clocks clk_out3_master_clock_TMR] -to [get_clocks clk_out2_input_clock_TMR]
set_false_path -from [get_clocks clk_out3_master_clock_TMR] -to [get_clocks clk_out3_input_clock_TMR]

# set false path clk40 to/from clk320
set_false_path -from [get_clocks clk_out1_input_clock_TMR] -to [get_clocks clk_out4_master_clock_TMR]
set_false_path -from [get_clocks clk_out1_input_clock_TMR] -to [get_clocks clk_out5_master_clock_TMR]
set_false_path -from [get_clocks clk_out1_input_clock_TMR] -to [get_clocks clk_out6_master_clock_TMR]
set_false_path -from [get_clocks clk_out2_input_clock_TMR] -to [get_clocks clk_out4_master_clock_TMR]
set_false_path -from [get_clocks clk_out2_input_clock_TMR] -to [get_clocks clk_out5_master_clock_TMR]
set_false_path -from [get_clocks clk_out2_input_clock_TMR] -to [get_clocks clk_out6_master_clock_TMR]
set_false_path -from [get_clocks clk_out3_input_clock_TMR] -to [get_clocks clk_out4_master_clock_TMR]
set_false_path -from [get_clocks clk_out3_input_clock_TMR] -to [get_clocks clk_out5_master_clock_TMR]
set_false_path -from [get_clocks clk_out3_input_clock_TMR] -to [get_clocks clk_out6_master_clock_TMR]

set_false_path -from [get_clocks clk_out4_master_clock_TMR] -to [get_clocks clk_out1_input_clock_TMR]
set_false_path -from [get_clocks clk_out4_master_clock_TMR] -to [get_clocks clk_out2_input_clock_TMR]
set_false_path -from [get_clocks clk_out4_master_clock_TMR] -to [get_clocks clk_out3_input_clock_TMR]
set_false_path -from [get_clocks clk_out5_master_clock_TMR] -to [get_clocks clk_out1_input_clock_TMR]
set_false_path -from [get_clocks clk_out5_master_clock_TMR] -to [get_clocks clk_out2_input_clock_TMR]
set_false_path -from [get_clocks clk_out5_master_clock_TMR] -to [get_clocks clk_out3_input_clock_TMR]
set_false_path -from [get_clocks clk_out6_master_clock_TMR] -to [get_clocks clk_out1_input_clock_TMR]
set_false_path -from [get_clocks clk_out6_master_clock_TMR] -to [get_clocks clk_out2_input_clock_TMR]
set_false_path -from [get_clocks clk_out6_master_clock_TMR] -to [get_clocks clk_out3_input_clock_TMR]


# set false path clk40C to/from all
set_false_path -from [get_clocks clk_out2_input_clock_TMR] 
set_false_path -to [get_clocks clk_out2_input_clock_TMR]



# set false path clk320C to/from all
set_false_path -from [get_clocks clk_out2_master_clock_TMR] 
set_false_path -to [get_clocks clk_out2_master_clock_TMR]

# set false path clk160C to/from all
set_false_path -from [get_clocks clk_out5_master_clock_TMR] 
set_false_path -to [get_clocks clk_out5_master_clock_TMR]


# # set false path clk40C to/from all
# set_false_path -from [get_clocks clk_out3_input_clock_TMR] 
# set_false_path -to [get_clocks clk_out3_input_clock_TMR]



# # set false path clk320C to/from all
# set_false_path -from [get_clocks clk_out3_master_clock_TMR] 
# set_false_path -to [get_clocks clk_out3_master_clock_TMR]

# # set false path clk160C to/from all
# set_false_path -from [get_clocks clk_out6_master_clock_TMR] 
# set_false_path -to [get_clocks clk_out6_master_clock_TMR]

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/01/2018 10:49:31 AM
// Design Name: 
// Module Name: input_sample_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////



module input_sample_tb(
input clk_p,
input clk_n,
input hit,
output ready_r,
output [3:0] q_r,
output [14:0] coarse_counter_r,
output [14:0] coarse_counter_r_inv,
output ready_f,
output [3:0] q_f,
output [14:0] coarse_counter_f,
output [14:0] coarse_counter_f_inv
    );
//// reg clk_p;
//// wire clk_n;
//// reg hit;   
//// always begin
//// clk_p=1'b0;
//// #2.5;
//// clk_p=1'b1;
//// #2.5;
//// end
////assign clk_n =  ~clk_p;
//// initial begin
////    hit=1'b0;
////    #15000;
////    #1.6;
////    hit = 1'b1;
////    #50;
////    hit =1'b0;
////    #31250;
////    hit = 1'b1;
////    #50;
////    hit =1'b0;
//// end


  wire clk320_0degree,clk320_90degree;
  wire clk160,clk40;

  master_clk_generator 
    master_clk_generator_inst(
     // Clock in ports
      .clk_in1_p(clk_p),    // input clk_in1_p
      .clk_in1_n(clk_n),    // input clk_in1_n
      // Clock out ports
      .clk_out1(clk320_0degree),     // output clk_out1
      .clk_out2(clk320_90degree),     // output clk_out2
      .clk_out3(clk160),     // output clk_out3
      .clk_out4(clk40));    // output clk_out4    
  //wire ready_r,ready_f;
  //wire [3:0] q_r,q_f;
  //wire [14:0] coarse_counter_r,coarse_counter_r_inv,coarse_counter_f,coarse_counter_f_inv;
  wire hit_i;
  wire [14:0] coarse_counter;
  reg bcr;
  input_sample
    input_sample_inst(
      .hit(hit),
      .clk(clk320_0degree),
      .coarse_data(coarse_counter),
      .hit_out(hit_i),
      .ready_r_out(ready_r),
      .q_r_out(q_r),
      .coarse_counter_r_out(coarse_counter_r),
      .coarse_counter_r_inv_out(coarse_counter_r_inv),
      .ready_f_out(ready_f),
      .q_f_out(q_f),
      .coarse_counter_f_out(coarse_counter_f),
      .coarse_counter_f_inv_out(coarse_counter_f_inv)
    );
   
tdc_coarse
tdc_coarse_inst
     ( .coarse_time(coarse_counter),// coarse_time_inv,
       .clk320(~clk320_0degree), 
       .LHC_BC_rst(bcr)
    );
reg [11:0] bucnh_count=12'b0;
always @(posedge clk40) begin
  if (bucnh_count==12'd300) begin
    bucnh_count <= 12'b0;
  end else begin
    bucnh_count <= bucnh_count + 12'b1;
    end
end

always @(posedge clk40) begin
    bcr = bucnh_count==12'd300;
end

endmodule

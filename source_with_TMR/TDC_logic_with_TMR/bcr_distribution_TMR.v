/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : bcr_distribution_TMR.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 21st, 2018
//  Note       : 
//     
`include "common_definition.v"
module bcr_distribution_TMR
(
	input clk160_A,clk160_B,clk160_C,
	//input clk320,
	input rst,

	input bcr,
	input [11:0] roll_over,
	input [11:0] coarse_count_offset,
	//input [11:0] bunch_offset,

	input auto_roll_over,
	input bypass_bcr_distribution,

	output  coarse_bcr,
	output  trigger_count_bcr
);

reg [2:0] bcr_r_A,bcr_r_B,bcr_r_C;
wire [2:0] bcr_r_vote;
wire bcr_inner;
assign  bcr_inner= (~bcr_r_vote[2])&bcr_r_vote[1];
always @(posedge clk160_A ) begin
	bcr_r_A <= {bcr_r_A[1:0], bcr};
end
always @(posedge clk160_B ) begin
	bcr_r_B <= {bcr_r_B[1:0], bcr};
end
always @(posedge clk160_C ) begin
	bcr_r_C <= {bcr_r_C[1:0], bcr};
end
majority_voter #(.WIDTH(3)) 
    majority_voter_inst_bcr_A (
      .inA(bcr_r_A),
      .inB(bcr_r_B),
      .inC(bcr_r_C),
      .out(bcr_r_vote),
      .err()); 


reg [13:0] global_counter_A,global_counter_B,global_counter_C;
wire [13:0] global_counter_vote_A,global_counter_vote_B,global_counter_vote_C;
always @(posedge clk160_A) begin
	if (rst & auto_roll_over) begin
		// reset
		global_counter_A <= 14'b11111111111111;
	end	else if (auto_roll_over) begin
		if(global_counter_vote_A == {roll_over,2'b11})begin
			global_counter_A <= 14'b0;
		end else begin
			global_counter_A <= global_counter_vote_A + 14'b1;
		end
	end else begin
		global_counter_A <= bcr_inner ? 14'b0 : (global_counter_vote_A + 14'b1);
	end
end
majority_voter #(.WIDTH(14)) 
    majority_voter_inst_global_counter_A (
      .inA(global_counter_A),
      .inB(global_counter_B),
      .inC(global_counter_C),
      .out(global_counter_vote_A),
      .err()); 


always @(posedge clk160_B) begin
	if (rst & auto_roll_over) begin
		// reset
		global_counter_B <= 14'b11111111111111;
	end	else if (auto_roll_over) begin
		if(global_counter_vote_B == {roll_over,2'b11})begin
			global_counter_B <= 14'b0;
		end else begin
			global_counter_B <= global_counter_vote_B + 14'b1;
		end
	end else begin
		global_counter_B <= bcr_inner ? 14'b0 : (global_counter_vote_B + 14'b1);
	end
end
majority_voter #(.WIDTH(14)) 
    majority_voter_inst_global_counter_B (
      .inA(global_counter_B),
      .inB(global_counter_C),
      .inC(global_counter_A),
      .out(global_counter_vote_B),
      .err()); 



always @(posedge clk160_C) begin
	if (rst & auto_roll_over) begin
		// reset
		global_counter_C <= 14'b11111111111111;
	end	else if (auto_roll_over) begin
		if(global_counter_vote_C == {roll_over,2'b11})begin
			global_counter_C <= 14'b0;
		end else begin
			global_counter_C <= global_counter_vote_C + 14'b1;
		end
	end else begin
		global_counter_C <= bcr_inner ? 14'b0 : (global_counter_vote_C + 14'b1);
	end
end
majority_voter #(.WIDTH(14)) 
    majority_voter_inst_global_counter (
      .inA(global_counter_C),
      .inB(global_counter_A),
      .inC(global_counter_B),
      .out(global_counter_vote_C),
      .err()); 





reg coarse_bcr_160M_A,coarse_bcr_160M_B,coarse_bcr_160M_C;
always @(posedge clk160_A) begin
//	if(&global_counter[1:0])begin
		coarse_bcr_160M_A <= bypass_bcr_distribution ? bcr :  {global_counter_vote_A[13:2] == coarse_count_offset};
//	end		
end
always @(posedge clk160_B) begin
//	if(&global_counter[1:0])begin
		coarse_bcr_160M_B <= bypass_bcr_distribution ? bcr :  {global_counter_vote_B[13:2] == coarse_count_offset};
//	end		
end
always @(posedge clk160_C) begin
//	if(&global_counter[1:0])begin
		coarse_bcr_160M_C <= bypass_bcr_distribution ? bcr :  {global_counter_vote_C[13:2] == coarse_count_offset};
//	end		
end
wire coarse_bcr_160M;
majority_voter #(.WIDTH(1)) 
    majority_voter_inst_coarse_bcr_160M_A (
      .inA(coarse_bcr_160M_A),
      .inB(coarse_bcr_160M_B),
      .inC(coarse_bcr_160M_C),
      .out(coarse_bcr_160M),
      .err()); 

reg coarse_bcr_A,coarse_bcr_B,coarse_bcr_C;
always @(posedge clk160_A) begin
	coarse_bcr_A <= coarse_bcr_160M;
end
always @(posedge clk160_B) begin
	coarse_bcr_B <= coarse_bcr_160M;
end
always @(posedge clk160_C) begin
	coarse_bcr_C <= coarse_bcr_160M;
end

majority_voter #(.WIDTH(1)) 
    majority_voter_inst_coarse_bcr (
      .inA(coarse_bcr_A),
      .inB(coarse_bcr_B),
      .inC(coarse_bcr_C),
      .out(coarse_bcr),
      .err()); 


//reg trigger_count_bcr_r;
//always @(posedge clk160) begin
//	trigger_count_bcr_r <= {global_counter[11:0] == bunch_offset};
//end
//assign trigger_count_bcr =trigger_count_bcr_r ;

reg trigger_count_bcr_A,trigger_count_bcr_B,trigger_count_bcr_C;
always @(posedge clk160_A) begin
	trigger_count_bcr_A <= coarse_bcr_160M;
end
always @(posedge clk160_B) begin
	trigger_count_bcr_B <= coarse_bcr_160M;
end
always @(posedge clk160_C) begin
	trigger_count_bcr_C <= coarse_bcr_160M;
end
majority_voter #(.WIDTH(1)) 
    majority_voter_inst_trigger_count_bcr (
      .inA(trigger_count_bcr_A),
      .inB(trigger_count_bcr_B),
      .inC(trigger_count_bcr_C),
      .out(trigger_count_bcr),
      .err()); 
endmodule
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : comparator_up_down.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on June 30st, 2017
//  Note       : 
// 
`include "common_definition.v"
module comparator_up_down #(
	parameter DATA_WIDTH=12
	)
(
    input [DATA_WIDTH-1:0] limit_up,
    input [DATA_WIDTH-1:0] limit_down,
    input [DATA_WIDTH-1:0] data_in,
    output inrange
    );
wire inrang_no_roll_over,inrang_roll_over;
assign inrang_no_roll_over = ((~(limit_down>limit_up))&&(data_in<=limit_up)&&(data_in>limit_down));
assign inrang_roll_over = ((limit_down>limit_up)&&((data_in<=limit_up)||(data_in>limit_down)));
assign inrange = inrang_no_roll_over|inrang_roll_over;
endmodule
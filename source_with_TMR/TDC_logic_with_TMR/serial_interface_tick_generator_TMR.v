/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : serial_interface_tick_generator_TMR.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-05-29 14:38:19
//  Note       : 
//     
module serial_interface_tick_generator_TMR(
	input clk_A,
	input clk_B,
	input clk_C,
	input rst,
	// input enable_TMR,
	input enable_320,
	output  tick
	);
reg [3:0] send_number_A,send_number_B,send_number_C;
wire [3:0] send_number_vote,send_number_vote_A,send_number_vote_B,send_number_vote_C;
always @(posedge clk_A  ) begin
	if (rst) begin
		// reset
		send_number_A <= 4'b0;
	end	else if (~|send_number_vote_A) begin
		send_number_A <= enable_320 ? 4'h4 : 4'h9;
	end else if(|send_number_vote_A)begin
		send_number_A <= send_number_vote_A-4'h1;
	end
end
always @(posedge clk_B  ) begin
	if (rst) begin
		// reset
		send_number_B <= 4'b0;
	end	else if (~|send_number_vote_B) begin
		send_number_B <= enable_320 ? 4'h4 : 4'h9;
	end else if(|send_number_vote_B)begin
		send_number_B <= send_number_vote_B-4'h1;
	end
end
always @(posedge clk_C  ) begin
	if (rst) begin
		// reset
		send_number_C <= 4'b0;
	end	else if (~|send_number_vote_C) begin
		send_number_C <= enable_320 ? 4'h4 : 4'h9;
	end else if(|send_number_vote_C)begin
		send_number_C <= send_number_vote_C-4'h1;
	end
end

majority_voter #(.WIDTH(4)) 
majority_voter_inst_A (
	.inA(send_number_A),
	.inB(send_number_B),
	.inC(send_number_C),
	.out(send_number_vote_A),
	.err());
majority_voter #(.WIDTH(4)) 
majority_voter_inst_B (
	.inA(send_number_B),
	.inB(send_number_C),
	.inC(send_number_A),
	.out(send_number_vote_B),
	.err());
majority_voter #(.WIDTH(4)) 
majority_voter_inst_C (
	.inA(send_number_C),
	.inB(send_number_A),
	.inC(send_number_B),
	.out(send_number_vote_C),
	.err());
// majority_voter #(.WIDTH(4)) 
// majority_voter_inst (
// 	.inA(send_number_vote_A),
// 	.inB(send_number_vote_B),
// 	.inC(send_number_vote_C),
// 	.out(send_number_vote),
// 	.err());
assign send_number_vote = send_number_vote_A;
// reg [3:0] send_number;
// always @(posedge clk_A  ) begin
// 	if (rst) begin
// 		// reset
// 		send_number <= 4'b0;
// 	end	else if (~|send_number) begin
// 		send_number <= enable_320 ? 4'h4 : 4'h9;
// 	end else if(|send_number)begin
// 		send_number <= send_number-4'h1;
// 	end
// end
reg tick_vote;
always @(posedge clk_A ) begin
	tick_vote <= ~|send_number_vote;
end
assign tick = tick_vote;
// always @(posedge clk_A ) begin
// 	tick<= enable_TMR ? tick_vote : (~|send_number);
// end

endmodule
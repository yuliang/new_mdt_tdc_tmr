/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : fine_encode.v
//  Author     : Jinhong Wang, Xiong Xiao, Yu Liang
//  Revision   : 
//               First created on June 1st, 2016
//               Modified on April 30th, 2018
//  Note       :  change loock up table form 6bit to 2bit, fix MSB 4 bit to constant
// 


`include "common_definition.v"
module fine_encode2(
 input [1:0]   lut0, lut1, lut2, lut3, lut4, lut5, lut6, lut7,
               lut8, lut9, luta, lutb, lutc, lutd, lute, lutf,

input  [3:0]   raw_tdc,
output [1:0]   fine_tdc);

reg [1:0] fine_tdc_r;

assign fine_tdc = fine_tdc_r;

always @(*) begin
      if( raw_tdc == 4'b0000)
        fine_tdc_r = lut0[1:0];
      else if( raw_tdc == 4'b0001)
        fine_tdc_r = lut1[1:0];
      else if( raw_tdc == 4'b0010)
        fine_tdc_r = lut2[1:0];
      else if( raw_tdc == 4'b0011)
        fine_tdc_r = lut3[1:0];
      else if( raw_tdc == 4'b0100)
        fine_tdc_r = lut4[1:0];
      else if( raw_tdc == 4'b0101)
        fine_tdc_r = lut5[1:0];
      else if( raw_tdc == 4'b0110)
        fine_tdc_r = lut6[1:0];
      else if( raw_tdc == 4'b0111)
        fine_tdc_r = lut7[1:0];
      else if( raw_tdc == 4'b1000)
        fine_tdc_r = lut8[1:0];
      else if( raw_tdc == 4'b1001)
        fine_tdc_r = lut9[1:0];
      else if( raw_tdc == 4'b1010)
        fine_tdc_r = luta[1:0];
      else if( raw_tdc == 4'b1011)
        fine_tdc_r = lutb[1:0];
      else if( raw_tdc == 4'b1100)
        fine_tdc_r = lutc[1:0];
      else if( raw_tdc == 4'b1101)
        fine_tdc_r = lutd[1:0];
      else if( raw_tdc == 4'b1110)
        fine_tdc_r = lute[1:0];
      else if( raw_tdc == 4'b1111)
        fine_tdc_r = lutf[1:0];
      else 
        fine_tdc_r = lutf[1:0];
end
endmodule
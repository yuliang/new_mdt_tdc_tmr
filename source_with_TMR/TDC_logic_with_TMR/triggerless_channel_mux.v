/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : triggerless_channel_mux.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 16st, 2018
//  Note       : choosen data to write to readout fifo
//               for channel data debug if any channel full, from 0 to 23 write to read_out fifo,else any channel no empty,  from 0 to 23 write to read_out fifo
`include "common_definition.v"
module triggerless_channel_mux
#(parameter
    CHANNEL_NUMBER=24,
    CHANNEL_DATA_WIDTH=45
)
(
    input clk,
    input rst,
    input enable,
    input channel_data_debug,

    input [CHANNEL_DATA_WIDTH*CHANNEL_NUMBER-1:0] channel_data,
    input [CHANNEL_NUMBER-1:0] channel_fifo_full,
    input [CHANNEL_NUMBER-1:0]  channel_fifo_empty,
    output[CHANNEL_NUMBER-1:0] channel_fifo_read,

    input read_out_fifo_full,    

    output data_ready,
    output [CHANNEL_DATA_WIDTH-1:0] data_out
);

reg high;
reg cycle_read_finished;
reg [CHANNEL_NUMBER-1:0] channel_fifo_full_r,channel_fifo_no_empty_r;
wire [CHANNEL_NUMBER-1:0] channel_fifo_full_one_hot,channel_fifo_no_empty_one_hot;
wire [CHANNEL_NUMBER-1:0] channel_fifo_full_next,channel_fifo_no_empty_next;

//assign channel_fifo_full_next = read_out_fifo_full ? channel_fifo_full_r : (channel_fifo_full_r & (~channel_fifo_full_one_hot)),
//       channel_fifo_no_empty_next = read_out_fifo_full ? channel_fifo_no_empty_r : (channel_fifo_no_empty_r & (~channel_fifo_no_empty_one_hot));

assign channel_fifo_full_next = read_out_fifo_full ? channel_fifo_full_r : 
                                                    ((channel_data_debug & ~high) ? channel_fifo_full_r : (channel_fifo_full_r & (~channel_fifo_full_one_hot))),
       channel_fifo_no_empty_next = read_out_fifo_full ?channel_fifo_no_empty_r :
                                                    ((channel_data_debug & ~high)  ? channel_fifo_no_empty_r : (channel_fifo_no_empty_r & (~channel_fifo_no_empty_one_hot)));
reg full_cycle;

always @(posedge clk  ) begin
  if (rst) begin
    // reset
    channel_fifo_full_r <= 'b0;
    channel_fifo_no_empty_r <= 'b0;
  end
  else if (cycle_read_finished & enable) begin
    channel_fifo_full_r <= channel_fifo_full & (~channel_fifo_empty) ;
    channel_fifo_no_empty_r <= ~channel_fifo_empty;
  end
  else begin
    channel_fifo_full_r <= channel_fifo_full_next;
    channel_fifo_no_empty_r <= channel_fifo_no_empty_next;
  end
end

always @(posedge clk  ) begin
  if (rst) begin
    // reset
    full_cycle <= 1'b0;
  end
  else if (cycle_read_finished) begin
    full_cycle <= (|channel_fifo_full);
  end
  else if (~cycle_read_finished) begin
    full_cycle <= |channel_fifo_full_next;
  end
  else begin
    full_cycle <= 1'b0;
  end
end

wire data_exit;
assign data_exit = (|channel_fifo_full)|(|(~channel_fifo_empty));
always @(posedge clk  ) begin
  if (rst) begin
    // reset
    cycle_read_finished <= 1'b1;
  end
  else if (cycle_read_finished) begin
    cycle_read_finished <= ~data_exit;
  end
  else if (~cycle_read_finished) begin
    cycle_read_finished <= full_cycle ? (~|channel_fifo_full_next):(~|channel_fifo_no_empty_next);
  end
end


multi_hot_one_hot #(.WIDTH(CHANNEL_NUMBER))
  multi_hot_one_hot_full(
    .multi_hot_code(channel_fifo_full_r),
    .one_hot_code(channel_fifo_full_one_hot)
    ),
  multi_hot_one_hot_no_empty(
    .multi_hot_code(channel_fifo_no_empty_r),
    .one_hot_code(channel_fifo_no_empty_one_hot)
    );

wire [CHANNEL_NUMBER-1:0] one_hot_code;

assign one_hot_code = cycle_read_finished ? 'b0:
                        full_cycle ?  channel_fifo_full_one_hot :
                          channel_fifo_no_empty_one_hot;

wire [CHANNEL_DATA_WIDTH-1:0] channel_data_selected;
one_hot_select #(.DATA_WIDTH(CHANNEL_DATA_WIDTH),.DATA_DEEP(CHANNEL_NUMBER))
  channel_mux_inst(
    .one_hot_code(one_hot_code),
    .data_in(channel_data),
    .data_out(channel_data_selected)
  );
//assign data_out = channel_data_selected;
assign data_out  = channel_data_debug ? (~high ? {channel_data_selected[44:40],channel_data_selected[39],15'b0,2'b00,channel_data_selected[44:40],channel_data_selected[16:0]}: {channel_data_selected[44:40],1'b0,15'b0,2'b11,channel_data_selected[38:17]}):
                                      channel_data_selected;

assign data_ready = |one_hot_code;

//assign channel_fifo_read = read_out_fifo_full ? 'b0:one_hot_code;
assign channel_fifo_read = read_out_fifo_full ? 'b0: ((channel_data_debug & (~high)) ? 'b0 :
                                                     one_hot_code);

always @(posedge clk  ) begin
  if (rst) begin
    // reset
    high <= 1'b0;
  end else if(cycle_read_finished)begin
    high <= 1'b0;
  end else if (data_ready&(~read_out_fifo_full)&channel_data_debug) begin
    high <= ~high; 
  end
end
endmodule
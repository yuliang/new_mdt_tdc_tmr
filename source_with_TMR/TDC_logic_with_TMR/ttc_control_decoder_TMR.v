/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : ttc_control_decoder_TMR.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 26th, 2018
//  Note       : 
//    

`include "common_definition.v"
module ttc_control_decoder_TMR
( 
	input clk160_A, clk160_B, clk160_C,


	input reset_in, 
	input trigger_direct_in,
	input bunch_reset_direct_in,
	input event_reset_direct_in,

	input reset_jtag_in, 
	input event_reset_jtag_in,

	input encoded_control_in,
    
    input enable_new_ttc,
	input enable_master_reset_code,
	input enable_trigger,
	input enable_direct_trigger,
	input enable_direct_bunch_reset,
	input enable_direct_event_reset,

	output reg trigger,

	output reg event_reset,
	output master_reset,

//	input clk320,

	input [11:0] roll_over,
	input [11:0] coarse_count_offset,

	input auto_roll_over,
	input bypass_bcr_distribution,

	output  coarse_bcr,
	output  trigger_count_bcr

);

reg [1:0] reset_jtag_syn_A;
//sync jtag_reset
always @(posedge clk160_A or posedge reset_in) begin
	if (reset_in) begin
		reset_jtag_syn_A <= 2'b0;
	end else begin
		reset_jtag_syn_A <=  {reset_jtag_syn_A[0],reset_jtag_in};
	end
end
reg [1:0] reset_jtag_syn_B;
//sync jtag_reset
always @(posedge clk160_B or posedge reset_in) begin
	if (reset_in) begin
		reset_jtag_syn_B <= 2'b0;
	end else begin
		reset_jtag_syn_B <=  {reset_jtag_syn_B[0],reset_jtag_in};
	end
end
reg [1:0] reset_jtag_syn_C;
//sync jtag_reset
always @(posedge clk160_C or posedge reset_in) begin
	if (reset_in) begin
		reset_jtag_syn_C <= 2'b0;
	end else begin
		reset_jtag_syn_C <=  {reset_jtag_syn_C[0],reset_jtag_in};
	end
end

wire reset_jtag_vote;
majority_voter #(.WIDTH(1)) 
    majority_voter_inst_reset_jtag (
      .inA(reset_jtag_syn_A[1]),
      .inB(reset_jtag_syn_B[1]),
      .inC(reset_jtag_syn_C[1]),
      .out(reset_jtag_vote),
      .err()); 



reg [1:0] event_reset_jtag_syn;
wire event_reset_jtag;
assign  event_reset_jtag = event_reset_jtag_syn[1];
//sync jtag_reset
always @(posedge clk160_A or posedge reset_in) begin
	if (reset_in) begin
		event_reset_jtag_syn <= 2'b0;
	end else begin
		event_reset_jtag_syn <=  {event_reset_jtag_syn[0],event_reset_jtag_in};
	end
end


wire bcr_TTC,trigger_TTC,event_reset_TTC,master_reset_TTC;
control_decoder_TMR
	control_decoder_inst( 
		.clk160_A(clk160_A), .clk160_B(clk160_B), .clk160_C(clk160_C), 
		.enable(~enable_new_ttc),	
		.reset_in(reset_in), 
	
		.encoded_control_in(encoded_control_in),
	
		.enable_master_reset_code(enable_master_reset_code),
		.enable_trigger(enable_trigger),

		.trigger(trigger_TTC),
		.bunch_reset(bcr_TTC),
		.event_reset(event_reset_TTC),
		.master_reset(master_reset_TTC)
	);
wire bcr_newTTC,trigger_newTTC,event_reset_newTTC,master_reset_newTTC;
control_decoder_with_newTTC_TMR
control_decoder_with_newTTC_inst( 
		.clk160_A(clk160_A), .clk160_B(clk160_B), .clk160_C(clk160_C), 
		.enable(enable_new_ttc),	
		.reset_in(reset_in), 
	
		.encoded_control_in(encoded_control_in),
	
		.enable_master_reset_code(enable_master_reset_code),
		.enable_trigger(enable_trigger),

		.trigger(trigger_newTTC),
		.bunch_reset(bcr_newTTC),
		.event_reset(event_reset_newTTC),
		.master_reset(master_reset_newTTC)
	);

reg  master_reset_r_A,master_reset_r_B,master_reset_r_C;
always @(posedge clk160_A or posedge reset_in) begin
	if (reset_in) begin
		// reset
		master_reset_r_A  <= 1'b0;
	end	else begin
		master_reset_r_A <= enable_master_reset_code& (enable_new_ttc ? master_reset_newTTC : master_reset_TTC);
	end
end
always @(posedge clk160_B or posedge reset_in) begin
	if (reset_in) begin
		// reset
		master_reset_r_B  <= 1'b0;
	end	else begin
		master_reset_r_B <= enable_master_reset_code& (enable_new_ttc ? master_reset_newTTC : master_reset_TTC);
	end
end
always @(posedge clk160_C or posedge reset_in) begin
	if (reset_in) begin
		// reset
		master_reset_r_C  <= 1'b0;
	end	else begin
		master_reset_r_C <= enable_master_reset_code& (enable_new_ttc ? master_reset_newTTC : master_reset_TTC);
	end
end
wire master_reset_r_vote;
majority_voter #(.WIDTH(1)) 
    majority_voter_inst_master_reset_r (
      .inA(master_reset_r_A),
      .inB(master_reset_r_B),
      .inC(master_reset_r_C),
      .out(master_reset_r_vote),
      .err()); 



reg [2:0] reset_r_A;
always @(posedge clk160_A) begin
	reset_r_A <= {reset_r_A[1:0],reset_jtag_vote |master_reset_r_vote|reset_in};
end
reg [2:0] reset_r_B;
always @(posedge clk160_B) begin
	reset_r_B <= {reset_r_B[1:0],reset_jtag_vote |master_reset_r_vote|reset_in};
end
reg [2:0] reset_r_C;
always @(posedge clk160_C) begin
	reset_r_C <= {reset_r_C[1:0],reset_jtag_vote |master_reset_r_vote|reset_in};
end
wire master_reset_vote;
majority_voter #(.WIDTH(1)) 
    majority_voter_inst_master_reset (
      .inA(reset_r_A[2]),
      .inB(reset_r_B[2]),
      .inC(reset_r_C[2]),
      .out(master_reset_vote),
      .err()); 
assign  master_reset = master_reset_vote;



reg bunch_reset_direct_in_r_A,bunch_reset_direct_in_r_B,bunch_reset_direct_in_r_C;
always @(posedge clk160_A) begin
	bunch_reset_direct_in_r_A <= bunch_reset_direct_in;
end
always @(posedge clk160_B) begin
	bunch_reset_direct_in_r_B <= bunch_reset_direct_in;
end
always @(posedge clk160_C) begin
	bunch_reset_direct_in_r_C <= bunch_reset_direct_in;
end
wire bunch_reset_direct_in_r;
majority_voter #(.WIDTH(1)) 
    majority_voter_inst_bunch_reset_direct_in (
      .inA(bunch_reset_direct_in_r_A),
      .inB(bunch_reset_direct_in_r_B),
      .inC(bunch_reset_direct_in_r_C),
      .out(bunch_reset_direct_in_r),
      .err()); 


reg bcr_A,bcr_B,bcr_C;
wire bcr;
always @(posedge clk160_A) begin
	bcr_A <= enable_direct_bunch_reset ? bunch_reset_direct_in_r : (enable_new_ttc ? bcr_newTTC : bcr_TTC);
end
always @(posedge clk160_B) begin
	bcr_B <= enable_direct_bunch_reset ? bunch_reset_direct_in_r : (enable_new_ttc ? bcr_newTTC : bcr_TTC);
end
always @(posedge clk160_C) begin
	bcr_C <= enable_direct_bunch_reset ? bunch_reset_direct_in_r : (enable_new_ttc ? bcr_newTTC : bcr_TTC);
end
majority_voter #(.WIDTH(1)) 
    majority_voter_inst_bcr (
      .inA(bcr_A),
      .inB(bcr_B),
      .inC(bcr_C),
      .out(bcr),
      .err()); 

bcr_distribution_TMR
	bcr_distribution_inst(
	.clk160_A(clk160_A),.clk160_B(clk160_B),.clk160_C(clk160_C),
//	.clk320(clk320),
	.rst(master_reset),

	.bcr(bcr),
	.roll_over(roll_over),
	.coarse_count_offset(coarse_count_offset),
	//input [11:0] bunch_offset,

	.auto_roll_over(auto_roll_over),
	.bypass_bcr_distribution(bypass_bcr_distribution),

	.coarse_bcr(coarse_bcr),
	.trigger_count_bcr(trigger_count_bcr)
	);


always @(posedge clk160_A) begin
	event_reset <= (enable_direct_event_reset ? event_reset_direct_in: (enable_new_ttc ? event_reset_newTTC : event_reset_TTC)) | event_reset_jtag;
	trigger  <=  enable_trigger & (enable_direct_trigger ? trigger_direct_in : (enable_new_ttc ? trigger_newTTC : trigger_TTC));
end

endmodule

/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : control_decoder_with_newTTC_TMR.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 26th, 2018
//  Note       : 
//    

`include "common_definition.v"
module control_decoder_with_newTTC_TMR
( 
	input clk160_A, clk160_B,clk160_C,
	input enable,
	input reset_in, 

	input encoded_control_in,

	input enable_master_reset_code,
	input enable_trigger,


	output reg trigger,
	output  bunch_reset,
	output reg event_reset,
	output  master_reset
);


reg[1:0] encoded_control_syn_r;
always @(posedge clk160_A or posedge reset_in) begin
	if (reset_in) begin
		// reset
		encoded_control_syn_r <= 2'b0;
	end	else begin
		encoded_control_syn_r <= {encoded_control_syn_r[0] & enable ,encoded_control_in };
	end
end


reg input_shift_reg;
reg [3:0] decoder_shift_reg;
//shift reg to decode commands
always @(posedge clk160_A or posedge reset_in) begin
	if (reset_in) begin
		input_shift_reg <= 1'b0;
		decoder_shift_reg <= 4'b0;
	end else  begin
		input_shift_reg      <= encoded_control_syn_r[1];
		decoder_shift_reg[0] <= input_shift_reg;
		decoder_shift_reg[1] <= decoder_shift_reg[0];
		decoder_shift_reg[2] <= decoder_shift_reg[1];
		decoder_shift_reg[3] <= decoder_shift_reg[2];
	end
end
reg [2:0] code_size_count;
//bit count from start bit
always @(posedge clk160_A or posedge reset_in)
	begin
	if (reset_in)
		code_size_count <=3'd0;
	else begin
		if ( ((code_size_count == 3'd0) | (code_size_count == 3'd4)) & input_shift_reg )
			code_size_count <= 3'd1;
		else if ( (code_size_count == 3'd1) | (code_size_count == 3'd2) | (code_size_count == 3'd3) )
			code_size_count <=  code_size_count + 3'd1;
		else
			code_size_count <= 3'd0;
		end
	end

wire code_detect,bunch_reset_detect,trigger_detect,event_reset_detect,master_reset_detect;

assign 	code_detect = (code_size_count == 4),
		bunch_reset_detect = code_detect & decoder_shift_reg[3] &  decoder_shift_reg[2], 
		trigger_detect     = code_detect & decoder_shift_reg[3] & ~decoder_shift_reg[1] & ~decoder_shift_reg[0], 		
		event_reset_detect = code_detect & decoder_shift_reg[3] &  decoder_shift_reg[1] & decoder_shift_reg[0], 
		master_reset_detect= code_detect & decoder_shift_reg[3] & ~decoder_shift_reg[1] & decoder_shift_reg[0];


//-----------------------------------------------------------
//registered versions of resets and trigger
always @(posedge clk160_A)
	begin
	trigger <= enable & enable_trigger &  trigger_detect;
	end

always @(posedge clk160_A)
	begin
	event_reset <= enable &  event_reset_detect ;
	end


reg bunch_reset_A,bunch_reset_B,bunch_reset_C;
always @(posedge clk160_A)
	begin
	bunch_reset_A <= enable & bunch_reset_detect;
	end
always @(posedge clk160_B)
	begin
	bunch_reset_B <= enable & bunch_reset_detect;
	end
always @(posedge clk160_C)
	begin
	bunch_reset_C <= enable & bunch_reset_detect;
	end
majority_voter #(.WIDTH(1)) 
    majority_voter_inst_bunch_reset (
      .inA(bunch_reset_A),
      .inB(bunch_reset_B),
      .inC(bunch_reset_C),
      .out(bunch_reset),
      .err()); 

reg master_reset_A,master_reset_B,master_reset_C;
always @(posedge clk160_A)
	begin
	master_reset_A = enable & enable_master_reset_code & master_reset_detect;
	end
always @(posedge clk160_B)
	begin
	master_reset_B = enable & enable_master_reset_code & master_reset_detect;
	end
always @(posedge clk160_C)
	begin
	master_reset_C = enable & enable_master_reset_code & master_reset_detect;
	end
majority_voter #(.WIDTH(1)) 
    majority_voter_inst_master_reset (
      .inA(master_reset_A),
      .inB(master_reset_B),
      .inC(master_reset_C),
      .out(master_reset),
      .err()); 


endmodule
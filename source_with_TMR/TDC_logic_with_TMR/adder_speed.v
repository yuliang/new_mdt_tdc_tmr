/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : adder_speed.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 6st, 2018
//  Note       : 
// 
`include "common_definition.v"
module adder_speed #(
    parameter WIDTH=12
    )
(
			a,b,carry_in,
			s,carry_out);

input[WIDTH-1:0]	a,b;
input		carry_in;

output[WIDTH-1:0]	s;
output		carry_out;

wire[WIDTH:0]	sum;

assign 	sum = a + b + carry_in;
assign	carry_out = sum[WIDTH];
assign	s = sum[WIDTH-1:0]; 
//assign {carry_out,s} = a + b + carry_in;
endmodule
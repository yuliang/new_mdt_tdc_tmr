/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : setup_reg_TMR.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-05-29 15:15:17
//  Note       : 
//     
`include "common_definition.v"
module setup_reg_TMR
#(parameter
	WIDTH = 1,
	initial_value = 0
)
(
	input clk,
	input trst,
	input clk_update_A,
    input clk_update_B,
    input clk_update_C,
	input shift,
	input enable,
	input update,
	input serial_data_in,
	output serial_data_out,
	output [WIDTH-1:0]  parallel_data_out
	);

	reg [WIDTH-1:0] serial_reg;
	assign  serial_data_out = serial_reg[0];
	always @(posedge clk or negedge trst) begin
		if(~trst)begin
			serial_reg <= initial_value;
		end else if(enable)begin
			if (shift) begin
				serial_reg <= {serial_data_in,serial_reg[WIDTH-1:1]};
			end else begin
				serial_reg <= parallel_data_out;
			end	
		end	
	end

	reg [1:0] update_A_syn;
	reg [1:0] update_B_syn;
	reg [1:0] update_C_syn;
	always @(posedge clk_update_A ) begin
		update_A_syn <= {update_A_syn[0],update};
	end
	always @(posedge clk_update_B ) begin
		update_B_syn <= {update_B_syn[0],update};
	end
	always @(posedge clk_update_C ) begin
		update_C_syn <= {update_C_syn[0],update};
	end

	reg [WIDTH-1:0]  parallel_data_out_A;
	reg [WIDTH-1:0]  parallel_data_out_B;
	reg [WIDTH-1:0]  parallel_data_out_C;
	wire [WIDTH-1:0]  parallel_data_out_vote_A;
	wire [WIDTH-1:0]  parallel_data_out_vote_B;
	wire [WIDTH-1:0]  parallel_data_out_vote_C;
	always @(posedge clk_update_A ) begin
		if (update_A_syn[1]|(~trst) ) begin
			parallel_data_out_A <= serial_reg;
		end else begin
			parallel_data_out_A <= parallel_data_out_vote_A;
		end
	end
	always @(posedge clk_update_B ) begin
		if (update_B_syn[1]|(~trst)) begin
			parallel_data_out_B <= serial_reg;
		end else begin
			parallel_data_out_B <= parallel_data_out_vote_B;
		end
	end	
	always @(posedge clk_update_C) begin
		if (update_C_syn[1]|(~trst)) begin
			parallel_data_out_C <= serial_reg;
		end else begin
			parallel_data_out_C <= parallel_data_out_vote_C;
		end
	end

	majority_voter #(.WIDTH(WIDTH)) 
	majority_voter_inst_A (
	.inA(parallel_data_out_A),
	.inB(parallel_data_out_B),
	.inC(parallel_data_out_C),
	.out(parallel_data_out_vote_A),
	.err());
	majority_voter #(.WIDTH(WIDTH)) 
	majority_voter_inst_B (
	.inA(parallel_data_out_B),
	.inB(parallel_data_out_C),
	.inC(parallel_data_out_A),
	.out(parallel_data_out_vote_B),
	.err());
	majority_voter #(.WIDTH(WIDTH)) 
	majority_voter_inst_C (
	.inA(parallel_data_out_C),
	.inB(parallel_data_out_A),
	.inC(parallel_data_out_B),
	.out(parallel_data_out_vote_C),
	.err());
	// majority_voter #(.WIDTH(WIDTH)) 
	// majority_voter_inst (
	// .inA(parallel_data_out_vote_A),
	// .inB(parallel_data_out_vote_B),
	// .inC(parallel_data_out_vote_C),
	// .out(parallel_data_out),
	// .err());
	assign parallel_data_out = parallel_data_out_vote_A;
endmodule
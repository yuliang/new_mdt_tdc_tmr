/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : channel_leading_trailing_edge_match3.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 6st, 2018
//  Note       : find corresponding trailing edge for leading edge sub channel 
//
`include "common_definition.v"
module channel_leading_trailing_edge_match3 
#(parameter 
	DATA_WIDTH = 17
)
(
    input clk,
    input rst,
    input enable,

    input data_ready_l,data_ready_t,
    input [DATA_WIDTH-1:0] effictive_data_l,effictive_data_t,
    

    //config for combine leading and trailing edge
    input [`COMBINE_TIME_OUT_SIZE-1:0] combine_time_out_config,	


    output combine_data_ready,
    output [DATA_WIDTH*2-1:0] effictive_combine_data,
    output miss_trailing_edge,
    output miss_trailing_edge_time_out,
    output miss_trailing_edge_new_edge
);
reg  combine_data_ready_r;
reg [33:0] effictive_combine_data_r;


reg [`COMBINE_TIME_OUT_SIZE-1:0] combine_time_out_count;
reg combine_time_out_count_start;
wire combine_time_reset;
wire combine_time_out;

//reg miss_trailing_edge_r;
reg miss_trailing_edge_time_out_r;
reg miss_trailing_edge_new_edge_r;


//reg[1:0] data_ready_t_r;
reg data_ready_t_r,data_ready_l_r;
reg [DATA_WIDTH-1:0] effictive_data_t_r;
reg [DATA_WIDTH-1:0] effictive_data_l_r;

wire lt_same_cycle;
wire lt_almost_same_cycle;
reg wait_for_t_edge;
wire end_wait_normal;
wire end_wait_abnormal;
wire end_wait_abnormal_time_out; 
wire end_wait_abnormal_new_edge; 
wire end_wait;

assign  combine_data_ready = combine_data_ready_r;
assign  effictive_combine_data = effictive_combine_data_r;
//assign  effictive_combine_data = {effictive_data_t_r,effictive_data_l_r};

assign miss_trailing_edge = end_wait_abnormal_time_out | (end_wait_abnormal_new_edge&(~data_ready_t));
assign miss_trailing_edge_time_out = miss_trailing_edge_time_out_r;
assign miss_trailing_edge_new_edge = miss_trailing_edge_new_edge_r;

//assign  combine_time_out = (combine_time_out_count == combine_time_out_config);
assign  combine_time_out = ~|combine_time_out_count;
//always @(posedge clk  ) begin
//	if (rst) begin
//		// reset
//		combine_time_out_count <= 'b0;
//	end
//	else if(combine_time_reset|combine_time_out) begin
//		combine_time_out_count  <= 'b0;
//	end
//	else if (combine_time_out_count_start|(|combine_time_out_count)) begin
//		combine_time_out_count <= combine_time_out_count+'b1;
//	end
//end

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		combine_time_out_count <= 'b0;
	end	else if(combine_time_reset) begin
		combine_time_out_count  <= 'b0;
	end	else if (combine_time_out_count_start) begin
		combine_time_out_count <= combine_time_out_config;
	end else if(|combine_time_out_count) begin
		combine_time_out_count <= combine_time_out_count - 'b1;
	end
end


always @(posedge clk  ) begin
	if (rst) begin
		// reset
		data_ready_t_r <= 1'b0;
		data_ready_l_r <= 1'b0;
		effictive_data_t_r <= 17'b0;
		effictive_data_l_r <= 17'b0;

	end
	else begin
		//data_ready_t_r <= {data_ready_t_r[0],data_ready_t};
		data_ready_t_r <= (~end_wait_abnormal_new_edge|end_wait_abnormal_time_out) & data_ready_t&enable;
		data_ready_l_r <= data_ready_l&enable;
		effictive_data_t_r <= data_ready_t ? effictive_data_t : effictive_data_t_r;
		effictive_data_l_r <= data_ready_l ? effictive_data_l : effictive_data_l_r; 
	end
end




assign lt_same_cycle = data_ready_l_r&data_ready_t_r;
//wire t_pre_cycle_it;
//assign t_pre_cycle_it = data_ready_l_r & data_ready_t_r[1];

//assign  lt_almost_same_cycle = lt_same_cycle | t_pre_cycle_it;
assign lt_almost_same_cycle = lt_same_cycle;



assign  end_wait_normal = wait_for_t_edge&data_ready_t_r;
assign  end_wait_abnormal_time_out = wait_for_t_edge & combine_time_out &(~end_wait_normal);
assign  end_wait_abnormal_new_edge = wait_for_t_edge & data_ready_l &(~end_wait_normal);
assign  end_wait_abnormal =end_wait_abnormal_time_out | end_wait_abnormal_new_edge;
assign  end_wait =end_wait_normal| end_wait_abnormal;

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		wait_for_t_edge <= 1'b0;
	end
	else if(end_wait) begin
		wait_for_t_edge <= 1'b0;
	end
	else if (data_ready_l_r) begin
		wait_for_t_edge <= ~lt_almost_same_cycle;
	end
end

assign combine_time_reset = end_wait;

//always @(posedge clk  ) begin
//	if (rst) begin
//		// reset
//		combine_time_out_count_start <= 1'b0;
//	end
//	else if (data_ready_l_r&(~lt_almost_same_cycle)) begin
//		combine_time_out_count_start <= 1'b1;
//	end
//	else begin
//		combine_time_out_count_start <= 1'b0;
//	end
//end
always @(*) begin
	combine_time_out_count_start = data_ready_l_r&(~lt_almost_same_cycle);
end

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		combine_data_ready_r <= 1'b0;
	end
	else if (lt_almost_same_cycle) begin
		combine_data_ready_r <=1'b1;
	end 
	else if(end_wait)begin
		combine_data_ready_r <=1'b1;
	end
	else begin
		combine_data_ready_r <=1'b0;
	end
end


always @(posedge clk  ) begin
	if (rst) begin
		// reset
		miss_trailing_edge_time_out_r <= 1'b0;
		miss_trailing_edge_new_edge_r <= 1'b0;

	end
	else if(end_wait_abnormal)begin
		miss_trailing_edge_time_out_r <= end_wait_abnormal_time_out;
		miss_trailing_edge_new_edge_r <= end_wait_abnormal_new_edge&(~data_ready_t);
	end

	else begin
		miss_trailing_edge_time_out_r <= 1'b0;
		miss_trailing_edge_new_edge_r <= 1'b0;
	end
end

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		effictive_combine_data_r <= 'b0;
	end
	else begin
		effictive_combine_data_r <= end_wait_abnormal ? {effictive_data_t,effictive_data_l_r} : {effictive_data_t_r,effictive_data_l_r};
	end
end
endmodule
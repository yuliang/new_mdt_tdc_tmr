/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : fifo1_TMR.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on April 12st, 2017
//  Note       : Top-level Verilog code for the FIFO 
//  Function   :
//               This is a asynchronous FIFO with a depth of 4
//               This fifo is referenced to:
//       "Simulation and Synthesis Techniques for Asynchronous FIFO Design"
//       by Clifford E. Cummings, @ Sunburst Design, Inc.
//       using gray counter to crossing time area


module fifo1_TMR #(parameter DSIZE = 17,parameter ASIZE = 2,SYN_DEPTH = 2)
(
output [DSIZE-1:0] rdata,
output wfull,
output rempty,
input [DSIZE-1:0] wdata,
input winc, wrst,
input wclk_A,wclk_B,wclk_C,
input rinc, rrst,
input  rclk_A,rclk_B,rclk_C
);

wire [ASIZE-1:0] waddr, raddr;
wire [ASIZE:0] wptr_A, rptr_A, wq2_rptr_A, rq2_wptr_A;
wire [ASIZE:0] wptr_B, rptr_B, wq2_rptr_B, rq2_wptr_B;
wire [ASIZE:0] wptr_C, rptr_C, wq2_rptr_C, rq2_wptr_C;

sync_DFFs #(.WIDTH(ASIZE+1),.SYN_DEPTH(SYN_DEPTH)) sync_r2w_A (
.data_out(wq2_rptr_A), .data_in(rptr_A),
.clk(wclk_A), .rst(wrst));

sync_DFFs #(.WIDTH(ASIZE+1),.SYN_DEPTH(SYN_DEPTH)) sync_w2r_A (
.data_out(rq2_wptr_A), .data_in(wptr_A),
.clk(rclk_A), .rst(rrst));

sync_DFFs #(.WIDTH(ASIZE+1),.SYN_DEPTH(SYN_DEPTH)) sync_r2w_B (
.data_out(wq2_rptr_B), .data_in(rptr_B),
.clk(wclk_B), .rst(wrst));

sync_DFFs #(.WIDTH(ASIZE+1),.SYN_DEPTH(SYN_DEPTH)) sync_w2r_B (
.data_out(rq2_wptr_B), .data_in(wptr_B),
.clk(rclk_B), .rst(rrst));

sync_DFFs #(.WIDTH(ASIZE+1),.SYN_DEPTH(SYN_DEPTH)) sync_r2w_C (
.data_out(wq2_rptr_C), .data_in(rptr_C),
.clk(wclk_C), .rst(wrst));

sync_DFFs #(.WIDTH(ASIZE+1),.SYN_DEPTH(SYN_DEPTH)) sync_w2r_C (
.data_out(rq2_wptr_C), .data_in(wptr_C),
.clk(rclk_C), .rst(rrst));

fifomem #(.DATASIZE(DSIZE), .ADDRSIZE(ASIZE)) fifomem_syn(
.rdata(rdata), .wdata(wdata),
.waddr(waddr), .raddr(raddr),
.wclken(winc&&(!wfull)), .wclk(wclk_A));

rptr_empty_TMR #(.ADDRSIZE(ASIZE)) rptr_empty_inst(
.rempty(rempty),
.raddr(raddr),
.rptr_A(rptr_A),.rptr_B(rptr_B),.rptr_C(rptr_C),
.rq2_wptr_A(rq2_wptr_A),.rq2_wptr_B(rq2_wptr_B),.rq2_wptr_C(rq2_wptr_C),
.rinc(rinc), 
.rclk_A(rclk_A),.rclk_B(rclk_B),.rclk_C(rclk_C),
.rrst(rrst));

wptr_full_TMR #(.ADDRSIZE(ASIZE)) wptr_full_inst(
.wfull(wfull), 
.waddr(waddr),
.wptr_A(wptr_A), .wptr_B(wptr_B),.wptr_C(wptr_C),
.wq2_rptr_A(wq2_rptr_A),.wq2_rptr_B(wq2_rptr_B),.wq2_rptr_C(wq2_rptr_C),
.winc(winc),
.wclk_A(wclk_A),.wclk_B(wclk_B),.wclk_C(wclk_C),
.wrst(wrst));

endmodule
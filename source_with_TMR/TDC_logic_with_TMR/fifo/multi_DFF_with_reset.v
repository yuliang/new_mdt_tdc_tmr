/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : multi_DFF_with_reset.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on April 12st, 2017
//  Note       : cross domain synchronizer DFF
// 


module multi_DFF_with_reset #(parameter WIDTH = 4)
(
output reg [WIDTH-1:0] q,
input [WIDTH-1:0] d,
input clk, rst
);
always @(posedge clk)
	if (rst) q <=   {WIDTH{1'b0}};
	else begin
		q <=  d;
	end

endmodule

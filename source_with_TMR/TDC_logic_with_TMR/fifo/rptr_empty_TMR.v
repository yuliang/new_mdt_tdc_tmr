/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : rptr_empty_TMR.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on April 12st, 2017
//  Note       : the read pointer and empty flag logic
// 


module rptr_empty_TMR #(parameter ADDRSIZE = 2)
(
output  rempty,
output [ADDRSIZE-1:0] raddr,
output reg [ADDRSIZE :0] rptr_A,rptr_B,rptr_C,
input [ADDRSIZE :0] rq2_wptr_A,rq2_wptr_B,rq2_wptr_C,
input rinc,  rrst,
input rclk_A, rclk_B, rclk_C
);


reg [ADDRSIZE:0] rbin_A,rbin_B,rbin_C;
wire [ADDRSIZE:0] rbin_vote_A,rbin_vote_B,rbin_vote_C;
wire [ADDRSIZE:0] rbinnext_vote_A,rbinnext_vote_B,rbinnext_vote_C;
wire rempty_vote_A,rempty_vote_B,rempty_vote_C;

assign rbinnext_vote_A = rbin_vote_A + (rinc & ~rempty_vote_A);
always @(posedge rclk_A) begin
  if (rrst) rbin_A<=  'b0;
  else rbin_A <= rbinnext_vote_A;
end

assign rbinnext_vote_B = rbin_vote_B + (rinc & ~rempty_vote_B);
always @(posedge rclk_B) begin
  if (rrst) rbin_B<=  'b0;
  else rbin_B <= rbinnext_vote_B;
end

assign rbinnext_vote_C = rbin_vote_C + (rinc & ~rempty_vote_C);
always @(posedge rclk_C) begin
  if (rrst) rbin_C<=  'b0;
  else rbin_C <= rbinnext_vote_C;
end
majority_voter #(.WIDTH(ADDRSIZE+1)) 
    majority_voter_inst_A (
      .inA(rbin_A),
      .inB(rbin_B),
      .inC(rbin_C),
      .out(rbin_vote_A),
      .err()); 
majority_voter #(.WIDTH(ADDRSIZE+1)) 
    majority_voter_inst_B (
      .inA(rbin_B),
      .inB(rbin_C),
      .inC(rbin_A),
      .out(rbin_vote_B),
      .err());
majority_voter #(.WIDTH(ADDRSIZE+1)) 
    majority_voter_inst_C (
      .inA(rbin_C),
      .inB(rbin_A),
      .inC(rbin_B),
      .out(rbin_vote_C),
      .err());



wire [ADDRSIZE:0] rgraynext_vote_A;
assign rgraynext_vote_A = (rbinnext_vote_A>>1) ^ rbinnext_vote_A;
always @(posedge rclk_A) begin
  if (rrst) rptr_A <=  'b0;
  else rptr_A <= rgraynext_vote_A;
end


wire [ADDRSIZE:0] rgraynext_vote_B;
assign rgraynext_vote_B = (rbinnext_vote_B>>1) ^ rbinnext_vote_B;
always @(posedge rclk_B) begin
  if (rrst) rptr_B <=  'b0;
  else rptr_B <= rgraynext_vote_B;
end

wire [ADDRSIZE:0] rgraynext_vote_C;
assign rgraynext_vote_C = (rbinnext_vote_C>>1) ^ rbinnext_vote_C;
always @(posedge rclk_C) begin
  if (rrst) rptr_C <=  'b0;
  else rptr_C <= rgraynext_vote_C;
end




//---------------------------------------------------------------
// FIFO empty when the next rptr == synchronized wptr or on reset
//---------------------------------------------------------------
wire [ADDRSIZE :0]  rq2_wptr_vote_A,rq2_wptr_vote_B,rq2_wptr_vote_C;
majority_voter #(.WIDTH(ADDRSIZE+1)) 
    majority_voter_inst_wptr_A (
      .inA(rq2_wptr_A),
      .inB(rq2_wptr_B),
      .inC(rq2_wptr_C),
      .out(rq2_wptr_vote_A),
      .err()); 
majority_voter #(.WIDTH(ADDRSIZE+1)) 
    majority_voter_inst_wptr_B (
      .inA(rq2_wptr_B),
      .inB(rq2_wptr_C),
      .inC(rq2_wptr_A),
      .out(rq2_wptr_vote_B),
      .err());
majority_voter #(.WIDTH(ADDRSIZE+1)) 
    majority_voter_inst_wptr_C (
      .inA(rq2_wptr_C),
      .inB(rq2_wptr_A),
      .inC(rq2_wptr_B),
      .out(rq2_wptr_vote_C),
      .err());

reg rempty_A,rempty_B,rempty_C;
always @(posedge rclk_A)
	if (rrst) rempty_A <=  1'b1;
	else rempty_A <= (rgraynext_vote_A == rq2_wptr_vote_A);
majority_voter #(.WIDTH(1)) 
    majority_voter_inst_rempty_A (
      .inA(rempty_A),
      .inB(rempty_B),
      .inC(rempty_C),
      .out(rempty_vote_A),
      .err());

always @(posedge rclk_B)
  if (rrst) rempty_B <=  1'b1;
  else rempty_B <= (rgraynext_vote_B == rq2_wptr_vote_B);
majority_voter #(.WIDTH(1)) 
    majority_voter_inst_rempty_B (
      .inA(rempty_B),
      .inB(rempty_C),
      .inC(rempty_A),
      .out(rempty_vote_B),
      .err());

always @(posedge rclk_C)
  if (rrst) rempty_C <=  1'b1;
  else rempty_C <= (rgraynext_vote_C == rq2_wptr_vote_C);
majority_voter #(.WIDTH(1)) 
    majority_voter_inst_rempty_C (
      .inA(rempty_C),
      .inB(rempty_A),
      .inC(rempty_B),
      .out(rempty_vote_C),
      .err());

assign rempty = rempty_vote_A;
assign raddr = rbin_vote_A[ADDRSIZE-1:0];
endmodule
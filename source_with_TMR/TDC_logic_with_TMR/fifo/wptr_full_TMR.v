/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : wptr_full_TMR.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on April 12st, 2017
//  Note       : the write pointer and full flag logic
// 


module wptr_full_TMR #(parameter ADDRSIZE = 2)
(
output  wfull,
output [ADDRSIZE-1:0] waddr,
output reg [ADDRSIZE :0] wptr_A,wptr_B,wptr_C,
input [ADDRSIZE :0] wq2_rptr_A,wq2_rptr_B,wq2_rptr_C,
input winc, wrst,
input wclk_A, wclk_B, wclk_C
);
reg [ADDRSIZE:0] wbin_A,wbin_B,wbin_C;
wire [ADDRSIZE:0] wbin_vote_A,wbin_vote_B,wbin_vote_C;
wire [ADDRSIZE:0] wbinnext_vote_A,wbinnext_vote_B,wbinnext_vote_C;
wire wfull_vote_A,wfull_vote_B,wfull_vote_C;
assign wbinnext_vote_A = wbin_vote_A + (winc & ~wfull_vote_A);
always @(posedge wclk_A)
	if (wrst) wbin_A <=  'b0;
	else wbin_A <= wbinnext_vote_A;

assign wbinnext_vote_B = wbin_vote_B + (winc & ~wfull_vote_B);
always @(posedge wclk_B)
  if (wrst) wbin_B <=  'b0;
  else wbin_B <= wbinnext_vote_B;

assign wbinnext_vote_C = wbin_vote_C + (winc & ~wfull_vote_C);
always @(posedge wclk_C)
  if (wrst) wbin_C <=  'b0;
  else wbin_C <= wbinnext_vote_C;
majority_voter #(.WIDTH(ADDRSIZE+1)) 
    majority_voter_inst_A (
      .inA(wbin_A),
      .inB(wbin_B),
      .inC(wbin_C),
      .out(wbin_vote_A),
      .err()); 
majority_voter #(.WIDTH(ADDRSIZE+1)) 
    majority_voter_inst_B (
      .inA(wbin_B),
      .inB(wbin_C),
      .inC(wbin_A),
      .out(wbin_vote_B),
      .err());
majority_voter #(.WIDTH(ADDRSIZE+1)) 
    majority_voter_inst_C (
      .inA(wbin_C),
      .inB(wbin_A),
      .inC(wbin_B),
      .out(wbin_vote_C),
      .err());


wire [ADDRSIZE :0] wgraynext_vote_A;
assign  wgraynext_vote_A = (wbinnext_vote_A>>1) ^ wbinnext_vote_A;
always @(posedge wclk_A) begin
  if (wrst) wptr_A <=  'b0;
  else wptr_A <= wgraynext_vote_A;
end
wire [ADDRSIZE :0] wgraynext_vote_B;
assign  wgraynext_vote_B = (wbinnext_vote_B>>1) ^ wbinnext_vote_B;
always @(posedge wclk_B) begin
  if (wrst) wptr_B <=  'b0;
  else wptr_B <= wgraynext_vote_B;
end
wire [ADDRSIZE :0] wgraynext_vote_C;
assign  wgraynext_vote_C = (wbinnext_vote_C>>1) ^ wbinnext_vote_C;
always @(posedge wclk_C) begin
  if (wrst) wptr_C <=  'b0;
  else wptr_C <= wgraynext_vote_C;
end

wire [ADDRSIZE :0] wq2_rptr_vote_A,wq2_rptr_vote_B,wq2_rptr_vote_C;
majority_voter #(.WIDTH(ADDRSIZE+1)) 
    majority_voter_inst_wptr_A (
      .inA(wq2_rptr_A),
      .inB(wq2_rptr_B),
      .inC(wq2_rptr_C),
      .out(wq2_rptr_vote_A),
      .err()); 
majority_voter #(.WIDTH(ADDRSIZE+1)) 
    majority_voter_inst_wptr_B (
      .inA(wq2_rptr_B),
      .inB(wq2_rptr_C),
      .inC(wq2_rptr_A),
      .out(wq2_rptr_vote_B),
      .err());
majority_voter #(.WIDTH(ADDRSIZE+1)) 
    majority_voter_inst_wptr_C (
      .inA(wq2_rptr_C),
      .inB(wq2_rptr_A),
      .inC(wq2_rptr_B),
      .out(wq2_rptr_vote_C),
      .err());


reg wfull_A,wfull_B,wfull_C;

always @(posedge wclk_A)
	if (wrst) wfull_A <=  1'b0;
	else wfull_A <=  (wgraynext_vote_A=={~wq2_rptr_vote_A[ADDRSIZE:ADDRSIZE-1],wq2_rptr_vote_A[ADDRSIZE-2:0]});
majority_voter #(.WIDTH(1)) 
  majority_voter_inst_wfull_A (
    .inA(wfull_A),
    .inB(wfull_B),
    .inC(wfull_C),
    .out(wfull_vote_A),
    .err());

always @(posedge wclk_B)
  if (wrst) wfull_B <=  1'b0;
  else wfull_B <=  (wgraynext_vote_B=={~wq2_rptr_vote_B[ADDRSIZE:ADDRSIZE-1],wq2_rptr_vote_B[ADDRSIZE-2:0]});
majority_voter #(.WIDTH(1)) 
  majority_voter_inst_wfull_B (
    .inA(wfull_B),
    .inB(wfull_C),
    .inC(wfull_A),
    .out(wfull_vote_B),
    .err());

always @(posedge wclk_C)
  if (wrst) wfull_C <=  1'b0;
  else wfull_C <=  (wgraynext_vote_C=={~wq2_rptr_vote_C[ADDRSIZE:ADDRSIZE-1],wq2_rptr_vote_C[ADDRSIZE-2:0]});
majority_voter #(.WIDTH(1)) 
  majority_voter_inst_wfull_C (
    .inA(wfull_C),
    .inB(wfull_A),
    .inC(wfull_B),
    .out(wfull_vote_C),
    .err());  

assign wfull = wfull_vote_A;
assign waddr = wbin_vote_A[ADDRSIZE-1:0];
endmodule
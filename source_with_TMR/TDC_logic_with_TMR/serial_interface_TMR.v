/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : serial_interface_TMR.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 8st, 2018
//  Note       : 
//    

`include "common_definition.v"
module serial_interface_TMR
(
    input clk_A,
    input clk_B,
    input clk_C,
    input rst,

	input enable_320,
	input enable,
	// input enable_TMR,

	input interface_fifo_empty,
	output interface_fifo_read,
	input [9:0] interface_fifo_data,

	output [1:0] d_line
);

wire fifo_empty; assign  fifo_empty = interface_fifo_empty;
reg  fifo_read; assign interface_fifo_read = fifo_read;

serial_interface_tick_generator_TMR serial_interface_tick_generator_inst
		(
			.clk_A      (clk_A),
			.clk_B      (clk_B),
			.clk_C      (clk_C),
			.rst        (rst),
			// .enable_TMR (enable_TMR),
			.enable_320 (enable_320),
			.tick       (tick)
		);

always @(posedge clk_A  ) begin
	if (rst) begin
		// reset
		fifo_read <= 1'b0;
	end
	else if (~fifo_empty&tick&enable) begin
		fifo_read <= 1'b1;
	end else begin
		fifo_read <= 1'b0;
	end
end

reg [19:0] data_send;

always @(posedge clk_A  ) begin
	if (rst) begin
		// reset
		data_send <= 20'b0;
	end
	else if (fifo_read) begin
		data_send <= enable_320 ? {interface_fifo_data,10'b0} : 
								 {
								  interface_fifo_data[9],interface_fifo_data[8],
								  interface_fifo_data[9],interface_fifo_data[8],
								  interface_fifo_data[7],interface_fifo_data[6],
								  interface_fifo_data[7],interface_fifo_data[6],
								  interface_fifo_data[5],interface_fifo_data[4],
								  interface_fifo_data[5],interface_fifo_data[4],
								  interface_fifo_data[3],interface_fifo_data[2],
								  interface_fifo_data[3],interface_fifo_data[2],
								  interface_fifo_data[1],interface_fifo_data[0],
								  interface_fifo_data[1],interface_fifo_data[0]
								 };
	end else begin
		data_send <= {data_send[17:0],data_send[19:18]};
	end
end


assign	d_line[0] = enable & data_send[18];
assign 	d_line[1] = enable & data_send[19];



endmodule
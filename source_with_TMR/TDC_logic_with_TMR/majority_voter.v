/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : majority_voter.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2018-05-29 14:17:17
//  Note       : 
//     
module majority_voter
#(
	parameter WIDTH=1
)
(
	input [WIDTH-1:0] inA,
	input [WIDTH-1:0] inB,
	input [WIDTH-1:0] inC,
	output [WIDTH-1:0] out,
	output err);

wire [WIDTH-1:0] err_inner;
generate
	genvar i;
	for (i = 0; i < WIDTH; i = i + 1)
	begin:voter_bits
		majority_voter_one_bit inst_majority_voter_one_bit (.inA(inA[i]), .inB(inB[i]), .inC(inC[i]), .out(out[i]), .err(err_inner[i]));
	end
endgenerate
assign err = |err_inner;
endmodule
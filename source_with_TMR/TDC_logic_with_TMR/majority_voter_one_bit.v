/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : majority_voter_one_bit.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on 2019-03-12 14:17:17
//  Note       : 
//     
module majority_voter_one_bit
(
	input  inA,
	input  inB,
	input  inC,
	output reg out,
	output reg err);
	always @(*) begin
		if (inA == inB) begin
			out = inA;
			err = (inB == inC) ? 1'b0 : 1'b1;
		end	else begin
			out = inC;
			err = 1'b1;
		end
	end
endmodule
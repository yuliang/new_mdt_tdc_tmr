/*
Copyright � 2012 JeffLieu-lieumychuong@gmail.com

	This file is part of SGMII-IP-Core.
    SGMII-IP-Core is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SGMII-IP-Core is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SGMII-IP-Core.  If not, see <http://www.gnu.org/licenses/>.

File		:
Description	:	
Remarks		:	
Revision	:
	Date	Author		Description
02/09/12	Jefflieu
*/
//suppress_messages 10036


`include "common_definition.v"
module mEnc8b10bMem_tb_TMR(
	input 	[7:0] i8_Din,		//HGFEDCBA
	input 	i_Kin,
	input 	i_ForceDisparity,
	input 	i_Disparity,		//1 Is negative, 0 is positive	
	output 	[9:0] o10_Dout,	//abcdeifghj
	output 	o_Rd,
	output 	o_KErr,
	input   i_Clk_A,
	input   i_Clk_B,
	input   i_Clk_C,
	// input   i_ARst_L,
    //SP
    input   i_enable,
	input	soft_reset_i
    );
	


    parameter TP = 0;
    parameter pERR = 2'b11;

	
	wire [9:0] 	r10_6bDCode_vote_A;
	wire [9:0] 	r10_6bKCode_vote_A;
	wire [7:0] 	r8_4bDCode_vote_A;
	wire [7:0] 	r8_4bKCode_vote_A;

	reg r_jDisp_A;
	reg [9:0] o10_Dout_A;
	reg r_KErr_A;

	wire  r_jDisp_vote_A;
	wire  w_jDisp_vote_A;
	wire w_Compl6b_vote_A,w_Compl4b_vote_A;
	wire w_K23_vote_A,w_K27_vote_A,w_K28_vote_A,w_K29_vote_A,w_K30_vote_A;	


	wire [9:0] 	r10_6bDCode_vote_B;
	wire [9:0] 	r10_6bKCode_vote_B;
	wire [7:0] 	r8_4bDCode_vote_B;
	wire [7:0] 	r8_4bKCode_vote_B;

	reg r_jDisp_B;
	reg [9:0] o10_Dout_B;
	reg r_KErr_B;

	wire  r_jDisp_vote_B;
	wire  w_jDisp_vote_B;
	wire w_Compl6b_vote_B,w_Compl4b_vote_B;
	wire w_K23_vote_B,w_K27_vote_B,w_K28_vote_B,w_K29_vote_B,w_K30_vote_B;	

	wire [9:0] 	r10_6bDCode_vote_C;
	wire [9:0] 	r10_6bKCode_vote_C;
	wire [7:0] 	r8_4bDCode_vote_C;
	wire [7:0] 	r8_4bKCode_vote_C;

	reg r_jDisp_C;
	reg [9:0] o10_Dout_C;
	reg r_KErr_C;

	wire  r_jDisp_vote_C;
	wire  w_jDisp_vote_C;
	wire w_Compl6b_vote_C,w_Compl4b_vote_C;
	wire w_K23_vote_C,w_K27_vote_C,w_K28_vote_C,w_K29_vote_C,w_K30_vote_C;	
		
////////////////////////////////////////////////////////A Part//////////////////////////////////////////////////////////////////////////////////////



		
	always@(posedge i_Clk_A )	
		if(soft_reset_i == 1'b1) begin
			r_jDisp_A <= #TP 1'b0;
			o10_Dout_A <= #TP 10'b0;
			r_KErr_A <= #TP 1'b0;
		end
		else begin
			if(i_enable) begin 
				r_jDisp_A <= #TP w_jDisp_vote_A;
				if(i_Kin)
					o10_Dout_A <= #TP {r10_6bKCode_vote_A[7:2]^{6{w_Compl6b_vote_A}},r8_4bKCode_vote_A[5:2]^{4{w_Compl4b_vote_A}}};   //if it is a special command symbol
				else 
					o10_Dout_A <= #TP {r10_6bDCode_vote_A[7:2]^{6{w_Compl6b_vote_A}},r8_4bDCode_vote_A[5:2]^{4{w_Compl4b_vote_A}}};   //if it is a data symbol
					r_KErr_A <= #TP i_Kin & ((r10_6bKCode_vote_A[1:0]==pERR)|((w_K23_vote_A|w_K27_vote_A|w_K29_vote_A|w_K30_vote_A)&&(~&i8_Din[7:5])));
			end	
		end

	majority_voter #(.WIDTH(1)) 
		majority_voter_inst_A (
			.inA(r_jDisp_A),
			.inB(r_jDisp_B),
			.inC(r_jDisp_C),
			.out(r_jDisp_vote_A),
			.err());	

	Encode8b10b_combine inst_Encode8b10b_combine_A
		(
			.i8_Din      (i8_Din),
			.r_jDisp     (r_jDisp_vote_A),
			.i_Kin       (i_Kin),
			.w_jDisp     (w_jDisp_vote_A),
			.r10_6bKCode (r10_6bKCode_vote_A),
			.r8_4bKCode  (r8_4bKCode_vote_A),
			.r10_6bDCode (r10_6bDCode_vote_A),
			.r8_4bDCode  (r8_4bDCode_vote_A),		
			.w_Compl6b   (w_Compl6b_vote_A),
			.w_Compl4b   (w_Compl4b_vote_A),
			.w_K23       (w_K23_vote_A),
			.w_K27       (w_K27_vote_A),
			.w_K28       (w_K28_vote_A),
			.w_K29       (w_K29_vote_A),
			.w_K30       (w_K30_vote_A)
		);



////////////////////////////////////////////////////////B Part//////////////////////////////////////////////////////////////////////////////////////	

		
	always@(posedge i_Clk_B )	
		if(soft_reset_i == 1'b1) begin
			r_jDisp_B <= #TP 1'b0;
			o10_Dout_B <= #TP 10'b0;
			r_KErr_B <= #TP 1'b0;
		end
		else begin
			if(i_enable) begin 
				r_jDisp_B <= #TP w_jDisp_vote_B;
				if(i_Kin)
					o10_Dout_B <= #TP {r10_6bKCode_vote_B[7:2]^{6{w_Compl6b_vote_B}},r8_4bKCode_vote_B[5:2]^{4{w_Compl4b_vote_B}}};   //if it is a special command symbol
				else 
					o10_Dout_B <= #TP {r10_6bDCode_vote_B[7:2]^{6{w_Compl6b_vote_B}},r8_4bDCode_vote_B[5:2]^{4{w_Compl4b_vote_B}}};   //if it is a data symbol
					r_KErr_B <= #TP i_Kin & ((r10_6bKCode_vote_B[1:0]==pERR)|((w_K23_vote_B|w_K27_vote_B|w_K29_vote_B|w_K30_vote_B)&&(~&i8_Din[7:5])));
			end	
		end

	majority_voter #(.WIDTH(1)) 
		majority_voter_inst_B (
			.inA(r_jDisp_B),
			.inB(r_jDisp_C),
			.inC(r_jDisp_A),
			.out(r_jDisp_vote_B),
			.err());	

	Encode8b10b_combine inst_Encode8b10b_combine_B
		(
			.i8_Din      (i8_Din),
			.r_jDisp     (r_jDisp_vote_B),
			.i_Kin       (i_Kin),
			.w_jDisp     (w_jDisp_vote_B),
			.r10_6bKCode (r10_6bKCode_vote_B),
			.r8_4bKCode  (r8_4bKCode_vote_B),
			.r10_6bDCode (r10_6bDCode_vote_B),
			.r8_4bDCode  (r8_4bDCode_vote_B),		
			.w_Compl6b   (w_Compl6b_vote_B),
			.w_Compl4b   (w_Compl4b_vote_B),
			.w_K23       (w_K23_vote_B),
			.w_K27       (w_K27_vote_B),
			.w_K28       (w_K28_vote_B),
			.w_K29       (w_K29_vote_B),
			.w_K30       (w_K30_vote_B)
		);


////////////////////////////////////////////////////////C Part//////////////////////////////////////////////////////////////////////////////////////	


	always@(posedge i_Clk_C )	
		if(soft_reset_i == 1'b1) begin
			r_jDisp_C <= #TP 1'b0;
			o10_Dout_C <= #TP 10'b0;
			r_KErr_C <= #TP 1'b0;
		end
		else begin
			if(i_enable) begin 
				r_jDisp_C <= #TP w_jDisp_vote_C;
				if(i_Kin)
					o10_Dout_C <= #TP {r10_6bKCode_vote_C[7:2]^{6{w_Compl6b_vote_C}},r8_4bKCode_vote_C[5:2]^{4{w_Compl4b_vote_C}}};   //if it is a special command symbol
				else 
					o10_Dout_C <= #TP {r10_6bDCode_vote_C[7:2]^{6{w_Compl6b_vote_C}},r8_4bDCode_vote_C[5:2]^{4{w_Compl4b_vote_C}}};   //if it is a data symbol
					r_KErr_C <= #TP i_Kin & ((r10_6bKCode_vote_C[1:0]==pERR)|((w_K23_vote_C|w_K27_vote_C|w_K29_vote_C|w_K30_vote_C)&&(~&i8_Din[7:5])));
			end	
		end

	majority_voter #(.WIDTH(1)) 
		majority_voter_inst_C (
			.inA(r_jDisp_C),
			.inB(r_jDisp_A),
			.inC(r_jDisp_B),
			.out(r_jDisp_vote_C),
			.err());	

	Encode8b10b_combine inst_Encode8b10b_combine_C
		(
			.i8_Din      (i8_Din),
			.r_jDisp     (r_jDisp_vote_C),
			.i_Kin       (i_Kin),
			.w_jDisp     (w_jDisp_vote_C),
			.r10_6bKCode (r10_6bKCode_vote_C),
			.r8_4bKCode  (r8_4bKCode_vote_C),
			.r10_6bDCode (r10_6bDCode_vote_C),
			.r8_4bDCode  (r8_4bDCode_vote_C),		
			.w_Compl6b   (w_Compl6b_vote_C),
			.w_Compl4b   (w_Compl4b_vote_C),
			.w_K23       (w_K23_vote_C),
			.w_K27       (w_K27_vote_C),
			.w_K28       (w_K28_vote_C),
			.w_K29       (w_K29_vote_C),
			.w_K30       (w_K30_vote_C)
		);
////////////////////////////////////////////////////////out Part//////////////////////////////////////////////////////////////////////////////////////	

	assign o_Rd = r_jDisp_vote_C;

	majority_voter #(.WIDTH(1)) 
	majority_voter_inst_KErr (
		.inA(r_KErr_A),
		.inB(r_KErr_B),
		.inC(r_KErr_C),
		.out(o_KErr),
		.err());	


	majority_voter #(.WIDTH(10)) 
	majority_voter_inst_o10_Dout (
		.inA(o10_Dout_A),
		.inB(o10_Dout_B),
		.inC(o10_Dout_C),
		.out(o10_Dout),
		.err());

endmodule

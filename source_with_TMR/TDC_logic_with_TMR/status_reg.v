/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : status_reg_back.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 22st, 2018
//  Note       : 
// 

`include "common_definition.v"
module status_reg
#(parameter
	WIDTH = 1
)
(
	input clk,
	input shift,
	input serial_data_in,
	output serial_data_out,
	input  [WIDTH-1:0] parallel_data_in
);

	reg [WIDTH-1:0] serial_reg;
	assign  serial_data_out = serial_reg[0];
	always @(posedge clk) begin
		if (shift) begin
			serial_reg <= {serial_data_in,serial_reg[WIDTH-1:1]};
		end else begin
			serial_reg <= parallel_data_in;
		end	
	end
endmodule


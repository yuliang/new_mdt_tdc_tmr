/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : chnl_tdc_front.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on July 1st, 2017
//  Note       : 
// 

`include "common_definition.v"
module chnl_tdc_front 
(
  //input  [4  :0]    chnl_id, // channel number
  input  [3  :0]    fine_sel, //encoding for fine time selection
  
  input  [1  :0]    lut0, lut1, lut2, lut3, lut4, lut5, lut6, lut7, lut8, lut9,
                    luta, lutb, lutc, lutd, lute, lutf,
  
  input             channel_enable,
  input             rst,      // reset, not BC reset
  input             edge_mode, // 1: leading; 0: trailing
  input             clk160,
  input             Rdy, // ready
  input  [3  :0]    q, // raw fine codes 
  input  [14 :0]    cnt, cnt_inv, // coarse counter
  
  output data_ready,
  output [16:0] effictive_data,
  output [`FULL_DATA_SIZE-1:0] full_data
);

  reg  [1 :0]            rdy_r;
  wire                   l_rdy_syn;
  reg  		           l_rdy_syn_r;
  
  reg  [14 :0]           cnt_r, cnt_inv_r;
  reg  [14 :0]           cnt_sel, cnt_drop;
  reg  [3:0]             q_r;
  reg  [1:0]             hitnum; // 2-bit hit counter
  wire [1:0]             fine_tdc;
  
  assign data_ready = l_rdy_syn_r;
  //syn. Rdy to clk160 from clk320
  always @( posedge clk160) begin
     if(rst==1'b1)
        rdy_r <= 2'b0;
     else
        rdy_r <= {rdy_r[0], Rdy};
  end
  
  //generate a Rdy pulse with one clk160 width
  assign l_rdy_syn = (rdy_r[0] & (~rdy_r[1]))&channel_enable;
  
  // generate wren
  always @(posedge clk160)
     l_rdy_syn_r <= l_rdy_syn;
  
  
  //latch the TDC channel info.
  always @(posedge clk160) begin
   if(rst == 1'b1) begin
     q_r        <= 4'b0;
     cnt_r      <= 15'b0;
     cnt_inv_r  <= 15'b0; end
   else if(l_rdy_syn == 1'b1) begin
     q_r        <= q[3:0];
     cnt_r      <= cnt;
     cnt_inv_r  <= cnt_inv; end
  end
  
  // 2-bit hit counter
  always @(posedge clk160) begin
   if(rst)  
        hitnum <= 2'b0;
   else if(data_ready) 
        hitnum <= hitnum + 1'b1;
  end
  
  /* fine_sel: 0--> cnt, 1 --> cnt_inv */
  always @(*) begin
   // bin 0
   if({fine_tdc, fine_sel[0] } == 3'b0)  begin 
        cnt_sel  = cnt_r;
        cnt_drop = cnt_inv_r; end
   else if({fine_tdc, fine_sel[0] }== 3'b1) begin
        cnt_sel  = cnt_inv_r;
        cnt_drop = cnt_r; end
  
   // bin 1
   else if({fine_tdc, fine_sel[1] } == 3'b10)  begin 
        cnt_sel  = cnt_r;
        cnt_drop = cnt_inv_r; end
   else if({fine_tdc, fine_sel[1] }== 3'b11) begin
        cnt_sel  = cnt_inv_r;
        cnt_drop = cnt_r; end 
  
   // bin 2
   else if({fine_tdc, fine_sel[2] } == 3'b100)  begin 
        cnt_sel  = cnt_r;
        cnt_drop = cnt_inv_r; end
   else if({fine_tdc, fine_sel[2] }== 3'b101) begin
        cnt_sel  = cnt_inv_r;
        cnt_drop = cnt_r; end 
  
   // bin 3
   else if({fine_tdc, fine_sel[3] } == 3'b110)  begin 
        cnt_sel  = cnt_r;
        cnt_drop = cnt_inv_r; end
   else if({fine_tdc, fine_sel[3] }== 3'b111) begin
        cnt_sel  = cnt_inv_r;
        cnt_drop = cnt_r; end 
  
   else begin
        cnt_sel  =15'h1991;
        cnt_drop =15'h1107; end
  end
  
  assign  effictive_data = {cnt_sel,fine_tdc};
  assign  full_data = {1'b1,edge_mode,hitnum,cnt_drop,q_r[3:0],effictive_data};
  //fine time encoding
  fine_encode2  
    fine_encode_inst(
      .lut0(lut0), .lut1(lut1), .lut2(lut2), .lut3(lut3),
      .lut4(lut4), .lut5(lut5), .lut6(lut6), .lut7(lut7),
      .lut8(lut8), .lut9(lut9), .luta(luta), .lutb(lutb),
      .lutc(lutc), .lutd(lutd), .lute(lute), .lutf(lutf),
      .raw_tdc(q_r[3:0]),
      .fine_tdc(fine_tdc)
    );
endmodule
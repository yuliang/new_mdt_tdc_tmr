/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : chnl_trigger_matching.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on June 30st, 2017
//  Note       : 
// 
`include "common_definition.v"
module chnl_trigger_matching #(
	parameter DATA_WIDTH=18,
	parameter DATA_DEEP	=32,
	parameter CHNL_FIFO_DATA_WIDTH=17
	)
(
    input clk,
    input rst,
    //ring buffer interface
	input [(DATA_WIDTH*DATA_DEEP-1):0] data_in,
    //trigger interface
    input trigger,
    input [11:0] limit_up,
    input [11:0] limit_down,
    output trigger_processing,
    //channel fifo interface
    input chnl_fifo_full,
    output reg chnl_fifo_write,
    output [CHNL_FIFO_DATA_WIDTH-1:0] data_out    
    );

reg [1:0] trigger_r;
wire trigger_inner;
reg [11:0] limit_up_r,limit_down_r;
reg [DATA_WIDTH-1:0] data [DATA_DEEP-1:0];

wire [DATA_DEEP-1:0] data_interest;
reg	 [DATA_DEEP-1:0] data_interest_r;

wire still_exist_interst;
reg trigger_processing_r;

wire [DATA_DEEP-1:0] data_interest_one_hot;
wire [DATA_WIDTH-1:0] data_need_to_write;

reg [CHNL_FIFO_DATA_WIDTH-1:0] data_out_r;


 
//syn trriger
always @(posedge clk) begin
	if (rst) begin
		trigger_r <= 2'b0;
	end
	else begin
		trigger_r <= {trigger_r[0],trigger};	
	end
end

assign  trigger_inner = trigger_r[0]&(~trigger_r[1]);

always @(posedge clk) begin
	if (rst) begin
		limit_up_r 	<= 12'hfff;
		limit_down_r <= 12'b0;
	end
	else if(trigger_inner) begin 
		limit_up_r 	<= limit_up;
		limit_down_r <= limit_down;
	end
end


//lock data

generate
	genvar i;
	for (i = 0; i < DATA_DEEP; i = i + 1)
	begin:data_update_generate
		always @(posedge clk) begin
			if (rst) begin
				data[i] <= {DATA_WIDTH{1'b0}}; 
			end
			else if (trigger_inner) begin
				data[i] <= data_in[(i+1)*DATA_WIDTH-1:i*DATA_WIDTH];
			end
		end
		
	end
endgenerate

//select interest data
generate
	genvar i0;
	for (i0 = 0; i0 < DATA_DEEP; i0 = i0 + 1)
	begin:data_interest_selector_generate
		data_interest_selector #(.DATA_WIDTH(DATA_WIDTH))data_interest_selector_inst(
			.limit_up(limit_up_r),
			.limit_down(limit_down_r),
			.data_in(data[i0]),
			.interest(data_interest[i0])
			);
	end
endgenerate

wire [(DATA_WIDTH*DATA_DEEP-1):0] data_inner;
generate
	genvar i1;
	for (i1 = 0; i1 < DATA_DEEP; i1 = i1 + 1)
	begin:data_assign
		assign data_inner[(i1+1)*DATA_WIDTH-1:i1*DATA_WIDTH] = data[i1];		
	end
endgenerate



assign still_exist_interst = (|data_interest_r)|(chnl_fifo_write&chnl_fifo_full);





reg [`TRIGGER_MATCHING_SIZE] state;
reg [`TRIGGER_MATCHING_SIZE] nextstate;
localparam IDLE=`CHNL_TRIGGER_MATCHING_IDLE;
localparam UPDATE_DATA=`CHNL_TRIGGER_MATCHING_UPDATE_DATA;
localparam WAIT_FOR_INTEREST_DATA=`CHNL_TRIGGER_MATCHING_WAIT_FOR_INTEREST_DATA;
localparam SEND_DATA_TOFIFO=`CHNL_TRIGGER_MATCHING_SEND_DATA_TOFIFO;
localparam WAIT_FIFO=`CHNL_TRIGGER_MATCHING_WAIT_FIFO;

always @(posedge clk) begin
	if (rst) begin
		state <= IDLE;
	end
	else begin
		state <= nextstate;
	end
end

always @(*) begin
	case(state)
		IDLE: 
			nextstate = trigger_inner ? UPDATE_DATA : IDLE;
		UPDATE_DATA: 
			nextstate = WAIT_FOR_INTEREST_DATA;
		WAIT_FOR_INTEREST_DATA:
			if(~still_exist_interst)begin
				nextstate = IDLE;
			end else if((~chnl_fifo_full)&still_exist_interst) begin
		 		nextstate = SEND_DATA_TOFIFO;
		 	end else if(chnl_fifo_full&still_exist_interst) begin
		 		nextstate = WAIT_FIFO;
		 	end else begin
		 		nextstate = IDLE;
		 	end
		SEND_DATA_TOFIFO: 
			if(~still_exist_interst) begin
				nextstate = IDLE;
			end else if((~chnl_fifo_full)&still_exist_interst)begin
				nextstate = SEND_DATA_TOFIFO;
			end else if(chnl_fifo_full&still_exist_interst)begin
				nextstate = WAIT_FIFO;
			end else begin
				nextstate = IDLE;
			end
		WAIT_FIFO:
			if(chnl_fifo_full)begin 
				nextstate = WAIT_FIFO;
			end else begin
				nextstate = SEND_DATA_TOFIFO;
			end 
		default: nextstate = IDLE;
	endcase
end

multi_hot_one_hot #(.WIDTH(DATA_DEEP)) 
multi_hot_one_hot_inst(
	.multi_hot_code(data_interest_r),
	.one_hot_code(data_interest_one_hot)
	);

one_hot_select #(.DATA_WIDTH(DATA_WIDTH),.DATA_DEEP(DATA_DEEP))
one_hot_select_inst(
	.one_hot_code(data_interest_one_hot),
	.data_in(data_inner),
	.data_out(data_need_to_write)
	);

always @(posedge clk) begin
	if (rst) begin
		data_interest_r <= {DATA_DEEP{1'b0}};		
	end	else if(nextstate[`CHNL_TRIGGER_MATCHING_WAIT_FOR_INTEREST_DATA_bit]) begin
		data_interest_r <= data_interest;
	end else if(nextstate[`CHNL_TRIGGER_MATCHING_SEND_DATA_TOFIFO_bit]&
				~state[`CHNL_TRIGGER_MATCHING_WAIT_FIFO_bit]) begin
		data_interest_r <= data_interest_r&(~data_interest_one_hot);
	end else begin
		data_interest_r <= data_interest_r;
	end
end

//always @(*) begin
//	if ((state[`CHNL_TRIGGER_MATCHING_SEND_DATA_TOFIFO_bit]|
//		 state[`CHNL_TRIGGER_MATCHING_WAIT_FIFO_bit]|
//		 state[`CHNL_TRIGGER_MATCHING_SEND_MISS_DATA_bit])
//		&&(~chnl_fifo_full)) begin
//		chnl_fifo_write <= 1'b1;
//	end else begin
//		chnl_fifo_write <= 1'b0;
//	end
//end

always @(posedge clk) begin
	if (nextstate[`CHNL_TRIGGER_MATCHING_SEND_DATA_TOFIFO_bit]) begin
		chnl_fifo_write <= 1'b1;
	end else begin
		chnl_fifo_write <= 1'b0;
	end
end

always @(posedge clk) begin
	if (rst) begin
		data_out_r <= `CHNL_FIFO_DATA_WIDTH'b0;		
	end
	else if (nextstate[`CHNL_TRIGGER_MATCHING_SEND_DATA_TOFIFO_bit]&
			 ~state[`CHNL_TRIGGER_MATCHING_WAIT_FIFO_bit]) begin
		data_out_r <= data_need_to_write[CHNL_FIFO_DATA_WIDTH-1:0];
	end
end
assign data_out = data_out_r;


assign trigger_processing = trigger_processing_r;
always @(posedge clk) begin
	if (rst) begin
		trigger_processing_r <= 1'b0;		
	end
	else begin
		trigger_processing_r <= (~state[`CHNL_TRIGGER_MATCHING_IDLE_bit])|trigger_r[0]|trigger_r[1]|trigger;
	end
end


endmodule
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : fifomem4.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 13st, 2018
//  Note       : FIFO buffer memory array
// 


module fifomem4 #(parameter DATASIZE = 17, // Memory data word width
parameter ADDRSIZE = 2) // Number of mem address bits
(
output reg [DATASIZE-1:0] rdata,
input [DATASIZE-1:0] wdata,
input [ADDRSIZE-1:0] waddr, raddr,
input wclken, wclk
);

localparam DEPTH = 1<<ADDRSIZE;
reg [DATASIZE-1:0] mem [0:DEPTH-1];
always @(*) begin
	case(raddr)
	2'b00:rdata = mem[0];
	2'b01:rdata = mem[1];
	2'b10:rdata = mem[2];
	2'b11:rdata = mem[3];
	endcase
end

always @(posedge wclk) begin
	if (wclken) begin
		case(waddr)
		2'b00:mem[0] <= wdata;
		2'b01:mem[1] <= wdata;
		2'b10:mem[2] <= wdata;
		2'b11:mem[3] <= wdata;
		endcase		
	end
end


endmodule
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : fifo_width.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 29st, 2018
//  Note       : Top-level Verilog code for the FIFO 
//  Function   :
//               This is a asynchronous FIFO with a depth of 1
//               This fifo is referenced to:
//       "Simulation and Synthesis Techniques for Asynchronous FIFO Design"
//       by Clifford E. Cummings, @ Sunburst Design, Inc.
//       using gray counter to crossing time area


module fifo_width #(parameter DSIZE = 10,parameter ASIZE = 1,SYN_DEPTH = 2)
(
output [DSIZE-1:0] rdata,
output wfull,
output rempty,
input [DSIZE-1:0] wdata,
input winc, wclk, wrst,
input rinc, rclk, rrst
);
reg [DSIZE*2-1:0] mem;
reg [DSIZE*2-1:0] mem_r;
reg empty; 
reg empty_out;assign rempty = empty_out;
reg [1:0] full; assign wfull = &full;
always @(posedge wclk ) begin
	if (wrst) begin
		// reset
		full <= 2'b0;
	end	else if ((~&full)&winc) begin
		full <= {full[0],1'b1};
	end else if (empty_syn[1]) begin
		full <= 2'b0;
	end 
end

always @(posedge wclk ) begin
	if((~&full)&winc) begin
		if(~full[0]) begin
			mem[DSIZE-1:0] <= wdata;
		end else begin
			mem[DSIZE*2-1:DSIZE] <= wdata;
		end		
	end
end

reg[1:0] full_syn;
always @(posedge rclk ) begin
	if (rrst) begin
		full_syn <= 2'b0;
	end	else begin
		full_syn <= {full_syn[0],full[1]};
	end
end
reg high;
always @(posedge rclk) begin
	if (rrst) begin
		high <= 'b0;
	end else if(full_syn[1]&(empty_out)) begin
		high <= 'b0;
	end	else if(rinc) begin
		high <= ~high;
	end
end

always @(posedge rclk ) begin
	if (rrst) begin
		empty <= 1'b1;
		mem_r <= 'b0;
	end	else if(full_syn[1]&(empty_out)) begin
		empty <= 1'b0;
		mem_r <= mem;
	end else if(rinc&(~high)) begin
		empty <= 1'b1;
		mem_r <= {mem_r[DSIZE-1:0],mem_r[DSIZE*2-1:DSIZE]};
	end
end

always @(posedge rclk ) begin
	if (rrst) begin
		empty_out <= 1'b1;
	end	else if(full_syn[1]&(empty_out)) begin
		empty_out <= 1'b0;
	end else if(rinc&(high)) begin
		empty_out <= 1'b1;
	end
end

assign  rdata = mem_r[DSIZE-1:0];

reg[1:0] empty_syn;
always @(posedge wclk ) begin
	if (rrst) begin
		empty_syn <= 2'b1;
	end	else begin
		empty_syn <= {empty_syn[0],empty};
	end
end



endmodule
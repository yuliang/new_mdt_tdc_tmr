/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : wptr_full_one_depth.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 27st, 2018
//  Note       : the write pointer and full flag logic
// 


module wptr_full_one_depth 
(
output reg wfull,
output reg  wptr,
input  wq2_rptr,
input winc, wclk, wrst
);
reg wbin;
wire wgraynext, wbinnext;
// GRAYSTYLE2 pointer
always @(posedge wclk)
	if (wrst)  wptr <=  0;
	else  wptr <=  wgraynext;
// Memory write-address pointer (okay to use binary to address memory)
assign  wgraynext = wptr + (winc & ~wfull);
//------------------------------------------------------------------
// Simplified version of the three necessary full-tests:
// assign wfull_val=((wgnext[ADDRSIZE] !=wq2_rptr[ADDRSIZE] ) &&
// (wgnext[ADDRSIZE-1] !=wq2_rptr[ADDRSIZE-1]) &&
// (wgnext[ADDRSIZE-2:0]==wq2_rptr[ADDRSIZE-2:0]));
//------------------------------------------------------------------
assign  wfull_val = (wgraynext==(~wq2_rptr));
always @(posedge wclk)
	if (wrst) wfull <=  1'b0;
	else wfull <=  wfull_val;
endmodule
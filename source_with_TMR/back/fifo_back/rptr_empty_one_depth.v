/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : rptr_empty_one_depth.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 27st, 2018
//  Note       : the read pointer and empty flag logic
// 


module rptr_empty_one_depth 
(
output reg rempty,
output reg  rptr,
input rq2_wptr,
input rinc, rclk, rrst
);
reg rbin;
wire rgraynext, rbinnext;
//-------------------
// GRAYSTYLE2 pointer
//-------------------
always @(posedge rclk) begin
	if (rrst) rptr <= 1'b0;
	else rptr <=  rgraynext;
end
// Memory read-address pointer (okay to use binary to address memory)
assign rgraynext = rptr + (rinc & ~rempty);
//---------------------------------------------------------------
// FIFO empty when the next rptr == synchronized wptr or on reset
//---------------------------------------------------------------
assign rempty_val = (rgraynext == rq2_wptr);
always @(posedge rclk)
	if (rrst) rempty <= 1'b1;
	else rempty <= rempty_val;
endmodule
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : fifo_exchange.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 29st, 2018
//  Note       : Top-level Verilog code for the FIFO 
//  Function   :
//               This is a asynchronous FIFO with a depth of 1
//               This fifo is referenced to:
//       "Simulation and Synthesis Techniques for Asynchronous FIFO Design"
//       by Clifford E. Cummings, @ Sunburst Design, Inc.
//       using gray counter to crossing time area


module fifo_exchange #(parameter DSIZE = 10,parameter ASIZE = 2,SYN_DEPTH = 2)
(
output [DSIZE-1:0] rdata,
output wfull,
output rempty,
input [DSIZE-1:0] wdata,
input winc, wclk, wrst,
input rinc, rclk, rrst
);
reg [DSIZE-1:0] mem;
reg empty; assign rempty = empty;
reg full; assign wfull = full;
always @(posedge wclk ) begin
	if (wrst) begin
		// reset
		full <= 1'b0;
	end	else if (~full&winc) begin
		full <= 1'b1;
	end else if (empty) begin
		full <= 1'b0;
	end 
end

always @(posedge wclk ) begin
	if(~full&winc) begin
		mem <= wdata;
	end
end
assign rdata = mem;
always @(posedge rclk ) begin
	if (rrst) begin
		// reset
		empty <= 1'b1;
	end	else if (rinc|(|rinc_r)) begin
		empty <= 1'b1;
	end else if(full) begin
		empty <= 1'b0;
	end
end

reg[1:0] rinc_r;
always @(posedge rclk ) begin
	if (rrst) begin
		// reset
		rinc_r <= 2'b0;
	end
	else begin
		rinc_r <= {rinc_r[0],rinc};
	end
end

endmodule
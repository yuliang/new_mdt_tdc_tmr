/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : fifo_one_depth.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on April 12st, 2017
//  Note       : Top-level Verilog code for the FIFO 
//  Function   :
//               This is a asynchronous FIFO with a depth of 1
//               This fifo is referenced to:
//       "Simulation and Synthesis Techniques for Asynchronous FIFO Design"
//       by Clifford E. Cummings, @ Sunburst Design, Inc.
//       using gray counter to crossing time area


module fifo_one_depth #(parameter DSIZE = 17,SYN_DEPTH = 2)
(
output [DSIZE-1:0] rdata,
output wfull,
output rempty,
input [DSIZE-1:0] wdata,
input winc, wclk, wrst,
input rinc, rclk, rrst
);
wire  wptr, rptr, wq2_rptr, rq2_wptr;
sync_DFFs #(.WIDTH(1),.SYN_DEPTH(SYN_DEPTH)) sync_r2w (
.data_out(wq2_rptr), .data_in(rptr),
.clk(wclk), .rst(wrst));
sync_DFFs #(.WIDTH(1),.SYN_DEPTH(SYN_DEPTH)) sync_w2r (
.data_out(rq2_wptr), .data_in(wptr),
.clk(rclk), .rst(rrst));
fifomem_one_depth #(.DATASIZE(DSIZE)) fifomem_syn(
.rdata(rdata), .wdata(wdata),
.wclken(winc&&(!wfull)), .wclk(wclk));
rptr_empty_one_depth #(.ADDRSIZE(ASIZE)) rptr_empty_inst(
.rempty(rempty),
.rptr(rptr), .rq2_wptr(rq2_wptr),
.rinc(rinc), .rclk(rclk),
.rrst(rrst));
wptr_full_one_depth #(.ADDRSIZE(ASIZE)) wptr_full_inst(
.wfull(wfull), 
.wptr(wptr), .wq2_rptr(wq2_rptr),
.winc(winc), .wclk(wclk),
.wrst(wrst));

endmodule
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : fifomem_param.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on April 12st, 2017
//  Note       : FIFO buffer memory array
// 


module fifomem_param #(parameter DATASIZE = 17, // Memory data word width
parameter ADDRSIZE = 2) // Number of mem address bits
(
output[DATASIZE-1:0] rdata,
input [DATASIZE-1:0] wdata,
input [ADDRSIZE-1:0] waddr, raddr,
input wclken, wclk
);

localparam DEPTH = 1<<ADDRSIZE;
localparam DEPTH_1 = DEPTH-1;
`ifdef FIFO_param_behave1
	reg [DATASIZE*DEPTH-1:0] mem;
	wire [DEPTH-1:0] read_one_hot;
	assign read_one_hot = ({{DEPTH_1{1'b0}},1'b1}<<raddr);
	
	one_hot_select #(.DATA_WIDTH(DATASIZE), .DATA_DEEP(DEPTH))
	one_hot_select_fifo_inst(
	    .one_hot_code(read_one_hot),
	    .data_in(mem),
	    .data_out(rdata)
	    );
	
	
	wire [DEPTH-1:0] write_one_hot;
	assign write_one_hot = ({{DEPTH_1{1'b0}},1'b1}<<waddr);
	generate
		genvar k;
		for (k = 0; k < DEPTH; k = k + 1)
		begin:write_block
			always @(posedge wclk) begin
				if (wclken& write_one_hot[k]) begin
					mem[((k+1)*DATASIZE-1):(k*DATASIZE)] <= wdata;		
				end
			end
		end
	endgenerate
`else 
	reg [DATASIZE-1:0] mem [DEPTH-1:0];
	assign rdata = mem[raddr];
	always @(posedge wclk) begin
		if (wclken) begin
				mem[waddr] <= wdata	;	
		end
	end
`endif

endmodule
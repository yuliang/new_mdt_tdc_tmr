/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : fifomem.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 27st, 2018
//  Note       : FIFO buffer memory array
// 


module fifomem_one_depth #(parameter DATASIZE = 17) // Memory data word width
(
output reg [DATASIZE-1:0] rdata,
input [DATASIZE-1:0] wdata,
input wclken, wclk
);


reg [DATASIZE-1:0] mem;
assign rdata = mem;

always @(posedge wclk) begin
	if (wclken) begin
		mem <= wdata;
	end
end

endmodule
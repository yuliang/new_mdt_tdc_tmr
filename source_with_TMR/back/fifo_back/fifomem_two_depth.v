/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : fifomem_two_depth.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 13st, 2018
//  Note       : FIFO buffer memory array
// 


module fifomem_two_depth #(parameter DATASIZE = 17, // Memory data word width
parameter ADDRSIZE = 1) // Number of mem address bits
(
output reg [DATASIZE-1:0] rdata,
input [DATASIZE-1:0] wdata,
input [ADDRSIZE-1:0] waddr, raddr,
input wclken, wclk
);

localparam DEPTH = 1<<ADDRSIZE;
reg [DATASIZE-1:0] mem [0:DEPTH-1];
always @(*) begin
	case(raddr)
	1'b0:rdata = mem[0];
	1'b1:rdata = mem[1];
	endcase
end

always @(posedge wclk) begin
	if (wclken) begin
		case(waddr)
		1'b0:mem[0] <= wdata;
		1'b1:mem[1] <= wdata;
		endcase		
	end
end


endmodule
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : fifo_no_syn.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 29st, 2018
//  Note       : Top-level Verilog code for the FIFO 
//  Function   :
//               This is a asynchronous FIFO with a depth of 1
//               This fifo is referenced to:
//       "Simulation and Synthesis Techniques for Asynchronous FIFO Design"
//       by Clifford E. Cummings, @ Sunburst Design, Inc.
//       using gray counter to crossing time area


module fifo_no_syn #(parameter DSIZE = 10,parameter ASIZE = 1,SYN_DEPTH = 2)
(
output [DSIZE-1:0] rdata,
output reg wfull,
output reg rempty,
input [DSIZE-1:0] wdata,
input winc, wclk, wrst,
input rinc, rclk, rrst
);
reg [DSIZE-1:0] mem;
reg wfull_second;
always @(posedge rclk) begin
	if (rrst) begin
		// reset
		wfull_second <=1'b0;
	end	else if (rempty) begin
		wfull_second <= 1'b1;
	end else begin
		wfull_second <= 1'b0;
	end
end

always @(posedge rclk) begin
	if (rrst) begin
		// reset
		rempty <= 1'b1;
	end	else if (rinc) begin
		rempty <= 1'b1;
	end else if(wfull_second&(rempty))begin
		rempty <= 1'b0;
	end
end

always @(posedge wclk) begin
	if (wrst) begin
		// reset
		wfull <= 1'b0;
	end
	else  begin
		wfull <= ~rempty;
	end
end


always @(posedge rclk) begin
	if (rrst) begin
		// reset
		mem <='b0;
	end	else if (rinc) begin
		mem <=wdata;
	end
end

assign rdata = mem;
endmodule
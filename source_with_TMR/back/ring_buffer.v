/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : ring_buffer.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on June 30st, 2017
//  Note       : 
// 
`include "common_definition.v"
module ring_buffer #(
	parameter DATA_WIDTH=18,
	parameter DATA_DEEP	=32
	)
(
    input clk,
    input rst,
    input enable,
    input [DATA_WIDTH-1:0] data_in,
    output [(DATA_WIDTH*DATA_DEEP-1):0] data_out
    );
reg [DATA_WIDTH-1:0] data [DATA_DEEP-1:0];

always @(posedge clk) begin
	if (rst) begin
		data[0] <= {DATA_WIDTH{1'b0}}; 		
	end
	else if (enable) begin
		data[0] <= data_in;
	end
end


generate
	genvar i0;
	for (i0 = 1; i0 < DATA_DEEP; i0 = i0 + 1)
	begin:ring_buffer_data_generate
		always @(posedge clk) begin
			if (rst) begin				
				data[i0] <= {DATA_WIDTH{1'b0}}; 
			end
			else if (enable) begin
				data[i0] <= data[i0-1];
			end
		end
	end
endgenerate

generate
	genvar i1;
	for (i1 = 0; i1 < DATA_DEEP; i1 = i1 + 1)
	begin:ring_buffer_data_out_generate
		assign data_out[(i1+1)*DATA_WIDTH-1:i1*DATA_WIDTH]= data[i1];
	end
endgenerate


endmodule
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : multi_hot_select.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on June 30st, 2017
//  Note       : 
// 
`include "common_definition.v"
module multi_hot_select #(
	parameter DATA_WIDTH=18,
	parameter DATA_DEEP=32
	)
(
    input [DATA_DEEP-1:0] multi_hot_code,
    input [(DATA_WIDTH*DATA_DEEP-1):0] data_in,
    output reg [DATA_WIDTH-1:0] data_out
    );
always @(*) begin
	case(multi_hot_code)
	(32'h8xxx_xxxx): data_out = data_in[31*DATA_WIDTH-1:30*DATA_WIDTH];
	(32'h4xxx_xxxx): data_out = data_in[30*DATA_WIDTH-1:29*DATA_WIDTH];   
	(32'h2xxx_xxxx): data_out = data_in[29*DATA_WIDTH-1:28*DATA_WIDTH]; 
	(32'h1xxx_xxxx): data_out = data_in[28*DATA_WIDTH-1:27*DATA_WIDTH];
	(32'h08xx_xxxx): data_out = data_in[27*DATA_WIDTH-1:26*DATA_WIDTH];
	(32'h04xx_xxxx): data_out = data_in[26*DATA_WIDTH-1:25*DATA_WIDTH];
	(32'h02xx_xxxx): data_out = data_in[25*DATA_WIDTH-1:24*DATA_WIDTH];
	(32'h01xx_xxxx): data_out = data_in[24*DATA_WIDTH-1:23*DATA_WIDTH];
	(32'h008x_xxxx): data_out = data_in[23*DATA_WIDTH-1:22*DATA_WIDTH];
	(32'h004x_xxxx): data_out = data_in[22*DATA_WIDTH-1:21*DATA_WIDTH];
	(32'h002x_xxxx): data_out = data_in[21*DATA_WIDTH-1:21*DATA_WIDTH];
	(32'h001x_xxxx): data_out = data_in[21*DATA_WIDTH-1:20*DATA_WIDTH];
	(32'h0008_xxxx): data_out = data_in[20*DATA_WIDTH-1:19*DATA_WIDTH];
	(32'h0004_xxxx): data_out = data_in[19*DATA_WIDTH-1:18*DATA_WIDTH];
	(32'h0002_xxxx): data_out = data_in[18*DATA_WIDTH-1:17*DATA_WIDTH];
	(32'h0001_xxxx): data_out = data_in[17*DATA_WIDTH-1:16*DATA_WIDTH];
	(32'h0000_8xxx): data_out = data_in[16*DATA_WIDTH-1:15*DATA_WIDTH];
	(32'h0000_4xxx): data_out = data_in[15*DATA_WIDTH-1:14*DATA_WIDTH];
	(32'h0000_2xxx): data_out = data_in[14*DATA_WIDTH-1:13*DATA_WIDTH];
	(32'h0000_1xxx): data_out = data_in[13*DATA_WIDTH-1:12*DATA_WIDTH];
	(32'h0000_08xx): data_out = data_in[12*DATA_WIDTH-1:11*DATA_WIDTH];
	(32'h0000_04xx): data_out = data_in[11*DATA_WIDTH-1:10*DATA_WIDTH];
	(32'h0000_02xx): data_out = data_in[10*DATA_WIDTH-1:9*DATA_WIDTH];
	(32'h0000_01xx): data_out = data_in[9*DATA_WIDTH-1:8*DATA_WIDTH];
	(32'h0000_008x): data_out = data_in[8*DATA_WIDTH-1:7*DATA_WIDTH];
	(32'h0000_004x): data_out = data_in[7*DATA_WIDTH-1:6*DATA_WIDTH];
	(32'h0000_002x): data_out = data_in[6*DATA_WIDTH-1:5*DATA_WIDTH];
	(32'h0000_001x): data_out = data_in[5*DATA_WIDTH-1:4*DATA_WIDTH];
	(32'h0000_0008): data_out = data_in[4*DATA_WIDTH-1:3*DATA_WIDTH];
	(32'h0000_0004): data_out = data_in[3*DATA_WIDTH-1:2*DATA_WIDTH];
	(32'h0000_0002): data_out = data_in[2*DATA_WIDTH-1:DATA_WIDTH];
	(32'h0000_0001): data_out = data_in[DATA_WIDTH-1:0];	
	default 		 data_out = {DATA_WIDTH{1'b0}};
	endcase
end
endmodule
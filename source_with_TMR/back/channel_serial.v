/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : channel_serial.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 5st, 2018
//  Note       : 
//     
`include "common_definition.v"
module channel_serial
#(parameter
    CHANNEL_NUMBER=24,
    CHANNEL_DATA_WIDTH=45
)
(
    input clk,
    input rst,


    input channel_data_debug,
	input enable_trigger,
	input enable_leading,
	input enable_pair,
	input [11:0] roll_over,
	input full_width_res,
	input [2:0] width_select,  


    input [CHANNEL_DATA_WIDTH*CHANNEL_NUMBER-1:0] channel_data,
    input [CHANNEL_NUMBER-1:0] channel_fifo_full,
    input [CHANNEL_NUMBER-1:0]  channel_fifo_empty,
    output[CHANNEL_NUMBER-1:0] channel_fifo_read,
    //for trigger
    input [CHANNEL_NUMBER -1 : 0] trigger_processing,
    output  channel_fifo_clear,

    input trigger_fifo_empty,
	input[11:0] trigger_event_id,
	input[11:0] trigger_bunch_id,
	input[(24-`TRIGGER_MATCHING_HIT_COUNT_SIZE)-1:0] error_flag,

	output  get_trigger,

    input read_out_fifo_full,
	output read_out_fifo_write,
	output [`READ_OUT_FIFO_SIZE-1:0] read_out_fifo_data
);

wire data_ready_triggerless;
wire [CHANNEL_DATA_WIDTH-1:0] data_out_triggerless;
wire [CHANNEL_NUMBER-1:0] channel_fifo_read_triggerless;
triggerless_channel_mux #(.CHANNEL_NUMBER(CHANNEL_NUMBER), .CHANNEL_DATA_WIDTH(CHANNEL_NUMBER))
	triggerless_channel_mux_inst(
    	.clk(clk),
    	.rst(rst),
    	.enable(~enable_trigger),
    	.channel_data_debug(channel_data_debug),

    	.channel_data(channel_data),
    	.channel_fifo_full(channel_fifo_full),
    	.channel_fifo_empty(channel_fifo_empty),
    	.channel_fifo_read(channel_fifo_read_triggerless),

    	.read_out_fifo_full(read_out_fifo_full),    

    	.data_ready(data_ready_triggerless),
    	.data_out(data_out_triggerless)
	);


wire data_ready_trigger;
wire [CHANNEL_DATA_WIDTH-1:0] data_out_trigger;
wire [CHANNEL_NUMBER-1:0] channel_fifo_read_trigger;
wire channel_fifo_clear_trigger;
assign  channel_fifo_clear=channel_fifo_clear_trigger & enable_trigger ;
wire get_trigger_inner;
assign get_trigger = get_trigger_inner & enable_trigger;
wire trigger_head,trigger_trail;
trigger_event_builder#(.CHANNEL_NUMBER(CHANNEL_NUMBER), .CHANNEL_DATA_WIDTH(CHANNEL_NUMBER))
	trigger_event_builder_inst(
		.clk(clk),
		.rst(rst),

		.enable(enable_trigger),

		.trigger_fifo_empty(trigger_fifo_empty),
		.trigger_event_id(trigger_event_id),
		.trigger_bunch_id(trigger_bunch_id),
		.error_flag(error_flag),

		.trigger_processing(trigger_processing),
		.chnl_fifo_empty(channel_fifo_empty),
		.chnl_fifo_read(channel_fifo_read_trigger),
		.chnl_data(channel_data),
		.channel_fifo_clear(channel_fifo_clear_trigger),

		.get_trigger(get_trigger_inner),	

		.read_out_fifo_full(read_out_fifo_full),

    	.data_ready(data_ready_trigger),
    	.data_out(data_out_trigger),
    	.trigger_head(trigger_head),
    	.trigger_trail(trigger_trail)
);

mux #(.WIDTH(CHANNEL_DATA_WIDTH+1))
	mux_trigger_triggerless_inst(
 		.a({data_ready_trigger,data_out_trigger}),
		.b({data_ready_triggerless,data_out_triggerless}),
		.out({data_ready,data_in}),
		.sel(enable_trigger)
	);

data_recombine_process
	data_recombine_process_inst(
		.clk(clk),
		.rst(rst),

		.channel_data_debug(channel_data_debug),

		.data_ready(data_ready),
		.data_in(data_in),
		.read_out_fifo_full(read_out_fifo_full),
		.read_out_fifo_write(read_out_fifo_write),
		.read_out_fifo_data(read_out_fifo_data),

		.enable_trigger(enable_trigger),
		.enable_leading(enable_leading),
		.enable_pair(enable_pair),
		.roll_over(roll_over),
		.full_width_res(full_width_res),
		.width_select(width_select),  

		.trigger_head(trigger_head&enable_trigger),
		.trigger_trail(trigger_trail&enable_trigger)
);

endmodule 
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : one_hot_select.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on June 30st, 2017
//  Note       : 
// 
`include "common_definition.v"
module one_hot_select #(
	parameter DATA_WIDTH=18,
	parameter DATA_DEEP=32
	)
(
    input [DATA_DEEP-1:0] one_hot_code,
    input [(DATA_WIDTH*DATA_DEEP-1):0] data_in,
    output reg [DATA_WIDTH-1:0] data_out
    );
`ifdef CASE_ONE_HOT_SELECT
always @(*) begin
	case(1'b1)
	(one_hot_code[31]):  data_out = data_in[31*DATA_WIDTH-1:30*DATA_WIDTH];
	(one_hot_code[30]):  data_out = data_in[30*DATA_WIDTH-1:29*DATA_WIDTH];   
	(one_hot_code[29]):  data_out = data_in[29*DATA_WIDTH-1:28*DATA_WIDTH]; 
	(one_hot_code[28]):  data_out = data_in[28*DATA_WIDTH-1:27*DATA_WIDTH];
	(one_hot_code[27]):  data_out = data_in[27*DATA_WIDTH-1:26*DATA_WIDTH];
	(one_hot_code[26]):  data_out = data_in[26*DATA_WIDTH-1:25*DATA_WIDTH];
	(one_hot_code[25]):  data_out = data_in[25*DATA_WIDTH-1:24*DATA_WIDTH];
	(one_hot_code[24]):  data_out = data_in[24*DATA_WIDTH-1:23*DATA_WIDTH];
	(one_hot_code[23]):  data_out = data_in[23*DATA_WIDTH-1:22*DATA_WIDTH];
	(one_hot_code[22]):  data_out = data_in[22*DATA_WIDTH-1:21*DATA_WIDTH];
	(one_hot_code[21]):  data_out = data_in[21*DATA_WIDTH-1:21*DATA_WIDTH];
	(one_hot_code[20]):  data_out = data_in[21*DATA_WIDTH-1:20*DATA_WIDTH];
	(one_hot_code[19]):  data_out = data_in[20*DATA_WIDTH-1:19*DATA_WIDTH];
	(one_hot_code[18]):  data_out = data_in[19*DATA_WIDTH-1:18*DATA_WIDTH];
	(one_hot_code[17]):  data_out = data_in[18*DATA_WIDTH-1:17*DATA_WIDTH];
	(one_hot_code[16]):  data_out = data_in[17*DATA_WIDTH-1:16*DATA_WIDTH];
	(one_hot_code[15]):  data_out = data_in[16*DATA_WIDTH-1:15*DATA_WIDTH];
	(one_hot_code[14]):  data_out = data_in[15*DATA_WIDTH-1:14*DATA_WIDTH];
	(one_hot_code[13]):  data_out = data_in[14*DATA_WIDTH-1:13*DATA_WIDTH];
	(one_hot_code[12]):  data_out = data_in[13*DATA_WIDTH-1:12*DATA_WIDTH];
	(one_hot_code[11]):  data_out = data_in[12*DATA_WIDTH-1:11*DATA_WIDTH];
	(one_hot_code[10]):  data_out = data_in[11*DATA_WIDTH-1:10*DATA_WIDTH];
	(one_hot_code[9]) :  data_out = data_in[10*DATA_WIDTH-1:9*DATA_WIDTH];
	(one_hot_code[8]) :  data_out = data_in[9*DATA_WIDTH-1:8*DATA_WIDTH];
	(one_hot_code[7]) :  data_out = data_in[8*DATA_WIDTH-1:7*DATA_WIDTH];
	(one_hot_code[6]) :  data_out = data_in[7*DATA_WIDTH-1:6*DATA_WIDTH];
	(one_hot_code[5]) :  data_out = data_in[6*DATA_WIDTH-1:5*DATA_WIDTH];
	(one_hot_code[4]) :  data_out = data_in[5*DATA_WIDTH-1:4*DATA_WIDTH];
	(one_hot_code[3]) :  data_out = data_in[4*DATA_WIDTH-1:3*DATA_WIDTH];
	(one_hot_code[2]) :  data_out = data_in[3*DATA_WIDTH-1:2*DATA_WIDTH];
	(one_hot_code[1]) :  data_out = data_in[2*DATA_WIDTH-1:DATA_WIDTH];
	(one_hot_code[0]) :  data_out = data_in[DATA_WIDTH-1:0];	
	default : data_out = {DATA_WIDTH{'bx}};
end
`else
	wire [DATA_WIDTH-1:0] data_out_31; assign data_out_31  = data_in[32*DATA_WIDTH-1:31*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[31]}};
	wire [DATA_WIDTH-1:0] data_out_30; assign data_out_30  = data_in[31*DATA_WIDTH-1:30*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[30]}};  
	wire [DATA_WIDTH-1:0] data_out_29; assign data_out_29  = data_in[30*DATA_WIDTH-1:29*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[29]}}; 
	wire [DATA_WIDTH-1:0] data_out_28; assign data_out_28  = data_in[29*DATA_WIDTH-1:28*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[28]}};
	wire [DATA_WIDTH-1:0] data_out_27; assign data_out_27  = data_in[28*DATA_WIDTH-1:27*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[27]}};
	wire [DATA_WIDTH-1:0] data_out_26; assign data_out_26  = data_in[27*DATA_WIDTH-1:26*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[26]}};
	wire [DATA_WIDTH-1:0] data_out_25; assign data_out_25  = data_in[26*DATA_WIDTH-1:25*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[25]}};
	wire [DATA_WIDTH-1:0] data_out_24; assign data_out_24  = data_in[25*DATA_WIDTH-1:24*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[24]}};
	wire [DATA_WIDTH-1:0] data_out_23; assign data_out_23  = data_in[24*DATA_WIDTH-1:23*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[23]}};
	wire [DATA_WIDTH-1:0] data_out_22; assign data_out_22  = data_in[23*DATA_WIDTH-1:22*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[22]}};
	wire [DATA_WIDTH-1:0] data_out_21; assign data_out_21  = data_in[22*DATA_WIDTH-1:21*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[21]}};
	wire [DATA_WIDTH-1:0] data_out_20; assign data_out_20  = data_in[21*DATA_WIDTH-1:20*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[20]}};
	wire [DATA_WIDTH-1:0] data_out_19; assign data_out_19  = data_in[20*DATA_WIDTH-1:19*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[19]}};
	wire [DATA_WIDTH-1:0] data_out_18; assign data_out_18  = data_in[19*DATA_WIDTH-1:18*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[18]}};
	wire [DATA_WIDTH-1:0] data_out_17; assign data_out_17  = data_in[18*DATA_WIDTH-1:17*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[17]}};
	wire [DATA_WIDTH-1:0] data_out_16; assign data_out_16  = data_in[17*DATA_WIDTH-1:16*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[16]}};
	wire [DATA_WIDTH-1:0] data_out_15; assign data_out_15  = data_in[16*DATA_WIDTH-1:15*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[15]}};
	wire [DATA_WIDTH-1:0] data_out_14; assign data_out_14  = data_in[15*DATA_WIDTH-1:14*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[14]}};
	wire [DATA_WIDTH-1:0] data_out_13; assign data_out_13  = data_in[14*DATA_WIDTH-1:13*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[13]}};
	wire [DATA_WIDTH-1:0] data_out_12; assign data_out_12  = data_in[13*DATA_WIDTH-1:12*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[12]}};
	wire [DATA_WIDTH-1:0] data_out_11; assign data_out_11  = data_in[12*DATA_WIDTH-1:11*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[11]}};
	wire [DATA_WIDTH-1:0] data_out_10; assign data_out_10  = data_in[11*DATA_WIDTH-1:10*DATA_WIDTH] & {DATA_WIDTH{one_hot_code[10]}};
	wire [DATA_WIDTH-1:0] data_out_9;  assign data_out_9  =  data_in[10*DATA_WIDTH-1:9*DATA_WIDTH]  & {DATA_WIDTH{one_hot_code[9]}};
	wire [DATA_WIDTH-1:0] data_out_8;  assign data_out_8  =  data_in[9*DATA_WIDTH-1:8*DATA_WIDTH]   & {DATA_WIDTH{one_hot_code[8]}};
	wire [DATA_WIDTH-1:0] data_out_7;  assign data_out_7  =  data_in[8*DATA_WIDTH-1:7*DATA_WIDTH]   & {DATA_WIDTH{one_hot_code[7]}};
	wire [DATA_WIDTH-1:0] data_out_6;  assign data_out_6  =  data_in[7*DATA_WIDTH-1:6*DATA_WIDTH]   & {DATA_WIDTH{one_hot_code[6]}};
	wire [DATA_WIDTH-1:0] data_out_5;  assign data_out_5  =  data_in[6*DATA_WIDTH-1:5*DATA_WIDTH]   & {DATA_WIDTH{one_hot_code[5]}};
	wire [DATA_WIDTH-1:0] data_out_4;  assign data_out_4  =  data_in[5*DATA_WIDTH-1:4*DATA_WIDTH]   & {DATA_WIDTH{one_hot_code[4]}};
	wire [DATA_WIDTH-1:0] data_out_3;  assign data_out_3  =  data_in[4*DATA_WIDTH-1:3*DATA_WIDTH]   & {DATA_WIDTH{one_hot_code[3]}};
	wire [DATA_WIDTH-1:0] data_out_2;  assign data_out_2  =  data_in[3*DATA_WIDTH-1:2*DATA_WIDTH]   & {DATA_WIDTH{one_hot_code[2]}};
	wire [DATA_WIDTH-1:0] data_out_1;  assign data_out_1  =  data_in[2*DATA_WIDTH-1:DATA_WIDTH]     & {DATA_WIDTH{one_hot_code[1]}};
	wire [DATA_WIDTH-1:0] data_out_0;  assign data_out_0  =  data_in[DATA_WIDTH-1:0]                & {DATA_WIDTH{one_hot_code[0]}};	
	always @(*) begin
		data_out = data_out_31|
                   data_out_30|
                   data_out_29|
                   data_out_28|
                   data_out_27|
                   data_out_26|
                   data_out_25|
                   data_out_24|
                   data_out_23|
                   data_out_22|
                   data_out_21|
                   data_out_20|
                   data_out_19|
                   data_out_18|
                   data_out_17|
                   data_out_16|
                   data_out_15|
                   data_out_14|
                   data_out_13|
                   data_out_12|
                   data_out_11|
                   data_out_10|
                   data_out_9|
                   data_out_8|
                   data_out_7|
                   data_out_6|
                   data_out_5|
                   data_out_4|
                   data_out_3|
                   data_out_2|
                   data_out_1|
                   data_out_0;
    end
`endif
endmodule
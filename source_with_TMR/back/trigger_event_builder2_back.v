/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : trigger_event_builder2.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 21st, 2018
//  Note       : read out all data of a channel instead of reading  all channels in turn
//     timing is tight
`include "common_definition.v"
module trigger_event_builder2
#(parameter 
	CHANNEL_NUMBER=24,
	CHANNEL_DATA_WIDTH=45
)
(
	input clk,
	input rst,
	
	input enable,
	input enable_timeout,

	input trigger_fifo_empty,
	input[11:0] trigger_event_id,
	input[11:0] trigger_bunch_id,
	input[(24-`TRIGGER_MATCHING_HIT_COUNT_SIZE)-1-1:0] error_flag,

	input [CHANNEL_NUMBER -1 : 0] trigger_processing,
	input [CHANNEL_NUMBER -1 : 0] chnl_fifo_empty,
	output [CHANNEL_NUMBER -1 : 0] chnl_fifo_read,
	input [CHANNEL_NUMBER*CHANNEL_DATA_WIDTH-1 : 0] chnl_data,
	output reg channel_fifo_clear,

	output reg get_trigger,	

	input read_out_fifo_full,

    output  data_ready,
    output  [CHANNEL_DATA_WIDTH-1:0] data_out,
    output reg trigger_head,
    output reg trigger_trail
);

reg trigger_end;
wire channel_data_not_empty;
reg [2:0] wait_for_data_counter;

reg [`TRIGGER_EVENT_BUILDER_SIZE] state;
reg [`TRIGGER_EVENT_BUILDER_SIZE] nextstate;
localparam IDLE 			 	= `TRIGGER_EVENT_BUILDER_IDLE;
localparam GET_TRIGGER_INFO 	= `TRIGGER_EVENT_BUILDER_GET_TRIGGER_INFO;
localparam WAIT_CHNL_DATA	    = `TRIGGER_EVENT_BUILDER_WAIT_CHNL_DATA;
localparam SEND_HEAD 			= `TRIGGER_EVENT_BUILDER_SEND_HEAD;
localparam GET_CHNL_DATA 		= `TRIGGER_EVENT_BUILDER_GET_CHNL_DATA;
localparam SEND_CHNL_DATA 		= `TRIGGER_EVENT_BUILDER_SEND_CHNL_DATA;
localparam SEND_TRAIL  			= `TRIGGER_EVENT_BUILDER_SEND_TRAIL;
reg [`TRIGGER_EVENT_BUILDER_TIME_OUT_COUNT_SIZE-1:0] time_out_counter;

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		time_out_counter <= 'b0;
	end
	else if (state==IDLE) begin
		time_out_counter <= 'b0;
	end else begin
		time_out_counter <= time_out_counter + 'b1;
	end
end
wire time_out;
assign  time_out = &time_out_counter;
reg time_out_error;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		time_out_error <= 1'b0;
	end	else if (nextstate[`TRIGGER_EVENT_BUILDER_GET_TRIGGER_INFO_bit]) begin
		time_out_error <= 1'b0;
	end else if(time_out&enable_timeout) begin
		time_out_error <= 1'b1;
	end
end

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		state <= IDLE;
	end else begin
		state <= nextstate;
	end
end

always @(*) begin
	case(state)
		IDLE: 
			nextstate = enable&(~trigger_fifo_empty) ? GET_TRIGGER_INFO : IDLE;
		GET_TRIGGER_INFO:
			nextstate = WAIT_CHNL_DATA;
		WAIT_CHNL_DATA:
			nextstate = |wait_for_data_counter ? WAIT_CHNL_DATA : SEND_HEAD;
		SEND_HEAD:
			nextstate = read_out_fifo_full ? SEND_HEAD : GET_CHNL_DATA;
		GET_CHNL_DATA:
			nextstate = trigger_end ?  SEND_TRAIL : 
									channel_data_not_empty ? SEND_CHNL_DATA : GET_CHNL_DATA;
		SEND_CHNL_DATA :
			nextstate = channel_data_not_empty ?  SEND_CHNL_DATA : GET_CHNL_DATA;
		SEND_TRAIL :
			nextstate = read_out_fifo_full ? SEND_TRAIL:IDLE;
		default: nextstate = IDLE;
	endcase
	if (time_out&enable_timeout) begin
		nextstate = SEND_TRAIL;
	end
end




always @(posedge clk  ) begin
	if (rst) begin
		// reset
		get_trigger <= 1'b0;
	end	else if (nextstate[`TRIGGER_EVENT_BUILDER_GET_TRIGGER_INFO_bit]) begin
		get_trigger <= 1'b1;
	end else begin
		get_trigger <= 1'b0;
	end
end
//assign  get_trigger = nextstate[`TRIGGER_EVENT_BUILDER_GET_TRIGGER_INFO_bit];

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		wait_for_data_counter <= 3'b0;
	end	else if (state[`TRIGGER_EVENT_BUILDER_GET_TRIGGER_INFO_bit]) begin
		wait_for_data_counter <= 3'b111;
	end else if (|wait_for_data_counter) begin
		wait_for_data_counter <= wait_for_data_counter - 3'b1;
	end
end




always @(posedge clk  ) begin
	if (rst) begin
		// reset
		trigger_head <= 1'b0;
	end	else if (nextstate[`TRIGGER_EVENT_BUILDER_SEND_HEAD_bit]) begin
		trigger_head <= 1'b1;
	end else begin
		trigger_head <= 1'b0;
	end
end

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		trigger_trail <= 1'b0;
	end	else if (nextstate[`TRIGGER_EVENT_BUILDER_SEND_TRAIL_bit]) begin
		trigger_trail <= 1'b1;
	end else begin
		trigger_trail <= 1'b0;
	end
end


reg [CHANNEL_NUMBER-1:0] channel_fifo_no_empty_r;
wire [CHANNEL_NUMBER-1:0] channel_fifo_no_empty_one_hot,channel_fifo_no_empty_next;
assign channel_fifo_no_empty_next = read_out_fifo_full ? channel_fifo_no_empty_r 
									:((~chnl_fifo_empty & channel_fifo_no_empty_one_hot) ? channel_fifo_no_empty_r
									:(channel_fifo_no_empty_r & (~channel_fifo_no_empty_one_hot)));
always @(posedge clk  ) begin
  if (rst) begin
    // reset
    channel_fifo_no_empty_r <= 'b0;
  end
  else if (nextstate[`TRIGGER_EVENT_BUILDER_GET_CHNL_DATA_bit]) begin
    channel_fifo_no_empty_r <= ~chnl_fifo_empty;
  end
  else begin
    channel_fifo_no_empty_r <= channel_fifo_no_empty_next;
  end
end
assign chnl_fifo_read = read_out_fifo_full ? 'b0: channel_fifo_no_empty_one_hot ;
assign  channel_data_not_empty = (|channel_fifo_no_empty_r);

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		trigger_end <= 1'b1;
	end
	else begin
		trigger_end <= (~|trigger_processing)&(&chnl_fifo_empty);
	end
end

multi_hot_one_hot #(.WIDTH(CHANNEL_NUMBER))
  multi_hot_one_hot_no_empty(
    .multi_hot_code(channel_fifo_no_empty_r),
    .one_hot_code(channel_fifo_no_empty_one_hot)
    );


wire [CHANNEL_DATA_WIDTH-1:0] selected_data;
one_hot_select #(.DATA_WIDTH(CHANNEL_DATA_WIDTH),.DATA_DEEP(CHANNEL_NUMBER))
	channel_mux_inst(
	    .one_hot_code(channel_fifo_no_empty_one_hot),
	    .data_in(chnl_data),
	    .data_out(selected_data)
	);

reg [`TRIGGER_MATCHING_HIT_COUNT_SIZE-1:0] hit_count;
wire [`TRIGGER_MATCHING_HIT_COUNT_SIZE-1:0] hit_count_adjust;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		hit_count <='b0;
//	end	else if (nextstate[`TRIGGER_EVENT_BUILDER_GET_TRIGGER_INFO_bit]) begin
	end	else if (nextstate[`TRIGGER_EVENT_BUILDER_GET_TRIGGER_INFO_bit]|trigger_head) begin
		hit_count <='b0;
	end else if(data_ready&(~read_out_fifo_full)) begin
		hit_count <= hit_count+'b1;
	end
end
//assign hit_count_adjust = hit_count -'b1;
assign hit_count_adjust = hit_count 
;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		channel_fifo_clear <= 1'b0;
	end	else if (nextstate[`TRIGGER_EVENT_BUILDER_SEND_TRAIL_bit]) begin
		channel_fifo_clear <= 1'b1;
	end else begin
		channel_fifo_clear <= 1'b0;
	end
end


reg data_ready_inner; assign  data_ready = data_ready_inner;
reg [CHANNEL_DATA_WIDTH-1:0] data_out_inner; assign data_out = data_out_inner;
//combination output 
always @(*) begin
	if (state[`TRIGGER_EVENT_BUILDER_SEND_HEAD_bit]) begin
		data_ready_inner = 1'b1;
		data_out_inner = {5'b11110,1'b0,15'h0000,trigger_event_id,trigger_bunch_id};
	end else if (state[`TRIGGER_EVENT_BUILDER_GET_CHNL_DATA_bit]) begin
		data_ready_inner = (|(~chnl_fifo_empty & channel_fifo_no_empty_one_hot));
		data_out_inner = selected_data;
	end else if (state[`TRIGGER_EVENT_BUILDER_SEND_CHNL_DATA_bit] ) begin
		data_ready_inner = (|(~chnl_fifo_empty & channel_fifo_no_empty_one_hot));
		data_out_inner = selected_data;
	end else if (state[`TRIGGER_EVENT_BUILDER_SEND_TRAIL_bit] ) begin
		data_ready_inner = 1'b1;
		data_out_inner = {5'b11101,1'b0,15'h00ff,error_flag,time_out_error,hit_count_adjust};
	end else begin
		data_ready_inner = 1'b0;
		data_out_inner = selected_data;
	end

	//case(state)
	//	SEND_HEAD: begin
	//		data_ready_inner = 1'b1;
	//		data_out_inner = {21'b0,trigger_event_id,trigger_bunch_id};
	//	end
	//	GET_CHNL_DATA: begin
	//		data_ready_inner = |channel_fifo_no_empty_one_hot;
	//		data_out_inner = selected_data;
	//	end
	//	SEND_CHNL_DATA:begin
	//		data_ready_inner = 1'b1;
	//		data_out_inner = selected_data;
	//	end
	//	SEND_TRAIL:begin
	//		data_ready_inner = 1'b1;
	//		data_out_inner = {21'h1fffff,error_flag,hit_count};
	//	end
	//	default:begin
	//		data_ready_inner = 1'b0;
	//		data_out_inner = selected_data;
	//	end
	//endcase
end




endmodule
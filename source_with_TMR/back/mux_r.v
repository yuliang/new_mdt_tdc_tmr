/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : mux.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 6st, 2018
//  Note       : mux_sel=1 : data_l = data_r , data_t= data_f ; mux_sel=0 : data_l = data_f, data_t = data_r
// 
`include "common_definition.v"
module mux_r 
#(parameter 
    WIDTH = 37
)
(
  input [WIDTH-1 : 0] a,
  input [WIDTH-1 : 0] b,
  input enable,
  input clk,
  input rst,
  output [WIDTH-1 : 0] out,
  input sel
 );

reg [WIDTH-1 : 0] a_r,b_r;
always @(posedge clk  ) begin
  if (rst) begin
    // reset
    a_r <= 'b0;
    b_r <= 'b0;
  end
  else if (enable) begin
    a_r <= a;
    b_r <= b;
  end
end

assign  out = sel ? a_r : b_r;


endmodule 
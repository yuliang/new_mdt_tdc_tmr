/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : control_decoder_back.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 26th, 2018
//  Note       : 
//    

`include "common_definition.v"
module control_decoder_back
( 
	input clk160, 

	input enable,

	input reset_in, 
	input trigger_direct_in,
	input bunch_reset_direct_in,
	input event_reset_direct_in,

	input reset_jtag_in, 
	input event_teset_jtag_in,

	input encoded_control_in,

	input enable_master_reset_code,
	input enable_trigger,
	input enable_direct_trigger,
	input enable_direct_bunch_reset,
	input enable_direct_event_reset,

	output reg trigger,
	output reg bunch_reset,
	output reg event_reset,
	output master_reset
);


reg reset_jtag;
//sync jtag_reset
always @(posedge clk160 or posedge reset_in) begin
	if (reset_in) begin
		reset_jtag <= 1'b0;
	end else begin
		reset_jtag <=  reset_jtag_in;
	end
end

reg event_teset_jtag;
//sync jtag_reset
always @(posedge clk160 or posedge reset_in) begin
	if (reset_in) begin
		event_teset_jtag <= 1'b0;
	end else begin
		event_teset_jtag <=  event_reset_direct_in;
	end
end


wire syn_40M;
reg [1:0] syn_40M_reg;
always @(posedge clk160 or posedge reset_in) begin
	if (reset_in) begin
		syn_40M_reg <= 2'b0;
	end else begin
		syn_40M_reg <=  syn_40M_reg + 2'b1;
	end	
end
assign syn_40M = &syn_40M_reg;

reg input_shift_reg;
reg [2:0] decoder_shift_reg;
//shift reg to decode commands
always @(posedge clk160 or posedge reset_in) begin
	if (reset_in) begin
		input_shift_reg <= 1'b0;
		decoder_shift_reg <= 3'b0;
	end else if (syn_40M) begin
		input_shift_reg      <= encoded_control_in&enable;
		decoder_shift_reg[0] <= input_shift_reg;
		decoder_shift_reg[1] <= decoder_shift_reg[0];
		decoder_shift_reg[2] <= decoder_shift_reg[1];
	end
end
reg [1:0] code_size_count;
//bit count from start bit
always @(posedge clk160 or reset_in)
	begin
	if (reset_in)
		code_size_count <=0;
	else if (syn_40M) begin
		if ( ((code_size_count == 0) | (code_size_count == 3)) & input_shift_reg )
			code_size_count <= 1;
		else if ( (code_size_count == 1) | (code_size_count == 2) )
			code_size_count <=  code_size_count + 2'd1;
		else
			code_size_count <= 0;
		end
	end

assign 	code_detect = (code_size_count == 3),
		trigger_detect =     code_detect & decoder_shift_reg[2] & ~decoder_shift_reg[1] & ~decoder_shift_reg[0], 
		bunch_reset_detect = code_detect & decoder_shift_reg[2] & decoder_shift_reg[1] & ~decoder_shift_reg[0], 
		event_reset_detect = code_detect & decoder_shift_reg[2] & decoder_shift_reg[1] & decoder_shift_reg[0], 
		master_reset_detect= code_detect & decoder_shift_reg[2] & ~decoder_shift_reg[1] & decoder_shift_reg[0];

assign  master_reset =  reset_in | reset_jtag |(enable_master_reset_code&master_reset_detect);
//-----------------------------------------------------------
//registered versions of resets and trigger
always @(posedge clk160)
	begin
	trigger <= enable & enable_trigger & (enable_direct_trigger ? trigger_direct_in : trigger_detect);
	end

always @(posedge clk160)
	begin
	bunch_reset <= enable & (enable_direct_bunch_reset ? bunch_reset_direct_in : bunch_reset_detect);
	end

always @(posedge clk160)
	begin
	event_reset <= enable & ((enable_direct_event_reset ? event_reset_direct_in: event_reset_detect) | event_teset_jtag);
	end
endmodule
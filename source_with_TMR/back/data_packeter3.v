/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : data_packeter3.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 6st, 2018
//  Note       : 
//     
`include "common_definition.v"
module data_packeter3
(
    input clk,
    input rst,

//config
	input enable_pair,
	input enable_trigger,
	input enable_leading,
	input enable_error_packet,
	input enable_8b10b,
	input enable_insert,
	input enable_TDC_ID,
	input [18:0] TDC_ID,
	input enable_config_error_notify,
	input config_error,

    input read_out_fifo_empty,
	output read_out_fifo_read,
	input [`READ_OUT_FIFO_SIZE-1:0] read_out_fifo_data,

	input interface_fifo_full,
	output interface_fifo_write,
	output [9:0] interface_fifo_data
);

wire [7:0] IDLE_packet;
wire [7:0] IDLE_packet_normal;
assign  IDLE_packet_normal = (enable_trigger ? (enable_leading ? `TRIGGER_LEADING_IDLE_packet : `TRIGGER_PAIRING_IDLE_packet)
									 : (enable_leading ? `TRIGGERLESS_LEADING_IDLE_packet : `TRIGGERLESS_PAIRING_IDLE_packet));
assign IDLE_packet = (enable_config_error_notify & config_error) ? `CONFIG_ERROR_packet : IDLE_packet_normal;
wire busy;

reg [47:0] packet_data;
reg [5:0] packet_data_k;
reg [2:0] data_number;
wire idle_insert;
reg k_out;
reg enable_8b10b_r;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		packet_data <= 48'b0;
		packet_data_k <= 6'b0;
	end else if(~busy) begin
		if(enable_TDC_ID) begin
			packet_data <= {`TDC_ID_IDLE_packet,5'b11111,TDC_ID,`TDC_ID_IDLE_packet,`TDC_ID_IDLE_packet};
			packet_data_k <= {1'b1,3'b0,2'b11};
		end	else if(~read_out_fifo_empty) begin
			if (read_out_fifo_data[40]&enable_error_packet) begin				
				if (read_out_fifo_data[42:41]==2'b00) begin
					packet_data <= {`TRIGGERLESS_ERROR_packet,read_out_fifo_data[23:0],IDLE_packet,IDLE_packet};
					packet_data_k <= {1'b1,3'b0,2'b11};
				end else if(read_out_fifo_data[42:41]==2'b01) begin
					packet_data <= {`TRIGGERLESS_ERROR_packet,read_out_fifo_data[39:8],IDLE_packet};
					packet_data_k <= {1'b1,4'b0,1'b1};
				end else begin
					packet_data <= {`TRIGGERLESS_ERROR_packet,read_out_fifo_data[39:0]};
					packet_data_k <= {1'b1,5'b0};
				end
			end else if(idle_insert) begin
				if (read_out_fifo_data[42:41]==2'b00) begin
					packet_data <= {IDLE_packet,read_out_fifo_data[23:0],IDLE_packet,IDLE_packet};
					packet_data_k <= {1'b1,3'b0,2'b11};
				end else if(read_out_fifo_data[42:41]==2'b01) begin
					packet_data <= {IDLE_packet,read_out_fifo_data[39:8],IDLE_packet};
					packet_data_k <= {1'b1,4'b0,1'b1};
				end else begin
					packet_data <= {IDLE_packet,read_out_fifo_data[39:0]};
					packet_data_k <= {1'b1,5'b0};
				end
			end else begin				
				if (read_out_fifo_data[42:41]==2'b00) begin
					if ((read_out_fifo_data[39:24]==16'h00ff) ) begin
						packet_data <= {IDLE_packet,read_out_fifo_data[23:0],IDLE_packet,IDLE_packet};
						packet_data_k <= {1'b1,3'b0,2'b11};
					end else begin
						packet_data <= {read_out_fifo_data[23:0],IDLE_packet,IDLE_packet,IDLE_packet};
						packet_data_k <= {3'b0,3'b111};
					end
				end else if(read_out_fifo_data[42:41]==2'b01) begin
					packet_data <= {read_out_fifo_data[39:8],IDLE_packet,IDLE_packet};
					packet_data_k <= {4'b0,2'b11};
				end else begin
					packet_data <= {read_out_fifo_data[39:0],IDLE_packet};
					packet_data_k <= {5'b0,1'b1};
				end
			end
		end else begin
			packet_data <= {IDLE_packet,IDLE_packet,IDLE_packet,IDLE_packet,IDLE_packet,IDLE_packet};
			packet_data_k <= {6'b11_1111};
		end
	end else if (enable_8b10b_r) begin
		if (|data_number) begin
			packet_data <= {packet_data[39:0],8'b0};  
			packet_data_k <= {packet_data_k[4:0],1'b0};
		end 
	end
end


assign read_out_fifo_read = (~busy)&(~enable_TDC_ID)&(~read_out_fifo_empty);



always @(posedge clk  ) begin
	if (rst) begin
		// reset
		data_number <= 3'b0;
	end else if(~busy) begin
		if(enable_TDC_ID) begin
			data_number <= 3'b011;
		end else if(~read_out_fifo_empty) begin
			if ((read_out_fifo_data[40]&enable_error_packet)|idle_insert) begin				
				if (read_out_fifo_data[42:41]==2'b00) begin
					data_number <= 3'b011;
				end else if(read_out_fifo_data[42:41]==2'b01) begin
					data_number <= 3'b100;
				end else begin
					data_number <= 3'b101;
				end
			end else begin				
				if (read_out_fifo_data[42:41]==2'b00) begin
					data_number <= (read_out_fifo_data[39:24]==16'h00ff) ? 3'b011 : 3'b010;
				end else if(read_out_fifo_data[42:41]==2'b01) begin
					data_number <= 3'b011;
				end else begin
					data_number <= 3'b100;
				end
			end
		end else begin
			data_number <= 3'b0;
		end
	end else if(enable_8b10b_r) begin
		if (|data_number) begin
			data_number <= data_number -'b1;
		end
	end
end





reg [`MAX_PACKET_NUM_SIZE-1:0] packet_number;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		packet_number <= 'b0;
	end else if (~enable_trigger ) begin
		if (~interface_fifo_full) begin
			if (k_out) begin
				packet_number <= 'b0;
			end else if((~idle_insert)&enable_insert) begin
				packet_number <= packet_number +1'b1;
			end
		end
	end else begin
		packet_number <= 'b0;
	end
end
assign idle_insert  = enable_insert&(&packet_number);


reg busy_r;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		busy_r <= 1'b0;
	end	else if (read_out_fifo_read|(enable_TDC_ID&(~|data_number)&(~interface_fifo_full))) begin
		busy_r <= 1'b1;
	end else if(busy_r&(~|data_number)&(~interface_fifo_full)) begin
		busy_r <= 1'b0;
	end 
end
assign busy = busy_r&((|data_number)|(interface_fifo_full));



reg updata_packet_data;
wire [9:0] interface_fifo_data_8b10b;
wire o_Rd;
reg last_o_Rd;

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		updata_packet_data <= 1'b0;
	end	else begin
		updata_packet_data <= (~busy)&(~read_out_fifo_empty);
	end
end


always @(*) begin
	if(enable_TDC_ID)begin
		enable_8b10b_r = ~interface_fifo_full;
	end else begin
		if(updata_packet_data)begin
			enable_8b10b_r <= 1'b1;
		end else begin
			enable_8b10b_r = ~interface_fifo_full;
		end
	end
end

reg i_rd;
always @(*) begin
	if(enable_TDC_ID) begin
		i_rd = o_Rd;
	end else if(updata_packet_data&(~interface_fifo_full))begin
		i_rd = last_o_Rd;
	end else begin
		i_rd = o_Rd;		
	end
end

mEnc8b10bMem_tb
	mEnc8b10bMem_tb_inst(
		.i8_Din(packet_data[47:40]),		//HGFEDCBA
		.i_Kin(packet_data_k[5]),
		.i_ForceDisparity(1'b1),
		.i_Disparity(i_rd),		//1 Is negative, 0 is positive	
		.o10_Dout(interface_fifo_data_8b10b),	//abcdeifghj
		.o_Rd(o_Rd),
		.o_KErr(),
		.i_Clk(clk),
		.i_ARst_L(rst),

    	.i_enable(enable_8b10b_r&enable_8b10b),
		.soft_reset_i(1'b0)
    );

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		last_o_Rd <= 1'b0;
	end
	else if (enable_8b10b_r) begin
		last_o_Rd <= o_Rd;
	end
end

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		k_out <= 1'b0;
	end
	else if (enable_8b10b_r) begin
		k_out <= packet_data_k[5];
	end
end

assign interface_fifo_data  = enable_8b10b ? interface_fifo_data_8b10b : {1'b0,packet_data_k[5],packet_data[47:40]};

assign interface_fifo_write = 1'b1;

endmodule
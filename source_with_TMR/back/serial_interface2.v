/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : serial_interface2.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on March 8st, 2018
//  Note       : 
//    

`include "common_definition.v"
module serial_interface2
(
    input clk,
    input rst,

	input enable_320,
	input enable,
	input enable_8b10b,
	input enable_trigger,enable_leading,enable_config_error_notify,config_error,

	input interface_fifo_empty,
	output interface_fifo_read,
	input [8:0] interface_fifo_data,

	output [1:0] d_line
);

wire fifo_empty; assign  fifo_empty = interface_fifo_empty;
reg  fifo_read; assign interface_fifo_read = fifo_read;
reg [3:0] send_number;




always @(posedge clk  ) begin
	if (rst) begin
		// reset
		send_number <= 4'b0;
	end	else if ((~|send_number)&enable) begin
		send_number <= enable_320 ? 4'h4 : 4'h9;
	end else if((|send_number))begin
		send_number <= send_number-4'h1;
	end
end

always @(*) begin
	fifo_read = ~fifo_empty&(~|send_number)&enable;
end


reg high;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		high <= 1'b0;
	end	else if ((~|send_number)) begin
		high <= 1'b0;
	end else if((~enable_320)&enable) begin
		high <= ~high;
	end
end


wire [7:0] IDLE_packet;
wire [7:0] IDLE_packet_normal;
assign  IDLE_packet_normal = (enable_trigger ? (enable_leading ? `TRIGGER_LEADING_IDLE_packet : `TRIGGER_PAIRING_IDLE_packet)
									 : (enable_leading ? `TRIGGERLESS_LEADING_IDLE_packet : `TRIGGERLESS_PAIRING_IDLE_packet));
assign IDLE_packet = (enable_config_error_notify & config_error) ? `CONFIG_ERROR_packet : IDLE_packet_normal;

reg [7:0] data_send_8b;
reg data_k;
always @(posedge clk  ) begin
	if (rst) begin
		// reset
		data_send_8b <= 8'b0;
	end
	else if ((~|send_number)&enable) begin
		if (~interface_fifo_empty) begin
			data_send_8b <= interface_fifo_data[7:0];
			data_k <=  interface_fifo_data[8];
		end else begin
			data_send_8b <= IDLE_packet;
			data_k <=  1'b0;
		end
	end
end

wire [9:0] data_send_8b10b;
mEnc8b10bMem_tb
	mEnc8b10bMem_tb_inst(
		.i8_Din(data_send_8b),		//HGFEDCBA
		.i_Kin(data_k),
		.i_ForceDisparity(1'b0),
		.i_Disparity(1'b0),		//1 Is negative, 0 is positive	
		.o10_Dout(data_send_8b10b),	//abcdeifghj
		.o_Rd(),
		.o_KErr(),
		.i_Clk(clk),
		.i_ARst_L(rst),

    	.i_enable((send_number==4'h1)&enable_8b10b),
		.soft_reset_i(1'b0)
    );
wire [9:0] data_send_10b;
assign data_send_10b = enable_8b10b ? data_send_8b10b : {1'b0,data_k,data_send_8b};


reg [9:0] data_send;

always @(posedge clk  ) begin
	if (rst) begin
		// reset
		data_send <= 10'b0;
	end
	else if ((send_number==4'h2)&enable) begin
		data_send <= data_send_10b;
	end else if(high|enable_320) begin
		data_send <= {data_send[7:0],data_send[9:8]};
	end
end


assign	d_line[0] = enable & data_send[9];
assign 	d_line[1] = enable & data_send[8];



endmodule
/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : tdc_channel.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on June 30st, 2017
//  Note       : 
// 


module tdc_channel #(
	parameter DATA_WIDTH=18,
	parameter DATA_DEEP	=32
	)
(
//input  [4  :0]    chnl_id, // channel number
input  [3  :0]    fine_sel, //encoding for fine time selection

input  [5  :0]    lut0, lut1, lut2, lut3, lut4, lut5, lut6, lut7, lut8, lut9,
                  luta, lutb, lutc, lutd, lute, lutf,

input             rst,      // reset, not BC reset
//input             edge_mode, // 1: leading; 0: trailing
input             clk160,
input             channel_enable,
input             Rdy, // ready
input  [5  :0]    q, // raw fine codes 
input  [14 :0]    cnt, cnt_inv, // coarse counter

input fake_hit,

input fifo_read,
input fifo_read_clk,
output [16:0] channel_data_out,
output fifo_empty,

input trigger,
input [11:0] limit_up,
input [11:0] limit_down,
output trigger_processing
);

wire data_ready;
wire [16:0] effictive_data;
//wire [`DEBUG_PACKET_WIDTH] debug_packet;
wire [(DATA_WIDTH*DATA_DEEP-1):0] ring_buffer_data;
wire chnl_fifo_full;
wire chnl_fifo_write;
wire [16:0] fifo_data_in;
wire [16:0] fifo_data_out;

chnl_tdc_front chnl_tdc_front_inst
(
//.chnl_id(chnl_id), // channel number
.fine_sel(fine_sel), //encoding for fine time selection

.lut0(lut0), .lut1(lut1), .lut2(lut2), .lut3(lut3), .lut4(lut4), .lut5(lut5), .lut6(lut6), .lut7(lut7), 
.lut8(lut8), .lut9(lut9), .luta(luta), .lutb(lutb), .lutc(lutc), .lutd(lutd), .lute(lute), .lutf(lutf),

.rst(rst),      // reset, not BC reset
//.edge_mode(edge_mode), // 1: leading; 0: trailing
.clk160(clk160),
.channel_enable(channel_enable),
.Rdy(Rdy), // ready
.q(q[3:0]), // raw fine codes 
.cnt(cnt), .cnt_inv(cnt_inv), // coarse counter

.data_ready(data_ready),
.effictive_data(effictive_data)
//.debug_packet(debug_packet)
);

//trigger mode
channel_ring_buffer1 #(.DATA_WIDTH(DATA_WIDTH),.DATA_DEEP(DATA_DEEP)  
	) channel_ring_buffer_inst
(
    .clk(clk160),
    .rst(rst),
    .enable(1'b1),
    .data_ready(data_ready),
    .effictive_data(effictive_data),
    .fake_hit(fake_hit),
    .data_out(ring_buffer_data)
    );
chnl_trigger_matching2 #(.DATA_WIDTH(DATA_WIDTH),.DATA_DEEP(DATA_DEEP)
	) chnl_trigger_matching_inst
(
    .clk(clk160),
    .rst(rst),
    .enable(1'b1),
    //ring buffer interface
	 .data_in(ring_buffer_data),
    //trigger interface
    .trigger(trigger),
    .limit_up(limit_up),
    .limit_down(limit_down),
    .trigger_processing(trigger_processing),
    //channel fifo interface
    .chnl_fifo_full(chnl_fifo_full),
    .chnl_fifo_write(chnl_fifo_write),
    .data_out(fifo_data_in)    
    );


//channle FIFO
fifo1 #(.DSIZE(17),.ASIZE(2),.SYN_DEPTH(2)
	) chnl_fifo1_inst
(
 .rdata(fifo_data_out),
 .wfull(chnl_fifo_full),
 .rempty(fifo_empty),
 .wdata(fifo_data_in),
.winc(chnl_fifo_write), .wclk(clk160), .wrst(rst),
.rinc(fifo_read), .rclk(fifo_read_clk), .rrst(rst)
);
assign channel_data_out = {fifo_data_out};
endmodule
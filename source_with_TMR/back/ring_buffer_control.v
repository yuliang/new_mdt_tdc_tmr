/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : ring_buffer_control.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on June 30st, 2017
//  Note       : 
// 
`include "common_definition.v"
module ring_buffer_control #(
	parameter DATA_WIDTH=18,
	parameter DATA_DEEP	=32
	)
(
    input clk,
    input rst,
    input data_ready,
    input [16:0] effictive_data,
    input fake_hit,
    output reg [17:0] ring_buffer_data,
    output reg write_ringbuffer
    );

reg hitted;
reg [1:0] fake_hit_r;
wire fake_hit_syn;
always @(posedge clk) begin
	if (rst) begin
		fake_hit_r <= 2'b0;
	end
	else begin
		fake_hit_r <= {fake_hit_r[0],fake_hit};
	end
end
assign  fake_hit_syn= fake_hit_r[0]&(~fake_hit_r[1]);


always @(posedge clk) begin
	if (rst) begin
		hitted <= 1'b0;		
	end
	else if (data_ready) begin
		hitted  <= 1'b1;	
	end else if(fake_hit_syn) begin
		hitted  <= 1'b0;
	end
end

always @(*) begin
	if(data_ready)begin
		ring_buffer_data = {1'b1,effictive_data};
	end else begin
		ring_buffer_data = {1'b0,effictive_data};
	end 
end
always @(*) begin
	if (data_ready|(fake_hit_syn&(~hitted))) begin
		write_ringbuffer = 1'b1;
	end else begin
		write_ringbuffer = 1'b0;
	end
end

//always @(posedge clk) begin
//	if(rst) begin
//		ring_buffer_data <= 18'b0;
//	end
//	if(data_ready)begin
//		ring_buffer_data <= {1'b1,effictive_data};
//	end else begin
//		ring_buffer_data <= {1'b0,effictive_data};
//	end 
//end
//always @(posedge clk) begin
//	if (rst) begin
//		// reset
//		write_ringbuffer <= 1'b0;
//	end
//	else if (data_ready|(fake_hit_syn&(~hitted))) begin
//		write_ringbuffer <= 1'b1;
//	end else begin
//		write_ringbuffer <= 1'b0;
//	end
//end
endmodule

/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : fine_encode.v
//  Author     : Jinhong Wang
//  Revision   : 
//               First created on June 1st, 2016
//  Note       : 
// 


`include "common_definition.v"
module fine_encode(
 input [5:0]   lut0, lut1, lut2, lut3, lut4, lut5, lut6, lut7,
               lut8, lut9, luta, lutb, lutc, lutd, lute, lutf,

input  [3:0]   raw_tdc,
output [1:0]   fine_tdc);

reg [1:0] fine_tdc_r;

assign fine_tdc = fine_tdc_r;

always @(*) begin
      if( raw_tdc == lut0[5:2])
              fine_tdc_r = lut0[1:0];
      else if( raw_tdc == lut1[5:2])
              fine_tdc_r = lut1[1:0];
      else if( raw_tdc == lut2[5:2])
              fine_tdc_r = lut2[1:0];
      else if( raw_tdc == lut3[5:2])
              fine_tdc_r = lut3[1:0];
      else if( raw_tdc == lut4[5:2])
              fine_tdc_r = lut4[1:0];
      else if( raw_tdc == lut5[5:2])
              fine_tdc_r = lut5[1:0];
      else if( raw_tdc == lut6[5:2])
              fine_tdc_r = lut6[1:0];
      else if( raw_tdc == lut7[5:2])
              fine_tdc_r = lut7[1:0];
      else if( raw_tdc == lut8[5:2])
              fine_tdc_r = lut8[1:0];
      else if( raw_tdc == lut9[5:2])
              fine_tdc_r = lut9[1:0];
      else if( raw_tdc == luta[5:2])
              fine_tdc_r = luta[1:0];
      else if( raw_tdc == lutb[5:2])
              fine_tdc_r = lutb[1:0];
      else if( raw_tdc == lutc[5:2])
              fine_tdc_r = lutc[1:0];
      else if( raw_tdc == lutd[5:2])
              fine_tdc_r = lutd[1:0];
      else if( raw_tdc == lute[5:2])
              fine_tdc_r = lute[1:0];
      else 
              fine_tdc_r = lutf[1:0];
end
endmodule
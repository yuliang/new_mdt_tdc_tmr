/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : roll_over_difference_back.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 6st, 2018
//  Note       : calculator if a>b, a_b= a-b;if a<b, a_b=a-b+roll_over+1
// 

module roll_over_difference_back
(
    input enable,
    input [11 :0 ] roll_over,
    input [16 :0 ] a,
    input [16:0 ] b,
    output [16 :0 ] a_b 
);

    wire [16 :0 ] a_gated,b_gated;
    assign a_gated = a & ({17{enable}});
    assign b_gated = b & ({17{enable}});
    wire carry_out_high,carry_out_low;
    wire [11:0] direct_a_b,direct_a_b_minus_1,a_b_add_roll_over,a_b_add_roll_over_minus_1;
    wire [4:0] direct_a_b_low;
    adder_speed #(12)
        sub_high_1(.a(a_gated[16:5]),.b(~b_gated[16:5]),.carry_in(1'b1),.s(direct_a_b),.carry_out(carry_out_high)),
        sub_high_2(.a(a_gated[16:5]),.b(~b_gated[16:5]),.carry_in(1'b0),.s(direct_a_b_minus_1),.carry_out()),
        add_high_1(.a(direct_a_b),.b(roll_over),.carry_in(1'b1),.s(a_b_add_roll_over),.carry_out()),
        add_high_2(.a(direct_a_b),.b(roll_over),.carry_in(1'b0),.s(a_b_add_roll_over_minus_1),.carry_out());
    adder_speed #(5)
        sub_low_1(.a(a_gated[4:0]),.b(~b_gated[4:0]),.carry_in(1'b1),.s(direct_a_b_low),.carry_out(carry_out_low));
    reg [16:0] a_b_reg;
    always @(*) begin
        if(carry_out_high)begin
            if (carry_out_low) begin
                a_b_reg = {direct_a_b,direct_a_b_low};
            end 
            else if(|direct_a_b) begin
                a_b_reg = {direct_a_b_minus_1,direct_a_b_low};
            end
            else begin
                a_b_reg = {a_b_add_roll_over_minus_1,direct_a_b_low};
            end
        end
        else begin
            if (carry_out_low) begin
                a_b_reg = {a_b_add_roll_over,direct_a_b_low};
            end 
            else begin
                a_b_reg = {a_b_add_roll_over_minus_1,direct_a_b_low};
            end
        end
    end
    assign  a_b = a_b_reg;
endmodule

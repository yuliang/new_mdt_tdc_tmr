///////////////////////////////////////////////////
//
// majority_voter.v
// Created on Jun  3  2008 
// by Sandro Bonacini
// CERN PH/ESE/ME
//
///////////////////////////////////////////////////
`include "common_definition.v"
module majority_voter (in1, in2, in3, out, err);
    // synopsys template
    
    parameter WIDTH = 1;
    
    input   [(WIDTH-1):0]   in1, in2, in3;
    
    output  [(WIDTH-1):0]   out;
    output          	    err;
    
    reg     [(WIDTH-1):0]   out;
    //reg     	    	    err;
	reg     [(WIDTH-1):0]   bit_err;
 
 /*
 in1	in2		in3	|	out		err
 0		0		0   |	0		0
 0		0		1   |	0		1
 0		1		0   |	0		1
 0		1		1   |	1		1
 1		0		0   |	0		1
 1		0		1   |	1		1
 1		1		0   |	1		1
 1		1		1   |	1		0

 
 */
 

integer i;

always @(*) begin
	for (i=0; i<WIDTH; i=i+1) begin
	    	out[i] = (in1[i] & in2[i]) | (in2[i] & in3[i]) | (in1[i] & in3[i]);
	end
end

always @(*) begin
	for (i=0; i<WIDTH; i=i+1) begin
	    	bit_err[i] = (in1[i] ^ in2[i]) | (in2[i] ^ in3[i]);
	
	end
end

assign err = | (bit_err);
	
 /* nesintetizabil cu XST!!!
    always @(in1 or in2 or in3) begin
    	err = 0;
	out = vote (in1,in2,in3);
    end

   
    function vote_atom;
    	input in1,in2,in3;
    	begin
		if (in1 == in2) begin

	    	    vote_atom = in1;

		    if (in2 != in3) err = 1;

		end
		else begin
	    	    vote_atom   = in3;
		    err     	= 1;
`ifdef DC
`else
			$display("SEU!!!");
`endif
		end
    	end
    endfunction
    
    function [(WIDTH-1):0] vote;
        input   [(WIDTH-1):0]   in1, in2, in3;
	integer i;
    	begin
	    for (i=0; i<WIDTH; i=i+1)
	    	vote[i] = vote_atom( in1[i], in2[i], in3[i] );
	end
    endfunction
    
 */
endmodule

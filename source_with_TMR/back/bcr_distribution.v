/* 
       _________   ________     ________       
      / U OF M  \ | LSA    \   / Physics\
      \__ATLAS__/ |   ___   | |   ______/
         |   |    |  |   \  | |  |
         |   |    |  |___/  | |  \______     
         |   |    |         | |         \
         \___/    |________/   \________/
*/  
//  File Name  : bcr_distribution.v
//  Author     : Yu Liang
//  Revision   : 
//               First created on Feb 21st, 2018
//  Note       : 
//     
`include "common_definition.v"
module bcr_distribution
(
	input clk160,
	//input clk320,
	input rst,

	input bcr,
	input [11:0] roll_over,
	input [11:0] coarse_count_offset,
	//input [11:0] bunch_offset,

	input auto_roll_over,
	input bypass_bcr_distribution,

	output reg coarse_bcr,
	output reg trigger_count_bcr
);

reg [2:0] bcr_r;
wire bcr_inner;
assign  bcr_inner= (~bcr_r[2])&bcr_r[1];
always @(posedge clk160 ) begin
	bcr_r <= {bcr_r[1:0], bcr};
end

reg [13:0] global_counter;
always @(posedge clk160) begin
	if (rst & auto_roll_over) begin
		// reset
		global_counter <= 14'b11111111111111;
	end	else if (auto_roll_over) begin
		if(global_counter == {roll_over,2'b11})begin
			global_counter <= 14'b0;
		end else begin
			global_counter <= global_counter + 14'b1;
		end
	end else begin
		global_counter <= bcr_inner ? 14'b0 : (global_counter + 14'b1);
	end
end

reg coarse_bcr_160M;
always @(posedge clk160) begin
//	if(&global_counter[1:0])begin
		coarse_bcr_160M <= bypass_bcr_distribution ? bcr :  {global_counter[13:2] == coarse_count_offset};
//	end		
end

always @(posedge clk160) begin
	coarse_bcr <= coarse_bcr_160M;
end

//reg trigger_count_bcr_r;
//always @(posedge clk160) begin
//	trigger_count_bcr_r <= {global_counter[11:0] == bunch_offset};
//end
//assign trigger_count_bcr =trigger_count_bcr_r ;

always @(posedge clk160) begin
	trigger_count_bcr <= coarse_bcr_160M;
end

endmodule
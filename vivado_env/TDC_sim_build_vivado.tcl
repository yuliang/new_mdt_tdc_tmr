
#  _________   ________     ________       
# / U OF M  / | LSA    /   / Physics/
# /__ATLAS__/ |   ___   | |   ______/
#    |   |    |  |   /  | |  |
#    |   |    |  |___/  | |  /______     
#    |   |    |         | |         /
#    /___/    |________/   /________/

# File Name  : TDC_sim_build_vivado.tcl
# Author     : Yuxiang Guo
# Revision   : 
#              First created on 2019-02-17 
# Note       : 
     


set_param general.maxThreads 8

# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir [file dirname [info script]]
set source_dir "$origin_dir/../source_with_TMR"
set firmware_dir "$origin_dir/../../TDC_TMR_sim_firmware"

create_project TDC_TMR_sim_firmware $firmware_dir -part xc7k325tffg900-2  -force
set_property board_part xilinx.com:kc705:part0:1.3 [current_project]

#sim files include all source files
# set_property SOURCE_SET {} [get_filesets sim_1]
set_property SOURCE_SET sources_1 [get_filesets sim_1]

# set files [list "$source_dir/TDC_logic_TMR_include" "$source_dir/sim/TMR_module_sim/include"]


add_files -fileset [get_filesets sources_1] $source_dir/TDC_logic_with_TMR
add_files -fileset [get_filesets sources_1] $source_dir/sim/top_sim
set_property "top" "tdc_logic_TMR" [get_filesets sources_1]
set_property include_dirs "$source_dir/TDC_logic_TMR_include $source_dir/sim/TMR_module_sim/include"  [current_fileset]
set_property include_dirs "$source_dir/TDC_logic_TMR_include $source_dir/sim/TMR_module_sim/include"  [get_filesets sim_1]
#add_files -fileset sim_1  $source_dir/top_sim
#set_property "top" "tdc_logic_TMR_top_sim" [get_filesets sim_1]


# set_property include_dirs $source_dir/TDC_logic_TMR_include [get_filesets sim_1]
# set_property SOURCE_SET sources_1 [get_filesets sim_1]
# set_property include_dirs $source_dir/include [get_filesets sim_1]
# add_files -fileset sim_1  $source_dir/top_sim

# set obj [get_filesets sim_1]
# set files [list \
#  "[file normalize "$origin_dir/../../source_files/sim/serial_interface_gold.v"]"\
#  "[file normalize "$origin_dir/../../source_files/sim/module_sim/serial_interface_TMR_sim.v"]"\
#  "[file normalize "$origin_dir/../../source_files/sim/module_sim/setup_control_status_TMR_tb.v"]"\
#  "[file normalize "$origin_dir/../../source_files/sim/module_sim/setup_reg_TMR_sim.v"]"\
#  "[file normalize "$origin_dir/../../source_files/sim/module_sim/ttc_control_decoder_TMR_tb.v"]"\
#  "[file normalize "$origin_dir/../../source_files/sim/module_sim/mEnc8b10bMem_tb_TMR_sim.v"]"\
#  "[file normalize "$origin_dir/../../source_files/sim/module_sim/bcr_distribution_TMR_sim.v"]"\
#  "[file normalize "$origin_dir/../../source_files/sim/module_sim/interface_fifo_TMR_sim.v"]"\
#  "[file normalize "$origin_dir/../../source_files/sim/module_sim/readout_fifo_TMR_sim.v"]"\
#  "[file normalize "$origin_dir/../../source_files/sim/JTAG_master.v"]"\
#  "[file normalize "$origin_dir/../../source_files/sim/SPI_master.v"]"\
# ]

# add_files -norecurse -fileset $obj $files

# set_property include_dirs $source_dir/include [get_filesets sources_1]
# set_property "top" "tdc_logic" [get_filesets sources_1]

# set_property include_dirs $source_dir/include [get_filesets sim_1]
# set_property "top" "setup_control_status_TMR_tb" [get_filesets sim_1]

# update_compile_order -fileset sources_1
# update_compile_order -fileset sim_1
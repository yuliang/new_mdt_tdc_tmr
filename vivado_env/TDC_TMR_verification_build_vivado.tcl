
#  _________   ________     ________       
# / U OF M  / | LSA    /   / Physics/
# /__ATLAS__/ |   ___   | |   ______/
#    |   |    |  |   /  | |  |
#    |   |    |  |___/  | |  /______     
#    |   |    |         | |         /
#    /___/    |________/   /________/

# File Name  : TDC_sim_build_vivado.tcl
# Author     : Yuxiang Guo
# Revision   : 
#              First created on 2019-02-17 
# Note       : 
     


set_param general.maxThreads 8

# Set the reference directory for source file relative paths (by default the value is script directory path)
set origin_dir [file dirname [info script]]
set source_dir "$origin_dir/../source_with_TMR"
set firmware_dir "$origin_dir/../../TMR_vrfc"

create_project TMR_vrfc $firmware_dir -part xc7k325tffg900-2  -force
set_property board_part xilinx.com:kc705:part0:1.3 [current_project]


add_files -fileset [get_filesets sources_1] $source_dir/TDC_logic_with_TMR
add_files -norecurse -fileset [get_filesets sources_1] $source_dir/verification

# add_files -fileset [get_filesets sources_1] $source_dir/sim/top_sim
set_property "top" "tdc_logic_DUT" [get_filesets sources_1]
set_property include_dirs "$source_dir/TDC_logic_TMR_include"  [current_fileset]
set_property include_dirs "$source_dir/TDC_logic_TMR_include"  [get_filesets sim_1]

#set_property "top" "tdc_logic_TMR_top_sim" [get_filesets sim_1]

# Set 'constrs_1' fileset object
add_files -fileset constrs_1 $source_dir/verification/constrain_tdc_position_for_KC705.xdc
add_files -fileset constrs_1 $source_dir/verification/constrain_tdc_timing.xdc

create_ip -name clk_wiz -vendor xilinx.com -library ip -version 5.3 -module_name input_clock_TMR
set_property -dict [list CONFIG.CLK_IN1_BOARD_INTERFACE {sys_diff_clock} CONFIG.CLKOUT2_USED {true} CONFIG.CLKOUT3_USED {true} CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {40} CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {40} CONFIG.CLKOUT3_REQUESTED_OUT_FREQ {40} CONFIG.USE_LOCKED {false} CONFIG.USE_RESET {false} CONFIG.CLK_IN1_BOARD_INTERFACE {sys_diff_clock} CONFIG.PRIM_SOURCE {Differential_clock_capable_pin} CONFIG.PRIM_IN_FREQ {200.000} CONFIG.CLKIN1_JITTER_PS {50.0} CONFIG.MMCM_DIVCLK_DIVIDE {1} CONFIG.MMCM_CLKFBOUT_MULT_F {5.000} CONFIG.MMCM_CLKIN1_PERIOD {5.0} CONFIG.MMCM_CLKOUT0_DIVIDE_F {25.000} CONFIG.MMCM_CLKOUT1_DIVIDE {25} CONFIG.MMCM_CLKOUT2_DIVIDE {25} CONFIG.NUM_OUT_CLKS {3} CONFIG.CLKOUT1_JITTER {135.255} CONFIG.CLKOUT1_PHASE_ERROR {89.971} CONFIG.CLKOUT2_JITTER {135.255} CONFIG.CLKOUT2_PHASE_ERROR {89.971} CONFIG.CLKOUT3_JITTER {135.255} CONFIG.CLKOUT3_PHASE_ERROR {89.971}] [get_ips input_clock_TMR]


create_ip -name clk_wiz -vendor xilinx.com -library ip -version 5.3 -module_name master_clock_TMR
set_property -dict [list CONFIG.PRIM_SOURCE {Global_buffer} CONFIG.PRIM_IN_FREQ {40} CONFIG.CLKOUT2_USED {true} CONFIG.CLKOUT3_USED {true} CONFIG.CLKOUT4_USED {true} CONFIG.CLKOUT5_USED {true} CONFIG.CLKOUT6_USED {true} CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {320} CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {320} CONFIG.CLKOUT3_REQUESTED_OUT_FREQ {320} CONFIG.CLKOUT4_REQUESTED_OUT_FREQ {160} CONFIG.CLKOUT5_REQUESTED_OUT_FREQ {160} CONFIG.CLKOUT6_REQUESTED_OUT_FREQ {160} CONFIG.CLKIN1_JITTER_PS {250.0} CONFIG.MMCM_DIVCLK_DIVIDE {1} CONFIG.MMCM_CLKFBOUT_MULT_F {24.000} CONFIG.MMCM_CLKIN1_PERIOD {25.0} CONFIG.MMCM_CLKOUT0_DIVIDE_F {3.000} CONFIG.MMCM_CLKOUT1_DIVIDE {3} CONFIG.MMCM_CLKOUT2_DIVIDE {3} CONFIG.MMCM_CLKOUT3_DIVIDE {6} CONFIG.MMCM_CLKOUT4_DIVIDE {6} CONFIG.MMCM_CLKOUT5_DIVIDE {6} CONFIG.NUM_OUT_CLKS {6} CONFIG.CLKOUT1_JITTER {151.082} CONFIG.CLKOUT1_PHASE_ERROR {196.976} CONFIG.CLKOUT2_JITTER {151.082} CONFIG.CLKOUT2_PHASE_ERROR {196.976} CONFIG.CLKOUT3_JITTER {151.082} CONFIG.CLKOUT3_PHASE_ERROR {196.976} CONFIG.CLKOUT4_JITTER {169.112} CONFIG.CLKOUT4_PHASE_ERROR {196.976} CONFIG.CLKOUT5_JITTER {169.112} CONFIG.CLKOUT5_PHASE_ERROR {196.976} CONFIG.CLKOUT6_JITTER {169.112} CONFIG.CLKOUT6_PHASE_ERROR {196.976}] [get_ips master_clock_TMR]

set_property strategy Flow_PerfOptimized_high [get_runs synth_1]
set_property strategy Performance_ExtraTimingOpt [get_runs impl_1]


